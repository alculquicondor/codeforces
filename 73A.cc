#include <iostream>
#include <algorithm>

using namespace std;
typedef long long ll;

int main() {
  ll A[3], k;
  for (int i = 0; i < 3; i++)
    cin >> A[i];
  cin >> k;
  sort(A, A+3);
  ll a = min(k/3, A[0]-1);
  k -= a;
  ll b = min(k/2, A[1]-1);
  k -= b;
  ll c = min(k, A[2]-1);
  cout << (a+1)*(b+1)*(c+1) << endl;
}
