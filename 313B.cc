#include <iostream>
#include <cstring>
#define MAXN 100004

using namespace std;

int A[MAXN];

int main() {
  ios::sync_with_stdio(0);
  string s;
  cin >> s;
  memset(A, 0, sizeof A);
  for (int i = 0; i < s.size() - 1; i++)
    if (s[i] == s[i+1])
      A[i+2] = 1;
  for (int i = 0; i < s.size(); i++)
    A[i+1] += A[i];
  int n, a, b, ans;
  cin >> n;
  while (n--) {
    cin >> a >> b;
    ans = A[b] - A[a-1] - (a >= 2 and s[a-2] == s[a-1]);
    cout << ans << endl;
  }
}

