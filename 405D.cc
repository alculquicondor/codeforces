#include <iostream>
#include <vector>
const int s = 1000000;

using namespace std;

bool T[s+1];
#define inv(x) (s-x+1)

int main() {
  ios::sync_with_stdio(0);
  int n, x;
  cin >> n;
  vector<int> A, B;
  A.reserve(n);
  for (int i = 0; i < n; ++i) {
    cin >> x;
    T[x] = true;
    A.push_back(x);
  }
  int id = 1;
  for (int i = 1; i <= s; ++i)
    if (T[i]) {
      if (T[inv(i)]) {
        while (T[id] or T[inv(id)]) ++id;
        B.push_back(id);
        B.push_back(inv(id));
        id++;
        T[inv(i)] = false;
      } else {
        B.push_back(inv(i));
      }
    }
  cout << B.size() << endl;
  cout << B[0];
  for (int i = 1; i < B.size(); ++i)
    cout << " " << B[i];
  cout << endl;
}
