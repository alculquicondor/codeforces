#include <iostream>
#include <algorithm>
#include <set>
#define MAXN 100005

using namespace std;

typedef pair<int, int> pii;
int idA[MAXN], idB[MAXN];
vector<pii> A, B;
int C[MAXN], ans[MAXN], n, m;
set<pii> S;

void cleanSet(int v) {
  while (not S.empty() and S.begin()->first < v)
    S.erase(S.begin());
}

void addNew(int v, int &id) {
  int y;
  for ( ; id < m and B[(y=idB[id])].first <= v; ++id)
    S.insert(pii(B[y].second, y));
}

int match(int v) {
  auto it = S.lower_bound(pii(v, 0));
  if (it == S.end())
    return -1;
  int y = it->second;
  if (--C[y] == 0)
    S.erase(it);
  return y;
}

bool solve() {
  int j = 0;
  for (int i = 0; i < n; ++i) {
    int x = idA[i], y;
    cleanSet(A[x].first);
    addNew(A[x].first, j);
    y = match(A[x].second);
    if (y == -1)
      return false;
    else
      ans[x] = y + 1;
  }
  return true;
}

int main() {
  ios::sync_with_stdio(0);
  cin >> n;
  A.resize(n);
  for (int i = 0; i < n; ++i) {
    cin >> A[i].first >> A[i].second;
    idA[i] = i;
  }
  sort(idA, idA+n, [&](int i, int j) {
      return A[i] < A[j];
    });
  cin >> m;
  B.resize(m);
  for (int i = 0; i < m; ++i) {
    cin >> B[i].first >> B[i].second >> C[i];
    idB[i] = i;
  }
  sort(idB, idB+m, [&](int i, int j) {
      return B[i] < B[j];
    });
  if (solve()) {
    cout << "YES" << endl;
    cout << ans[0];
    for (int i = 1; i < n; ++i)
      cout << ' ' << ans[i];
    cout << endl;
  } else {
    cout << "NO" << endl;
  }
  return 0;
}
