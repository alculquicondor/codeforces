#include <iostream>

using namespace std;

int solve(int a, int b) {
  if (a == b)
    return -1;
  if (a < b)
    return 0;
  int q = a - b, x, cnt = 0;
  for (x = 1; x * x < q; ++x)
    if (q % x == 0) {
      if (x > b)
        ++cnt;
      if (q / x > b)
        ++cnt;
    }
  if (x * x == q and x > b)
    ++cnt;
  return cnt;
}

int main() {
  int a, b, ans;
  cin >> a >> b;
  ans = solve(a, b);
  if (ans == -1)
    cout << "infinity";
  else
    cout << ans;
  cout << endl;
}
