#include<vector>
#include<cstdio>

using namespace std;

int n,r1,r2;
vector<vector<int> > adj;
vector<bool> visit;
vector<int> pi;

void dfs(int u)
{
//	printf("#%d\n",u);
	visit[u]=1;
	for(int v:adj[u])
		if(!visit[v])
		{
			pi[v]=u;
			dfs(v);
		}
}

int main()
{
	scanf("%d %d %d",&n,&r1,&r2);
	r1--,r2--;
	adj.resize(n);
	visit.assign(n,0);
	pi.resize(n);
	int v;
	for(int i=0;i<n;i++)
		if(i!=r1)
		{
			scanf("%d",&v);
			v--;
			adj[v].push_back(i);
			adj[i].push_back(v);
		}
	dfs(r2);
	int c=0;
	for(int i=0;i<n;i++)
		if(i!=r2)
		{
			if(c!=0)
				printf(" ");
			printf("%d",pi[i]+1);
			c++;
		}
	puts("");
}
