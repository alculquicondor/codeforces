#include <iostream>
#define MAXN 26

using namespace std;

int C[2][MAXN];

string solve(const string &s, const string &t) {
  int i = 0;
  bool valid = true;
  for (int j = 0; valid and j < t.size(); ++j) {
    while (i < s.size() and s[i] != t[j])
      ++i;
    valid &= i++ < s.size();
  }
  if (valid)
    return "automaton";
  for (i = 0; i < s.size(); ++i)
    ++C[0][s[i]-'a'];
  for (i = 0; i < t.size(); ++i)
    ++C[1][t[i]-'a'];
  valid = true;
  for (i = 0; valid and i < MAXN; ++i)
    valid &= C[0][i] == C[1][i];
  if (valid)
    return "array";
  valid = true;
  for (i = 0; valid and i < 26; ++i)
    valid &= C[0][i] >= C[1][i];
  if (valid)
    return "both";
  return "need tree";
}

int main() {
  string s, t;
  cin >> s >> t;
  cout << solve(s, t) << endl;
}
