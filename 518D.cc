#include <bits/stdc++.h>
using namespace std;

#define MAXN 2010
typedef long double ld;
ld DP[MAXN][MAXN];

int main() {
  ios::sync_with_stdio(false);
  int N, T;
  ld p;
  cin >> N >> p >> T;
  for (int t = 1; t <= T; ++t)
    for (int n = 1; n <= N; ++n)
      DP[t][n] = p * (1+DP[t-1][n-1]) + (1-p) * DP[t-1][n];
  cout << fixed << setprecision(7);
  cout << DP[T][N] << endl;
  return 0;
}

