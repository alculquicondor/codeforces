#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
  int n, x, y;
  cin >> n;
  vector<double> A;
  A.reserve(n);
  for (int i = 0; i < n; i++) {
    cin >> x >> y;
    A.push_back(atan2((double)x, (double)y) / M_PI * 180.);
  }
  sort(A.begin(), A.end());
  double ans = A[n-1] - A[0];
  for (int i = 1; i < n; i++)
    ans = min(ans, A[i-1] + 360. - A[i]);
  cout.precision(10);
  cout << ans << endl;
}
