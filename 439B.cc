#include <iostream>
#include <algorithm>
#define MAXN 100005

using namespace std;

typedef long long ll;
int A[MAXN];

int main() {
  std::ios::sync_with_stdio(false);
  int n;
  ll x;
  cin >> n >> x;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  sort(A, A+n);
  ll ans = 0;
  for (int i = 0; i < n; ++i) {
    ans += x * A[i];
    if (x > 1)
      --x;
  }
  cout << ans << endl;
}
