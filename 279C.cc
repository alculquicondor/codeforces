#include <iostream>
#define MAXN 100005

using namespace std;

int A[MAXN], incr[MAXN], decr[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n, m;
  cin >> n >> m;
  for (int i = 0; i < n; i++)
    cin >> A[i];
  incr[n-1] = decr[n-1] = n - 1;
  for (int i = n - 2; i >= 0; i--) {
    incr[i] = A[i] <= A[i+1] ? incr[i+1] : i;
    decr[i] = A[i] >= A[i+1] ? decr[i+1] : i;
  }
  int l, r;
  while (m--) {
    cin >> l >> r;
    l--, r--;
    if (incr[l] >= r or decr[incr[l]] >= r)
      cout << "Yes\n";
    else
      cout << "No\n";
  }
}
