#include<cstdio>
#include<algorithm>
#include<cmath>

using namespace std;

int n,k,A[10001];

bool play(double mid)
{
	double reserva=0;
	for(int i=n-1;i>=0;i--)
	{
		if(A[i]>mid)
			reserva+=(A[i]-mid)*k/100;
		else
			reserva-=(mid-A[i]);
		if(reserva<0)
			return false;
	}
	return true;
}

int main()
{
	scanf("%d %d",&n,&k);
	k=100-k;
	for(int i=0;i<n;i++)
		scanf("%d",&A[i]);
	sort(A,A+n);
	double low=0,up=A[n-1],mid;
	while(abs(low-up)>=1e-7)
	{
		mid=(low+up)/2;
		if(play(mid))
			low=mid;
		else
			up=mid;
	}
	printf("%.9lf\n",(low+up)/2);
}
