#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
typedef pair<ll, ll> pii;

int n;
ll pot[11];

void pre() {
  pot[0] = 1;
  for (int i = 1; i < 11; ++i)
    pot[i] = pot[i-1] * 11;
}

struct Queue {
  ll data;
  int sz;
  Queue() : data(0), sz(0) { }
  void push(int x) {
    data = data * 11 + x;
    ++sz;
  }
  int pop() {
    int r = data / pot[sz-1];
    data = data % pot[sz-1];
    --sz;
    return r;
  }
};

int main() {
  ios::sync_with_stdio(false);
  pre();
  cin >> n;
  Queue q[2];
  int sz, x;
  for (int i = 0; i < 2; ++i) {
    cin >> sz;
    for (int j = 0; j < sz; ++j) {
      cin >> x;
      q[i].push(x);
    }
  }
  set<pii> used;
  int st = 0;
  while (q[0].sz and q[1].sz) {
    pii t(q[0].data, q[1].data);
    if (used.find(t) != used.end())
      break;
    used.insert(t);
    int a[] = {q[0].pop(), q[1].pop()};
    if (a[0] > a[1]) {
      q[0].push(a[1]);
      q[0].push(a[0]);
    } else {
      q[1].push(a[0]);
      q[1].push(a[1]);
    }
    ++st;
  }
  if (q[0].sz == 0)
    cout << st << ' ' << 2;
  else if (q[1].sz == 0)
    cout << st << ' ' << 1;
  else
    cout << -1;
  cout << endl;
  return 0;
}

