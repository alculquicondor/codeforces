#include <iostream>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <map>
#include <set>
#define MAXN 1000005
using namespace std;

typedef pair<int, int> pii;
int z1[2*MAXN], z2[2*MAXN];
void zfunction(string &s, int z[]){
    int sz = s.size();
    for(int i = 1, l = 0, r = 0; i < sz; ++i){
        z[i] = 0;
        if (i <= r)
            z[i] = min(z[i - l], r - i + 1);
        while (i + z[i] < sz && s[z[i]] == s[i + z[i]])
            ++z[i];
        if (i + z[i] - 1 > r){
            l = i;
            r = i + z[i] - 1;
        }
    }
    z[0] = sz;  
}

int main() {
    string a, b;
    getline(cin, a);
    getline(cin, b);
    int ansi = -1, ansj = -1, n = a.size(), m = b.size();
    if(n!=m) {
        puts("-1 -1");
        return 0;
    }
    string L = b + a, ar = a;
    zfunction(L, z1);
    reverse(ar.begin(), ar.end());
    L = ar + b;
    zfunction(L, z2);
    map<int, set<int> > M;
    for(int i=0; i<n; i++)
        if(z2[n+i])
            M[min(z2[n+i], n)].insert(i);
    vector<pair<int, set<int> > > V(M.begin(), M.end());
    int i, j, maxj, minj, lp, hp;
    vector<pair<int, set<int> > >::iterator it;
    for(i=0; i<n-1 and a[i]==b[n-i-1]; i++);
    while(i--) {
        maxj = min(i+1+z1[n+i+1], n-1);
        minj = i+1;
        //it = upper_bound(V.begin(), V.end(), pii(n-i-1,1));
        it = V.end();
        set<int>::iterator it1, it2;
        while(it!=V.begin()) {
            -- it;
            if(maxj+it->first<n)
                break;
            lp = max(minj, n-it->first)-i-1, hp = maxj-i-1;
            it1 = it->second.upper_bound(lp-1);
            it2 = it->second.upper_bound(hp);
            if(it1!=it2) {
                ansi = i, ansj = *it1+i+1;
                goto solved;
            }
        }
    }
solved:
    printf("%d %d\n", ansi, ansj);
    return 0;
}
