#include <iostream>
#define MAXN 1005

using namespace std;

int id[MAXN];
int T[4][MAXN];
int DP[MAXN];

int main() {
  std::ios::sync_with_stdio(false);
  int n, k, x;
  cin >> n >> k;
  for (int i = 0; i < n; ++i) {
    cin >> x;
    id[x] = i;
  }
  --k;
  for (int i = 0; i < k; ++i)
    for (int j = 0; j < n; ++j) {
      cin >> x;
      T[i][id[x]] = j;
    }
  int ans = 0;
  for (int i = n - 1; i >= 0; --i) {
    for (int j = i + 1; j < n; ++j) {
      bool valid = true;
      for (int r = 0; r < k; ++r)
        valid &= T[r][j] > T[r][i];
      if (valid)
        DP[i] = max(DP[i], DP[j]);
    }
    ans = max(ans, ++DP[i]);
  }
  cout << ans << endl;
}
