#include <bits/stdc++.h>
using namespace std;

#define MAXN 100002
struct Leg {
  int l, d, i;
  bool operator < (const Leg &o) const {
    return d < o.d;
  }
} legs[MAXN];
set<Leg*> g[MAXN];
int C[MAXN];
int n;

#define center(x, y) (x + y) >> 1
#define left(x) (x << 1) + 1
int st[262144], cnt[262144];
int query(int q, int idx = 0, int l = 0, int r = n - 1) {
  if (q >= cnt[idx])
    return st[idx];
  if (q < 0 or l == r)
    return 0;
  int L = left(idx), R = L + 1;
  int mid = center(l, r);
  return query(q, L, l, mid) + query(q - cnt[L], R, mid + 1, r);
}

void insert(int p, int q, int idx = 0, int l = 0, int r = n - 1) {
  if (l == r) {
    st[idx] += q;
    ++cnt[idx];
    return;
  }
  int L = left(idx), R = L + 1, mid = center(l, r);
  if (p < mid - l + 1)
    insert(p, q, L, l, mid);
  else
    insert(p - mid + l - 1, q, R, mid + 1, r);
  st[idx] = st[L] + st[R];
  cnt[idx] = cnt[L] + cnt[R];
}

int main() {
  ios::sync_with_stdio(false);
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> legs[i].l;
  for (int i = 0; i < n; ++i)
    cin >> legs[i].d;
  sort(legs, legs+n);
  for (int i = 0; i < n; ++i) {
    Leg &leg = legs[i];
    leg.i = i;
    g[leg.l].insert(&leg);
    C[leg.l] += leg.d;
  }
  for (int i = MAXN - 2; i >= 0; --i)
    C[i] += C[i + 1];
  int ans = C[0], cnt = 0;
  for (int i = 1; i < MAXN; ++i)
    if (not g[i].empty()) {
      int cost = C[i + 1];
      if (cnt > g[i].size() - 1)
        cost += query(cnt - g[i].size() + 1);
      ans = min(ans, cost);
      for (Leg *leg : g[i]) {
        insert(leg->i, leg->d);
        ++cnt;
      }
    }
  cout << ans << endl;
  return 0;
}

