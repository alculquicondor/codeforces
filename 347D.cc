#include <iostream>
#include <cstring>
#define MAXN 102

using namespace std;

int pi[MAXN];

void kmpPreprocess(const string &P) {
  int i = 0, j = -1, m = P.size();
  pi[0] = -1;
  while(i < m) {
    while(j>=0 and P[i] != P[j])
      j = pi[j];
    i ++, j++;
    pi[i] = j;
  }
}

string s1, s2, p;
int M[MAXN][MAXN][MAXN];
int mov[MAXN][MAXN][MAXN];
int DP(int a, int b, int j) {
  if (a == s1.size() or b == s2.size())
    return 0;
  int &ans = M[a][b][j], &mv = mov[a][b][j], tmp;
  if (ans >= 0)
    return ans;
  ans = DP(a+1, b, j);
  mv = -1;
  tmp = DP(a, b+1, j);
  if (tmp > ans) {
    ans = tmp;
    mv = -2;
  }
  if (s1[a] == s2[b]) {
    char c = s1[a];
    while (j >= 0 and c != p[j])
      j = pi[j];
    j++;
    if (j < p.size()) {
      tmp = 1 + DP(a+1, b+1, j);
      if (tmp > ans) {
        ans = tmp;
        mv = j;
      }
    }
  }
  return ans;
}

int main() {
  ios::sync_with_stdio(0);
  cin >> s1 >> s2 >> p;
  kmpPreprocess(p);
  memset(M, -1, sizeof M);
  int a = 0, b = 0, j = 0, i = 0, ans = DP(0, 0, 0), mv;
  if (ans) {
    while (i < ans) {
      mv = mov[a][b][j];
      if (mv == -1) {
        ++a;
      } else if (mv == -2) {
        ++b;
      } else {
        char c = s1[a];
        cout << c;
        j = mv;
        ++a;
        ++b;
        ++i;
      }
    }
  } else {
    cout << 0;
  }
  cout << endl;
}
