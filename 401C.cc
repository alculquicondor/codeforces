#include <iostream>

using namespace std;

int main() {
  ios::sync_with_stdio(0);
  int n, m;
  cin >> n >> m;
  if ((m+1) / 2 > n + 1 or m < n - 1) {
    cout << -1;
  } else if (m == n - 1) {
    cout << "0";
    while (m--)
      cout << "10";
  } else if (n == m) {
    while (n--)
      cout << "01";
  } else {
    int x = m - n - 1, y = m - 2 * x;
    if (x) {
      cout << "11";
      --x;
      while (x--) {
        cout << "011";
      }
      while (y--) {
        cout << "01";
      }
    } else {
      cout << "1";
      --y;
      while (y--)
        cout << "01";
    }
  }
  cout << endl;
}
