#include <cstdio>
#include <algorithm>
#include <cstring>
#define MAXN 4001
#define MOD 1000000009
using namespace std;
typedef long long ll;

inline ll san(ll x) {
  if (x < 0)
    x = (x % MOD) + MOD;
  if (x >= MOD)
    x %= MOD;
  return x;
}

ll P[MAXN];
int C[2*MAXN][MAXN][2];

int main() {
  int N, W, B;
  scanf("%d %d %d", &N, &W, &B);
  P[0] = 1;
  for (int i = 1; i < MAXN; i++)
    P[i] = san(P[i-1]*i);
  memset(C, 0, sizeof C);
  C[0][0][0] = C[0][0][1] = 1;
  for (int i = 1; i < 2 * MAXN - 1; i++) {
    C[i][0][0] = 1;
    C[i][0][1] = 1 + C[i-1][0][1];
    for (int j = 1; 2 * j < i; j++) {
      for (int k = 0; k < 2; k++) {
        if (k)
          C[i][j][k] = san(C[i][j][0] + C[i-1][j][1]);
        else if (i-2 >= 0)
          C[i][j][k] = san(C[i-2][j-1][1]-C[0][j-1][1]);
      }
    }
  }
  int w;
  ll ans = 0;
  for (int b = max(1, N-W); b <= B and b <= N - 2; b++) {
    w = N - b;
    ans = san(ans + san((ll)C[W+w-1][w-1][0]*C[B+b-1][b-1][0])*(w-1));
  }
  ans = san(ans * san(P[W] * P[B]));
  printf("%d\n", (int)ans);
}

