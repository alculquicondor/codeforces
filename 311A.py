n, k = [int(x) for x in input().split()]
if n * (n-1) / 2 <= k:
  print("no solution")
else:
  for x in range(n):
    print(0, x)
