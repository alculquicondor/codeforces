#include<iostream>
#include<vector>
#include<map>
#include<algorithm>

using namespace std;

int main()
{
	int n,m;
	cin>>n>>m;
	bool R[17][17];
	vector<string> name(n);
	map<string,int> ID;
	for(int i=0;i<n;i++)
	{
		cin>>name[i];
		for(int j=0;j<n;j++)
			R[i][j]=0;
		ID[name[i]]=i;
	}
	string s;
	int u,v;
	for(int i=0;i<m;i++)
	{
		cin>>s;
		u=ID[s];
		cin>>s;
		v=ID[s];
		R[u][v]=1;
		R[v][u]=1;
	}
	vector<int> ans;
	for(int mask=0;mask<(1<<n);mask++)
	{
		vector<int> este;
		for(int i=0;i<n;i++)
			if(mask&(1<<i))
				este.push_back(i);
		bool vale=1;
		for(int i=1;i<este.size();i++)
			for(int j=0;j<i;j++)
				if(R[este[i]][este[j]])
				{
					vale=0;
					goto sigue;
				}
		sigue:
		if(vale and este.size()>ans.size())
			ans=este;
	}
	cout<<ans.size()<<endl;
	sort(ans.begin(),ans.end(),
		[&name](int i,int j) {return name[i]<name[j];}
	);
	for(int i=0;i<ans.size();i++)
		cout<<name[ans[i]]<<endl;
}
