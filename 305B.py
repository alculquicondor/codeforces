from fractions import gcd

p, q = [int(x) for x in input().split()]
n = int(input())
A = [int(x) for x in input().split()]
g = gcd(p, q)
p, q = p // g, q // g
poss = True
for a in A:
  if q == 0 or p < a * q:
    poss = False
    break
  p -= a * q
  g = gcd(p, q)
  p, q = q // g, p // g
print("YES" if poss and q == 0 else "NO")
