know=[[0]*5 for i in range(5)]
m=int(raw_input())
for i in range(m):
	s=raw_input().split()
	a=int(s[0])-1
	b=int(s[1])-1
	know[a][b]=1
	know[b][a]=1
for i in range(5):
	for j in range(5):
		if i!=j :
			for k in range(5):
				if k!=i and k!=j and know[i][j]==know[j][k]==know[i][k]:
					print 'WIN'
					exit(0)
print 'FAIL'
