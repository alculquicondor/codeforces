#include <iostream>
#include <algorithm>
#define MAXN 300001

using namespace std;
typedef pair<int, int> pii;
pii A[MAXN];
int ans[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> A[i].first;
    A[i].second = i;
  }
  sort(A, A+n);
  int last = 0;
  for (int i = 0; i < n; ++i)
    last = ans[A[i].second] = max(last+1, A[i].first);
  cout << ans[0];
  for (int i = 1; i < n; ++i)
    cout << " " << ans[i];
  cout << endl;
}
