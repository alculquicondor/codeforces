#include <iostream>
#define MAXN 200005
using namespace std;
typedef long long ll;

ll A[MAXN], ac[MAXN];
int main() {
  ios::sync_with_stdio(0);
  int n, k;
  cin >> n >> k;
  for (int i = 0; i < n; i++)
    cin >> A[i];
  int a, b, tb;
  ll maxb = -1, maxt = -1;
  for (int i = n - 1; i >= 0; i--)
    ac[i] = A[i] + ac[i+1];
  for (int i = 0; i + k <= n; i++)
    A[i] = ac[i] - ac[i+k];
  for (int i = n - 2 * k; i >= 0; i--) {
    if (A[i+k] >= maxb)
      maxb = A[tb = (i + k)];
    if (A[i] + A[tb] >= maxt)
      maxt = A[a=i] + A[b=tb];
  }
  cout << a+1 << " " << b+1 << endl;
}
