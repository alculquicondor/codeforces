#include <iostream>
#include <queue>
#include <algorithm>

using namespace std;
typedef pair<int, int> pii;
typedef pair<int, pii> piii;

struct order {
  bool operator()(const piii &a, const piii &b) {
    if (a.second.first == b.second.first)
      return a.second.second < b.second.second;
    return a.second.first > b.second.first;
  }
};

int main() {
  int n, m;
  cin >> n >> m;
  int a, l;
  priority_queue<piii, vector<piii>, order> Q;
  for (int i = 1; i <= m; i++) {
    cin >> a >> l;
    Q.push(piii(i, pii(a, l)));
  }
  int sofar = 0;
  vector<int> elim;
  piii t;
  pii x;
  while (not Q.empty()) {
    t = Q.top();
    x = t.second;
    Q.pop();
    if (x.first <= sofar) {
      if (x.first + x.second - 1 <= sofar)
        elim.push_back(t.first);
      else
        Q.push(piii(t.first, pii(sofar+1, x.second - (sofar-x.first+1))));
    } else {
      sofar = x.first + x.second - 1;
    }
  }
  cout << elim.size() << endl;
  if (elim.size()) {
    cout << elim[0];
    for (int i = 1; i < elim.size(); i++)
      cout << " " << elim[i];
    cout << endl;
  }
}
