#include <iostream>
#include <algorithm>

using namespace std;

int main() {
  int n, k, A[100010];
  cin >> n >> k;
  for (int i = 0; i < n; i++)
    A[i] = i + 1;
  reverse(A, A+k+1);
  cout << A[0];
  for (int i = 1; i < n; i++)
    cout << " " << A[i];
  cout << endl;
}
