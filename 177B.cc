#include <iostream>

using namespace std;
typedef long long ll;

int main() {
  ll n, ans = 0;
  cin >> n;
  for (ll i = 2; i * i <= n; i++) {
    while (n % i == 0) {
      ans += n;
      n /= i;
    }
  }
  ans += n;
  ans += n > 1;
  cout << ans << endl;
}
