#include <iostream>

using namespace std;

int main() {
  string s;
  cin >> s;
  int a = 0, b = 0;
  for(int i=0; i<s.size(); i++)
    if(s[i] == 'x')
      ++ a;
    else
      ++ b;
  int m = min(a, b);
  a -= m;
  b -= m;
  while(a--)
    cout << 'x';
  while(b--)
    cout << 'y';
  cout << endl;
}
