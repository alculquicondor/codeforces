a,c = [int(x) for x in raw_input().split()]
ans = 0
fact = 1
while a or c:
    ans += ((3+(c%3)-(a%3)) % 3) * fact
    c /= 3
    a /= 3
    fact *= 3
print ans
