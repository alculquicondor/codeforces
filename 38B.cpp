#include<cstdio>

int Cx[]={-2,-2,2,2,-1,-1,1,1},Cy[]={-1,1,-1,1,-2,2,-2,2};

bool valid(int x,int y)
{
	return x>=0 and y>=0 and x<8 and y<8;
}

int main()
{
	char s[3];
	scanf("%s",s);
	int cx,cy,rx,ry;
	rx=s[0]-'a';
	ry=s[1]-'1';
	scanf("%s",s);
	cx=s[0]-'a';
	cy=s[1]-'1';
	bool T[8][8];
	for(int i=0;i<8;i++)
		for(int j=0;j<8;j++)
			T[i][j]=1;
	for(int i=0;i<8;i++)
		T[rx][i]=T[i][ry]=0;
	T[cx][cy]=0;
	int xx,yy;
	for(int i=0;i<8;i++)
	{
		xx=rx+Cx[i];
		yy=ry+Cy[i];
		if(valid(xx,yy))
			T[xx][yy]=0;
	}
	for(int i=0;i<8;i++)
	{
		xx=cx+Cx[i];
		yy=cy+Cy[i];
		if(valid(xx,yy))
			T[xx][yy]=0;
	}
	int r=0;
	for(int i=0;i<8;i++)
		for(int j=0;j<8;j++)
			r+=T[i][j];
	printf("%d\n",r);
}
