#include <iostream>
#include <vector>
#define MAXN 100001

using namespace std;
int P[2][MAXN];
int sz[2];
vector<int> A[MAXN];

int n, k, p, sum = 0;
bool solve() {
  if (sum != (p&1))
    return false;
  for (int i = 0; i < p; ++i) {
    if (not sz[1])
      return false;
    A[i].push_back(P[1][--sz[1]]);
  }
  for (int i = p; i < k; ++i) {
    if (sz[1]) {
      A[i].push_back(P[1][--sz[1]]);
      A[i].push_back(P[1][--sz[1]]);
    } else if (sz[0]) {
      A[i].push_back(P[0][--sz[0]]);
    } else {
      return false;
    }
  }
  while (sz[1]) {
    A[0].push_back(P[1][--sz[1]]);
    A[0].push_back(P[1][--sz[1]]);
  }
  while (sz[0])
    A[0].push_back(P[0][--sz[0]]);
  return true;
}

int main() {
  std::ios::sync_with_stdio(false);
  int x;
  cin >> n >> k >> p;
  for (int i = 0; i < n; ++i) {
    cin >> x;
    sum += x;
    sum &= 1;
    P[x&1][sz[x&1]++] = x;
  }
  p = k - p;
  if (solve()) {
    cout << "YES" << endl;
    for (int i = 0; i < k; ++i) {
      cout << A[i].size();
      for (int x : A[i])
        cout << " " << x;
      cout << endl;
    }
  } else {
    cout << "NO" << endl;
  }
}
