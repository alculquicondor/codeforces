#include <iostream>

using namespace std;

int main() {
  int n, m, k, x;
  cin >> n >> m >> k;
  int ans = 0;
  while (n--) {
    cin >> x;
    if (x == 1) {
      if (m)
        --m;
      else
        ++ans;
    } else {
      if (k)
        --k;
      else if (m)
        --m;
      else
        ++ans;
    }
  }
  cout << ans << endl;
}
