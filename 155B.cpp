#include<algorithm>
#include<iostream>
#include<vector>

using namespace std;
typedef pair<int, int> pii;

bool order(pii a, pii b) {
    if(a.second == b.second)
        return a.first > b.first;
    return a.second > b.second;
}

int main() {
    int n;
    cin>>n;
    vector<pii> A(n);
    for(int i=0; i<n; i++)
        cin>>A[i].first>>A[i].second;
    sort(A.begin(), A.end(), order);
    int q = 1, ans = 0, i = 0;
    while(q-- and i<n) {
        ans += A[i].first;
        q += A[i].second;
        i ++;
    }
    cout<<ans<<endl;
}
