#include <iostream>
#include <cstring>

using namespace std;
typedef long long ll;

ll M[64][2][2][2][2];
ll l, r;

ll DP(int p, bool f1, bool f2, bool g1, bool g2) {
  if (p == -1)
    return 0;
  ll & ans = M[p][f1][f2][g1][g2];
  if (ans >= 0)
    return ans;
  ans = 0;
  for (int i = f1 ? (l>>p)&1 : 0; i <= (f2 ? (r>>p)&1 : 1); i++)
    for (int j = g1 ? (l>>p)&1 : 0; j <= (g2 ? (r>>p)&1 : 1); j++)
      ans = max(ans, (((ll)i^j)<<p) + DP(p-1,
            f1 and i == ((l>>p)&1),
            f2 and i == ((r>>p)&1),
            g1 and j == ((l>>p)&1),
            g2 and j == ((r>>p)&1)));
  return ans;
}

int main() {
  memset(M, -1, sizeof M);
  cin >> l >> r;
  cout << DP(63, 1, 1, 1, 1) << endl;
}
