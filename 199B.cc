#include <iostream>

using namespace std;

int X[4], Y[4], R[4];
typedef long long ll;

ll hypot2(ll x, ll y) {
  return x * x + y * y;
}

ll square(ll x) {
  return x * x;
}

bool inter(int i, int j) {
  ll d2 = hypot2(X[i]-X[j], Y[i]-Y[j]);
  if (d2 >= square(R[i] + R[j]))
    return false;
  if (R[i] < R[j])
    return d2 > square(R[j] - R[i]);
  return d2 > square(R[i] - R[j]);
}

bool inside(int i, int j) {
  ll d2 = hypot2(X[i]-X[j], Y[i]-Y[j]);
  return R[j] > R[i] and d2 <= square(R[j] - R[i]);
}

bool valid[4];

int main() {
  for (int i = 0; i < 4; i += 2) {
    cin >> X[i] >> Y[i];
    X[i+1] = X[i];
    Y[i+1] = Y[i];
    cin >> R[i];
    cin >> R[i+1];
  }
  int ans = 0;
  valid[0] = not inter(0, 2) and not inter(0, 3) and
    (not inside(0, 3) or inside(0, 2));
  valid[1] = not inter(1, 2) and not inter(1, 3) and
    (not inside(1, 3) or inside(1, 2));
  valid[2] = not inter(2, 0) and not inter(2, 1) and
    (not inside(2, 1) or inside(2, 0));
  valid[3] = not inter(3, 0) and not inter(3, 1) and
    (not inside(3, 1) or inside(3, 0));
  for (int i = 0; i < 4; ++i)
    ans += valid[i];
  cout << ans << endl;
}
