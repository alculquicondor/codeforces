#include <iostream>
#include <bitset>

using namespace std;
#define MAXN 2000000

bitset<MAXN> isp;
typedef long long ll;

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  cout << 2;
  n --;
  isp.set();
  for (ll i = 3; n; i+= 2)
    if (isp[i]) {
      for (ll j = i * i; j < MAXN; j+= i)
        isp.reset(j);
      cout << " " << i;
      n --;
      if (n == 0)
        cerr << i << endl;
    }
  cout << endl;
}
