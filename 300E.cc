#include <iostream>
#include <cstring>
#include <vector>
#define MAXA 10000001

using namespace std;
typedef long long ll;

int F[MAXA];
vector<int> P;

void pre() {
  memset(F, 0, sizeof F);
  for (ll i = 2; i < MAXA; ++i)
    if (F[i] == 0) {
      F[i] = i;
      P.push_back(i);
      for (ll j = i * i; j < MAXA; j += i)
        F[j] = i;
    }
}

ll A[MAXA];
int maxi = 0;

bool go(ll x) {
  for (int p : P) {
    ll f = p, c = 0;
    while (f <= x) {
      c += x / f;
      f *= p;
    }
    if (c < A[p])
      return false;
  }
  return true;
}

int main() {
  pre();
  int k, x;
  ll sum = 0;
  cin >> k;
  for (int i = 0; i < k; ++i) {
    cin >> x;
    A[x] ++;
    maxi = max(maxi, x);
    sum += x;
  }
  for (int i = maxi - 1; i; --i)
    A[i] += A[i+1];
  int f;
  for (int i = maxi; i > 1; --i) {
    f = F[i];
    if (f != i) {
      A[f] += A[i];
      A[i/f] += A[i];
      A[i] = 0;
    }
  }
  /*
  for (int i = 0; i < maxi; ++i)
    cout << A[i] << endl;
    */
  ll lo = 0, hi = sum, mid;
  while (hi - lo > 1) {
    mid = (hi + lo) >> 1;
    if (go(mid))
      hi = mid;
    else
      lo = mid;
  }
  cout << hi << endl;
}

