#include <iostream>

using namespace std;
typedef long long ll;

int main() {
  ios::sync_with_stdio(0);
  int n, last = -0x7fffffff, ct = 0, x;
  ll ans = 0;
  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> x;
    if (x == last) {
      ct ++;
    } else {
      ans += ct * (ct+1LL) / 2;
      ct = 1;
      last = x;
    }
  }
  ans += ct * (ct+1LL) / 2;
  cout << ans << endl;
}
