#include <iostream>
#include <cstring>
#define MAXN 19
using namespace std;

bool adj[19][19];

typedef long long ll;

int start;
ll M[19][1<<19];

ll dp(int u, int mask) {
  ll &ans = M[u][mask];
  if (ans >= 0)
    return ans;
  ans = 0;
  if (adj[u][start] and __builtin_popcount(mask) > 1)
    ans ++;
  for (int v = 0; v < start; v++)
    if (not (mask & (1 << v)) and adj[u][v])
        ans += dp(v, mask | (1<<v));
  return ans;
}

int main() {
  ios::sync_with_stdio(0);
  int n, m;
  cin >> n >> m;
  int u, v;
  while (m--) {
    cin >> u >> v;
    u--, v--;
    adj[u][v] = adj[v][u] = true;
  }
  ll ans = 0;
  for (int i = n-1; i >= 0; i--) {
    memset(M, -1, sizeof M);
    start = i;
    ans += dp(i, 0);
  }
  cout << ans / 2 << endl;
}
