#include <iostream>
#include <algorithm>

using namespace std;
typedef long long ll;

int C[26];

int main() {
  int n, k;
  string s;
  cin >> n >> k >> s;
  for (char c : s)
    ++C[c-'A'];
  sort(C, C + 26, greater<int>());
  ll ans = 0, t;
  for (int i = 0; k; ++i) {
    t = min(k, C[i]);
    k -= t;
    ans += t * t;
  }
  cout << ans << endl;
}
