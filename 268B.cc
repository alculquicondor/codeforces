#include <iostream>
using namespace std;

int main() {
  int n, ans;
  cin >> n;
  ans = n;
  for (int i = 1; i < n; i++)
    ans += 1 + (i+1) * (n-i-1);
  cout << ans << endl;
}
