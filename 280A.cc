#include <iostream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <cstdio>

using namespace std;

typedef double type;

template<typename T>
struct p2d {
  T x, y;
  p2d(T x = 0, T y = 0) : x(x), y(y) {}
  p2d(const p2d &a) : x(a.x), y(a.y) {}
  p2d operator - (const p2d &a) const {
    return p2d(x-a.x, y-a.y);
  }
  p2d operator + (const p2d &a) const {
    return p2d(x+a.x, y+a.y);
  }
  p2d operator * (T k) const {
    return p2d(x*k, y*k);
  }
  p2d operator / (T k) const {
    return p2d(x/k, y/k);
  }
  void operator -= (const p2d &a) {
    x -= a.x, y -= a.y;
  }
  void operator += (const p2d &a) {
    x += a.x, y += a.y;
  }
  void operator *= (T k) {
    x *= k, y *= k;
  }
  void operator /= (T k) {
    x /= k, y /= k;
  }
  bool operator == (const p2d &a) const {
    return x == a.x and y == a.y;
  }
  bool operator != (const p2d &a) const {
    return x != a.x or y != a.y;
  }
  bool operator < (const p2d &a) const {
    if (x == a.x)
      return y < a.y;
    return x < a.x;
  }

  T norm2() const {
    return x * x + y * y;
  }
  T norm() const {
    return sqrt(norm2());
  }
  p2d ort() const {
    return p2d(-y, x);
  }
  T operator % (const p2d &a) const {
    return x * a.y - y * a.x;
  }
  T operator * (const p2d &a) const {
    return x * a.x + y * a.y;
  }
};
typedef p2d<type> pt;
vector<pt> P;
const type pi = atan2(0, -1);

type areaPoly(const vector<pt> &P) {
  type ans = 0;
  for (int i = 0; i < P.size(); i++)
    ans += P[i] % P[i+1 < P.size() ? i+1 : 0];
  ans /= 2;
  return ans >= 0 ? ans : -ans;
}

long long w, h;
void interceptX(int x, const pt &loc, const pt &dir) {
  if (dir.x == 0)
    return;
  type t = (x - loc.x) / dir.x;
  pt dest = loc + dir * t;
  if (dest.y >= - h / 2 and dest.y <= h / 2)
    P.push_back(dest);
}
void interceptY(int y, const pt &loc, const pt &dir) {
  if (dir.y == 0)
    return;
  type t = (y - loc.y) / dir.y;
  pt dest = loc + dir * t;
  if (dest.x >= - w / 2 and dest.x <= w / 2)
    P.push_back(dest);
}

void intercept(const pt &loc, const pt &dir) {
  interceptX(w/2, loc, dir);
  interceptX(-w/2, loc, dir);
  interceptY(h/2, loc, dir);
  interceptY(-h/2, loc, dir);
}

bool order(const pt &a, const pt &b) {
  return atan2(a.y, a.x) < atan2(b.y, b.x);
}

int main() {
  int alpha;
  cin >> w >> h >> alpha;
  if (alpha == 0 or alpha == 180) {
    cout << w * h << endl;
    return 0;
  }
  if (alpha == 90) {
    h = min(h, w);
    cout << h * h << endl;
    return 0;
  }
  w <<= 1;
  h <<= 1;
  type ang = alpha * pi / 180.;
  pt u(cos(ang), sin(ang));
  pt loc = u * (w/2), dir = u.ort();
  intercept(loc, dir);
  u = dir;
  loc = u * (h/2), dir = u.ort();
  intercept(loc, dir);
  u = dir;
  loc = u * (w/2), dir = u.ort();
  intercept(loc, dir);
  u = dir;
  loc = u * (h/2), dir = u.ort();
  intercept(loc, dir);
  sort(P.begin(), P.end(), order);
  printf("%.7f\n", areaPoly(P) / 4);
}
