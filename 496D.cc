#include <iostream>
#include <vector>
#include <algorithm>
#define MAXN 100005

using namespace std;

typedef pair<int, int> pii;
bool V[MAXN], winner;
int C[2], P[2][MAXN], A[2][MAXN], n;

inline int getNext(bool c, int ac, int t) {
  return C[c] < ac + t ? MAXN : P[c][ac + t];
}
inline int getAc(bool c, int idx) {
  return idx ? A[c][idx-1] : 0;
}

int go(int t) {
  int S[] = {0, 0}, nidx[2], idx = 0;
  bool setWinner;
  while (idx < n) {
    for (int c = 0; c < 2; ++c)
      nidx[c] = getNext(c, getAc(c, idx), t);
    setWinner = nidx[1] < nidx[0];
    ++S[setWinner];
    idx = nidx[setWinner];
  }
  return idx == n and S[winner] > S[not winner] ? S[winner] : -1;
}

int main() {
  ios::sync_with_stdio(0);
  int x;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> x;
    V[i] = x == 2;
  }
  winner = V[n-1];
  for (int c = 0; c < 2; ++c) {
    A[c][0] = V[0] == c;
    for (int i = 1; i < n; ++i)
      A[c][i] = A[c][i-1] + (V[i] == c);
  }
  for (int i = 0; i < n; ++i)
    P[V[i]][++C[V[i]]] = i + 1;
  vector<pii> ans;
  int s;
  for (int i = C[winner]; i > 0; --i) {
    s = go(i);
    if (s != -1)
      ans.push_back(pii(s, i));
  }
  sort(ans.begin(), ans.end());
  cout << ans.size() << endl;
  for (pii t : ans)
    cout << t.first << ' ' << t.second << endl;
  cout << endl;
  return 0;
}
