#include <iostream>
#include <queue>
#include <cstring>

using namespace std;

int A[33];
priority_queue<int> Q[33];

void dec(int x) {
  int i = 0;
  while (x) {
    A[i] += x & 1;
    x >>= 1;
    i ++;
  }
}

int main() {
  ios::sync_with_stdio(0);
  int n, m, x;
  cin >> n >> m;
  memset(A, 0, sizeof A);
  for (int i = 0; i < n; i++) {
    cin >> x;
    dec(x);
  }
  for (int i = 0; i < m; i++) {
    cin >> x;
    Q[x].push(1);
  }
  int ans = 0;
  for (int i = 0; i < 32; i++) {
    while (A[i] and not Q[i].empty()) {
      A[i] --;
      ans += Q[i].top();
      Q[i].pop();
    }
    int t;
    while (not Q[i].empty()) {
      t = Q[i].top();
      Q[i].pop();
      if (not Q[i].empty()) {
        t += Q[i].top();
        Q[i].pop();
      }
      Q[i+1].push(t);
    }
  }
  cout << ans << endl;
}
