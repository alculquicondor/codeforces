#include <iostream>
#include <vector>
#define MAXN 1000005

using namespace std;

typedef pair<int, int> pii;
int nexti[MAXN], q = 0, ans[MAXN];
bool used[MAXN];

int main() {
  vector<int> S;
  std::ios::sync_with_stdio(false);
  int n;
  cin >> n;
  S.reserve(n);
  for (int i = 0; i < n; ++i) {
    int x, y;
    cin >> x >> y;
    nexti[x] = y;
    S.push_back(x);
    used[y] = true;
  }
  int id = 0;
  for (int i = 1; i < n; i += 2) {
    id = nexti[id];
    ans[i] = id;
  }
  for (int i = 0; i < (int)S.size(); ++i)
    if (not used[S[i]]) {
      id = S[i];
      break;
    }
  for (int i = 0; i < n; i += 2) {
    ans[i] = id;
    id = nexti[id];
  }
  cout << ans[0];
  for (int i = 1; i < n; ++i)
    cout << ' ' << ans[i];
  cout << endl;
}
