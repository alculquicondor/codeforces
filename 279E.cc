#include <iostream>
#include <cstring>
#define MAXN 1000005

using namespace std;

bool P[MAXN];
string s;
int n, M[MAXN][2];

int DP(int id, bool f) {
  if (id == n)
    return f ? 1 << 30 : 0;
  int &ans = M[id][f];
  if (ans >= 0)
    return ans;
  if (f)
    ans = min((not s[id]) + DP(id+1, 1), 1 + DP(id+1, 0));
  else
    ans = s[id] ? 1 + DP(id+1, 0) : min(DP(id+1, 0), 1 + DP(id+1, 1));
  return ans;
}

int main() {
  cin >> s;
  n = s.size();
  for (int i = 0; i < n; i++)
    s[i] -= '0';
  memset(M, -1, sizeof M);
  cout << min(DP(0, 0), 1 + DP(0, 1)) << endl;
}
