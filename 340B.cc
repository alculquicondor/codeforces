#include <iostream>
#include <vector>
#include <algorithm>
#include <iostream>
#include <cstdio>

using namespace std;

template<typename T>
struct p2d {
  T x, y;
  p2d(T x = 0, T y = 0) : x(x), y(y) {}
  p2d(const p2d &a) : x(a.x), y(a.y) {}
  p2d operator - (const p2d &a) const {
    return p2d(x-a.x, y-a.y);
  }
  p2d operator + (const p2d &a) const {
    return p2d(x+a.x, y+a.y);
  }
  p2d operator * (T k) const {
    return p2d(x*k, y*k);
  }
  p2d operator / (T k) const {
    return p2d(x/k, y/k);
  }
  void operator -= (const p2d &a) {
    x -= a.x, y -= a.y;
  }
  void operator += (const p2d &a) {
    x += a.x, y += a.y;
  }
  void operator *= (T k) {
    x *= k, y *= k;
  }
  void operator /= (T k) {
    x /= k, y /= k;
  }
  bool operator < (const p2d &a) const {
    if (x == a.x)
      return y < a.y;
    return x < a.x;
  }

  T operator % (const p2d &a) const {
    return x * a.y - y * a.x;
  }
  T operator * (const p2d &a) const {
    return x * a.x + y * a.y;
  }
};

typedef p2d<int> pt;

pt A[301];

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  for (int i = 0; i < n; i++)
    cin >> A[i].x >> A[i].y;
  int ans = 0;
  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++) {
      int up = -1, down = -1;
      pt d = A[j] - A[i];
      for (int k = 0; k < n; k++)
        if (k != i and k != j) {
          int t = (A[k] - A[i]) % d;
          if (t > 0)
            up = max(up, t);
          else
            down = max(down, -t);
        }
      if (up != -1 and down != -1)
        ans = max(ans, up + down);
    }
  printf("%.1lf\n", ans / 2.);
}
