n=int(raw_input())
A=[int(x) for x in raw_input().split()]
total=sum(A)
if total%2:
	print total
else:
	A=filter(lambda x: x%2,A)
	A.sort()
	if len(A)==0:
		print 0
	else:
		print total-A[0]
