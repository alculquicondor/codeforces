package net.alculquicondor.yaal.graphs;

import net.alculquicondor.yaal.io.InputReader;

import java.util.Iterator;

public class Grid {
    public static final int[] mv4r = {-1, 0, 1, 0},
                              mv4c = {0, 1, 0, -1};

    public static final int[] mv8r = {-1, -1, 0, 1, 1, 1, 0, -1},
                              mv8c = {0, 1, 1, 1, 0, -1, -1, -1};

    public static final int[] mvKnightR = {-2, -1, 1, 2, 2, 1, -1, -2},
                              mvKnightkC = {1, 2, 2, 1, -1, -2, -2, -1};

    protected char[][] table;
    protected int n, m;

    public boolean valid(int r, int c) {
        return r >= 0 && r < n && c >= 0 && c < m;
    }

    public boolean valid(Location loc) {
        return valid(loc.r, loc.c);
    }

    public Grid(char[][] table) {
        this.table = table;
        n = table.length;
        m = table[0].length;
    }

    public Grid(int n, int m) {
        this.n = n;
        this.m = m;
        table = new char[n][];
    }

    public void read(InputReader in) {
        for (int i = 0; i < n; ++i)
            table[i] = in.next().toCharArray();
    }

    public class Location {
        public int r, c;
        public Location(int r, int c) {
            this.r = r;
            this.c = c;
        }
    }

    public Iterable<Location> get4Mv(Location loc) {
        return () -> new MvIterator(loc, this, mv4r, mv4c);
    }

    public Iterable<Location> get4Mv(int r, int c) {
        return get4Mv(new Location(r, c));
    }

    public Iterable<Location> get8Mv(Location loc) {
        return () -> new MvIterator(loc, this, mv8r, mv8c);
    }

    public Iterable<Location> get8Mv(int r, int c) {
        return get8Mv(new Location(r, c));
    }

    public Iterable<Location> getKnightMv(Location loc) {
        return () -> new MvIterator(loc, this, mvKnightR, mvKnightkC);
    }

    public Iterable<Location> getKnightMv(int r, int c) {
        return getKnightMv(new Location(r, c));
    }

    protected Iterable<Location> getMv(Location loc, int[] mvR, int[] mvC) {
        return () -> new MvIterator(loc, this, mvR, mvC);
    }

    public Iterable<Location> getMv(int r, int c, int[] mvR, int[] mvC) {
        return getMv(new Location(r, c), mvR, mvC);
    }

    public class MvIterator implements Iterator<Location> {
        private Grid grid;
        private Location orig;
        private final int[] mvR, mvC;
        private int p;

        public MvIterator(Location orig, Grid grid, int[] mvR, int[] mvC) {
            this.orig = orig;
            this.grid = grid;
            this.mvC = mvC;
            this.mvR = mvR;
            p = 0;
        }

        @Override
        public boolean hasNext() {
            Location loc = nextValid();
            return loc != null;
        }

        @Override
        public Location next() {
            Location loc = nextValid();
            ++p;
            return loc;
        }

        Location nextValid() {
            while (p < mvR.length) {
                Location loc = new Location(orig.r + mvR[p], orig.c + mvC[p]);
                if (grid.valid(loc))
                    return loc;
                ++p;
            }
            return null;
        }
    }
}
