package net.alculquicondor.yaal.graphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TopoSorter {
    private static byte[] visited;
    private static List<Integer> list;
    private static Graph g;

    private static boolean dfs(int u) {
        visited[u] = 1;
        for (int edge = g.last[u]; edge != -1; edge = g.prev[edge]) {
            int v = g.getAdj(edge);
            if (visited[v] == 1)
                return false;
            else if (visited[v] == 0 && !dfs(v))
                return false;
        }
        visited[u] = 2;
        list.add(u);
        return true;
    }

    public static List<Integer> sort(Graph g) {
        TopoSorter.g = g;
        visited = new byte[g.getVertexCount()];
        list = new ArrayList<>();
        for (int i = 0; i < g.getVertexCount(); ++i)
            if (visited[i] == 0 && !dfs(i))
                return null;
        Collections.reverse(list);
        return list;
    }
}

