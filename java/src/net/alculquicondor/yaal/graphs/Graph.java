package net.alculquicondor.yaal.graphs;

import java.util.Arrays;
import java.util.Iterator;
import java.util.function.Consumer;

public class Graph {
    int[] prev, last;

    protected int[] adj, cnt;
    protected int edgeCount;
    protected int vertexCount;

    public Graph(int n, int m) {
        vertexCount = n;
        edgeCount = 0;
        adj = new int[m];
        prev = new int[m];
        last = new int[n];
        cnt = new int[n];
        Arrays.fill(last, -1);
    }

    public void addEdge(int from, int to) {
        adj[edgeCount] = to;
        prev[edgeCount] = last[from];
        last[from] = edgeCount++;
        ++cnt[from];
    }
    public void addBiEdge(int u, int v) {
        addEdge(u, v);
        addEdge(v, u);
    }

    public int getVertexCount() {
        return vertexCount;
    }

    public int getEdgeCount() {
        return edgeCount;
    }

    public int getAdj(int edge) {
        return adj[edge];
    }

    public int getAdjCount(int vertex) {
        return cnt[vertex];
    }

    public void forEachEdge(int u, Consumer<Integer> exeEdge) {
        for (int edge = last[u]; edge != -1; edge = prev[edge])
            exeEdge.accept(edge);
    }

    public void forEachAdjVertex(int u, Consumer<Integer> exeVertex) {
        for (int edge = last[u]; edge != -1; edge = prev[edge])
            exeVertex.accept(adj[edge]);
    }

    public Iterable<Integer> getAdjEdges(final int u) {
        return () -> new EdgeIterator(this, u);
    }

    public Iterable<Integer> getAdjVertices(int u) {
        return () -> new VertexIterator(this, u);
    }

    class EdgeIterator implements Iterator<Integer> {
        protected Graph graph;
        protected int edge;

        public EdgeIterator(Graph graph, int vertex) {
            this.edge = graph.last[vertex];
            this.graph = graph;
        }

        @Override
        public boolean hasNext() {
            return edge != -1;
        }

        @Override
        public Integer next() {
            int rEdge = edge;
            edge = graph.prev[edge];
            return rEdge;
        }
    }

    class VertexIterator extends EdgeIterator {
        public VertexIterator(Graph graph, int edge) {
            super(graph, edge);
        }

        @Override
        public Integer next() {
            return graph.adj[super.next()];
        }
    }

}
