package net.alculquicondor.yaal.graphs;

public class LongWeightedGraph extends Graph {
    protected long[] weight;

    public LongWeightedGraph(int n, int m) {
        super(n, m);
        weight = new long[m];
    }

    public void addEdge(int u, int v, long w) {
        weight[edgeCount] = w;
        super.addEdge(u, v);
    }

    public void addBiEdge(int u, int v, long w) {
        addEdge(u, v, w);
        addEdge(v, u, w);
    }

    public void setWeight(int edge, int w) {
        weight[edge] = w;
    }

    public long getWeight(int edge) {
        return weight[edge];
    }
}
