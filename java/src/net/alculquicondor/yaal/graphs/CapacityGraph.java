package net.alculquicondor.yaal.graphs;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

/**
 * Dinic's algorithm for Maximum FLow
 * Total running time: O(V^2 E)
 * For bipartite graphs: O(V^2.5)
 * For graphs with unit capacities: O(VE)
 */
public class CapacityGraph extends Graph {

    private int dst;

    protected int[] cap;
    protected int[] flow;
    protected int[] level;
    protected int[] currentEdge;

    public CapacityGraph(int n, int m) {
        super(n, m);
        cap = new int[m];
        flow = new int[m];
        level = new int[n];
        currentEdge = new int[n];
    }

    protected void addEdge(int from, int to, int cap) {
        this.cap[edgeCount] = cap;
        super.addEdge(from, to);
    }

    public void addBiEdge(int from, int to, int cap) {
        this.addEdge(from, to, cap);
        this.addEdge(to, from, cap);
    }

    public void addDirectedEdge(int from, int to, int cap) {
        addEdge(from, to, cap);
        addEdge(to, from, 0);
    }

    public int getCap(int edge) {
        return cap[edge];
    }

    public void setCap(int edge, int cap) {
        this.cap[edge] = cap;
    }

    private boolean buildLevel(int src, int dst) {
        Arrays.fill(level, -1);
        Queue<Integer> queue = new ArrayDeque<>(vertexCount);
        queue.add(dst);
        level[dst] = 1;
        while (!queue.isEmpty()) {
            int u = queue.poll();
            for (int edge = last[u]; edge != -1; edge = prev[edge]) {
                int v = adj[edge];
                if (cap[edge ^ 1] - flow[edge ^ 1] > 0 && level[v] == -1) {
                    queue.add(v);
                    level[v] = level[u] + 1;
                }
            }
        }
        return level[src] > -1;
    }

    int dfs(int u, int minCap) {
        if (u == dst) return minCap;
        for (int edge = currentEdge[u]; edge != -1; edge = currentEdge[u] = prev[edge]) {
            int v = adj[edge];
            if (cap[edge] - flow[edge] > 0 && level[v] == level[u] - 1) {
                int augment = dfs(v, Math.min(minCap, cap[edge] -flow[edge]));
                if (augment > 0) {
                    flow[edge] += augment;
                    flow[edge ^ 1] -= augment;
                    return augment;
                }
            }
        }
        return 0;
    }

    public long maxFlow(int src, int dst) {
        Arrays.fill(flow, 0);
        this.dst = dst;
        long ans = 0;
        int augment;

        while (buildLevel(src, dst)) {
            System.arraycopy(last, 0, currentEdge, 0, vertexCount);
            while ((augment = dfs(src, Integer.MAX_VALUE)) > 0)
                ans += augment;
        }
        return ans;
    }
}
