package net.alculquicondor.yaal.datastructures;

import java.util.Comparator;

/**
 * Implementation of Range Maximum Query
 * @param <T>
 */
public class RMQ<T extends Comparable<T>> {
    private Comparator<T> cmp;
    private T []v;
    private int [][]ds;
    static private int log(int x) {
        return 31 - Integer.numberOfLeadingZeros(x);
    }
    private void pre() {
        int id1, id2;
        for (int i = 0; i < v.length; ++i)
            ds[0][i] = i;
        for (int k = 1; (1<<k) <= v.length; ++k)
            for (int i = 0; i + (1<<k) <= v.length; ++i) {
                id1 = ds[k-1][i];
                id2 = ds[k-1][i+(1<<(k-1))];
                ds[k][i] = cmp.compare(v[id1], v[id2]) > 0 ? id1 : id2;
            }
    }
    public RMQ(T[] v, Comparator<T> cmp) {
        this.cmp = cmp;
        this.v = v;
        ds = new int[log(v.length)+1][v.length];
        pre();
    }
    public RMQ(T[] v) {
        this(v, null);
        this.cmp = T::compareTo;
    }
    public int query(int i, int j) {
        int lg = log(j-i+1);
        int id1 = ds[lg][i],
                id2 = ds[lg][j-(1<<lg)+1];
        return cmp.compare(v[id1], v[id2]) > 0 ? id1 : id2;
    }
}
