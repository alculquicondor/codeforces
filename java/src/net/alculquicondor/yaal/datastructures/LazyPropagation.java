package net.alculquicondor.yaal.datastructures;

public abstract class LazyPropagation extends SegmentTree {
    protected long[] add;
    public LazyPropagation(int n, long defaultValue) {
        super(n, defaultValue);
        add = new long[upperPowTwo(n) << 1];
    }
    public LazyPropagation(int n) {
        this(n, 0);
    }
    protected abstract long nodeUpdate(long prev, long up, int size);
    void push(int idx, int l, int r, int L, int R) {
        if (add[idx] > 0) {
            data[idx] = nodeUpdate(data[idx], add[idx], r - l + 1);
            if (l != r) {
                add[L] = mergeAdd(add[L], add[idx]);
                add[R] = mergeAdd(add[R], add[idx]);
            }
            add[idx] = 0;
        }
    }
    @Override
    public void update(int i, long up) {
        update(i, i, up, 0, 0, n - 1);
    }
    public void update(int i, int j, long up) {
        update(i, j, up, 0, 0, n - 1);
    }
    void update(int i, int j, long up, int idx, int l, int r) {
        int L = left(idx), R = L + 1;
        if (l >= i && r <= j)
            add[idx] = mergeAdd(add[idx], up);
        push(idx, l, r, L, R);
        if ((l >= i && r <= j) || r < i || l > j)
            return;
        int mid = center(l, r);
        update(i, j, up, L, l, mid);
        update(i, j, up, R, mid + 1, r);
        data[idx] = merge(data[L], data[R]);
    }
    @Override
    long query(int i, int j, int idx, int l, int r) {
        int L = left(idx), R = L + 1;
        push(idx, l, r, L, R);
        if (l >= i && r <= j)
            return data[idx];
        if (r < i || l > j)
            return defaultValue;
        int mid = center(l, r);
        return merge(query(i, j, L, l, mid), query(i, j, R, mid + 1, r));
    }

    protected long mergeAdd(long prev, long up) {
        return merge(prev, up);
    }
}
