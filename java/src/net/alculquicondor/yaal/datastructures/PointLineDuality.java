package net.alculquicondor.yaal.datastructures;

import net.alculquicondor.yaal.geometry.Point2DLong;

public class PointLineDuality {
    private Point2DLong[] q;
    private int ini, fin;

    public PointLineDuality(int cap) {
        q = new Point2DLong[cap];
        ini = 0;
        fin = 0;
    }

    /**
     * Use with increasing values of x
     */
    public void add(long x, long y) {
        add(new Point2DLong(x, y));
    }

    public void add(Point2DLong p) {
        while (fin - ini >= 2 && p.diff(q[fin-1]).cross(p.diff(q[fin-2])) >= 0)
            --fin;
        q[fin++] = p;
    }

    /**
     * Call only when decreasing values of x, else use ternary search
     */
    public void remove(long x) {
        while (fin - ini >= 2 && eval(ini, x) > eval(ini + 1, x))
            ++ini;
    }

    public long evalFirst(long x) {
        return eval(ini, x);
    }

    public long eval(int i, long x) {
        return q[i].getX() * x + q[i].getY();
    }

    public boolean empty() {
        return fin == ini;
    }

    public void clear() {
        ini = 0;
        fin = 0;
    }
}
