package net.alculquicondor.yaal.datastructures;


import java.util.Arrays;

public class DisjointSet {
    private int[] pi, rank;
    private int numberOfSets;

    public DisjointSet(int size) {
        pi = new int[size];
        rank = new int[size];
        Arrays.fill(rank, 0);
        for (int i = 0; i < size; ++i)
            pi[i] = i;
        numberOfSets = size;
    }

    public int numberOfSets() {
        return numberOfSets;
    }

    public int find(int id) {
        return pi[id] = (id == pi[id] ? id : find(pi[id]));
    }

    public void join(int x, int y) {
        x = find(x);
        y = find(y);
        if (x == y)
            return;
        --numberOfSets;
        if (rank[x] < rank[y]) {
            pi[x] = y;
        } else if (rank[x] > rank[y]) {
            pi[y] = x;
        } else {
            pi[x] = y;
            ++rank[y];
        }
    }

    public boolean isSame(int x, int y) {
        return find(x) == find(y);
    }
}
