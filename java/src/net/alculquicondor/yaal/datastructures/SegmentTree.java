package net.alculquicondor.yaal.datastructures;

import java.util.Arrays;

public abstract class SegmentTree {
    protected int n;
    protected long[] data;
    protected long defaultValue;
    protected static int upperPowTwo(int x) {
        return 1 << (32 - Integer.numberOfLeadingZeros(x - 1));
    }
    protected static int center(int x, int y) {
        return (x + y) >> 1;
    }
    protected static int left(int x) {
        return (x << 1) + 1;
    }
    public SegmentTree(int n, long defaultValue) {
        this.n = n;
        this.defaultValue = defaultValue;
        data = new long[upperPowTwo(n) << 1];
        Arrays.fill(data, defaultValue);
    }
    public SegmentTree(int n) {
        this(n, 0);
    }
    public void update(int i, long up) {
        update(i, up, 0, 0, n - 1);
    }
    void update(int i, long up, int idx, int l, int r) {
        int L = left(idx), R = L + 1;
        if (r < i || l > i)
            return;
        if (l == r) {
            data[idx] = up;
            return;
        }
        int mid = center(l, r);
        update(i, up, L, l, mid);
        update(i, up, R, mid + 1, r);
        data[idx] = merge(data[L], data[R]);
    }
    protected abstract long merge(long x, long y);
    public long query(int i) {
        return query(i, i, 0, 0, n - 1);
    }
    public long query(int i, int j) {
        return query(i, j, 0, 0, n - 1);
    }
    long query(int i, int j, int idx, int l, int r) {
        int L = left(idx), R = L + 1;
        if (r < i || l > j)
            return defaultValue;
        if (l >= i && r <= j)
            return data[idx];
        int mid = center(l, r);
        return merge(query(i, j, L, l, mid), query(i, j, R, mid + 1, r));
    }
}
