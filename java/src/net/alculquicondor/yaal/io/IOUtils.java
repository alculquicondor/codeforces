package net.alculquicondor.yaal.io;

import net.alculquicondor.yaal.matrixes.SquareMatrix;

import java.io.PrintWriter;
import java.util.Iterator;


public class IOUtils {

    public static void read(InputReader in, int A[], int start, int end) {
        for (int i = start; i < end; ++i)
            A[i] = in.nextInt();
    }

    public static void read(InputReader in, int A[]) {
        read(in, A, 0, A.length);
    }

    public static void read(InputReader in, long A[], int start, int end) {
        for (int i = start; i < end; ++i)
            A[i] = in.nextLong();
    }

    public static void read(InputReader in, long A[]) {
        read(in, A, 0, A.length);
    }

    public static void read(InputReader in, String A[], int start, int end) {
        for (int i = start; i < end; ++i)
            A[i] = in.next();
    }

    public static void read(InputReader in, int[][] A) {
        for (int[] line : A) {
            read(in, line);
        }
    }

    public static void read(InputReader in, long[][] A) {
        for (long[] line : A) {
            read(in, line);
        }
    }

    public static void read(InputReader in, String A[]) {
        read(in, A, 0, A.length);
    }

    public static void print(PrintWriter out, int A[], int start, int end) {
        if (start < end)
            out.print(A[start]);
        for (int i = start + 1; i < end; ++i)
            out.printf(" %d", A[i]);
    }

    public static void print(PrintWriter out, int A[]) {
        print(out, A, 0, A.length);
    }

    public static void print(PrintWriter out, long A[], int start, int end) {
        if (start < end)
            out.print(A[start]);
        for (int i = start + 1; i < end; ++i)
            out.printf(" %d", A[i]);
    }

    public static void print(PrintWriter out, long A[]) {
        print(out, A, 0, A.length);
    }

    public static <T> void print(PrintWriter out, Iterable<T> iterable) {
        print(out, iterable.iterator());
    }

    public static <T> void print(PrintWriter out, Iterator<T> it) {
        if (it.hasNext()) {
            out.print(it.next());
        }
        while (it.hasNext()) {
            out.print(' ');
            out.print(it.next());
        }
    }

    public static void print(PrintWriter out, SquareMatrix matrix) {
        for (int i = 0; i < matrix.size(); ++i) {
            out.print(matrix.get(i, 0));
            for (int j = 1; j < matrix.size(); ++j)
                out.printf(" %d", matrix.get(i, j));
            out.println();
        }
    }
}
