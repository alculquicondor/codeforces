package net.alculquicondor.yaal.matrixes;

import net.alculquicondor.yaal.numbertheory.NumberTheory;

public class SquareMatrix {
    static long mod = 1000000007;
    protected long[] data;
    protected int n;

    public SquareMatrix(int n) {
        this.n = n;
        data = new long[n * n];
    }

    public SquareMatrix(int n, long[] data) {
        if (data.length != n * n)
            throw new IllegalArgumentException("array length doesn't match");
        this.n = n;
        this.data = data.clone();
    }

    public SquareMatrix(SquareMatrix t) {
        this(t.n, t.data);
    }

    public int size() {
        return n;
    }

    public long get(int i, int j) {
        return data[i * n + j];
    }
    public void set(int i, int j, long v) {
        data[i * n + j] = v;
    }
    public SquareMatrix multiply(SquareMatrix o) {
        SquareMatrix m = new SquareMatrix(n);
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j) {
                for (int k = 0; k < n; ++k)
                    m.data[i * n + j] = NumberTheory.mod(
                            m.data[i * n + j] + data[i * n + k] * o.data[k * n + j],
                            mod);
                if (m.data[i * n + j] < 0)
                    m.data[i * n + j ] += mod;
            }
        return m;
    }
    public SquareMatrix exp(long e) {
        SquareMatrix r = SquareMatrix.identity(n);
        SquareMatrix a = new SquareMatrix(this);
        while (e > 0) {
            if ((e & 1) > 0)
                r = r.multiply(a);
            e >>= 1;
            a = a.multiply(a);
        }
        return r;
    }
    public static SquareMatrix identity(int n) {
        SquareMatrix m = new SquareMatrix(n);
        for (int i = 0; i < n; ++i)
            m.data[i * n + i] = 1;
        return m;
    }
    public static void setMod(long mod) {
        SquareMatrix.mod = mod;
    }
}
