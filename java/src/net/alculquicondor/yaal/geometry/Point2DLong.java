package net.alculquicondor.yaal.geometry;


public class Point2DLong implements Comparable<Point2DLong> {
    private long x, y;
    private double angle, pseudoAngle;
    public Point2DLong(long x, long y) {
        this.x = x;
        this.y = y;
        this.angle = Double.NaN;
        this.pseudoAngle = Double.NaN;
    }
    public long getX() {
        return x;
    }
    public long getY() {
        return y;
    }
    public Point2DLong diff(Point2DLong o) {
        return new Point2DLong(x - o.x, y - o.y);
    }
    public long norm2() {
        return x * x + y * y;
    }
    public double norm() {
        return Math.sqrt(norm2());
    }
    public Point2DLong direction() {
        long g = gcd(Math.abs(x), Math.abs(y));
        return new Point2DLong(x / g, y / g);
    }
    public long inner(Point2DLong o) {
        return x * o.x + y * o.y;
    }
    public long cross(Point2DLong o) {
        return x * o.y - y * o.x;
    }
    public double angle() {
        if (Double.isNaN(angle))
            angle = Math.atan2(y, x);
        return angle;
    }
    public double pseudoAngle() {
        if (Double.isNaN(pseudoAngle))
            pseudoAngle = Math.copySign(1. - (double)x / (Math.abs(x) + Math.abs(y)), y);
        return pseudoAngle;
    }
    @Override
    public int compareTo(Point2DLong o) {
        if (x == o.x)
            return Long.compare(y, o.y);
        return Long.compare(x, o.x);
    }
    private static long gcd(long a, long b) {
        while (b != 0) {
            long c = a % b;
            a = b;
            b = c;
        }
        return a;
    }
}
