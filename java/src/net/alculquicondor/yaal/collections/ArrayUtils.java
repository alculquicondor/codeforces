package net.alculquicondor.yaal.collections;

public class ArrayUtils {
    public static void reverse(int[] A, int start, int end) {
        int n = end - start;
        for (int i = 0; i < (n >> 1); ++i) {
            int tmp = A[i];
            A[i] = A[end - i - 1];
            A[end - i -1] = tmp;
        }
    }
    public static void reverse(int[] A) {
        reverse(A, 0, A.length);
    }
    public static <T> void reverse(T[] A, int start, int end) {
        int n = end - start;
        for (int i = 0; i < (n >> 1); ++i) {
            T tmp = A[i];
            A[i] = A[end - i - 1];
            A[end - i -1] = tmp;
        }
    }
    public static <T> void reverse(T[] A) {
        reverse(A, 0, A.length);
    }
    public static int lowerBound(int[] A, int x) {
        int lo = -1, hi = A.length, mid;
        while (hi - lo > 1) {
            mid = (hi + lo) >> 1;
            if (A[mid] < x)
                lo = mid;
            else
                hi = mid;
        }
        return hi;
    }
    public static int upperBound(int[] A, int x) {
        int lo = -1, hi = A.length, mid;
        while (hi - lo > 1) {
            mid = (hi + lo) >> 1;
            if (A[mid] > x)
                hi = mid;
            else
                lo = mid;
        }
        return hi;
    }
}
