package net.alculquicondor.yaal.collections;

import java.util.NavigableMap;
import java.util.SortedMap;
import java.util.TreeMap;

public class MultiSet<K extends Comparable<K>> {
    private final NavigableMap<K, Integer> data;
    private int size;

    public MultiSet() {
        data = new TreeMap<>();
    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean contains(K key) {
        return data.containsKey(key);
    }

    public void add(K key) {
        data.put(key, data.getOrDefault(key, 0) + 1);
        ++size;
    }

    public void remove(K key) {
        int value = data.getOrDefault(key, 0);
        if (value == 0)
            return;
        --size;
        if (value == 1) {
            data.remove(key);
            return;
        }
        data.put(key, value - 1);
    }

    public K gte(K key) {
        SortedMap<K, Integer> m = data.tailMap(key);
        if (m.isEmpty())
            return null;
        return m.firstKey();
    }

    public K gt(K key) {
        return data.ceilingKey(key);
    }

    public K lte(K key) {
        return data.floorKey(key);
    }

    public K lt(K key) {
        SortedMap<K, Integer> m = data.headMap(key);
        if (m.isEmpty())
            return null;
        return m.lastKey();
    }

    public K last() {
        if (data.isEmpty())
            return null;
        return data.lastKey();
    }
}
