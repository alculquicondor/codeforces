package net.alculquicondor.yaal.numbertheory;

public class Sieve {
    protected int[] factor;
    public Sieve(int n) {
        factor = new int[(n+1)>>1];
        factor[0] = 1;
        for (int i = 3; i <= n; i += 2)
            if (factor[i>>1] == 0) {
                factor[i>>1] = i;
                if ((long) i * i <= n)
                    for (int j = i * i; j <= n; j += (i << 1))
                        factor[j>>1] = i;
            }
    }
    public boolean isPrime(int x) {
        return (x & 1) == 0 ? x == 2 : factor[x>>1] == x;
    }
    public int getFactor(int x) {
        return (x & 1) == 0 ? 2 : factor[x>>1];
    }
}
