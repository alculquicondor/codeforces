package net.alculquicondor.yaal.numbertheory;

public class NumberTheory {
    public static long mod(long x, long mod) {
        return x >= mod || x <= -mod ? x % mod : x;
    }
    public static long expMod(long a, long e, long mod) {
        long r = 1;
        while (e > 0) {
            if ((e & 1) != 0)
                r = mod(a * r, mod);
            e >>= 1;
            a = mod(a * a, mod);
        }
        return r < 0 ? r + mod : r;
    }
}
