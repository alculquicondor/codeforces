package net.alculquicondor.yaal.strings;

public class ZAlgorithm {
    int[] z;
    public ZAlgorithm(String s) {
        int n = s.length();
        z = new int[n];
        for (int i = 1, l = 0, r = 0; i < n; ++i) {
            z[i] = 0;
            if (i <= r)
                z[i] = Math.min(z[i-l], r - i + 1);
            while (i + z[i] < n && s.charAt(z[i]) == s.charAt(i + z[i]))
                ++z[i];
            if (i + z[i] - 1 > r) {
                l = i;
                r = i + z[i] - 1;
            }
        }
        z[0] = n;
    }
    public int get(int i) {
        return z[i];
    }
}
