package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class TaskB {
    String a, b;
    long[] A, B, pot;
    int p, n;
    static final long cte = 31;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        a = in.next();
        b = in.next();
        pre();
        for (int i = 0; i * p < n; ++i) {
            A[i] = hash(a, i * p, i * p + p);
            B[i] = hash(b, i * p, i * p + p);
        }
        if (solve())
            out.println("YES");
        else
            out.println("NO");
    }
    boolean solve() {
        while (p <= n) {
            Map<Long, Integer> C = new HashMap<>(2 * n);
            for (int i = 0; i < n / p; ++i) {
                if (C.containsKey(A[i]))
                    C.put(A[i], C.get(A[i]) + 1);
                else
                    C.put(A[i], 1);
            }
            for (int i = 0; i < n / p; ++i) {
                if (C.containsKey(B[i]) && C.get(B[i]) > 0)
                    C.put(B[i], C.get(B[i]) - 1);
                else
                    return false;
            }
            if (p < n)
                for (int i = 0; i < n / p; i += 2) {
                    A[i >> 1] = merge(A[i], A[i + 1], p);
                    B[i >> 1] = merge(B[i], B[i + 1], p);
                }
            p <<= 1;
        }
        return true;
    }
    void pre() {
        p = n = a.length();
        while ((p & 1) == 0) {
            p >>= 1;
        }
        A = new long[n / p];
        B = new long[n / p];
        pot = new long[n];
        pot[0] = 1;
        for (int i = 1; i < n; ++i)
            pot[i] = pot[i-1] * cte;
    }
    long hash(String s, int ini, int fin) {
        long h = 0;
        for (int i = ini; i < fin; ++i) {
            h = h * cte + (s.charAt(i) - 'a');
        }
        return h;
    }
    long merge(long h1, long h2, int sz) {
        return h1 < h2 ? h1 * pot[sz] + h2 : h2 * pot[sz] + h1;
    }
}
