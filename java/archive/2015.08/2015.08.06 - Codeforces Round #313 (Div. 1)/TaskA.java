package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int[] T = new int[6];
        for (int i = 0; i < 6; ++i)
            T[i] = in.nextInt();
        int l = T[0];
        long ans = 0;
        for (int i = 0; i < T[1] + T[2]; ++i) {
            ans += 2 * l - 1;
            --l;
            if (i < T[1]) {
                ++ans;
                ++l;
            }
            if (i < T[5]) {
                ++ans;
                ++l;
            }
        }
        out.println(ans);
    }
}
