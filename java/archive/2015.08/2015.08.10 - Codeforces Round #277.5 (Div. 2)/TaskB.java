package chelper;



import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.Arrays;

public class TaskB {
    int[] M, W;
    int m, w;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        m = in.nextInt();
        M = new int[m];
        for (int i = 0; i < m; ++i)
            M[i] = in.nextInt();
        w = in.nextInt();
        W = new int[w];
        for (int i = 0; i < w; ++i)
            W[i] = in.nextInt();
        Arrays.sort(M);
        Arrays.sort(W);
        int j = 0, ans = 0;
        for (int i = 0; i < m; ++i) {
            while (j < w && W[j] < M[i] - 1)
                ++j;
            if (j == w)
                break;
            if (W[j] <= M[i] + 1) {
                ++j;
                ++ans;
            }
        }
        out.println(ans);
    }
}
