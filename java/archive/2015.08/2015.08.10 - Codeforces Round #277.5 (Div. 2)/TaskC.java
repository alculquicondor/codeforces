package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskC {
    String mini(int m, int s) {
        if (s == 0) {
            return m == 1 ? "0" : "-1";
        }
        int v = 1;
        --m;
        while (v < 10 && s - v > m * 9)
            ++v;
        if (v == 10)
            return "-1";
        StringBuilder ans = new StringBuilder(m);
        ans.append(v);
        s -= v;
        while (m > 0) {
            --m;
            v = 0;
            while (v < 10 && s - v > m * 9)
                ++v;
            s -= v;
            ans.append(v);
        }
        return ans.toString();
    }
    String maxi(int m, int s) {
        if (s == 0) {
            return m == 1 ? "0" : "-1";
        }
        int v = 9;
        StringBuilder ans = new StringBuilder(m);
        for (int i = 0; i < m; ++i) {
            while (v >= 0 && v > s)
                --v;
            s -= v;
            ans.append(v);
        }
        if (s > 0)
            return "-1";
        return ans.toString();
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int m = in.nextInt(), s = in.nextInt();
        out.printf("%s %s\n", mini(m, s), maxi(m, s));
    }
}
