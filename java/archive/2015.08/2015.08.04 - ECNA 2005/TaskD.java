package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskD {
    static final int[] qx = {-1, -1, -1, 0, 0, 1, 1, 1},
                       qy = {-1, 0, 1, -1, 1, -1, 0, 1},
                       kx = {-2, -2, -1, -1, 1, 1, 2, 2},
                       ky = {-1, 1, -2, 2, -2, 2, -1, 1};
    int n, m;
    short[][] table;
    Pair[] queens, knights, pawns;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        m = in.nextInt();
        int cs = 1;
        while (n != 0 && m != 0) {
            table = new short[n][m];
            queens = readElements(in);
            knights = readElements(in);
            pawns = readElements(in);
            processQueens();
            processKnights();
            int cnt = 0;
            for (int i = 0; i < n; ++i)
                for (int j = 0; j < m; ++j)
                    if (table[i][j] == 0)
                        ++cnt;
            out.printf("Board %d has %d safe squares.\n", cs, cnt);
            n = in.nextInt();
            m = in.nextInt();
            ++cs;
        }
    }

    private Pair[] readElements(InputReader in) {
        int k = in.nextInt();
        Pair[] data = new Pair[k];
        for (int i = 0; i < k; ++i) {
            data[i] = new Pair(in.nextInt() - 1, in.nextInt() - 1);
            table[data[i].x][data[i].y] = 1;
        }
        return data;
    }

    boolean valid(int x, int y) {
        return x >= 0 && x < n && y >= 0 && y < m && table[x][y] != 1;
    }

    private void processKnights() {
        int x, y;
        for (Pair k: knights) {
            for (int c = 0; c < 8; ++c) {
                x = k.x + kx[c];
                y = k.y + ky[c];
                if (valid(x, y))
                    table[x][y] = 2;
            }
        }
    }

    private void processQueens() {
        int x, y;
        for (Pair q : queens) {
            for (int c = 0; c < 8; ++c) {
                x = q.x + qx[c];
                y = q.y + qy[c];
                while (valid(x, y)) {
                    table[x][y] = 2;
                    x += qx[c];
                    y += qy[c];
                }
            }
        }
    }
}

class Pair {
    int x, y;
    Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
