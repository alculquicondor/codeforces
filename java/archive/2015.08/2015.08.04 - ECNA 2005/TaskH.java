package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskH {
    int n;
    int[] A;
    int[][] DP;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        int cs = 1;
        while (n != 0) {
            A = new int[n];
            for (int i = 0; i < n; ++i)
                A[i] = in.nextInt();
            out.printf("In game %d, the greedy strategy might lose by as many as %d points.\n", cs, solve());
            n = in.nextInt();
            ++cs;
        }
    }
    int solve() {
        DP = new int[n][n];
        for (int i = 0; i < n; ++i)
            DP[i][i] = A[i];
        for (int k = 1; k < n; ++k)
            for (int i = 0; i + k < n; ++i) {
                int j = i + k, rm = i + n - 1 - j;
                if ((rm & 1) == 0) { // first player
                    DP[i][j] = Math.max(
                            A[i] - DP[i+1][j],
                            A[j] - DP[i][j-1]);
                } else { // second player
                    DP[i][j] = A[i] >= A[j] ?
                            A[i] - DP[i+1][j] :
                            A[j] - DP[i][j-1];
                }
            }
        return DP[0][n-1];
    }
}
