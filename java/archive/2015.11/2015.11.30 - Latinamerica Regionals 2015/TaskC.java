package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskC {
    int n;
    long[] X, Y;
    long[] acc;
    long totalArea;

    public long cross(int i, int j) {
        return X[i] * Y[j] - X[j] * Y[i];
    }

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        X = new long[2 * n];
        Y = new long[2 * n];
        for (int i = 0; i < n; ++i) {
            X[i] = X[i + n] = in.nextInt();
            Y[i] = Y[i + n] = in.nextInt();
        }

        acc = new long[2 * n];
        acc[0] = 0;
        for (int i = 1; i < 2 * n; ++i) {
            acc[i] = acc[i-1] + cross(i - 1, i);
        }

        totalArea = acc[n];

        long carol = 0, tmp;
        int j = 2;
        for (int i = 0; i < n; ++i) {
            while (j < i + n - 1 && area(i, j) * 2 < totalArea)
                ++j;
            if (Math.abs(2 * area(i, j) - totalArea) < Math.abs(2 * area(i, j - 1) - totalArea))
                tmp = area(i, j);
            else
                tmp = area(i, j - 1);
            carol = Math.max(carol, Math.max(tmp, totalArea - tmp));
        }

        out.printf("%d %d\n", carol, totalArea - carol);
    }

    long area(int i, int j) {
        return acc[j] - acc[i] + cross(j, i);
    }
}
