package chelper;

import net.alculquicondor.yaal.graphs.Graph;
import net.alculquicondor.yaal.io.IOUtils;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class TaskE {
    int d, p, budget;
    int[] price;
    Graph g;
    List<Component> components;
    boolean[] seen;

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        d = in.nextInt();
        p = in.nextInt();
        int r = in.nextInt();
        budget = in.nextInt();
        price = new int[d + p];
        IOUtils.read(in, price);
        g = new Graph(d + p, 2 * r);
        for (int i = 0; i < r; ++i)
            g.addBiEdge(in.nextInt() - 1, d + in.nextInt() - 1);
        components = new ArrayList<>();
        seen = new boolean[d + p];

        for (int u = 0; u < d + p; ++u)
            if (!seen[u]) {
                Component c = new Component();
                dfs(u, c);
                components.add(c);
            }

        out.printf("%d %d\n", corruption(0), corruption(1));
    }

    void dfs(int u, Component c) {
        seen[u] = true;
        ++c.sz[u < d ? 0 : 1];
        c.cost += price[u];
        for (int v : g.getAdjVertices(u))
            if (!seen[v])
                dfs(v, c);
    }

    int corruption(int pid) {
        int[][] DP = new int[components.size() + 1][budget + 1];
        for (int b = 0; b <= budget; ++b)
            DP[components.size()][b] = 0;
        for (int i = components.size() - 1; i >= 0; --i) {
            Component c = components.get(i);
            for (int b = 0; b <= budget; ++b) {
                DP[i][b] = c.sz[pid] + DP[i + 1][b];
                if (b >= c.cost)
                    DP[i][b] = Math.max(DP[i][b],
                            components.get(i).sz[1 - pid] + DP[i + 1][b - c.cost]);
            }
        }
        return DP[0][budget];
    }
}

class Component {
    int[] sz;
    int cost;

    Component() {
        sz = new int[2];
        cost = 0;
    }
}
