package chelper;

import net.alculquicondor.yaal.graphs.CapacityGraph;
import net.alculquicondor.yaal.io.IOUtils;
import net.alculquicondor.yaal.io.InputReader;

import java.io.PrintWriter;

public class TaskB {
    int n;
    CapacityGraph g;
    boolean allNonFull;

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        int q = in.nextInt(), c;

        g = new CapacityGraph(2 * n + 2, 2 * n * (n + 2));
        allNonFull = true;

        for (int i = 0; i < n; ++i)
            g.addDirectedEdge(n + 1 + i, 2 * n + 1, 0);

        for (int i = 0; i < n; ++i) {
            g.addDirectedEdge(0, i + 1, 1);
            c = in.nextInt();
            allNonFull &= c < n;
            for (int j = 0; j < c; ++j)
                g.addDirectedEdge(i + 1, n + in.nextInt(), 1);
        }

        for (int i = 0; i < q; ++i) {
            c = in.nextInt();
            int[] child = new int[c];
            IOUtils.read(in, child);
            out.println(solve(child) ? 'Y' : 'N');
        }
    }

    public boolean solve(int[] child) {
        if (child.length == 0)
            return allNonFull;
        for (int i = 0; i < n; ++i)
            g.setCap(2 * i, 0);
        for (int i = 0; i < child.length; ++i)
            g.setCap(2 * (child[i] - 1), 1);
        return g.maxFlow(0, 2 * n + 1) == child.length;
    }
}
