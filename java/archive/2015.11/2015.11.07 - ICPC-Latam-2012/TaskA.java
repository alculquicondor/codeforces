package chelper;

import net.alculquicondor.yaal.datastructures.PointLineDuality;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskA {
    long[][] DP;
    long[] A, X, W;
    int n, m;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        m = in.nextInt();
        A = new long[n];
        X = new long[n];
        W = new long[n];
        DP = new long[m + 1][n + 1];
        for (int i = 0; i < n; ++i) {
            X[i] = in.nextLong();
            A[i] = in.nextLong();
        }
        W[0] = A[0] * X[0];
        for (int i = 1; i < n; ++i) {
            W[i] = W[i-1] + A[i] * X[i];
            A[i] += A[i - 1];
        }
        PointLineDuality duality = new PointLineDuality(n);
        for (int k = 0; k <= m; ++k) {
            for (int i = n; i >= 0; --i) {
                if (i == n) {
                    DP[k][i] = k != 0 ? Long.MAX_VALUE : 0;
                } else {
                    long acc = i > 0 ? A[i-1] : 0, wcc = i > 0 ? W[i-1] : 0;
                    if (k > 0 && DP[k-1][i+1] < Long.MAX_VALUE)
                        duality.add(-X[i], -wcc + X[i] * acc + DP[k-1][i+1]);
                    duality.remove(acc);
                    DP[k][i] = duality.empty() ? Long.MAX_VALUE : (duality.evalFirst(acc) + wcc);
                }
            }
            duality.clear();
        }
        out.println(DP[m][0]);
    }
}
