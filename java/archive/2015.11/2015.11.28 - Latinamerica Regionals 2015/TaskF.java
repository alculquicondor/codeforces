package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

public class TaskF {
    Event[] events;

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int p = in.nextInt(), v = in.nextInt(), sz = 0;
        events = new Event[p + v];

        for (int i = 0; i < p; ++i)
            events[sz++] = new Event(in.nextInt(), in.nextInt(), i + 1, 0);
        int firstX = in.nextInt(), firstY = in.nextInt(),
                prevX = firstX, prevY = firstY;
        for (int i = 1; i < v; ++i) {
            int x = in.nextInt(), y = in.nextInt();
            if (y == prevY) {
                events[sz++] = new Event(prevX, prevY, 0, prevX < x ? 1 : 2);
                events[sz++] = new Event(x, y, 0, x < prevX ? 1 : 2);
            }
            prevX = x;
            prevY = y;
        }
        if (prevY == firstY) {
            events[sz++] = new Event(firstX, firstY, 0,  firstX < prevX ? 1 : 2);
            events[sz++] = new Event(prevX, prevY, 0,  prevX < firstX ? 1 : 2);
        }

        out.println(outsideValue());
    }

    long outsideValue() {
        Arrays.sort(events);
        SortedSet<Integer> set = new TreeSet<>();
        long ans = 0;
        for (int i = 0; i < events.length; ++i) {
            Event e = events[i];
            if (e.t == 1)
                set.add(e.y);
            else if (e.t == 2)
                set.remove(e.y);
            else if (set.headSet(e.y).size() % 2 == 0)
                ans += e.v;
        }
        return ans;
    }
}

class Event implements Comparable<Event> {
    final int x, y, v, t;

    Event(int x, int y, int v, int t) {
        this.x = x;
        this.y = y;
        this.v = v;
        this.t = t;
    }

    @Override
    public int compareTo(Event o) {
        if (x != o.x)
            return x - o.x;
        if (t != o.t)
            return t - o.t;
        return y - o.y;
    }
}
