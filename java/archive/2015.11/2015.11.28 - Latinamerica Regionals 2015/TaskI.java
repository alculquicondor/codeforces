package chelper;

import net.alculquicondor.yaal.io.IOUtils;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskI {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int t = in.nextInt(), cnt = 0;
        int[] ans = new int[5];
        IOUtils.read(in, ans);
        for (int i = 0; i < 5; ++i)
            if (ans[i] == t)
                ++cnt;
        out.println(cnt);
    }
}
