package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskA {

    long[] pot10;
    int[] pot3;
    final int maxMask = 59049;

    int getDigit(String s, int i) {
        return s.charAt(i) - '0';
    }

    long maxNumFixed(int q, int mask) {
        int d = 9;
        long ans = 0;
        for (int i = 0; i < q && d >= 0; ++i) {
            while (d >= 0 && mask / pot3[d] == 0)
                --d;
            if (d == -1)
                return Long.MIN_VALUE;
            ans = ans * 10 + d;
            mask -= pot3[d];
        }
        return ans;
    }

    long maxNum(String num) {
        int mask = maxMask - 1;
        long ans = 0, acc = 0;
        boolean valid = true;
        for (int i = 0, r = num.length() - 1; i < num.length() && valid; ++i, --r) {
            for (int d = 0; d < getDigit(num, i); ++d)
                if (mask / pot3[d] % 3 > 0)
                    ans = Math.max(ans,
                            (acc * 10 + d) * pot10[r] + maxNumFixed(r, mask - pot3[d]));
            int d = getDigit(num, i);
            if (mask / pot3[d] % 3 > 0) {
                acc = acc * 10 + d;
                mask -= pot3[d];
            } else {
                valid = false;
            }
        }
        if (valid)
            return acc;
        return Math.max(ans, maxNumFixed(maxMask - 1, num.length() - 1));
    }

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        String num = in.next();
        pot10 = new long[19];
        pot3 = new int[19];
        pot10[0] = pot3[0] = 1;
        for (int i = 1; i < 19; ++i) {
            pot10[i] = pot10[i - 1] * 10;
            pot3[i] = pot3[i - 1] * 3;
        }
        out.println(maxNum(num));
    }
}
