package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt(), r, c;
        boolean[] row = new boolean[n], col = new boolean[n];
        n *= n;
        for (int i = 0; i < n; ++i) {
            r = in.nextInt() - 1;
            c = in.nextInt() - 1;
            if (!row[r] && !col[c]) {
                row[r] = true;
                col[c] = true;
                out.print(i + 1);
                out.print(' ');
            }
        }
    }
}
