package chelper;

import net.alculquicondor.yaal.io.IOUtils;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskB {
    int n;
    int[] a;
    boolean[] used;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        a = new int[n];
        used = new boolean[n];
        IOUtils.read(in, a);

        int q = 0, dir = 1, id = -1, ans = -1;
        while (q < n) {
            for (id += dir; id >= 0 && id < n; id += dir)
                if (!used[id] && q >= a[id]) {
                    used[id] = true;
                    ++q;
                }
            dir = -dir;
            ++ans;
        }
        out.println(ans);
    }
}

