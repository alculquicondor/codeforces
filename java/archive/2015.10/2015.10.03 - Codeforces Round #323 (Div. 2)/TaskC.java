package chelper;

import net.alculquicondor.yaal.io.IOUtils;
import net.alculquicondor.yaal.io.InputReader;

import java.io.PrintWriter;
import java.util.Arrays;
import java.util.TreeMap;

public class TaskC {
    int n;
    int[] g;
    int[] sol;
    TreeMap<Integer, Integer> map;

    int gcd(int a, int b) {
        return b == 0 ? a : gcd(b, a % b);
    }

    int mcm(int a, int b) {
        return a / gcd(a, b) * b;
    }

    void remove(int v) {
        map.put(v, map.get(v) - 1);
        if (map.get(v) == 0)
            map.remove(v);
    }
    void add(int d) {
        map.put(d, map.getOrDefault(d, 0) + 1);
    }

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        g = new int[n * n];
        sol = new int[n];
        map = new TreeMap<>();

        IOUtils.read(in, g);
        Arrays.sort(g);
        int id = 0, q = 0;
        while (id < n * n) {
            if (id + 1 < n * n && g[id] == g[id + 1]) {
                add(g[id]);
                id += 2;
            } else {
                sol[q++] = g[id++];
            }
        }

        for (int i = 0; i < q; ++i)
            for (int j = i + 1; j < q; ++j)
                remove(gcd(sol[i], sol[j]));

        while (!map.isEmpty()) {
            sol[q++] = map.lastKey();
            sol[q++] = map.lastKey();
            remove(map.lastKey());
            for (int i = 0; i < q - 2; ++i)
                remove(gcd(sol[i], sol[q-2]));
            for (int i = 0; i < q - 1; ++i)
                remove(gcd(sol[i], sol[q-1]));
        }

        IOUtils.print(out, sol);
    }
}
