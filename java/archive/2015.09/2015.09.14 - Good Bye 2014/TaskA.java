package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt(), t = in.nextInt() - 1;
        int[] a = new int[n];
        for (int i = 0; i < n - 1; ++i)
            a[i] = in.nextInt();
        int x = 0;
        while (x < t)
            x += a[x];
        out.println(x == t ? "YES" : "NO");
    }
}
