package chelper;



import net.alculquicondor.yaal.graphs.Grid;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.Queue;

public class TaskA {
    ChessGrid grid;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        grid = new ChessGrid(8, 8);
        grid.read(in);
        out.println(grid.bfs() ? "WIN" : "LOSE");
    }
}

class ChessGrid extends Grid {
    static int[] mvC, mvR;

    ChessGrid(int n, int m) {
        super(n, m);
        mvC = new int[9];
        mvR = new int[9];
        for (int i = 0; i < 8; ++i) {
            mvC[i] = mv8c[i];
            mvR[i] = mv8r[i];
        }
        mvC[8] = 0;
        mvR[8] = 0;
    }

    boolean valid(TimeLocation loc) {
        return super.valid(loc) && loc.r < loc.t && table[loc.r][loc.c] != 'S';
    }

    boolean bfs() {
        Queue<TimeLocation> q = new ArrayDeque<>(64);
        q.add(new TimeLocation(7, 0, 8));
        while (!q.isEmpty()) {
            TimeLocation loc = q.poll();
            if (loc.r == 0)
                return true;
            for (Location tLoc : getMv(loc, mvR, mvC)) {
                TimeLocation nextLoc = new TimeLocation(tLoc.r, tLoc.c, loc.t);
                if (valid(nextLoc)) {
                    --nextLoc.t;
                    --nextLoc.r;
                    if (valid(nextLoc))
                        q.add(nextLoc);
                }
            }
        }
        return false;
    }

    class TimeLocation extends Location {
        int t;
        TimeLocation(int r, int c, int t) {
            super(r, c);
            this.t = t;
        }
    }
}
