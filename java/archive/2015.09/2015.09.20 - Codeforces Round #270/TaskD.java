package chelper;

import net.alculquicondor.yaal.datastructures.DisjointSet;
import net.alculquicondor.yaal.graphs.Graph;
import net.alculquicondor.yaal.io.IOUtils;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.Arrays;

public class TaskD {
    int[][] dist;
    int n;
    Edge[] edges;
    Graph graph;

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        dist = new int[n][n];
        edges = new Edge[n * (n - 1) / 2];
        IOUtils.read(in, dist);
        out.println(solve() ? "YES" : "NO");
    }

    public boolean solve() {
        int cnt = 0;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < i; ++j) {
                if (dist[i][j] != dist[j][i] || dist[i][j] == 0)
                    return false;
                else
                    edges[cnt++] = new Edge(i, j, dist[i][j]);
            }
            if (dist[i][i] != 0)
                return false;
        }
        Arrays.sort(edges);
        DisjointSet set = new DisjointSet(n);
        graph = new Graph(n, 2 * n - 2);
        for (Edge edge : edges) {
            if (!set.isSame(edge.src, edge.dst)) {
                graph.addBiEdge(edge.src, edge.dst);
                set.join(edge.src, edge.dst);
            }
        }
        for (int i = 0; i < n; ++i)
            if (!dfs(i))
                return false;
        return true;
    }

    boolean dfs(int orig, int u, int p, long acc) {
        if (dist[orig][u] != acc)
            return false;
        for (int v : graph.getAdjVertices(u))
            if (v != p && !dfs(orig, v, u, acc + dist[u][v]))
                return false;
        return true;
    }

    boolean dfs(int orig) {
        return dfs(orig, orig, -1, 0);
    }
}

class Edge implements Comparable<Edge> {
    final int src, dst, weight;
    Edge(int src, int dst, int weight) {
        this.src = src;
        this.dst = dst;
        this.weight = weight;
    }

    @Override
    public int compareTo(Edge o) {
        return Integer.compare(weight, o.weight);
    }
}
