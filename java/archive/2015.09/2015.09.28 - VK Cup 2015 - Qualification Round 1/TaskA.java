package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.Map;
import java.util.TreeMap;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        Map<String, Integer> depth = new TreeMap<>();
        int n = in.nextInt();
        depth.put("polycarp", 1);
        String u, v;
        int ans = 0, d;
        for (int i = 0; i < n; ++i) {
            v = in.next().toLowerCase();
            in.next();
            u = in.next().toLowerCase();
            d = depth.get(u) + 1;
            ans = Math.max(ans, d);
            depth.put(v, d);
        }
        out.println(ans);
    }
}
