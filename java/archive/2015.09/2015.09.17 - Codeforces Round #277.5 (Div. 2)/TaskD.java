package chelper;

import net.alculquicondor.yaal.graphs.Graph;
import net.alculquicondor.yaal.io.InputReader;

import java.io.PrintWriter;

public class TaskD {
    int[] c;
    Graph graph;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt(), m = in.nextInt();
        graph = new Graph(n, m);
        while (m-- > 0)
            graph.addEdge(in.nextInt() - 1, in.nextInt() - 1);
        long cnt = 0;
        for (int i = 0; i < n; ++i) {
            c = new int[n];
            go(i, 2);
            for (int j = 0; j < n; ++j)
                if (j != i)
                    cnt += c[j] * (c[j] - 1) / 2;
        }
        out.println(cnt);
    }
    void go(int u, int depth) {
        if (depth == 0) {
            ++c[u];
            return;
        }
        for (int v : graph.getAdjVertices(u))
            go(v, depth - 1);
    }
}
