package chelper;

import net.alculquicondor.yaal.graphs.Graph;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskC {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt(), m = in.nextInt();
        CatTree ct = new CatTree(n, 2 * n - 2);
        for (int i = 0; i < n; ++i)
            ct.cat[i] = in.nextInt() == 1;
        for (int i = 1; i < n; ++i)
            ct.addBiEdge(in.nextInt() - 1, in.nextInt() - 1);
        out.println(ct.dfs(m));
    }
}

class CatTree extends Graph {
    boolean[] cat;
    CatTree(int n, int m) {
        super(n, m);
        cat = new boolean[n];
    }
    int dfs(int u, int p, int m, int c) {
        if (cat[u])
            c += 1;
        else
            c = 0;
        if (c > m)
            return 0;
        int ans = 0, child = 0;
        for (int e = last[u]; e != -1; e = prev[e]) {
            int v = adj[e];
            if (v != p) {
                ++child;
                ans += dfs(v, u, m, c);
            }
        }
        ans += child == 0 ? 1 : 0;
        return ans;
    }
    int dfs(int m) {
        return dfs(0, -1, m, 0);
    }
}
