package chelper;

import net.alculquicondor.yaal.graphs.LongWeightedGraph;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskA {
    LongWeightedGraph graph;
    List<Solution> solutions;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt(), p = in.nextInt();
        solutions = new ArrayList<>(n);
        boolean[] start = new boolean[n];
        Arrays.fill(start, true);
        graph = new LongWeightedGraph(n, p);
        for (int i = 0; i < p; ++i) {
            int u = in.nextInt() - 1, v = in.nextInt() - 1, d = in.nextInt();
            graph.addEdge(u, v, d);
            start[v] = false;
        }
        for (int i = 0; i < n; ++i)
            if (start[i])
                dfs(i);
        out.println(solutions.size());
        for (Solution sol : solutions)
            out.printf("%d %d %d\n", sol.u + 1, sol.v + 1, sol.w);
    }
    void dfs(int u, int orig, int d) {
        boolean child = false;
        for (int e : graph.getAdjEdges(u)) {
            child = true;
            dfs(graph.getAdj(e), orig, Math.min(d, (int)graph.getWeight(e)));
        }
        if (!child && d < Integer.MAX_VALUE)
            solutions.add(new Solution(orig, u, d));
    }
    void dfs(int u) {
        dfs(u, u, Integer.MAX_VALUE);
    }
}

class Solution {
    int u, v, w;
    Solution(int u, int v, int w) {
        this.u = u;
        this.v = v;
        this.w = w;
    }
}
