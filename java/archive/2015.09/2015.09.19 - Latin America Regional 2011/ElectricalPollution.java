package chelper;

import net.alculquicondor.yaal.graphs.Graph;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.ArrayDeque;
import java.util.Arrays;

public class ElectricalPollution {

    Map<Integer, Integer> set;
    ForceGraph graph;

    int getId(int x) {
        if (!set.containsKey(x))
            set.put(x, set.size());
        return set.get(x);
    }

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int m = in.nextInt(), q = in.nextInt();
        set = new HashMap<>(2 * m);
        if (m + q == 0)
            return;
        graph = new ForceGraph(4 * m, 4 * m);
        for (int i = 0; i < m; ++i) {
            int u = getId(in.nextInt()), v = getId(in.nextInt()),
                    f = in.nextInt() << (u == v ? 1 : 0);
            graph.addEdge(2 * u, 2 * v + 1, f);
            graph.addEdge(2 * v + 1, 2 * u, -f);
            graph.addEdge(2 * v, 2 * u + 1, f);
            graph.addEdge(2 * u + 1, 2 * v, -f);
        }
        for (int i = 0; i < q; ++i) {
            int u = getId(in.nextInt()), v = getId(in.nextInt());
            if (u < 2 * m && v < 2 * m && graph.search(2 * u, 2 * v + 1))
                out.println(graph.getAcc(2 * v + 1) >> (u == v ? 1 : 0));
            else
                out.println('*');
        }
        out.println('-');
    }
}

class ForceGraph extends Graph {
    private int[] force;
    private int[] acc;
    private boolean[] seen;
    Queue<Integer> q;

    public ForceGraph(int n, int m) {
        super(n, m);
        acc = new int[n];
        seen = new boolean[n];
        force = new int[m];
        q = new ArrayDeque<>(n);
    }

    public void addEdge(int u, int v, int f) {
        force[edgeCount] = f;
        super.addEdge(u, v);
    }

    boolean search(int src, int dst) {
        acc[src] = 0;
        Arrays.fill(seen, false);
        q.clear();
        q.add(src);
        seen[src] = true;
        while (!q.isEmpty()) {
            int u = q.poll();
            if (u == dst)
                return true;
            for (int e = last[u]; e >= 0; e = prev[e]) {
                int v = adj[e];
                if (!seen[v]) {
                    acc[v] = acc[u] + force[e];
                    seen[v] = true;
                    q.add(v);
                }
            }
        }
        return false;
    }

    int getAcc(int u) {
        return acc[u];
    }
}
