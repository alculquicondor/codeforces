package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskB {
    int n;
    int[] a;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        a = new int[1 << n + 1];
        for (int i = 1; i < (1 << n + 1) - 1; ++i)
            a[i] = in.nextInt();
        int cnt = 0;
        for (int i = (1 << n) - 2; i >= 0; --i) {
            cnt += Math.abs(a[(i << 1) + 1] - a[(i << 1) + 2]);
            a[i] = a[i] + Math.max(a[(i << 1) + 1], a[(i << 1) + 2]);
        }
        out.println(cnt);
    }
}
