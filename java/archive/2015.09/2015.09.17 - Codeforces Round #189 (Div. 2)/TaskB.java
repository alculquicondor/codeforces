package chelper;

import net.alculquicondor.yaal.collections.Pair;
import net.alculquicondor.yaal.graphs.Graph;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class TaskB {
    List<Pair<Integer, Integer>> intervals;
    Graph graph;
    boolean[] seen;

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt(), q = 0;
        graph = new Graph(n, n * (n - 1) / 2);
        intervals = new ArrayList<>(n);
        int op, a, b;
        while (n-- > 0) {
            op = in.nextInt();
            a = in.nextInt();
            b = in.nextInt();
            if (op == 1) {
                Pair<Integer, Integer> interval = new Pair<>(a, b);
                for (int i = 0; i < q; ++i) {
                    Pair<Integer, Integer> other = intervals.get(i);
                    if ((other.first < interval.first && interval.first < other.second) ||
                            (other.first < interval.second && interval.second < other.second))
                        graph.addEdge(q, i);
                    if ((interval.first < other.first && other.first < interval.second) ||
                            (interval.first < other.second && other.second < interval.second))
                        graph.addEdge(i, q);
                }
                intervals.add(interval);
                ++q;
            } else {
                out.println(find(a - 1, b - 1) ? "YES" : "NO");
            }
        }
    }

    boolean find(int u, int v) {
        seen = new boolean[intervals.size()];
        return dfs(u, v);
    }

    boolean dfs(int u, int f) {
        seen[u] = true;
        if (u == f)
            return true;
        for (int v : graph.getAdjVertices(u))
            if (!seen[v] && dfs(v, f))
                return true;
        return false;
    }
}
