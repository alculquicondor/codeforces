package chelper;

import net.alculquicondor.yaal.collections.Pair;
import net.alculquicondor.yaal.io.InputReader;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class TaskB {
    final static int[] dr = {-1, 0, 0, 1},
                       dc = {0, -1, 1, 0};
    int n, m;
    int er, ec;
    char[][] table;
    boolean[][] seen;

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        m = in.nextInt();
        table = new char[n][];
        seen = new boolean[n][m];
        for (int i = 0; i < n; ++i) {
            table[i] = in.next().toCharArray();
            for (int j = 0; j < m; ++j)
                if (table[i][j] == 'E') {
                    er = i;
                    ec = j;
                }
        }
        out.println(bfs());
    }

    boolean valid(int r, int c) {
        return 0 <= r && r < n && 0 <= c && c < m && !seen[r][c] &&
                (table[r][c] != 'T' || table[r][c] == 'S');
    }

    int quantity(int r, int c) {
        return table[r][c] - '0';
    }

    int bfs() {
        List<Pair<Integer, Integer>> frontier = new ArrayList<>(1), nextFrontier;
        frontier.add(new Pair<>(er, ec));
        seen[er][ec] = true;
        int cnt = 0;
        boolean found = false;
        while (!found) {
            nextFrontier = new ArrayList<>();
            for (Pair<Integer, Integer> loc : frontier) {
                if (table[loc.first][loc.second] == 'S')
                    found = true;
                else if (table[loc.first][loc.second] != 'E')
                    cnt += quantity(loc.first, loc.second);
                for (int i = 0; i < 4; ++i) {
                    int r = loc.first + dr[i], c = loc.second + dc[i];
                    if (valid(r, c)) {
                        nextFrontier.add(new Pair<>(r, c));
                        seen[r][c] = true;
                    }
                }
            }
            frontier = nextFrontier;
        }
        return cnt;
    }

}
