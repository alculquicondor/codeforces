package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskA {
    int n, m;
    char[][] table;
    boolean[][] marked;
    static final int[] dr = {-1, 0, 0, 1},
                       dc = {0, -1, 1, 0};

    boolean valid(int r, int c) {
        return r >= 0 && r < n && c >= 0 && c < m && table[r][c] == '.' && !marked[r][c];
    }

    int dfs(int r, int c, int k) {
        marked[r][c] = true;
        int cnt = 1;
        if (k == 1)
            return cnt;
        int rr, cc;
        for (int p = 0; p < 4; ++p) {
            rr = r + dr[p];
            cc = c + dc[p];
            if (valid(rr, cc) && k - cnt > 0)
                cnt += dfs(rr, cc, k - cnt);
        }
        return cnt;
    }

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        m = in.nextInt();
        int k = in.nextInt(), s = 0, r = -1, c = -1;
        table = new char[n][m];
        marked = new boolean[n][m];
        for (int i = 0; i < n; ++i) {
            table[i] = in.next().toCharArray();
            for (int j = 0; j < m; ++j)
                if (table[i][j] == '.') {
                    ++s;
                    r = i;
                    c = j;
                }
        }
        dfs(r, c, s - k);
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j)
                if (table[i][j] == '.' && !marked[i][j])
                    table[i][j] = 'X';
        for (int i = 0; i < n; ++i)
            out.println(table[i]);
    }
}
