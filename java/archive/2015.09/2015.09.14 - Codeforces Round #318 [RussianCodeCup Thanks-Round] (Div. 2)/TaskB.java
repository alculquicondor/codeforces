package chelper;

import net.alculquicondor.yaal.graphs.Graph;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskB {
    int n;
    Graph g;
    int[] cnt;
    boolean[][] know;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        know = new boolean[n][n];
        cnt = new int[n];
        int m = in.nextInt(), a, b;
        g = new Graph(n, 2 * m);
        for (int i = 0; i < m; ++i) {
            a = in.nextInt() - 1;
            b = in.nextInt() - 1;
            know[a][b] = know[b][a] = true;
            g.addBiEdge(a, b);
        }

        int mini = 3 * m;
        for (int u = 0; u < n; ++u)
            for (int v : g.getAdjVertices(u))
                for (int k = 0; k < n; ++k)
                    if (know[k][u] && know[k][v])
                        mini = Math.min(mini,
                                g.getAdjCount(u) + g.getAdjCount(v) + g.getAdjCount(k) - 6);

        out.println(mini == 3 * m ? -1 : mini);
    }
}
