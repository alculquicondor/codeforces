package chelper;

import net.alculquicondor.yaal.graphs.Graph;
import net.alculquicondor.yaal.io.IOUtils;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TaskB {
    boolean[] seen;
    Graph graph;
    List<Integer> group;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt(), m = in.nextInt();
        seen = new boolean[n];
        graph = new Graph(n, 2 * m);
        for (int i = 0; i < m; ++i)
            graph.addBiEdge(in.nextInt() - 1, in.nextInt() - 1);
        List<List<Integer>> groups = new ArrayList<>(n / 3), ones = new ArrayList<>(),
                            twos = new ArrayList<>();
        for (int i = 0; i < n; ++i)
            if (!seen[i]) {
                group = new ArrayList<>(3);
                dfs(i);
                if (group.size() > 3) {
                    groups = null;
                    break;
                } else if (group.size() == 3) {
                    groups.add(group);
                } else if (group.size() == 2) {
                    twos.add(group);
                } else {
                    ones.add(group);
                }
            }
        if (groups != null) {
            if (ones.size() < twos.size()) {
                groups = null;
            } else {
                Iterator<List<Integer>> it = ones.iterator();
                for (List<Integer> g : twos) {
                    g.addAll(it.next());
                    groups.add(g);
                }
                while (it.hasNext()) {
                    List<Integer> g = it.next();
                    g.addAll(it.next());
                    g.addAll(it.next());
                    groups.add(g);
                }
            }
        }
        if (groups != null) {
            for (List<Integer> g: groups) {
                IOUtils.print(out, g);
                out.println();
            }
        } else {
            out.println(-1);
        }
    }

    private void dfs(int u) {
        seen[u] = true;
        for (int v : graph.getAdjVertices(u))
            if (!seen[v])
                dfs(v);
        group.add(u + 1);
    }
}
