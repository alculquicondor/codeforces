package chelper;

import net.alculquicondor.yaal.datastructures.DisjointSet;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskB {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt(), m = in.nextInt();
        DisjointSet[] set = new DisjointSet[m];
        for (int i = 0; i < m; ++i)
            set[i] = new DisjointSet(n);
        int u, v, c;
        for (int i = 0; i < m; ++i) {
            u = in.nextInt() - 1;
            v = in.nextInt() - 1;
            c = in.nextInt() - 1;
            set[c].join(u, v);
        }
        int q = in.nextInt();
        while (q-- > 0) {
            u = in.nextInt() - 1;
            v = in.nextInt() - 1;
            c = 0;
            for (int i = 0; i < m; ++i)
                if (set[i].same(u, v))
                    ++c;
            out.println(c);
        }
    }
}
