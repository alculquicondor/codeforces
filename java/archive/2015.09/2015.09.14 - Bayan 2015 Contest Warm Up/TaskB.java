package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskB {
    int n, m, N;
    boolean[][] path;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        m = in.nextInt();
        N = n * m;
        path = new boolean[N][N];
        for (int i = 0; i < N; ++i)
            path[i][i] = true;
        char[] s = in.next().toCharArray();
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m - 1; ++j) {
                if (s[i] == '>')
                    path[i * m + j][i * m + j + 1] = true;
                else
                    path[i * m + j + 1][i * m + j] = true;
            }
        s = in.next().toCharArray();
        for (int j = 0; j < m; ++j)
            for (int i = 0; i < n - 1; ++i) {
                if (s[j] == 'v')
                    path[i * m + j][(i + 1) * m + j] = true;
                else
                    path[(i + 1) * m + j][i * m + j] = true;
            }

        for (int k = 0; k < N; ++k)
            for (int i = 0; i < N; ++i)
                for (int j = 0; j < N; ++j)
                    path[i][j] |= path[i][k] && path[k][j];
        boolean allPaths = true;
        for (int i = 0; i < N; ++i)
            for (int j = 0; j < N; ++j)
                allPaths &= path[i][j];

        out.println(allPaths ? "YES" : "NO");
    }
}
