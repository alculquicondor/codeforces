package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class TaskB {
    static final int MAXN = 20001;
    int n, m;
    int[] dist;
    int BFS(int u, int f) {
        Queue<Integer> q = new LinkedList<>();
        Arrays.fill(dist, -1);
        q.add(u);
        dist[u] = 0;
        while (!q.isEmpty()) {
            u = q.poll();
            if (u == f)
                break;
            for (int v : new int[]{2 * u, u - 1})
                if (v >= 0 && v < MAXN && dist[v] == -1) {
                    dist[v] = dist[u] + 1;
                    q.add(v);
                }
        }
        return dist[f];
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        m = in.nextInt();
        dist = new int[MAXN];
        out.println(BFS(n, m));
    }
}
