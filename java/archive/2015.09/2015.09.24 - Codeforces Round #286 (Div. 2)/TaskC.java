package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskC {
    final static int N = 30001;
    int[] cnt;
    int[][] dp;

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt(), d = in.nextInt(), off = d - 247;
        cnt = new int[N];
        dp = new int[N][500];
        for (int i = 0; i < n; ++i)
            ++cnt[in.nextInt()];
        for (int i = N - 1; i >= d; --i) {
            for (int k = Math.max(d - 246, 1); k - d <= 246; ++k) {
                dp[i][k - off] = 0;
                if (k > 1 && i + k - 1 < N)
                    dp[i][k - off] = dp[i + k - 1][k - 1 - off];
                if (i + k < N)
                    dp[i][k - off] = Math.max(dp[i][k - off], dp[i + k][k - off]);
                if (i + k + 1 < N)
                    dp[i][k - off] = Math.max(dp[i][k - off], dp[i + k + 1][k + 1 - off]);
                dp[i][k - off] += cnt[i];
            }
        }
        out.println(dp[d][d - off]);
    }
}
