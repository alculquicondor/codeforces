package chelper;

import net.alculquicondor.yaal.datastructures.DisjointSet;
import net.alculquicondor.yaal.io.IOUtils;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.Vector;

public class TaskB {
    int[] x, cnt;
    DisjointSet set;
    Vector<Vector<Integer>> numbs;

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt();
        x = new int[n];
        cnt = new int[n];
        set = new DisjointSet(n);
        numbs = new Vector<>(n);
        for (int i = 0; i < n; ++i) {
            x[i] = in.nextInt();
            numbs.add(new Vector<>());
        }
        for (int i = 0; i < n; ++i) {
            String s = in.next();
            for (int j = 0; j < n; ++j)
                if (s.charAt(j) == '1')
                    set.join(i, j);
        }

        for (int i = 0; i < n; ++i)
            numbs.elementAt(set.find(i)).add(x[i]);
        for (int i = 0; i < n; ++i)
            numbs.elementAt(i).sort(Comparator.naturalOrder());
        for (int i = 0; i < n; ++i) {
            int s = set.find(i);
            x[i] = numbs.elementAt(s).elementAt(cnt[s]++);
        }

        IOUtils.printArray(out, x);
    }
}
