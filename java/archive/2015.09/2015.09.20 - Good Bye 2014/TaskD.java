package chelper;

import net.alculquicondor.yaal.graphs.LongWeightedGraph;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskD {
    int n;
    CountGraph graph;

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        graph = new CountGraph(n, 2 * n - 2);
        for (int i = 1; i < n; ++i)
            graph.addBiEdge(in.nextInt() - 1, in.nextInt() - 1, in.nextInt());
        graph.dfs(0);
        double ans = graph.sumUp(), fact = 6. / (n * (n - 1l) * (n - 2l));
        int q = in.nextInt();
        while (q-- > 0) {
            int e = 2 * (in.nextInt() - 1);
            if (!graph.seen[e])
                ++e;
            ans -= graph.sumEdge(e);
            graph.setWeight(e, in.nextInt());
            ans += graph.sumEdge(e);
            out.printf("%.10f\n", ans * fact);
        }
    }
}

class CountGraph extends LongWeightedGraph {
    int[] cnt;
    boolean[] seen;

    CountGraph(int n, int m) {
        super(n, m);
        cnt = new int[n];
        seen = new boolean[m];
    }

    int dfs(int u) {
        cnt[u] = 1;
        for (int edge = last[u]; edge != -1; edge = prev[edge]) {
            int v = adj[edge];
            if (cnt[v] == 0) {
                seen[edge] = true;
                cnt[u] += dfs(v);
            }
        }
        return cnt[u];
    }

    double sumUp() {
        double ans = 0;
        for (int e = 0; e < edgeCount; e += 2) {
            ans += sumEdge(seen[e] ? e : e + 1);
        }
        return ans;
    }

    long sumEdge(int edge) {
        long a = cnt[adj[edge]], b = vertexCount - a;
        return weight[edge] * a * b * (b + a - 2);
    }
}