package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskB {
    int n, m;
    char[][] table;
    boolean[][] seen;
    static final int[] dr = {-1, 0, 0, 1},
                       dc = {0, -1, 1, 0};

    boolean valid(int r, int c) {
        return r >= 0 && r < n && c >= 0 && c < m;
    }

    boolean dfs(int r, int c, int pr, int pc) {
        seen[r][c] = true;
        int rr, cc;
        for (int i = 0; i < 4; ++i) {
            rr = r + dr[i];
            cc = c + dc[i];
            if (valid(rr, cc) && table[r][c] == table[rr][cc] && (rr != pr || cc != pc) &&
                    (seen[rr][cc] || dfs(rr, cc, r, c)))
                return true;
        }
        return false;
    }

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        m = in.nextInt();
        table = new char[n][m];
        seen = new boolean[n][m];
        for (int i = 0; i < n; ++i)
            table[i] = in.next().toCharArray();
        boolean found = false;
        for (int i = 0; i < n && !found; ++i)
            for (int j = 0; j < m && !found; ++j)
                if (!seen[i][j])
                    found = dfs(i, j, -1, -1);
        out.println(found ? "Yes" : "No");
    }
}
