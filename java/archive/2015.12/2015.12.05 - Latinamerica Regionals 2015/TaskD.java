package chelper;

import net.alculquicondor.yaal.io.IOUtils;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskD {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt(), m = in.nextInt(), win = 0, sum;
        int[] c = new int[n];
        for (int i = 0; i < m; ++i) {
            sum = in.nextInt();
            IOUtils.read(in, c);
            for (int j = 1; j < c.length; ++j)
                sum -= c[j];
            if (sum > 0) {
                win += Math.pow(10, Math.min((int)Math.log10(sum), 4));
                if (sum - c[0] >= 0)
                    win -= c[0];
            }
        }
        out.println(win);
    }
}
