package chelper;

import net.alculquicondor.yaal.datastructures.DisjointSet;
import net.alculquicondor.yaal.io.IOUtils;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskH {
    int r, c;
    int[][] h;

    int getId(int i, int j) {
        return c * i + j;
    }

    public void solve(int testNumber, InputReader in, PrintWriter out) {
        r = in.nextInt();
        c = in.nextInt();
        h = new int[r][c];
        IOUtils.read(in, h);

        DisjointSet top = new DisjointSet(r * c);
        for (int i = 0; i < r; ++i) {
            for (int j = 0; j < c; ++j) {
                if (i > 0 && h[i][j] == h[i - 1][j])
                    top.join(getId(i, j), getId(i - 1, j));
                if (j > 0 && h[i][j] == h[i][j - 1])
                    top.join(getId(i, j), getId(i, j - 1));
            }
        }

        int ans = top.numberOfSets() + 5 + countSide();

        int[][] tmp = new int[c][r];
        for (int i = 0; i < r; ++i)
            for (int j = 0; j < c; ++j)
                tmp[j][i] = h[i][j];
        h = tmp;
        int tt = r;
        r = c;
        c = tt;

        ans += countSide();

        out.println(ans);
    }

    int countSide() {
        int ans = 0;
        for (int i = 1; i < r; ++i) {
            int j = 0;
            while (j < c) {
                if (h[i][j] < h[i - 1][j]) {
                    ++ans;
                    ++j;
                    while (j < c && h[i][j] < h[i - 1][j] && h[i][j] < h[i-1][j-1] && h[i-1][j] > h[i][j-1])
                        ++j;
                } else if (h[i][j] > h[i - 1][j]) {
                    ++ans;
                    ++j;
                    while (j < c && h[i][j] > h[i - 1][j] && h[i][j] > h[i-1][j-1] && h[i-1][j] < h[i][j-1])
                        ++j;
                } else {
                    ++j;
                }
            }
        }
        return ans;
    }
}
