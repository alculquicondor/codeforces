package chelper;

import net.egork.utils.io.InputReader;
import java.io.PrintWriter;

public class TaskB {
    int n, m, q;
    boolean[][] A;
    int[] rmax;
    void calcRow(int i) {
        rmax[i] = 0;
        int q = 0;
        for (int j = 0; j < m; ++j) {
            if (A[i][j]) {
                if (++q > rmax[i])
                    rmax[i] = q;
            } else {
                q = 0;
            }
        }
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.readInt();
        m = in.readInt();
        q = in.readInt();
        A = new boolean[n][m];
        rmax = new int[n];
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j)
                A[i][j] = in.readInt() == 1;
            calcRow(i);
        }
        while (q-- > 0) {
            int r = in.readInt() - 1, c = in.readInt() - 1;
            A[r][c] = !A[r][c];
            calcRow(r);
            int ans = 0;
            for (int i = 0; i < n; ++i)
                ans = Math.max(ans, rmax[i]);
            out.println(ans);
        }
    }
}
