package chelper;

import net.egork.utils.io.InputReader;
import java.io.PrintWriter;

public class TaskB {
    int n;
    int[] A, L, R, S, ans;
    void calcL() {
        int sz = 0;
        for (int i = 0; i < n; ++i) {
            while (sz > 0 && A[S[sz-1]] >= A[i])
                --sz;
            L[i] = sz > 0 ? S[sz-1] : -1;
            S[sz++] = i;
        }
    }
    void calcR() {
        int sz = 0;
        for (int i = n-1; i >= 0; --i) {
            while (sz > 0 && A[S[sz-1]] >= A[i])
                --sz;
            R[i] = sz > 0 ? S[sz-1] : n;
            S[sz++] = i;
        }
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.readInt();
        A = new int[n];
        L = new int[n];
        R = new int[n];
        S = new int[n];
        ans = new int[n+1];
        for (int i = 0; i < n; ++i)
            A[i] = in.readInt();
        calcL();
        calcR();
        for (int i = 0; i < n; ++i) {
            int q = R[i] - L[i] - 1;
            ans[q] = Math.max(ans[q], A[i]);
        }
        for (int i = n-1; i > 0; --i)
            ans[i] = Math.max(ans[i], ans[i+1]);
        out.print(ans[1]);
        for (int i = 2; i <= n; ++i) {
            out.print(' ');
            out.print(ans[i]);
        }
        out.println();
    }
}
