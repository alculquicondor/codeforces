package chelper;

import net.alculquicondor.yaal.io.InputReader;
import net.alculquicondor.yaal.matrixes.SquareMatrix;

import java.io.PrintWriter;

public class TaskD {
    long MOD;
    long powMod(long a, long e) {
        long r = 1;
        while (e > 0) {
            if ((e & 1) > 0)
                r = mod(a * r);
            e >>= 1;
            a = mod(a * a);
        }
        return r;
    }
    private long mod(long x) {
        if (x >= MOD || x <= -MOD)
            x %= MOD;
        return x < 0 ? x + MOD : x;
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        long n = in.nextLong(), k = in.nextLong(), l = in.nextLong();
        MOD = in.nextLong();
        SquareMatrix.setMod(MOD);
        SquareMatrix mat = new SquareMatrix(2, new long[]{1, 1, 1, 0});
        mat = mat.exp(n);
        long zeros = mod(mat.get(1, 0) * 2 + mat.get(1, 1)),
             ones = mod(powMod(2, n) - zeros);
        long ans = mod(l >= 63 || (1L<<l) > k ? 1 : 0);
        for (int i = 0; i < l; ++i)
            ans = mod(ans * ((k & (1L<<i)) != 0 ? ones : zeros));
        out.print(ans);
    }
}
