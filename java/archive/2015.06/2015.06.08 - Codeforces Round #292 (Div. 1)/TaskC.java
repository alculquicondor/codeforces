package chelper;

import net.egork.utils.io.InputReader;
import java.io.PrintWriter;
import java.util.Comparator;

public class TaskC {
    int n;
    int[] d, h;
    long[] v1, v2;
    RMQ mini, maxi;
    void pre() {
        v1 = new long[2*n];
        v2 = new long[2*n];
        long p = 0;
        for (int i = 0; i < 2 * n; ++i) {
            v1[i] = p - h[i % n];
            v2[i] = p + h[i % n];
            p += d[i % n];
        }
        mini = new RMQ((Long a, Long b) -> a < b ? 1 : 0, v1);
        maxi = new RMQ((Long a, Long b) -> a > b ? 1 : 0, v2);
    }
    long query(int a, int b) {
        int p1 = mini.query(a, b-1);
        long r1 = v2[maxi.query(p1+1, b)] - v1[p1];
        int p2 = maxi.query(a+1, b);
        long r2 = v2[p2] - v1[mini.query(a, p2-1)];
        return Math.max(r1, r2);
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.readInt();
        int m = in.readInt();
        d = new int[n];
        h = new int[n];
        for (int i = 0; i < n; ++i)
            d[i] = in.readInt();
        for (int i = 0; i < n; ++i)
            h[i] = 2 * in.readInt();
        pre();
        int a, b;
        while (m-- > 0) {
            a = in.readInt() - 1;
            b = in.readInt() - 1;
            if (a <= b)
                out.println(query(b+1, a+n-1));
            else
                out.println(query(b+1, a-1));
        }
    }
}

class RMQ {
    Comparator<Long> cmp;
    long []v;
    int [][]ds;
    int log(int x) {
        return 31 - Integer.numberOfLeadingZeros(x);
    }
    void pre() {
        int id1, id2;
        for (int i = 0; i < v.length; ++i)
            ds[0][i] = i;
        for (int k = 1; (1<<k) <= v.length; ++k)
            for (int i = 0; i + (1<<k) <= v.length; ++i) {
                id1 = ds[k-1][i];
                id2 = ds[k-1][i+(1<<(k-1))];
                ds[k][i] = cmp.compare(v[id1], v[id2]) > 0 ? id1 : id2;
            }
    }
    RMQ(Comparator<Long> cmp, long[] v) {
        this.cmp = cmp;
        this.v = v;
        ds = new int[log(v.length)+1][v.length];
        pre();
    }
    int query(int i, int j) {
        int lg = log(j-i+1);
        int id1 = ds[lg][i],
                id2 = ds[lg][j-(1<<lg)+1];
        return cmp.compare(v[id1], v[id2]) > 0 ? id1 : id2;
    }
}
