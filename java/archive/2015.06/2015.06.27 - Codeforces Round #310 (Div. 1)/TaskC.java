package chelper;

import net.alculquicondor.yaal.collections.ArrayUtils;
import net.alculquicondor.yaal.datastructures.LazyPropagation;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.Arrays;

public class TaskC {
    int[][] ps;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt(), q = in.nextInt();
        Query[] queries = new Query[q];
        ps = new int[2][q];
        for (int i = 0; i < q; ++i) {
            queries[i] = new Query(in.nextInt(), in.nextInt(), in.next().charAt(0) == 'U');
            ps[0][i] = queries[i].p[0];
            ps[1][i] = queries[i].p[1];
        }
        Arrays.sort(ps[0]);
        Arrays.sort(ps[1]);
        Maximum[] maximum = new Maximum[2];
        maximum[0] = new Maximum(q);
        maximum[1] = new Maximum(q);
        for (int i = 0; i < q; ++i) {
            Query query = queries[i];
            int stId = ArrayUtils.lowerBound(ps[query.up], query.other());
            long maxi = maximum[query.up].query(stId);
            out.println(query.dir() - maxi);
            maximum[query.up].update(stId, (long)query.dir());
            maximum[1 - query.up].update(
                    ArrayUtils.lowerBound(ps[1 - query.up], (int)maxi),
                    ArrayUtils.upperBound(ps[1 - query.up], query.dir()) - 1, query.other());
        }
    }
}

class Query {
    int[] p;
    int up;
    Query(int x, int y, boolean up) {
        p = new int[]{y, x};
        this.up = up ? 1 : 0;
    }
    int dir() {
        return p[1 - up];
    }
    int other() {
        return p[up];
    }
}

class Maximum extends LazyPropagation {
    Maximum(int n) {
        super(n);
    }

    @Override
    protected long merge(long x, long y) {
        return Math.max(x, y);
    }

    @Override
    protected long nodeUpdate(long prev, long up, int size) {
        return Math.max(prev, up);
    }
}
