package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt(), k = in.nextInt();
        int ans = 0;
        for (int i = 0; i < k; ++i) {
            int m = in.nextInt();
            int[] X = new int[m];
            for (int j = 0; j < m; ++j)
                X[j] = in.nextInt();
            if (X[0] == 1) {
                int j = 1;
                while (j < m && X[j] == X[j-1] + 1)
                    ++j;
                ans += 2 * (m - j) + 1;
            } else {
                ans += 2 * m - 1;
            }
        }
        out.println(ans-1);
    }
}
