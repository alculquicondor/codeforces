package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskC {
    int n, m;
    long[] A;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        m = in.nextInt();
        A = new long[n];
        for (int i = 0; i < n; ++i)
            A[i] = in.nextLong();
        for (int i = 1; i < n; ++i)
            A[i] += A[i-1];
        long lo = 0, hi = Long.MAX_VALUE, mid;
        while (hi - lo > 1) {
            mid = lo + ((hi-lo) >> 1);
            if (possible(mid))
                hi = mid;
            else
                lo = mid;
        }
        out.println(hi);
    }

    boolean possible(long mtime) {
        long left = A[n-1];
        int idx = n;
        for (int i = 0; left > 0 && i < m; ++i) {
            while (idx > 0 && A[idx-1] >= left)
                --idx;
            left -= mtime - idx - 1;
        }
        return left <= 0;
    }
}
