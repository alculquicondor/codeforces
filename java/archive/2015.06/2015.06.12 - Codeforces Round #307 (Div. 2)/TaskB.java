package chelper;

import net.egork.utils.io.InputReader;
import java.io.PrintWriter;

public class TaskB {
    String a, b, c;
    int[] A, B, C;
    void count(int[] X, String s) {
        for (int i = 0; i < s.length(); ++i)
            ++X[s.charAt(i) - 'a'];
    }
    int points(int fa) {
        int pt = fa;
        remove(B, fa);
        pt += maxRep(C);
        for (int i = 0; i < 26; ++i)
            A[i] += B[i] * fa;
        return pt;
    }
    int maxRep(int[] X) {
        int mr = a.length() + 1;
        for (int i = 0; i < 26; ++i)
            if (X[i] > 0)
                mr = Math.min(mr, A[i] / X[i]);
        return mr == a.length() + 1 ? 0 : mr;
    }
    void remove(int[] X, int r) {
        for (int i = 0; i < 26; ++i)
            A[i] -= X[i] * r;
    }
    void print(PrintWriter out, String s, int r) {
        for (int i = 0; i < r; ++i)
            out.print(s);
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        a = in.readString();
        b = in.readString();
        c = in.readString();
        A = new int[26];
        B = new int[26];
        C = new int[26];
        count(A, a);
        count(B, b);
        count(C, c);
        int mp = -1, rep = maxRep(B), ib= 0;
        for (int i = 0; i <= rep; ++i) {
            int p = points(i);
            if (p > mp) {
                mp = p;
                ib = i;
            }
        }
        print(out, b, ib);
        remove(B, ib);
        ib = maxRep(C);
        print(out, c, ib);
        remove(C, ib);
        for (int i = 0; i < 26; ++i)
            for (int j = 0; j < A[i]; ++j)
                out.print((char)('a' + i));
        out.println();
    }
}
