package chelper;

import net.alculquicondor.yaal.matrixes.SquareMatrix;
import net.egork.utils.io.InputReader;
import java.io.PrintWriter;

public class TaskA {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        long n = in.readLong();
        SquareMatrix m = new SquareMatrix(2, new long[]{3L, 1L, 1L, 3L});
        m = m.exp(n);
        out.println(m.get(0, 0));
    }
}
