package chelper;

import net.alculquicondor.yaal.io.IOUtils;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskB {
    void move(int[] A, int pos) {
        int tmp = A[pos];
        A[pos] = A[pos+1];
        A[pos+1] = tmp;
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt();
        long k = in.nextLong() - 1;
        int[] A = new int[n];
        for (int i = 0; i < n; ++i)
            A[i] = i + 1;
        long[] F = new long[n + 1];
        F[0] = F[1] = 1;
        for (int i = 2; i < n; ++i)
            F[i] = F[i-1] + F[i-2];
        int i = 0;
        while (i < n) {
            if (k >= F[n-i-1]) {
                move(A, i);
                k -= F[n-i-1];
                i += 2;
            } else {
                ++i;
            }
        }
        IOUtils.printArray(out, A);
        out.println();
    }
}
