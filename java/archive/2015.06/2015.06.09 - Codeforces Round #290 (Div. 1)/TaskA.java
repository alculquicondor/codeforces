package chelper;

import net.alculquicondor.yaal.graphs.TopoSorter;
import net.egork.utils.io.InputReader;
import java.io.PrintWriter;
import java.util.List;

public class TaskA {
    String[] word;
    TopoSorter g;
    boolean addEdges(int s, int e, int c) {
        while (s < e) {
            int j = s + 1;
            if (word[s].length() == c) {
                while (j < e && word[j].length() == c)
                    ++j;
            } else {
                while (j < e && word[j].length() > c && word[j].charAt(c) == word[s].charAt(c))
                    ++j;
                if (!addEdges(s, j, c + 1))
                    return false;
                if (j < e) {
                    if (word[j].length() <= c)
                        return false;
                    g.addEdge(word[s].charAt(c) - 'a', word[j].charAt(c) - 'a');
                }
            }
            s = j;
        }
        return true;
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        word = new String[n];
        for (int i = 0; i < n; ++i)
            word[i] = in.readString();
        g = new TopoSorter(26);
        List<Integer> list = null;
        if (addEdges(0, n, 0))
            list = g.sort();
        if (list != null) {
            for (int v : list)
                out.print((char) ('a' + v));
            out.println();
        } else {
            out.println("Impossible");
        }
    }
}

