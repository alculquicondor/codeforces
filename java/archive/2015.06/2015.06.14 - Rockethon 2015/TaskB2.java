package chelper;

import net.egork.utils.io.InputReader;
import java.io.PrintWriter;
import java.util.Arrays;

public class TaskB2 {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        long m = in.readLong() - 1;
        int[] a = new int[n];
        int sz = 0;
        for (int i = 0; i < n - 1; ++i)
            if ((m & (1L<<i)) == 0)
                a[sz++] = n - i - 1;
        Arrays.sort(a, 0, sz);
        a[sz++] = n;
        for (int i = 0; i < n - 1; ++i)
            if ((m & (1L<<i)) != 0)
                a[sz++] = n - i - 1;
        out.print(a[0]);
        for (int i = 1; i < n; ++i) {
            out.print(' ');
            out.print(a[i]);
        }
        out.println();
    }
}
