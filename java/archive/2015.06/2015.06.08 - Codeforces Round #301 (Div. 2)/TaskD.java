package chelper;

import net.egork.utils.io.InputReader;
import java.io.PrintWriter;

public class TaskD {
    int R, S, P;
    double[][][] DP;
    double calc(int idx) {
        DP = new double[R+1][S+1][P+1];
        for (int r = 0; r <= R; ++r) {
            for (int s = 0; s <= S; ++s) {
                for (int p = 0; p <= P; ++p) {
                    int t = r + s + p;
                    if (r == t) {
                        DP[r][s][p] = idx == 0 ? 1 : 0;
                    } else if (s == t) {
                        DP[r][s][p] = idx == 1 ? 1 : 0;
                    } else if (p == t) {
                        DP[r][s][p] = idx == 2 ? 1 : 0;
                    } else {
                        double div = 1 - (double)(
                                r * (r-1) + s * (s-1) + p * (p-1)) /
                                (t * (t-1));
                        DP[r][s][p] = 0;
                        if (r > 0)
                            DP[r][s][p] += r * (
                                    (s > 0 ? s * DP[r][s-1][p] : 0) +
                                            p * DP[r-1][s][p]);
                        if (s > 0)
                            DP[r][s][p] += s * (
                                    r * DP[r][s-1][p] +
                                            (p > 0 ? p * DP[r][s][p-1] : 0));
                        if (p > 0)
                            DP[r][s][p] += p * (
                                    (r > 0 ? r * DP[r-1][s][p] : 0) +
                                            s * DP[r][s][p-1]);
                        if (t > 0)
                            DP[r][s][p] /= t * (t-1) * div;
                    }
                }
            }
        }
        return DP[R][S][P];
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        R = in.readInt();
        S = in.readInt();
        P = in.readInt();
        out.printf("%.12f %.12f %.12f\n", calc(0), calc(1), calc(2));
    }
}
