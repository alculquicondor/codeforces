package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskD {
    int n, m;
    int[][] map;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        m = in.nextInt();
        map = new int[n+1][m+1];
        for (int i = 0; i < n; ++i) {
            String s = in.next();
            for (int j = 0; j < m; ++j)
                map[i][j] = s.charAt(j) == 'B' ? -1 : 1;
        }
        int ans = 0;
        for (int i = n - 1; i >= 0; --i)
            for (int j = m - 1; j >= 0; --j)
                if (map[i+1][j] + map[i][j+1] - map[i+1][j+1] - map[i][j] != 0)
                    ++ans;
        out.println(ans);
    }
}
