package chelper;

import net.egork.utils.io.InputReader;
import java.io.PrintWriter;

public class TaskB {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        String s = in.next();
        String bear = "bear";
        int last = -1, ans = 0;
        for (int i = 0; i < s.length(); ++i)
            if (s.startsWith(bear, i)) {
                ans += (i-last) * (s.length()-i-3);
                last = i;
            }
        out.println(ans);
    }
}
