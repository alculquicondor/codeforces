package chelper;

import net.alculquicondor.yaal.collections.Pair;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.SortedSet;
import java.util.TreeSet;

public class TaskD {
    int n;
    List<SortedSet<Pair<Integer, Integer>>> people;
    int[] ans;
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        people = new ArrayList<>(3);
        ans = new int[n];
        for (int i = 0; i < 3; ++i)
            people.add(new TreeSet<>());
        for (int i = 0; i < n; ++i) {
            int x = in.nextInt();
            people.get(x % 3).add(new Pair<>(x, i + 1));
        }
        int p = 0, i = 0;
        SortedSet<Pair<Integer, Integer>> head;
        while (i < n && !(head = people.get(p % 3).headSet(new Pair<>(p + 1, 0))).isEmpty()) {
            Pair<Integer, Integer> t = head.last();
            ans[i] = t.second;
            people.get(p % 3).remove(t);
            p = t.first + 1;
            ++i;
        }
        if (i == n) {
            out.println("Possible");
            out.print(ans[0]);
            for (i = 1; i < n; ++i)
                out.printf(" %d", ans[i]);
            out.println();
        } else {
            out.println("Impossible");
        }
    }
}
