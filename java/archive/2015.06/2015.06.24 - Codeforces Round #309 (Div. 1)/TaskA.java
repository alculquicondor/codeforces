package chelper;

import net.alculquicondor.yaal.io.InputReader;
import net.alculquicondor.yaal.numbertheory.NumberTheory;

import java.io.PrintWriter;

public class TaskA {
    long[] FI;
    final long mod = 1000000007;
    long comb(int n, int m) {
        long ans = 1;
        for (int i = 0; i < m; ++i)
            ans = NumberTheory.mod(ans * (n - i), mod);
        ans = NumberTheory.mod(ans * FI[m], mod);
        return ans;
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        FI = new long[1001];
        long fa = 1;
        FI[0] = 1;
        for (int i = 1; i <= 1000; ++i) {
            fa = NumberTheory.mod(fa * i, mod);
            FI[i] = NumberTheory.expMod(fa, mod - 2, mod);
        }
        int k = in.nextInt(), sum = 0;
        int[] C = new int[k];
        for (int i = 0; i < k; ++i) {
            C[i] = in.nextInt();
            sum += C[i];
        }
        long ans = 1;
        for (int i = k - 1; i >= 0; --i) {
            ans = NumberTheory.mod(ans * comb(sum - 1, C[i] - 1), mod);
            sum -= C[i];
        }
        out.println(ans);
    }
}
