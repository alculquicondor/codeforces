package chelper;

import net.egork.utils.io.InputReader;
import java.io.PrintWriter;

public class TaskA {
    int[] C;
    void move() {
        C[2] += C[4] * 2;
        C[3] += C[4];
        C[4] = 0;

        C[3] += C[6];
        C[5] += C[6];
        C[6] = 0;

        C[2] += C[8] * 3;
        C[7] += C[8];
        C[8] = 0;

        C[3] += C[9] * 2;
        C[2] += C[9];
        C[7] += C[9];
        C[9] = 0;
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.readInt();
        String s = in.next();
        C = new int[10];
        for (int i = 0; i < n; ++i)
            ++C[s.charAt(i)-'0'];
        move();
        for (int i = 9; i > 1; --i)
            for (int j = 0; j < C[i]; ++j)
                out.print((char)('0'+i));
        out.println();
    }
}
