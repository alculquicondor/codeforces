package chelper;

import net.egork.utils.io.InputReader;
import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.Queue;

public class TaskB {
    static int[] dx = {-1, 0, 0, 1},
            dy = {0, -1, 1, 0};
    static char[] c1 = {'v', '>', '<', '^'},
            c2 = {'^', '<', '>', 'v'};
    int n, m;
    char[][] T;
    int[][] C;
    boolean valid(int x, int y) {
        return x >= 0 && x < n && y >= 0 && y < m;
    }
    boolean go() {
        Queue<Pos> Q = new ArrayDeque<>(n * m);
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j)
                if (T[i][j] == '.') {
                    for (int k = 0; k < 4; ++k) {
                        int ii = i + dx[k], jj = j + dy[k];
                        if (valid(ii, jj) && T[ii][jj] == '.')
                            ++C[i][j];
                    }
                    if (C[i][j] <= 1)
                        Q.add(new Pos(i, j));
                }
        while (!Q.isEmpty()) {
            Pos t = Q.poll();
            if (T[t.x][t.y] != '.')
                continue;
            int ii = 0, jj = 0, k;
            for (k = 0; k < 4; ++k) {
                ii = t.x + dx[k];
                jj = t.y + dy[k];
                if (valid(ii, jj) && T[ii][jj] == '.')
                    break;
            }
            if (k == 4)
                return false;
            T[t.x][t.y] = c1[k];
            T[ii][jj] = c2[k];
            t.x = ii;
            t.y = jj;
            for (k = 0; k < 4; ++k) {
                ii = t.x + dx[k];
                jj = t.y + dy[k];
                if (valid(ii, jj) && T[ii][jj] == '.') {
                    --C[ii][jj];
                    if (C[ii][jj] <= 1)
                        Q.add(new Pos(ii, jj));
                }
            }
        }
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < m; ++j)
                if (T[i][j] == '.')
                    return false;
        return true;
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.readInt();
        m = in.readInt();
        T = new char[n][m];
        C = new int[n][m];
        for (int i = 0; i < n; ++i) {
            String s = in.readString();
            T[i] = s.toCharArray();
        }
        if (go()) {
            for (int i = 0; i < n; ++i)
                out.println(T[i]);
        } else {
            out.print("Not unique");
        }
    }
}

class Pos {
    public int x, y;
    public Pos(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
