package chelper;

import net.egork.utils.io.InputReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskB {
    int n;
    int[] V, C;
    List<List<Integer>> numbers;
    List<Integer> decompose(int x) {
        List<Integer> ans = new ArrayList<>();
        for (int i = 2; i * i <= x; ++i)
            if (x % i == 0) {
                ans.add(i);
                while (x % i == 0)
                    x /= i;
            }
        if (x > 1)
            ans.add(x);
        return ans;
    }
    int getMask(List<Integer> pr, int x) {
        int ans = 0;
        for (int i = 0; i < pr.size(); ++i)
            if (x % pr.get(i) == 0)
                ans |= 1 << i;
        return ans;
    }
    int run(int id) {
        List<Integer> number = numbers.get(id);
        int k = number.size();
        int[] DP = new int[1<<k];
        Arrays.fill(DP, Integer.MAX_VALUE);
        DP[0] = 0;
        for (int i = 0; i < n; ++i) {
            int mi = getMask(number, V[i]);
            for (int m = 0; m < (1<<k); ++m)
                DP[m] = (int)Math.min((long)DP[m],
                        (long)DP[m&mi] + C[i]);
        }
        int c = DP[(1<<k)-1];
        return c == Integer.MAX_VALUE ? -1 : c + C[id];
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.readInt();
        numbers = new ArrayList<>(n);
        V = new int[n];
        C = new int[n];
        for (int i = 0; i < n; ++i) {
            V[i] = in.readInt();
            numbers.add(decompose(V[i]));
        }
        for (int i = 0; i < n; ++i)
            C[i] = in.readInt();
        int ans = Integer.MAX_VALUE;
        for (int i = 0; i < n; ++i)
            ans = Math.min(ans, run(i));
        out.print(ans);
    }
}
