package chelper;

import net.alculquicondor.yaal.geometry.Point2DLong;
import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;
import java.util.Arrays;

public class TaskD {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt();
        Point2DLong[] points = new Point2DLong[n];
        for (int i = 0; i < n; ++i)
            points[i] = new Point2DLong(in.nextInt(), in.nextInt());
        Point2DLong[] tPoints = new Point2DLong[n-1];
        long ans = 0;
        for (int i = 0; i < n; ++i) {
            for (int j = 0, k = 0; j < n; ++j)
                if (i != j)
                    tPoints[k++] = points[j].diff(points[i]).direction();
            Arrays.sort(tPoints, (Point2DLong a, Point2DLong b)
                            -> Double.compare(a.pseudoAngle(), b.pseudoAngle()));
            ans += count(tPoints);
        }
        out.println(ans / 3);
    }
    int count(Point2DLong[] points) {
        int n = points.length, i = 0, j = 1, ans = 0;
        while (i < n) {
            int k = 1;
            while (i + k < n && points[i + k].compareTo(points[i]) == 0)
                ++k;
            j = Math.max(i + k, j);
            while (j < i + n && points[i].cross(points[j % n]) > 0)
                ++j;
            i += k;
            ans += (j - i) * k;
        }
        return ans;
    }
}
