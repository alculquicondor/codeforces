package chelper;

import net.alculquicondor.yaal.io.InputReader;
import net.alculquicondor.yaal.numbertheory.NumberTheory;
import net.alculquicondor.yaal.strings.ZAlgorithm;

import java.io.PrintWriter;

public class TaskB {
    int n, m;
    String p;
    int[] x;
    int calc() {
        if (m == 0)
            return n;
        ZAlgorithm z = new ZAlgorithm(p);
        int ans = x[0] - 1;
        for (int i = 1; i < m; ++i) {
            if (x[i] - x[i - 1] < p.length()) {
                if (x[i] - x[i - 1] + z.get(x[i] - x[i - 1]) < p.length())
                    return -1;
            } else {
                ans += x[i] - x[i-1] - p.length();
            }
        }
        ans += n - x[m - 1] - p.length() + 1;
        return ans;
    }
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        n = in.nextInt();
        m = in.nextInt();
        p = in.next();
        x = new int[n];
        for (int i = 0; i < m; ++i)
            x[i] = in.nextInt();
        int q = calc();
        if (q >= 0)
            out.println(NumberTheory.expMod(26, q, 1000000007));
        else
            out.println(0);
    }
}
