package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskC {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int w = in.nextInt(), m = in.nextInt();
        while (m > 0) {
            int r = m % w;
            if (r > 1) {
                if (r == w - 1)
                    m += w;
                else
                    break;
            }
            m /= w;
        }
        out.println(m == 0 ? "YES" : "NO");
    }
}
