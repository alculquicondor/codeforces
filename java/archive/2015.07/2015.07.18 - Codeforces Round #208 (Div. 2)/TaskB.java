package chelper;

import net.alculquicondor.yaal.io.InputReader;
import java.io.PrintWriter;

public class TaskB {
    public void solve(int testNumber, InputReader in, PrintWriter out) {
        int n = in.nextInt();
        StringBuilder patternBuilder = new StringBuilder("<3");
        for (int i = 0; i < n; ++i)
            patternBuilder.append(in.next()).append("<3");
        String pattern = patternBuilder.toString();
        String text = in.next();
        int j = 0;
        for (int i = 0; i < text.length() && j < pattern.length(); ++i)
            if (text.charAt(i) == pattern.charAt(j))
                ++j;
        out.print(j == pattern.length() ? "yes" : "no");
    }
}
