#include <cstdio>
#include <cstring>

int main() {
    int n;
    scanf("%d", &n);
    int A[31][31], R[31], C[31];
    memset(R, 0, sizeof R);
    memset(C, 0, sizeof C);
    for(int i=0; i<n; i++)
        for(int j=0; j<n; j++) {
            scanf("%d", &A[i][j]);
            R[i] += A[i][j];
            C[j] += A[i][j];
        }
    int ans = 0;
    for(int i=0; i<n; i++)
        for(int j=0; j<n; j++)
            ans += C[j]>R[i];
    printf("%d\n", ans);
}
