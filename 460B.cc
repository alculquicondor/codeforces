#include <iostream>
#include <vector>
#include <algorithm>
#define MAXN 1000000000

using namespace std;

int sumdig(int x) {
  int ans = 0;
  while (x) {
    ans += x % 10;
    x /= 10;
  }
  return ans;
}

typedef long long ll;

ll pow(ll a, int e) {
  ll r = 1;
  while (e) {
    if (e & 1)
      r *= a;
    e >>= 1;
    a *= a;
  }
  return r;
}

int main() {
  int a, b, c;
  ll x;
  cin >> a >> b >> c;
  vector<int> A;
  for (int i = 1; i < 82; ++i) {
    x = b * pow(i, a) + c;
    if (x < MAXN and i == sumdig(x))
      A.push_back(x);
  }
  sort(A.begin(), A.end());
  cout << A.size() << endl;
  if (A.size()) {
    cout << A[0];
    for (size_t i = 1; i < A.size(); ++i)
      cout << ' ' << A[i];
    cout << endl;
  }
}
