#include<vector>
#include<cstdio>

using namespace std;

int n;
vector<vector<int> > adj;
vector<bool> visit;
vector<int> pi;
int ciclos;

void dfs(int u)
{
//	printf("#%d\n",u+1);
	visit[u]=1;
	for(int v:adj[u])
	{
		if(!visit[v])
		{
			pi[v]=u;
			dfs(v);
		}
		else
			if(v!=pi[u])
			{
	//			printf("#%d %d\n",u+1,v+1);
				ciclos++;
			}
	}
}

int main()
{
	int m;
	ciclos=0;
	scanf("%d %d",&n,&m);
	adj.resize(n);
	pi.resize(n);
	visit.assign(n,0);
	int u,v;
	for(int i=0;i<m;i++)
	{
		scanf("%d%d",&u,&v);
		u--,v--;
		adj[u].push_back(v);
		adj[v].push_back(u);
	}
	for(int u=0;u<n;u++)
		if(!visit[u])
			dfs(0);
	//printf("%d\n",ciclos);
	if(ciclos==2)
		puts("FHTAGN!");
	else
		puts("NO");
}
