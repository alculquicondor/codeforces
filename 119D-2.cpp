#include <iostream>
#include <cstdio>
#include <algorithm>
#define MAXN 1000005
#define MOD 1000000000
using namespace std;

int pi[MAXN];
void kmpPreprocess(string &P) {
    int i = 0, j = -1, m = P.size();
    pi[0] = -1;
    while(i < m) {
        while(j>=0 and P[i] != P[j])
            j = pi[j];
        i ++, j++;
        pi[i] = j;
    }
}
int res[MAXN];
void kmpSearch(string &T, string &P) {
    int i = 0, j = 0, n = T.size(), m = P.size();
    while(i < n) {
        while(j>=0 and T[i]!=P[j])
            j = pi[j];
        res[++i] = ++j;
        if(j == m)
            j = pi[j];
    }
}
int H1[MAXN], H2[MAXN];
void letshash(int h[], string &s) {
    h[0] = s[0];
    for(int i=1; i<s.size(); i++)
        h[i] = (h[i-1]+s[i])%MOD;
}
bool eq(string &a, int ia, int ja, string &b, int ib, int jb) {
    int h1 = (H1[ja]-(ia?H1[ia-1]:0)+MOD)%MOD;
    int h2 = (H2[jb]-(ib?H2[ib-1]:0)+MOD)%MOD;
    //cout<<a.substr(ia, ja-ia+1)<<endl;
    //cout<<b.substr(ib, jb-ib+1)<<endl;
    if(h1!=h2)
        return false;
    for(int i=ia; i<=ja; i++)
        if(a[i]!=b[jb-i+ia])
            return false;
    return true;
}

int main() {
    string a, b;
    getline(cin, a);
    getline(cin, b);
    int ansi = -1, ansj = -1, i, t, n = a.size(), m = b.size();
    if(n!=m) {
        puts("-1 -1");
        return 0;
    }
    kmpPreprocess(b);
    kmpSearch(a, b);
    letshash(H1, a);
    letshash(H2, b);
    int jj;
    for(int j=n-1; j; j--) {
        jj = res[j];
        while(jj>=0) {
            i = j-jj-1;
            t = j-i-1;
            //printf("%d %d %d\n", i, j, t);
            if(eq(a, 0, i, b, n-i-1, n-1) and eq(a, j, n-1, b, t, n-i-2)) {
                if(ansi<i)
                    ansi = i, ansj = j;
                else if(ansi==i)
                    ansj = min(ansj, j);
            }
            jj = pi[jj];
        }
        if(ansi!=-1)
            break;
    }
    printf("%d %d\n", ansi, ansj);
    return 0;
}
