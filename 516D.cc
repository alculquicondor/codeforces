#include <bits/stdc++.h>
using namespace std;

#define MAXN 100005
typedef long long ll;
typedef pair<int, int> pii;
vector<pii> adj[MAXN];
ll md[MAXN], d[MAXN];

int fur_dfs(int u, int p=-1, ll s=0) {
  d[u] = s;
  md[u] = max(md[u], d[u]);
  int r = u, f;
  for (pii &t : adj[u])
    if (t.first != p) {
      f = fur_dfs(t.first, u, s + t.second);
      if (d[f] > d[r])
        r = f;
    }
  return r;
}

int S[MAXN], R[MAXN];
int ans;
ll l;

int calc_dfs(int u, int p=-1, int pid=-1, int sz=0) {
  int q = 1, lo = pid, hi = sz, mid;
  while (hi - lo > 1) {
    mid = (hi + lo) >> 1;
    if (md[S[mid]] < md[u] - l)
      lo = mid;
    else
      hi = mid;
  }
  pid = lo;
  if (pid >= 0)
    ++R[S[pid]];
  S[sz] = u;
  for (pii &t : adj[u]) {
    int &v = t.first;
    if (v != p)
      q += calc_dfs(v, u, pid, sz+1);
  }
  q -= R[u];
  ans = max(ans, q);
  return q;
}

int main() {
  int n;
  scanf("%d", &n);
  int u, v, w;
  for (int i = 1; i < n; ++i) {
    scanf("%d %d %d", &u, &v, &w);
    adj[u].push_back(pii(v, w));
    adj[v].push_back(pii(u, w));
  }
  int f = fur_dfs(1);
  f = fur_dfs(f);
  fur_dfs(f);
  for (int i = 1; i <= n; ++i)
    if (md[i] < md[f])
      f = i;
  int q;
  scanf("%d", &q);
  while (q--) {
    cin >> l;
    ans = 0;
    memset(R, 0, sizeof R);
    calc_dfs(f);
    printf("%d\n", ans);
  }
  return 0;
}

