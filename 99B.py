n=int(raw_input())
A=[]
for i in range(n):
	A.append(int(raw_input()))
mini=0
maxi=0
for i in range(n):
	if(A[i]<A[mini]):
		mini=i
	elif(A[i]>A[maxi]):
		maxi=i
if(A[mini]==A[maxi]):
	print 'Exemplary pages.'
	exit(0)
if((A[maxi]-A[mini])%2==0):
	mustbe=(A[maxi]+A[mini])/2
	noes=0
	for x in A:
		if x!=mustbe:
			noes+=1
	if noes>2:
		print 'Unrecoverable configuration.'
	else:
		print (A[maxi]-A[mini])/2,'ml. from cup #'+str(mini+1),'to cup #'+str(maxi+1)+'.'
else:
	print 'Unrecoverable configuration.'

