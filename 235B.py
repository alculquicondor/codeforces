n = int(raw_input())
P = [float(x) for x in raw_input().split()]
P.append(0)
DP = [0] * (n+3)
cc = 1
c1 = cc
cj = cc * n
cj2 = cc * n * n
dp = 0
for i in xrange(n-1, -1, -1):
  cc = 1 - P[i]
  c1 = c1 * P[i] + cc
  cj = cj * P[i] + cc * i
  cj2 = cj2 * P[i] + cc * i * i
  dp = dp * P[i] + cc * DP[i+1]
  DP[i] = cj2 - 2 * i * cj + i * i * c1 + dp
print DP[0]
