#include <bits/stdc++.h>
using namespace std;

#define MAXN 5000001
typedef long long ll;
int F[MAXN];
int C[MAXN];

void sieve() {
  for (int i = 2; i < MAXN; ++i)
    if (not F[i]) {
      F[i] = i;
      for (ll j = (ll)i * i; j < MAXN; j += i)
        F[j] = i;
    }
}

void pre() {
  sieve();
  for (int i = 2; i < MAXN; ++i) {
    int r = i;
    C[i] = C[i-1];
    while (r > 1) {
      ++C[i];
      r /= F[r];
    }
  }
}

int main() {
  ios::sync_with_stdio(false);
  pre();
  int n, a, b;
  scanf("%d", &n);
  while (n--) {
    scanf("%d %d", &a, &b);
    printf("%d\n", C[a] - C[b]);
  }
  return 0;
}

