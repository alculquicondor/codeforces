#include <iostream>
#include <cstdio>
#include <cmath>

using namespace std;

typedef long double type;
typedef long double ld;

template<typename T>
struct p3d {
  T x, y, z;
  p3d(T x = 0, T y = 0, T z = 0) : x(x), y(y), z(z) {}
  p3d(const p3d &a) : x(a.x), y(a.y), z(a.z) {}
  p3d operator - (const p3d &a) const {
    return p3d(x-a.x, y-a.y, z-a.z);
  }
  p3d operator + (const p3d &a) const {
    return p3d(x+a.x, y+a.y, z+a.z);
  }
  p3d operator * (T k) const {
    return p3d(x*k, y*k, z*k);
  }
  p3d operator / (T k) const {
    return p3d(x/k, y/k, z/k);
  }
  p3d& operator -= (const p3d &a) {
    x -= a.x, y -= a.y, z -= a.z;
    return *this;
  }
  p3d& operator += (const p3d &a) {
    x += a.x, y += a.y, z += a.z;
    return *this;
  }
  p3d& operator *= (T k) {
    x *= k, y *= k, z *= k;
    return *this;
  }
  p3d& operator /= (T k) {
    x /= k, y /= k, z/= k;
    return *this;
  }
  bool operator == (const p3d &a) const {
    return x == a.x and y == a.y and z == a.z;
  }
  bool operator != (const p3d &a) const {
    return x != a.x or y != a.y or z != a.z;
  }
  T norm2() const {
    return x * x + y * y + z * z;
  }
  ld  norm() const {
    return sqrt((ld)norm2());
  }
};
typedef p3d<type> pt;
#define MAXN 10006
int n;
type vs, vp;
pt A[MAXN], H;
ld L[MAXN];

bool canreach(int id) {
  type mdist = (ld)L[id] / vs * vp;
  //cout << "@ " << id << " " << mdist << "  " << (A[id]-H).norm() << endl;
  return mdist*mdist + 1e-7 >= (A[id] - H).norm2();
}

int getId() {
  int lo = 0, hi = n, mid;
  while (hi - lo > 1) {
    mid = (lo+hi) / 2;
    if (canreach(mid))
      hi = mid;
    else
      lo = mid;
  }
  return hi;
}

bool canreach(int id, ld l) {
  ld mdist = l / vs * vp;
  pt diff = A[id] - A[id-1];
  pt P = A[id-1] + diff * ((l-L[id-1]) / (L[id]-L[id-1]));
  return mdist*mdist + 1e-7 >= (P-H).norm2();
}

pt getInter(int id, ld l) {
  ld mdist = l / vs * vp;
  pt diff = A[id] - A[id-1];
  return A[id-1] + diff * ((l-L[id-1]) / (L[id]-L[id-1]));
}

int main() {
  cin >> n;
  for (int i = 0; i <= n; ++i)
    cin >> A[i].x >> A[i].y >> A[i].z;
  cin >> vp >> vs;
  cin >> H.x >> H.y >> H.z;
  L[0] = 0;
  for (int i = 1; i <= n; ++i)
    L[i] = L[i-1] + (A[i]-A[i-1]).norm();
  int id = getId();
  if (not canreach(id)) {
    cout << "NO" << endl;
  } else {
    cout << "YES" << endl;
    ld lo = L[id-1], hi = L[id], mid;
    for (int i = 0; i < 40; ++i) {
      mid = (lo+hi) / 2;
      if (canreach(id, mid))
        hi = mid + 1e-7;
      else
        lo = mid - 1e-7;
    }
    mid = (lo+hi) / 2;
    pt R = getInter(id, mid);
    printf("%.6f\n", (double)(mid/vs));
    printf("%.6f %.6f %.6f\n", (double)R.x, (double)R.y, (double)R.z);
  }
}

