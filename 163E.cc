#include <cstring>
#include <cstdio>
#include <iostream>
#include <queue>
using namespace std;

#define MAXN 1000005
int state[MAXN][26];
int last[MAXN];
bool terminal[MAXN];
int statecount  = 1 ;
int F[MAXN];

void clear(){
  memset( state ,  0 , sizeof state );// 0 is a not valid state
  memset( terminal , 0 , sizeof terminal );
  statecount = 1;
}

void add( char *s , int id ){
  int pos = 0; // and the root too :3
  for( int i = 0 ; s[i] ; ++i ){
    int next = s[i] - 'a';
    if( state[pos][next] == 0 ){
      state[pos][next] = statecount++;
    }
    pos = state[pos][next];
  }
  terminal[pos] = true;
  last[ id ] = pos;
}

void AC(){
  std::queue<int> Q;
  for( int i = 0 ; i < 26 ; ++i ){
    int cur = state[0][i];
    if( cur ){
      F[cur] = 0;
      Q.push(cur);
    }
  }
  while(!Q.empty()){
    int cur = Q.front(); Q.pop();
    for( int i = 0 ; i < 26 ; ++i ){

      int u = state[cur][i];
      if( u ){
        int p = F[cur];
        while( p && state[ p ] [ i ] == 0  )
          p = F[ p ]; 
        p = state[p][i];
        F[ u ] = p;
        // terminal[ u ] |= terminal[ p ];
        Q.push( u );
      }
    }
  }

}

int dp[MAXN];
int done[MAXN];
int iter;
char cad[MAXN];
int next[MAXN];
int DP[MAXN][26];

int pre( int node ){
  if( node == 0 ) return 0;
  if( done[node] == iter ) {
    if( terminal[ node ] ) return node;
    return next[node];
  }
  done[node] = iter;
  next[ node ] = pre( F[ node ] );
  //cout << node <<" :::: "<< next[ node ] <<endl;
  if( terminal[node] ) return node;
  else return pre( F[ node ] );
}


int go( int node ){
  if( node == 0 ) return 0;
  int & ans = dp[ node ];
  if( done[node] == iter ) return ans;
  done[node] = iter;
  ans = 0;
  if( terminal[node] ) ans = 1;
  ans += go( next[ node ]);
  return ans;
}

bool done2[MAXN][26];

int go2( int node , int nxt ){
  if( node == 0 ) return state[node][nxt];
  int &ans = DP[node][nxt];
  if( done2[node][nxt]  ) return ans;
  done2[node][nxt] = true;
  ans = state[node][nxt];
  if( !ans ) ans = go2( F[node],nxt);
  return ans;
}
int main(){
  int n,k;
  iter = 1;
  scanf("%d %d",&n,&k);
  for( int i = 0 ; i < k ; ++i ){
    scanf("%s",cad);
    add(cad,i);
  }
  AC();
  for( int i = 1 ; i < statecount ; ++i){
    pre( i );
    //cout << i <<" "<<next[i]<<" "<< endl;
  }
  ++iter;
  /*
  for( int i = 0 ; i < 26 ; ++i )
    for( int j = 0 ; j < statecount ; ++j){
      int pos = j;
      while( pos && state[pos][i] == 0 )
        pos = F[ pos ];
      pos = state[pos][i];
      DP[ j ][ i ] = pos;
    }
  */
  while(n--){
    scanf("%s",cad);
    if( cad[0] == '?'){
      int pos = 0 ;
      int ans = 0 ;
      for( int i = 1 ; cad[i] ; ++i ){
        int nxt = cad[i] - 'a';
        //pos = DP[pos][nxt];
        pos = go2( pos , nxt );
        ans += go( pos );
    //    cout << pos << " :: "<<go( pos )<<endl;
      }
      printf("%d\n",ans);
    }
    else{
      int pos;
      ++iter;
      sscanf( cad  + 1 , "%d" , &pos);
      terminal[ last[pos-1] ] = cad[ 0 ] == '+';
    }
  }
}
