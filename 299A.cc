#include <iostream>
#include <algorithm>
#define MAXN 100004

using namespace std;

int A[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n, g = 0;
  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> A[i];
    g = __gcd(A[i], g);
  }
  for (int i = 0; i < n; i++)
    if (A[i] == g) {
      cout << A[i] << endl;
      return 0;
    }
  cout << -1 << endl;
}
