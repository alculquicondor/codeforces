#include <iostream>
#include <vector>
#include <bitset>
#define MOD 1073741824LL

using namespace std;

vector<int> prim;

void pre() {
    bitset<100> isprime;
    isprime.set();
    for(int i=2; i<100; i++)
        if(isprime[i]) {
            prim.push_back(i);
            for(int j=i*i; j<100; j+=i)
                isprime[j] = 0;
        }
}

int div(int n) {
    int ans = 1, i=0, p;
    while(n>1) {
        p = 0;
        while(n%prim[i]==0) {
            p ++;
            n /= prim[i];
        }
        ans = (1LL*ans*(p+1))%MOD;
        i++;
    }
    return ans;
}

int main() {
    pre();
    int a, b, c;
    cin>>a>>b>>c;
    int ans = 0;
    for(int i=1; i<=a; i++)
        for(int j=1; j<=b; j++)
            for(int k=1; k<=c; k++)
                ans = (ans+div(i*j*k))%MOD;
    cout<<ans<<endl;
}
