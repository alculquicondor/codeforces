#include<cstdio>

struct vect {
    int x, y;
    vect(int x,int y) {
        this->x=x;
        this->y=y;
    }
    vect() {
    }
};

int sign(long long x) {
    if(x==0)
        return 0;
    if(x<0)
        return -1;
    return 1;
}

long long crossp(vect a, vect b) {
    return (long long)a.x*b.y-(long long)a.y*b.x;
}

vect operator-(vect a, vect b) {
    return vect(a.x-b.x,a.y-b.y);
}

int n;
vect A[100001];

bool isin(vect &t) {
    int dir = sign(crossp(A[n-1]-t,A[0]-t));
    if(dir==0)
        return 0;
    for(int i=0;i<n-1;i++)
        if(dir != sign(crossp(A[i]-t,A[i+1]-t)) )
            return 0;
    return 1;
}

int main() {
    int x,y,m;
    scanf("%d",&n);
    for(int i=0;i<n;i++) {
        scanf("%d %d",&x,&y);
        A[i]=vect(x,y);
    }
    scanf("%d",&m);
    bool ans=1;
    vect t;
    for(int i=0;i<m;i++) {
        scanf("%d %d",&x,&y);
        t=vect(x,y);
        if(ans)
            if(not isin(t))
                ans=0;
    }
    if(ans)
        puts("YES");
    else
        puts("NO");
}
