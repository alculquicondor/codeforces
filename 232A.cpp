#include <cstdio>
#include <cstring>

bool M[101][101];

int main() {
    int k;
    scanf("%d", &k);
    int n, h = 0;
    memset(M, 0, sizeof M);
    M[1][0] = 1;
    for(n=2; h+1<=k; n++) {
        M[n][0] = M[n][1] = 1;
        h ++;
        for(int j=2; j<n and h+j<=k; j++) {
            M[n][j] = 1;
            h += j;
        }
    }
    printf("%d\n", n);
    for(int i=0; i<n; i++) {
        for(int j=0; j<n; j++)
            printf("%d", j<=i?M[i][j]:M[j][i]);
        puts("");
    }
    puts("");
}
