#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
int val[26];
string s;
map<ll, int> cnt[26];

int main() {
  ios::sync_with_stdio(false);
  for (int i = 0; i < 26; ++i)
    cin >> val[i];
  cin >> s;
  ll sum = 0, ans = 0;
  int id;
  for (int i = s.size() - 1; i >= 0; --i) {
    id = s[i] - 'a';
    map<ll, int> &C = cnt[id];
    ans += C[sum];
    sum += val[id];
    ++C[sum];
  }
  cout << ans << endl;
  return 0;
}

