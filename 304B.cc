#include <cstdio>
#include <cstdlib>

using namespace std;
int month[] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

int calc(int y, int m, int d) {
  d += month[m-1];
  d += (y-1) * 365;
  for (int i = 4; i < y; i += 4)
    if (i % 100 != 0 or i % 400 == 0)
      d++;
  if (y % 4 == 0 and (y % 100 != 0 or y % 400 == 0) and m > 2)
    d++;
  return d;
}

int main() {
  for (int i = 1; i <= 12; i++)
    month[i] += month[i-1];
  int yy, mm, dd;
  scanf("%d:%d:%d", &yy, &mm, &dd);
  int d1 = calc(yy, mm, dd);
  scanf("%d:%d:%d", &yy, &mm, &dd);
  int d2 = calc(yy, mm, dd);
  printf("%d\n", abs(d1-d2));
}
