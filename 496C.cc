#include <iostream>
#include <vector>
#define MAXN 101

using namespace std;

int n, m;
bool rmcol[MAXN];
string T[MAXN];

int solve() {
  vector<int> pstop{n}, cstop;
  int ans = 0;
  for (int col = 0; col < m; ++col) {
    bool valid = true;
    cstop.clear();
    int prev = 0;
    for (int id = 0; valid and id < (int)pstop.size(); ++id) {
      for (int i = prev + 1; valid and i < pstop[id]; ++i) {
        valid &= T[i][col] >= T[i-1][col];
        if (T[i][col] > T[i-1][col])
          cstop.push_back(i);
      }
      if (cstop.empty() or cstop.back() != pstop[id])
        cstop.push_back(pstop[id]);
      prev = pstop[id];
    }
    if (not valid) {
      ++ans;
    } else {
      pstop = cstop;
    }
  }
  return ans;
}

int main() {
  cin >> n >> m;
  for (int i = 0; i < n; ++i)
    cin >> T[i];
  cout << solve() << endl;
  return 0;
}
