#include <iostream>
#include <vector>
#define MOD 1000000009

using namespace std;

typedef long long ll;

inline int value(int m, int k, int r) {
  int ans = k * r, q;
  q = (m - ans) / (k-1);
  if ((k-1) * q == m - ans)
    ans +=  k * q - 1;
  else
    ans += k * q + (m - ans - (k-1) * q);
  return ans;
}

inline int san(ll x) {
  if (x >= MOD)
    x %= MOD;
  return x;
}

class Matrix {
  private:
    int n;
    vector<ll> M;
  public:
    Matrix(int n) : n(n) {
      M.assign(n*n, 0);
    }
    inline ll& operator() (int i, int j) {
      return M[i*n + j];
    }
    inline ll operator() (int i, int j) const {
      return M[i*n + j];
    }
    static Matrix identity(int n) {
      Matrix id(n);
      for (int i = 0; i < n; i++)
        id(i, i) = 1;
      return id;
    }
    Matrix operator*(Matrix &b) const {
      Matrix ans(n);
      for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
          for (int k = 0; k < n; k++)
            ans(i, j) = san(ans(i, j) + (ll)(*this)(i, k)*b(k, j));
      return ans;
    }
    Matrix exp(ll e) const {
      Matrix a = *this;
      Matrix p = identity(n);
      while (e) {
        if (e & 1)
          p = a * p;
        e >>= 1;
        a = a * a;
      }
      return p;
    }
};

int calculate(int k, int r) {
  Matrix M(2);
  M(0, 0) = 2;
  M(0, 1) = san(2 * k);
  M(1, 0) = 0;
  M(1, 1) = 1;
  M = M.exp(r);
  return M(0, 1);
}

int solve(int n, int m, int k) {
  int lo = -1, hi = m / k, mid;
  while (hi - lo > 1) {
    mid = (lo + hi) >> 1;
    if (value(m, k, mid) > n)
      lo = mid;
    else
      hi = mid;
  }
  cerr << hi << endl;
  return san(calculate(k, hi) + m - hi * k);
}

int main() {
  int n, m, k;
  cin >> n >> m >> k;
  cout << solve(n, m, k) << endl;
  return 0;
}
