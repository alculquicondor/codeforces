#include <iostream>
#include <cstring>
#define MAXN 100010

using namespace std;
short M[MAXN][7][3];
short P[MAXN][7][3];
int n;

short DP(int id, int sv, int tr) {
  short &ans = M[id][sv][tr];
  if (ans >= 0)
    return ans;
  //cout << id << " " << sv << " " << tr << endl;
  if (id == n)
    return ans = (sv*10)%7 == 0 and tr == 0;
  ans = false;
  int i;
  for (i = 0; not ans and i < 10; i++)
    ans = DP(id+1, (sv*10+i)%7,(tr+i)%3);
  if (ans)
    P[id][sv][tr] = i - 1;
  return ans;
}

int main() {
  cin >> n;
  if (n == 1) {
    cout << -1 << endl;
    return 0;
  }
  memset(M, -1, sizeof M);
  bool poss = false;
  int id;
  for (id = 1; not poss and id < 10; id++)
    poss = DP(2, id % 7, id % 3);
  if(poss) {
    id--;
    int sv = id % 7, tr = id % 3;
    for (int i = 2; i <= n; i++) {
      cout << id;
      id = P[i][sv][tr];
      sv = (sv*10+id)%7;
      tr = (tr+id)%3;
    }
    cout << 0 << endl;
  } else {
    cout << -1 << endl;
  }
}
