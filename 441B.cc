#include <iostream>
#define MAXN 3002

using namespace std;

int A[MAXN];

int main() {
  std::ios::sync_with_stdio(false);
  int n, v, a, b;
  cin >> n >> v;
  for (int i = 0; i < n; ++i) {
    cin >> a >> b;
    A[a] += b;
  }
  int ans = 0, can, get;
  for (int i = 1; i < MAXN; ++i) {
    get = min(A[i-1], v);
    can = v - get;
    ans += get;
    get = min(A[i], can);
    ans += get;
    A[i] -= get;
  }
  cout << ans << endl;
}

