#include <iostream>
#define MAXN 1001

using namespace std;

int DP[4][MAXN][MAXN], A[MAXN][MAXN], n, m;
int dx[] = {1, 1, -1, -1},
    dy[] = {1, -1, 1, -1};

bool valid(int x, int y) {
  return x >= 0 and x < n and y >= 0 and y < m;
}

void solve(int p) {
  int l1 = dx[p] < 0 ? n : -1,
      l2 = dy[p] < 0 ? m : -1;
  int ii, jj, maxi;
  for (int i = dx[p] < 0 ? 0 : n-1; i != l1; i -= dx[p])
    for (int j = dy[p] < 0 ? 0 : m-1; j != l2; j -= dy[p]) {
      ii = i + dx[p], jj = j + dy[p];
      maxi = 0;
      if (valid(ii, j))
        maxi = max(maxi, DP[p][ii][j]);
      if (valid(i, jj))
        maxi = max(maxi, DP[p][i][jj]);
      DP[p][i][j] += maxi;
    }
}

int main() {
  ios::sync_with_stdio(0);
  cin >> n >> m;
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j)
      cin >> A[i][j];
  for (int p = 0; p < 4; ++p) {
    for (int i = 0; i < n; ++i)
      for (int j = 0; j < m; ++j)
        DP[p][i][j] = A[i][j];
    solve(p);
  }
  int ans = 0, t;
  for (int i = 1; i < n - 1; ++i)
    for (int j = 1; j < m - 1; ++j) {
      t = DP[0][i+1][j] + DP[1][i][j-1] + DP[2][i][j+1] + DP[3][i-1][j];
      ans = max(ans, t);
      t = DP[0][i][j+1] + DP[1][i+1][j] + DP[2][i-1][j] + DP[3][i][j-1];
      ans = max(ans, t);
    }
  cout << ans << endl;
}
