#include <iostream>

using namespace std;

int main() {
  ios::sync_with_stdio(0);
  int n, zero = 0, five = 0, x;
  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> x;
    if (x == 0)
      ++zero;
    else
      ++five;
  }
  if (zero) {
    five /= 9;
    for (int i = 0; i < five; ++i)
      for (int j = 0; j < 9; ++j)
        cout << 5;
    if (five) {
      for (int i = 0; i < zero; i++)
        cout << 0;
    } else if (zero) {
      cout << 0;
    }
  } else {
    cout << -1;
  }
  cout << endl;
}
