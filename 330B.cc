#include <iostream>
#include <cstring>
#define MAXN 1003

using namespace std;

bool valid[MAXN];

int main() {
  int n, m;
  cin >> n >> m;
  int u, v;
  memset(valid, 1, sizeof valid);
  while (m--) {
    cin >> u >> v;
    valid[--u] = valid[--v] = false;
  }
  int id;
  for (id = 0; not valid[id]; id++) {}
  cout << n - 1 << endl;
  for (int i = 0; i < n; i++)
    if (i != id)
      cout << i+1 << " " << id + 1 << endl;
}
