#include <iostream>
#include <algorithm>
#define MOD 1000000007
#define MAXN 1001

using namespace std;
typedef long long ll;

inline ll san(ll x) {
  if (x >= MOD)
    x %= MOD;
  return x;
}

int C[MAXN][MAXN];

ll pow(ll a, int e) {
  ll r = 1;
  while (e > 0) {
    if (e & 1)
      r = san(r * a);
    e >>= 1;
    a = san(a * a);
  }
  return r;
}

void pre(int n) {
  for (int i = 0; i <= n; i++) {
    C[i][0] = C[i][i] = 1;
    for (int j = 1; j < i; j++)
      C[i][j] = san(C[i-1][j-1]+C[i-1][j]);
  }
}

int A[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n, m;
  cin >> n >> m;
  pre(n);
  for (int i = 0; i < m; i++)
    cin >> A[i];
  sort(A, A+m);
  int h = A[0] - 1;
  for (int i = 0; i < m - 1; i++)
    h += A[i+1] - A[i] - 1;
  h += n - A[m-1];
  ll ans = C[h][A[0]-1];
  h -= A[0] - 1;
  int d;
  for (int i = 0; i < m - 1; i++) {
    d = A[i+1] - A[i] - 1;
    ans = san(ans * san(pow(2, d-1) * C[h][d]));
    h -= d;
  }
  cout << ans << endl;
}
