#include <cstdio>
#include <bitset>
#include <cstring>
#include <vector>
#define MAXN 1002

using namespace std;

bitset<MAXN> ans;
vector<int> A;
bool gone[MAXN][MAXN];
int go[MAXN];

void DP(int id, int ac) {
  if (id == A.size()) {
    ans.set(ac);
    return;
  }
  bool &ans = gone[id][ac];
  if (ans)
    return;
  DP(id+1, ac+A[id]);
  DP(id+1, ac);
  ans = true;
}

int main() {
  int n, x, t, localdist;
  scanf("%d %d", &n, &x);
  x--;
  memset(gone, 0, sizeof gone);
  memset(go, -1, sizeof go);
  vector<int> ini;
  ini.reserve(n);
  for (int i = 0; i < n; i++) {
    scanf("%d", &t);
    if (t)
      go[t-1] = i;
    else
      ini.push_back(i);
  }
  for (int t : ini) {
    int d = 0;
    bool vale = true;
    while (t >= 0) {
      if (t == x) {
        vale = false;
        localdist = d;
      }
      d++;
      t = go[t];
    }
    if (vale)
      A.push_back(d);
  }
  DP(0, 0);
  for (int i = 0; i < n; i++)
    if (ans[i])
      printf("%d\n", i + localdist + 1); 
}
