n=int(raw_input())

M=dict()

v=[]

for i in range(n):
	s=raw_input().split()
	if(s[0] in M):
		M[s[0]]+=int(s[1])
	else:
		M[s[0]]=int(s[1])
	v.append(s)

m=max(M,key=lambda x:M[x])
comp=M[m]
valid=set()
for x in M:
	if M[x]==comp:
		valid.add(x)
if len(valid)==1:
	print m
	exit(0)

M=dict()

for s in v:
	if(s[0] in M):
		M[s[0]]+=int(s[1])
	else:
		M[s[0]]=int(s[1])
	if(M[s[0]]>=comp and s[0] in valid):
		print s[0]
		exit(0)
