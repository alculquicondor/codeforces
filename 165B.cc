#include <iostream>

using namespace std;

typedef long long ll;
ll n, k;

bool possible(ll v) {
  ll den = k;
  ll ac = v;
  while (v / den) {
    ac += v / den;
    if (ac >= n)
      break;
    den *= k;
  }
  return ac >= n;
}

int main() {
  cin >> n >> k;
  ll lo = 0, hi = n * k, mid;
  while (hi - lo > 1) {
    mid = (hi + lo) / 2;
    if (possible(mid))
      hi = mid;
    else
      lo = mid;
  }
  cout << hi << endl;
}
