#include <iostream>
#include <algorithm>

using namespace std;

int x, y;
typedef long long ll;

string solve(int t) {
  if (t == 0 or t == x + y - 1)
    return "Both";
  ll lo = 0, hi = (ll)x * y, mid;
  ll q;
  while (hi - lo > 1) {
    mid = (hi + lo) >> 1;
    q = mid / x + mid / y;
    if (q >= t)
      hi = mid;
    else
      lo = mid;
  }
  return hi % x == 0 ? "Vova" : "Vanya";
}

int main() {
  int n, g;
  cin >> n >> x >> y;
  g = __gcd(x, y);
  x /= g;
  y /= g;
  for (int i = 0; i < n; ++i) {
    cin >> g;
    cout << solve(g % (x+y)) << endl;
  }
  return 0;
}
