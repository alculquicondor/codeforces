#include<cstdio>
#include<algorithm>
#include<vector>
#include<iostream>

using namespace std;

struct obj
{
    long long pr;
    int id;
    bool t;
    obj(long long pr,bool t,int id)
    {
        this->pr=pr;
        this->t=t;
        this->id=id;
    }
    obj(){}
};

bool order(obj a,obj b)
{
    if(a.t==b.t)
        return a.pr>b.pr;
    return a.t<b.t;
}

long long findmin(vector<obj> &A)
{
    long long ans=1LL<<62;
    for(auto x:A)
        ans=min(ans,x.pr);
    return ans;
}

int main()
{
    int n,k;
    scanf("%d %d",&n,&k);
    obj A[1001];
    int pr,t;
    for(int i=0;i<n;i++)
    {
        scanf("%d %d",&pr,&t);
        A[i]=obj(2LL*pr,t-1,i+1);
    }
    sort(A,A+n,order);
    vector<obj> C[1001];
    for(int i=0;i<k;i++)
        C[i].push_back(A[i]);
    int i=k;
    while(i<n)
    {
        C[k-1].push_back(A[i]);
        i++;
    }
    long long ans=0;
    for(int i=0;i<k;i++)
    {
        long long mini=findmin(C[i]);
        for(auto x:C[i])
            ans=ans+x.pr;
        if(C[i][0].t==0)
            ans-=mini/2;
    }
    cout<<(ans/2)<<"."<<(ans%2?5:0)<<"\n";
    for(int i=0;i<k;i++)
    {
        printf("%d",C[i].size());
        for(auto x:C[i])
            printf(" %d",x.id);
        puts("");
    }
}
