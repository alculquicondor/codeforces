n,k,l,c,d,p,nl,np = [int(x) for x in raw_input().split()]
liquid = k * l
lemon = c * d

print min(liquid/(nl*n), lemon/n, p/(np*n))
