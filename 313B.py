from itertools import accumulate
s = input()
A = [0] * (len(s)+1)
for i in range(len(s)-1):
  if s[i] == s[i+1]:
    A[i+2] = 1
A = [x for x in accumulate(A)]
n = int(input())
for i in range(n):
  a, b = [int(x) for x in input().split()]
  ans = A[b] - A[a-1] - (s[a-2] == s[a-1] if a >= 2 else 0)
  print(ans)

