#include <iostream>
#include <cstring>
#define MAXN 1001
#define MOD 1000000007

using namespace std;

int n, A[MAXN], M[MAXN][20002];

int DP(int id, int sum) {
  if (id == n)
    return sum == 0;
  int &ans = M[id][sum+10000];
  if (ans >= 0)
    return ans;
  ans = DP(id+1, sum+A[id]) + DP(id+1, sum-A[id]);
  if (sum == 0)
    ++ans;
  if (ans >= MOD)
    ans -= MOD;
  return ans;
}

int main() {
  memset(M, -1, sizeof M);
  ios::sync_with_stdio(0);
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  int ans = 0;
  for (int i = 0; i < n; ++i) {
    ans += DP(i, 0) - 1;
    if (ans >= MOD)
      ans -= MOD;
  }
  cout << ans << endl;
}
