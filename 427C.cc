#include <iostream>
#include <vector>
#include <cstring>
#define MAXN 100004
#define MOD 1000000007

using namespace std;

typedef long long ll;
vector<int> adj[MAXN];
bool visit[MAXN], inS[MAXN];
int sz, ini;
int S[MAXN], idx[MAXN], low[MAXN], C[MAXN];

void tarjan(int u) {
  inS[u] = visit[u] = true;
  low[u] = idx[u] = ini++;
  S[sz++] = u;
  int v;
  for (int i = 0; i < adj[u].size(); ++i) {
    v = adj[u][i];
    if (not visit[v]) {
      tarjan(v);
      low[u] = min(low[u], low[v]);
    } else if (inS[v]) {
      low[u] = min(low[u], idx[v]);
    }
  }
  if (low[u] == idx[u]) {
    do {
      v = S[--sz];
      C[v] = C[u];
      inS[v] = false;
    } while (v != u);
  }
}

int G[MAXN], B[MAXN], W[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n, m, u, v;
  cin >> n;
  for (u = 0; u < n; ++u) {
    cin >> G[u];
    C[u] = u;
  }
  cin >> m;
  while (m--) {
    cin >> u >> v;
    adj[--u].push_back(--v);
  }
  for (u = 0; u < n; ++u)
    if (not visit[u])
      tarjan(u);
  memset(B, -1, sizeof B);
  for (u = 0; u < n; ++u)
    if (B[C[u]] == -1 or B[C[u]] > G[u])
      B[C[u]] =  G[u];
  for (u = 0; u < n; ++u)
    if (B[C[u]] == G[u])
      W[C[u]]++;
  ll sum = 0, ways = 1;
  for (u = 0; u < n; ++u) 
    if (u == C[u]) {
      sum += B[C[u]];
      ways = (ways * W[C[u]]) % MOD;
    }
  cout << sum << " " << ways << endl;
}

