#include <iostream>
#include <algorithm>
#define MAXN 500005

char s[MAXN];
int dp1[2], dp2[2];

using namespace std;

int main() {
  ios::sync_with_stdio(0);
  int n, k;
  cin >> n >> k;
  cin >> s;
  int ans;
  if (k == 2) {
    for (int j = 0; j < 2; j++)
      dp1[j] = 'A' + (1-j) != s[n-1];
    for (int i = n - 2; i >= 0; i--) {
      for (int j = 0; j < 2; j++)
        dp2[j] = ('A' + (1-j) != s[i]) + dp1[1-j];
      copy(dp2, dp2+2, dp1);
    }
    int id = 0, c = dp1[0] < dp1[1];
    ans = min(dp1[0], dp1[1]);
    while (id < n) {
      s[id] = 'A' + c;
      c = 1 - c;
      id ++;
    }
  } else {
    int i = 0, j;
    ans = 0;
    while (i < n) {
      j = i+1;
      while (j < n and s[j] == s[i])
        j ++;
      if (j - i > 1) {
        char c = 'A';
        while (c == s[j] or c == s[i])
          c++;
        for (int k = i+1; k < j; k += 2) {
          s[k] = c;
          ans ++;
        }
      }
      i = j;
    }
  }
  cout << ans << endl << s << endl;
}
