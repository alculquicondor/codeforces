M={'rock':0,'paper':1,'scissors':2}
a=M[raw_input()]
b=M[raw_input()]
c=M[raw_input()]

if (b+1)%3==a and (c+1)%3==a:
	print 'F'
	exit(0)
if (c+1)%3==b and (a+1)%3==b:
	print 'M'
	exit(0)

if (b+1)%3==c and (a+1)%3==c:
	print 'S'
	exit(0)

print '?'
