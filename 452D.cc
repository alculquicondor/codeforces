#include <iostream>
#include <queue>

using namespace std;

int N[3], T[3], P[4];
int main() {
  int k;
  cin >> k;
  for (int i = 0; i < 3; ++i)
    cin >> N[i];
  for (int i = 0; i < 3; ++i)
    cin >> T[i];
  queue<int> Q[3];
  P[0] = k;
  int t;
  for (t = 0; k; ++t) {
    for (int i = 0; i < 3; ++i)
      while (not Q[i].empty() and t - Q[i].front() == T[i]) {
        Q[i].pop();
        ++P[i+1];
      }
    k -= P[3];
    P[3] = 0;
    for (int i = 0; i < 3; ++i)
      while (P[i] and (int)Q[i].size() < N[i]) {
        --P[i];
        Q[i].push(t);
      }
    for (int i = 0; i < 3; ++i)
      if (Q[i].size())
        t = min(Q[i].front() + T[i] - 1, t);
  }
  cout << t-1 << endl;
}
