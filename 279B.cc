#include <iostream>
#include <algorithm>

using namespace std;

int A[100002];

int main() {
  ios::sync_with_stdio(0);
  int n, t;
  cin >> n >> t;
  for (int i = 0; i < n; i++)
    cin >> A[i];
  for (int i = 1; i < n; i++)
    A[i] += A[i-1];
  int ans = 0;
  for (int i = 0; i < n; i++)
    ans = max(ans, int(upper_bound(A+i, A+n, (i?A[i-1]:0)+t)-(A+i)));
  cout << ans << endl;
}
