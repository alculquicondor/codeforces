#include <iostream>
#include <algorithm>
#define MAXN 200005

using namespace std;

int A[MAXN], B[MAXN], n, m, C[2*MAXN], t;

int main() {
  t = 0;
  ios::sync_with_stdio(0);
  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> A[i];
    C[t++] = A[i];
  }
  cin >> m;
  for (int i = 0; i < m; ++i) {
    cin >> B[i];
    C[t++] = B[i];
  }
  sort(A, A+n);
  sort(B, B+m);
  sort(C, C+t);
  t = unique(C, C+t) - C;
  C[t] = C[t-1] + 1;
  ++t;
  int mdif = -1e9, ma, mb, p1 = 0, p2 = 0;
  for (int i = 0; i < t; ++i) {
    while (p1 < n and A[p1] < C[i])
      ++p1;
    while (p2 < m and B[p2] < C[i])
      ++p2;
    int a = 2 * p1 + 3 * (n-p1),
        b = 2 * p2 + 3 * (m-p2),
        dif = a - b;
    if (dif > mdif) {
      mdif = dif;
      ma = a;
      mb = b;
    }
  }
  cout << ma << ':' << mb << endl;
  return 0;
}
