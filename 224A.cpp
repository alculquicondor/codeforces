#include <cstdio>
#include <algorithm>
#include <cmath>
using namespace std;

int main() {
    int x, y, z, a, b, c, ans;
    scanf("%d %d %d", &x, &y, &z);
    int g = __gcd(x, y);
    for(int i = 1; i<=sqrt(g); i++)
        if(g%i==0) {
            a = i;
            b = x/a;
            c = y/a;
            if(b*c==z) {
                ans = a+b+c;
                break;
            }
            a = g/i;
            b = x/a;
            c = y/a;
            if(b*c==z) {
                ans = a+b+c;
                break;
            }
        }
    printf("%d\n", ans*4);
}
