#include <iostream>
#include <algorithm>
#include <set>
#define MAXN 100002

using namespace std;
typedef pair<int, int> pii;

int A[MAXN];
pii Q[MAXN];
int ans[MAXN];

int main() {
  int n, m, x;
  cin >> n >> m;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  for (int i = 0; i < m; ++i) {
    cin >> x;
    Q[i] = pii(--x, i);
  }
  sort(Q, Q+m, greater<pii>());
  int id = n;
  set<int> S;
  for (int i = 0; i < m; ++i) {
    while (id > Q[i].first)
      S.insert(A[--id]);
    ans[Q[i].second] = S.size();
  }
  for (int i = 0; i < m; ++i)
    cout << ans[i] << endl;
}
