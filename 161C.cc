#include <iostream>

typedef long long ll;
struct pt {
  ll l, r;
};

using namespace std;

ll ans = 0;

void calc(const pt &a, const pt &b, int k) {
  if (k == -1)
    return;
  ll pos = 1LL << (k+1);
  pt A[] = {a, b};
  ll mtam = 1LL << 60;
  for (int i = 0; i < 2; ++i) {
    mtam = min(mtam, A[i].r - A[i].l + 1);
    if (A[i].l > pos) {
      A[i].l -= pos;
      A[i].r -= pos;
    }
  }
  if (mtam <= ans)
    return;
  pos >>= 1;
  bool c[2];
  for (int i = 0; i < 2; ++i)
    c[i] = pos >= A[i].l and pos <= A[i].r;
  ans = max(ans, min(A[0].r, A[1].r) - max(A[0].l, A[1].l) + 1);
  if (c[0] and c[1]) {
    for (int i = 0; i < 2; ++i) {
      if (pos - A[i].l >= A[i].r - pos)
        A[i].r = pos - 1;
      else
        A[i].l = pos + 1;
    }
    for (int i = 0; i < 2; ++i)
      if (A[i].l > pos) {
        A[i].l -= pos;
        A[i].r -= pos;
      }
    calc(A[0], A[1], k-1);
    return;
  }
  for (int i = 0; i < 2; ++i)
    if (c[i]) {
      if (A[1-i].l > pos) {
        A[1-i].l -= pos;
        A[1-i].r -= pos;
      }
      calc({A[i].l, pos-1}, A[1-i], k-1);
      calc({pos+1, A[i].r}, A[1-i], k-1);
      return;
    }
  calc(A[0], A[1], k-1);
}

int main() {
  pt A[2];
  for (int i = 0; i < 2; ++i)
    cin >> A[i].l >> A[i].r;
  calc(A[0], A[1], 30);
  /*
     ll ans = 0, pos;
     bool c[2];
     for (int k = 30; k >= 0; --k) {
     pos = 1 << k;
     for (int i = 0; i < 2; ++i)
     c[i] = pos >= A[i].l and pos <= A[i].r;
     if (c[0] and c[1])
     ans = max(ans, min(A[0].r, A[1].r) - max(A[0].l, A[1].l) + 1);
     for (int i = 0; i < 2; ++i)
     if (c[i]) {
     if (pos - A[i].l >= A[i].r - pos)
     A[i].r = pos - 1;
     else
     A[i].l = pos + 1;
     }
     for (int i = 0; i < 2; ++i)
     if (A[i].l > pos) {
     A[i].l -= pos;
     A[i].r -= pos;
     }
     }
     */
  cout << ans << endl;
}
