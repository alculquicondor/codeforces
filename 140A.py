from math import asin,pi

n,R,r = [int(x) for x in raw_input().split()]

if R < r:
    print 'NO'
    exit(0)
if R-r < r:
    if n == 1:
        print 'YES'
    else:
        print 'NO'
    exit(0)

ang = 2 * asin(float(r)/(R-r))
if n*ang <= 2*pi or abs(n*ang-2*pi) <= 1e-9:
    print 'YES'
else:
    print 'NO'
