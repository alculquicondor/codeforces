#include <iostream>

using namespace std;

int main() {
  ios::sync_with_stdio(0);
  int n, m;
  cin >> n >> m;
  int r = n / m, x = m * (r+1) - n;
  cout << r;
  for (int i = 1; i < x; i++)
    cout << " " << r;
  for (int i = x; i < m; i++)
    cout << " " << (r+1);
  cout << endl;
}
