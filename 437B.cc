#include <iostream>
#define MAXN 100001

using namespace std;
int A[MAXN], sz;

int main() {
  int s, l;
  cin >> s >> l;
  for (int i = l; i and s; --i)
    if (s >= (i & -i)) {
      A[sz++] = i;
      s -= i & -i;
    }
  if (s) {
    cout << -1;
  } else {
    cout << sz << endl;
    cout << A[0];
    for (int i = 1; i < sz; ++i)
      cout << " " << A[i];
  }
  cout << endl;
}
