#include <iostream>

using namespace std;

string s;

bool valid(int i) {
  return (i < 1 or s[i] != s[i-1]) and (i < 2 or s[i] != s[i-2]);
}

int main() {
  int n, p;
  cin >> n >> p;
  cin >> s;
  for (int i = n - 1; i >= 0; --i) {
    ++s[i];
    while (s[i] - 'a' < p) {
      if (valid(i)) {
        //cerr << i << ' ' << s[i] << endl;
        bool got = true;
        for (int j = i + 1; got and j < n; ++j) {
          s[j] = 'a';
          while (s[j] - 'a' < p and not valid(j))
            ++s[j];
          got &= s[j] - 'a' < p;
        }
        if (got) {
          cout << s << endl;
          return 0;
        }
      }
      ++s[i];
    }
  }
  cout << "NO" << endl;
  return 0;
}
