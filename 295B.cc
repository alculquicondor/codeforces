#include <iostream>
#include <cstring>
#define MAXN 500

using namespace std;

typedef long long ll;

bool act[MAXN];
ll W[MAXN][MAXN];
ll ans[MAXN];
int A[MAXN], n;

void activate(int id) {
  act[id] = true;
  for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j)
          W[i][j] = min(W[i][j], W[i][id]+W[id][j]);
}

ll sum() {
  ll ac = 0;
  for (int i = 0; i < n; ++i)
    if (act[i])
      for (int j = 0; j < n; ++j)
        if (act[j])
          ac += W[i][j];
  return ac;
}

int main() {
  ios::sync_with_stdio(0);
  cin >> n;
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      cin >> W[i][j];
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  for (int i = n-1; i >= 0; --i) {
    activate(A[i]-1);
    ans[i] = sum();
  }
  cout << ans[0];
  for (int i = 1; i < n; ++i)
    cout << " " << ans[i];
  cout << endl;
}

