#include <bits/stl_algobase.h>
#include <cstring>
#include <iostream>

using namespace std;

#define MAXN 300

typedef pair<int, int> pii;

const int INF = 1 << 30;
int N;
int cost[MAXN][MAXN], cap[MAXN][MAXN], flow[MAXN][MAXN];
int d[MAXN], pi[MAXN], visit[MAXN];

void initGraph(int n) {
  N = n;
  memset(cap, 0, sizeof cap);
  memset(cost, 0, sizeof cost);
  memset(flow, 0, sizeof flow);
}

void addEdge(int u, int v, int c = 1, int w = 0) {
  cap[u][v] = c;
  cost[u][v] = w;
}

int getCap(int u, int v) {
  if (flow[v][u])
    return flow[v][u];
  return cap[u][v] - flow[u][v];
}

int getCost(int u, int v) {
  if (flow[v][u])
    return -cost[v][u];
  return cost[u][v];
}

int getmin() {
  int ans = -1;
  for (int i = 0; i < N; i++)
    if (not visit[i] and (ans == -1 or d[ans] > d[i]))
      ans = i;
  return ans;
}

bool dijkstra(int s, int t) {
  memset(pi, -1, sizeof pi);
  memset(visit, false, sizeof visit);
  for (int i = 0; i < N; i++)
    d[i] = INF;
  d[s] = 0;
  int u;
  while ((u=getmin()) != -1) {
    visit[u] = true;
    for (int v = 0; v < N; v++)
      if (getCap(u, v) and d[v] > d[u] + getCost(u, v)) {
        visit[v] = false;
        d[v] = d[u] + getCost(u, v); 
        pi[v] = u;
      }
  }
  return d[t] < INF;
}

int mincap(int s, int u) {
  int ans = INF;
  while (u != s) {
    ans = min(ans, getCap(pi[u], u));
    u = pi[u];
  }
  return ans;
}

int addFlow(int u, int v, int f) {
  if (flow[v][u]) {
    flow[v][u] -= f;
    return -f * cost[v][u];
  }
  flow[u][v] += f;
  return f * cost[u][v];
}

int updateflow(int s, int u, int cf) {
  int tcost = 0;
  while (u != s) {
    tcost += addFlow(pi[u], u, cf);
    u = pi[u];
  }
  return tcost;
}

pii mincostmaxflow(int s = 0, int t = N-1) {
  int maxflow = 0, totalcost = 0;
  while (dijkstra(s, t)) {
    int cf = mincap(s, t);
    maxflow += cf;
    totalcost += updateflow(s, t, cf);
  }
  return pii(totalcost, maxflow);
}

int cant[26];

int main() {
  ios::sync_with_stdio(0);
  string s;
  int n, q, t;
  cin >> s >> n;
  t = s.size();
  initGraph(28+n);
  memset(cant, 0, sizeof cant);
  for (int i = 0; i < t; i++)
    cant[s[i]-'a']++;
  for (int i = 0; i < 26; i++)
    if (cant[i])
      addEdge(0, i+1, cant[i]);
  for (int i = 0; i < n; i++) {
    memset(cant, 0, sizeof cant);
    cin >> s >> q;
    for (int j = 0; j < s.size(); j++)
      cant[s[j]-'a'] ++;
    for (int j = 0; j < 26; j++)
      if (cant[j])
        addEdge(j+1, 27+i, cant[j]);
    addEdge(27+i, N-1, q, i+1);
  }
  pii p = mincostmaxflow();
  if (p.second == t)
    cout << p.first;
  else
    cout << -1;
  cout << endl;
}
