n = int(raw_input())
A = [ord(x)-ord('0') for x in raw_input()]
B = A[n:]
A = A[:n]
A.sort()
B.sort()
isun = True
for i in range(n):
    if A[i]>=B[i]:
        isun = False
        break
if isun:
    print 'YES'
    exit(0)

isun = True
for i in range(n):
    if A[i]<=B[i]:
        isun = False
        break
if isun:
    print 'YES'
else:
    print 'NO'
