#include <iostream>
#define MAXN 101

using namespace std;

int A[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  for (int i = 0; i < n; i++)
    cin >> A[i];
  for (int i = 1; i < n; i++)
    A[i] += A[i-1];
  int ans = 0;
  for (int i = 0; i < n; i++)
    for (int j = i; j < n; j++)
      ans = max(ans,
          A[n-1] + (j-i+1) - 2 * (A[j] - (i?A[i-1]:0)));
  cout << ans << endl;
}
