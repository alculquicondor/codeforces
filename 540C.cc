#include <iostream>
#include <queue>
#define MAXN 505

using namespace std;

char T[MAXN][MAXN];
bool visit[MAXN][MAXN];

typedef pair<int, int> pii;
queue<pii> Q;

int dx[] = {-1, 1, 0, 0},
    dy[] = {0, 0, -1, 1};
int n, m;

int valid(int x, int y) {
  return x >= 0 and x < n and y >= 0 and y < m;
}

int main() {
  cin >> n >> m;
  for (int i = 0; i < n; ++i)
    cin >> T[i];
  int r1, c1, r2, c2;
  cin >> r1 >> c1 >> r2 >> c2;
  --r1; --c1; --r2; --c2;
  T[r1][c1] = '.';
  Q.push(pii(r1, c1));
  bool could = false;
  int x, y, xx, yy;
  if (r1 == r2 and c1 == c2) {
    int q = 0;
    for (int i = 0; i < 4; ++i) {
      xx = r1 + dx[i];
      yy = c1 + dy[i];
      if (valid(xx, yy) and T[xx][yy] == '.')
        ++q;
    }
    could = q >= 1;
  } else {
    T[r1][c1] = '.';
    Q.push(pii(r1, c1));
    visit[r1][c1] = true;
    while (not Q.empty()) {
      x = Q.front().first;
      y = Q.front().second;
      Q.pop();
      if (x == r2 and y == c2) {
        if (T[x][y] == 'X') {
          could = true;
        } else {
          int q = 0;
          for (int i = 0; i < 4; ++i) {
            xx = x + dx[i];
            yy = y + dy[i];
            if (valid(xx, yy) and T[xx][yy] == '.')
              ++q;
          }
          could = q >= 2;
        }
        break;
      }
      if (T[x][y] == '.') {
        for (int i = 0; i < 4; ++i) {
          xx = x + dx[i];
          yy = y + dy[i];
          if (valid(xx, yy) and not visit[xx][yy]) {
            visit[xx][yy] = true;
            Q.push(pii(xx, yy));
          }
        }
      }
    }
  }
  cout << (could ? "YES" : "NO") << endl;
  return 0;
}

