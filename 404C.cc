#include <iostream>
#include <vector>
#define MAXN 100001

using namespace std;

vector<int> L[MAXN];

inline void edge(int u, int v) {
  cout << u + 1 << " " << v + 1 << endl;
}

int main() {
  ios::sync_with_stdio(0);
  int n, k, x;
  cin >> n >> k;
  for (int i = 0; i < n; ++i) {
    cin >> x;
    L[x].push_back(i);
  }
  bool valid = L[0].size() == 1;
  for (int i = 1; valid and i < n; ++i)
    valid &= L[i].size() <= L[i-1].size() * (i-1 ? k-1LL : k);
  if (valid) {
    cout << n - 1 << endl;
    for (int i = 1; i < n; ++i) {
      int id = 0, c = 0;
      for (int j = 0; j < L[i].size(); ++j) {
        edge(L[i-1][id], L[i][j]);
        if (++c == (i-1 ? k-1 : k)) {
          c = 0;
          ++id;
        }
      }
    }
  } else {
    cout << -1 << endl;
  }
}

