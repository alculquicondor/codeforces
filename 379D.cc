#include <iostream>
#define TEST s2 = construct(f2, l2, j, m); if (s2.size() and go(f1, l1, f2, l2, i, j)) goto done;

using namespace std;

string construct(char first, char last, int rep, int n) {
  if (n == 1 and first != last)
    return "";
  if (2*rep+2 <= n) {
    if (n == 2 and rep == 0 and first == 'A' and last == 'C')
      return "";
    string s(n, 'X');
    s[0] = first;
    s[n-1] = last;
    for (int i = 0; i < rep; ++i) {
      s[1+2*i] = 'A';
      s[2+2*i] = 'C';
    }
    return s;
  } else if (2 * rep == n-1) {
    if (first == 'A') {
      string s(n, 'A');
      for (int i = 1; i < n; i += 2)
        s[i] = 'C';
      s[n-1] = last;
      return s;
    }
    if (last == 'C') {
      string s(n, 'C');
      for (int i = 1; i < n; i += 2)
        s[i] = 'A';
      s[0] = first;
      return s;
    }
    if (n == 1 and first == 'X')
      return "X";
  } else if (2 * rep == n) {
    if (first == 'A' and last == 'C') {
      string s(n, 'A');
      for (int i = 1; i < n; i += 2)
        s[i] = 'C';
      return s;
    }
  }
  return "";
}

typedef long long ll;
struct T {
  char first, last;
  ll n;
  T(char first, char last, ll n) : first(first), last(last), n(n) {}
  T() {}
};

#define MAXN 51
T F[MAXN];

int k, x, n, m;
bool go(char f1, char l1, char f2, char l2, int i, int j) {
  F[0] = T(f1, l1, i);
  F[1] = T(f2, l2, j);
  for (int i = 2; i < k; ++i)
    F[i] = T(F[i-2].first, F[i-1].last, F[i-1].n + F[i-2].n +
        (F[i-2].last == 'A' and F[i-1].first == 'C'));
  return F[k-1].n == x;
}

int main() {
  cin >> k >> x >> n >> m;
  string s1, s2;
  char f1, l1, f2, l2;
  for (int i = 0; 2 * i <= n; ++i) {
    f1 = 'X'; l1 = 'X';
    s1 = construct(f1, l1, i, n);
    if (s1.size())
      for (int j = 0; 2 * j <= m; ++j) {
        f2 = 'X'; l2 = 'X';
        TEST
        f2 = 'A'; l2 = 'A';
        TEST
        l2 = 'C';
        TEST
        f2 = 'C';
        TEST
        l2 = 'A';
        TEST
      }
    f1 = 'A'; l1 = 'A';
    s1 = construct(f1, l1, i, n);
    if (s1.size())
      for (int j = 0; 2 * j <= m; ++j) {
        f2 = 'X'; l2 = 'X';
        TEST
        f2 = 'A'; l2 = 'A';
        TEST
        l2 = 'C';
        TEST
        f2 = 'C';
        TEST
        l2 = 'A';
        TEST
      }
    l1 = 'C';
    s1 = construct(f1, l1, i, n);
    if (s1.size())
      for (int j = 0; 2 * j <= m; ++j) {
        f2 = 'X'; l2 = 'X';
        TEST
        f2 = 'A'; l2 = 'A';
        TEST
        l2 = 'C';
        TEST
        f2 = 'C';
        TEST
        l2 = 'A';
        TEST
      }
    f1 = 'C';
    s1 = construct(f1, l1, i, n);
    if (s1.size())
      for (int j = 0; 2 * j <= m; ++j) {
        f2 = 'X'; l2 = 'X';
        TEST
        f2 = 'A'; l2 = 'A';
        TEST
        l2 = 'C';
        TEST
        f2 = 'C';
        TEST
        l2 = 'A';
        TEST
      }
    l1 = 'A';
    s1 = construct(f1, l1, i, n);
    if (s1.size())
      for (int j = 0; 2 * j <= m; ++j) {
        f2 = 'X'; l2 = 'X';
        TEST
        f2 = 'A'; l2 = 'A';
        TEST
        l2 = 'C';
        TEST
        f2 = 'C';
        TEST
        l2 = 'A';
        TEST
      }
  }
  cout << "Happy new year!" << endl;
  return 0;
done:
  cout << s1 << endl << s2 << endl;
}
