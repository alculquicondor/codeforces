#include <iostream>
#include <cstring>
#define MAXN 2003
#define MOD 1000000007

using namespace std;

int M[MAXN][MAXN], n;

int DP(int k, int p) {
  //cerr << k << " " << p << endl;
  int &ans = M[k][p];
  if (ans >= 0)
    return ans;
  ans = 0;
  for (int i = p; i <= n; i += p) {
    ans += DP(k-1, i);
    if (ans >= MOD)
      ans -= MOD;
  }
  return ans;
}

int main() {
  int k;
  cin >> n >> k;
  memset(M, -1, sizeof M);
  for (int p = 1; p <= n; ++p)
    M[0][p] = 1;
  cout << DP(k, 1) << endl;
}
