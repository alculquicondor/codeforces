n = int(raw_input())
A = [int(x) for x in raw_input().split()]
A.sort()
i = 1
c = 0
for x in A:
    if i > n :
        break
    if i == x:
        i += 1
        c += 1
    elif i < x:
        if x > n :
            break
        i = x+1
        c += 1
print n-c
