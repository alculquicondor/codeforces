#include<cstdio>
#include<queue>

using namespace std;

char B[11][11][11];
bool visit[11][11][11];
int h,n,m;

struct tri
{
	int x,y,z;
	tri(int xx,int yy,int zz)
	{
		x=xx;
		y=yy;
		z=zz;
	}
	tri(){}
};

bool valid(tri t)
{
	int x=t.x,y=t.y,z=t.z;
	return z>=0 and z<h and x>=0 and x<n and y>=0 and y<m;
}

int dx[]={-1,0,0,1},dy[]={0,-1,1,0};

int bfs(int x,int y)
{
	for(int i=0;i<h;i++)
		for(int j=0;j<n;j++)
			for(int k=0;k<m;k++)
				visit[i][j][k]=0;
	visit[0][x][y]=1;
	queue<tri> Q;
	Q.push(tri(x,y,0));
	int xx,yy,zz;
	tri act,t;
	int rpta=0;
	while(!Q.empty())
	{
		act=Q.front();
	//	printf(">%d %d %d\n",act.z,act.x,act.y);
		rpta++;
		Q.pop();
		for(int k=0;k<4;k++)
		{
			t=tri(act.x+dx[k],act.y+dy[k],act.z);
			if(valid(t) and B[t.z][t.x][t.y]=='.' and !visit[t.z][t.x][t.y])
			{
				Q.push(t);
				visit[t.z][t.x][t.y]=1;
			}
		}
		t=act;
		t.z+=1;
		if(valid(t) and B[t.z][t.x][t.y]=='.' and !visit[t.z][t.x][t.y])
		{
			Q.push(t);
			visit[t.z][t.x][t.y]=1;
		}
		t.z-=2;
		if(valid(t) and B[t.z][t.x][t.y]=='.' and !visit[t.z][t.x][t.y])
		{
			Q.push(t);
			visit[t.z][t.x][t.y]=1;
		}
	}
	return rpta;
}

int main()
{
	scanf("%d %d %d",&h,&n,&m);
	for(int i=0;i<h;i++)
		for(int j=0;j<n;j++)
			scanf("%s",B[i][j]);
	int x,y;
	scanf("%d %d",&x,&y);
	printf("%d\n",bfs(x-1,y-1));
}
