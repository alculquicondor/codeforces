#include <iostream>
#include <cstring>
#define MAXN 101

using namespace std;
int D[MAXN][MAXN];
bool B[MAXN];
int A[MAXN], M[MAXN];

int main() {
  std::ios::sync_with_stdio(false);
  int n, m, k;
  cin >> n >> m >> k;
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j)
      cin >> D[j][i];
  for (int i = 0; i < m; ++i) {
    memset(M, 0, sizeof M);
    for (int j = 0; j < n; ++j)
      if (not A[j])
        ++M[D[i][j]];
    for (int j = 1; j <= k; ++j)
      if (M[j] > 1)
        B[j] = true;
    for (int j = 0; j < n; ++j)
      if (B[D[i][j]] and not A[j])
        A[j] = i + 1;
  }
  for (int i = 0; i < n; ++i)
    cout << A[i] << endl;
}
