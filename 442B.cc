#include <iostream>
#include <iomanip>
#include <algorithm>
#define MAXN 105

using namespace std;

double P[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> P[i];
  sort(P, P+n, greater<double>());
  double ans = P[0], neg = 1 - P[0], tmp;
  for (int i = 1; i < n; ++i) {
    tmp = ans * (1-P[i]) + P[i] * neg;
    if (tmp > ans) {
      ans = tmp;
      neg *= (1-P[i]);
    }
  }
  cout << fixed << setprecision(12) << ans << endl;
}
