#include <iostream>
#include <vector>
#include <algorithm>
#include <cstring>
#include <queue>
#define MAXN 100005

using namespace std;
typedef long long ll;
typedef pair<ll, int> pii;
const int inf = 0x7f7f7f7f;

vector<pii> adj[MAXN];
ll dist[MAXN];
int n;
bool visit[MAXN], tr_used[MAXN];

void dijkstra() {
  priority_queue<pii> Q;
  dist[1] = 0;
  int u, v;
  for (u = 1; u <= n; ++u)
    if (dist[u] != inf)
      Q.push({-dist[u], u});
  while (not Q.empty()) {
    u = Q.top().second;
    Q.pop();
    if (visit[u])
      continue;
    //cerr << u << ' ' << dist[u] << endl;
    visit[u] = true;
    for (pii &t : adj[u]) {
      v = t.second;
      //cerr << ">> " << v << endl;
      if (dist[v] >= dist[u] + t.first) {
        tr_used[v] = false;
        dist[v] = dist[u] + t.first;
        Q.push({-dist[v], v});
      }
    }
  }
}

int main() {
  std::ios::sync_with_stdio(false);
  int m, k;
  cin >> n >> m >> k;
  int u, v;
  ll w;
  while (m--) {
    cin >> u >> v >> w;
    adj[u].push_back({w, v});
    adj[v].push_back({w, u});
  }
  memset(dist, 0x7f, sizeof dist);
  for (int i = 0; i < k; ++i) {
    cin >> u >> w;
    dist[u] = min(dist[u], w);
    tr_used[u] = true;
  }
  dijkstra();
  for (u = 2; u <= n; ++u)
    if (tr_used[u])
      --k;
  cout << k << endl;
}

