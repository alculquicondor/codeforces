s=raw_input().split()
n=int(s[0])
m=int(s[1])
k=int(s[2].split('.')[1])

M=dict()

for i in range(n):
	s=raw_input().split()
	aug=int(s[1])*k/100
	if(aug>=100):
		M[s[0]]=aug

for i in range(m):
	s=raw_input()
	if s not in M:
		M[s]=0

print len(M)

for x in sorted(M):
	print x,M[x]
