#include <iostream>

using namespace std;

int A[100];

int main() {
  int n, x, y;
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  cin >> x >> y;
  for (int i = 1; i < n; ++i)
    A[i] += A[i-1];
  for (register int i = 0; i < n and A[i] <= y; ++i) {
    int t2 = A[n-1]-A[i];
    if (A[i] >= x and t2 >= x and t2 <= y) {
      cout << i + 2 << endl;
      return 0;
    }
  }
  cout << 0 << endl;
}

