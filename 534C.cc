#include <iostream>
#define MAXN 200005

using namespace std;

typedef long long ll;

int n;
ll sum, A;
int d[MAXN];

int solve(int i) {
  int x = max(0LL, A - (sum - d[i]) - 1);
  int y = min((ll)d[i], A - (n - 1));
  return x + d[i] - y;
}

int main() {
  cin >> n >> A;
  for (int i = 0; i < n; ++i) {
    cin >> d[i];
    sum += d[i];
  }
  cout << solve(0);
  for (int i = 1; i < n; ++i)
    cout << ' ' << solve(i);
  cout << endl;
  return 0;
}

