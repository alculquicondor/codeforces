#include <cstdio>
#include <vector>
#include <algorithm>
#include <cmath>
#include <set>
#include <iostream>
#define MAXN 3001

using namespace std;

typedef float type;

template<typename T>
struct p2d {
  T x, y;
  inline p2d(T x = 0, T y = 0) : x(x), y(y) {}
  inline p2d(const p2d &a) : x(a.x), y(a.y) {}
  inline p2d operator - (const p2d &a) const {
    return p2d(x-a.x, y-a.y);
  }
  inline p2d operator + (const p2d &a) const {
    return p2d(x+a.x, y+a.y);
  }
  inline T norm2() const {
    return x * x + y * y;
  }
};
typedef p2d<int> pt;
const type eps = 1e-10, pi = atan2(0, -1), sixty = pi/3, pi2 = 2*pi;
pt A[MAXN];
int sz, n, q;

typedef pair<double, int> pdi;
bool order(const pdi &i, const pdi &j) {
  return i.first < j.first;
}
pdi D[MAXN*2];

int ans;

inline void find(int idx) {
  sort(D, D+sz, order);
  multiset<int> S;
  multiset<int>::iterator it;
  type x1, x2;
  int p1 = 1, p2 = 1;
  for (int i = 0; i < q; ++i) {
    x1 = D[i].first + sixty, x2 = x1 + 4*sixty;
    while (p2 < sz and D[p2].first <= x2+eps)
      S.insert(D[p2++].second);
    while (p1 < p2 and D[p1].first < x1-eps) {
      it = S.find(D[p1++].second);
      S.erase(it);
    }
    if (not S.empty()) {
      it = S.end();
      ans = max(ans, min(D[i].second, *--it));
    }
  }
}

void solve() {
  type ang;
  int ds2;
  for (int i = 0; i < n; ++i) {
    sz = q = 0;
    for (int j = 0; j < n; ++j)
      if (i != j) {
        pt d = A[j] - A[i];
        ang = atan2((type)d.y, (type)d.x);
        ds2 = d.norm2();
        if (ds2 > ans) {
          D[sz++] = pdi(ang, ds2);
          D[sz++] = pdi(ang+pi2+eps, ds2);
          ++q;
        }
      }
    find(i);
  }
}

int main() {
  scanf("%d", &n);
  for (int i = 0; i < n; ++i)
    scanf("%d %d", &A[i].x, &A[i].y);
  ans = 0;
  solve();
  printf("%.7f\n", sqrt(ans) / 2);
}

