#include <iostream>
#include <cstdlib>
#include <vector>
#define MAXN 100005
const int INF = 1 << 30;

using namespace std;

vector<int> adj[MAXN];
bool C[MAXN];
bool M[MAXN];

int dfs(int u, bool impar, bool c1, bool c2, int p = -1) {
  int ans = 0, v;
  M[u] = 0;
  bool ch[] = {c1, c2};
  if (C[u] ^ ch[impar]) {
    ans += 1;
    ch[impar] = not ch[impar];
    M[u] = 1;
  }
  for (int i = 0; i < adj[u].size(); ++i) {
    v = adj[u][i];
    if (v != p)
      ans += dfs(v, not impar, ch[0], ch[1], u);
  }
  return ans;
}

int main(void) {
  ios::sync_with_stdio(0);
  int n, u, v;
  cin >> n;
  for (int i = 1; i < n; ++i) {
    cin >> u >> v;
    --u, --v;
    adj[u].push_back(v);
    adj[v].push_back(u);
  }
  for (int i = 0; i < n; ++i)
    cin >> C[i];
  for (int i = 0; i < n; ++i) {
    cin >> u;
    C[i] ^= u;
  }
  cout << dfs(0, 0, 0, 0) << endl;
  for (u = 0; u < n; ++u)
    if (M[u])
      cout << u + 1 << endl;
  return 0;
}

