#include <iostream>

using namespace std;

typedef long long ll;
typedef pair<int, string> pis;

ll go(ll n, ll m, int p) {
  ll ans = (n-p+1) * (n-p+1) + (p-1);
  if (m == p) {
    ans -= m;
  } else {
    ll a = m / (p+1), t = m % (p + 1);
    ans -= (m-t*(a+1)) * a + t * (a+1) * (a+1);
  }
  return ans;
}

string construct(ll n, ll m, int p) {
  string s;
  bool first = true;
  ll t;
  while (p) {
    t = m / (p+1);
    s += string(t, 'x');
    m -= t;
    if (first) {
      first = false;
      s += string(n - p + 1, 'o');
    } else {
      s.push_back('o');
    }
    --p;
  }
  s += string(m, 'x');
  return s;
}

int main() {
  ios::sync_with_stdio(0);
  ll n, m, ans, tmp;
  cin >> n >> m;
  string s;
  if (m <= 1 or n == 0) {
    ans = n * n - m * m;
    s = string(n, 'o') + string(m, 'x');
  } else if (n <= 1) {
    ans = go(n, m, 1);
    s = construct(n, m, 1);
  } else {
    ans = go(n, m, 1);
    int id = 1;
    ll tmp;
    for (int i = 2; i <= min(n, m); ++i) {
      tmp = go(n, m, i);
      if (tmp > ans) {
        ans = tmp;
        id = i;
      }
    }
    s = construct(n, m, id);
  }
  cout << ans << endl << s << endl;
}
