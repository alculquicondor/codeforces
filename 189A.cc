#include <iostream>

using namespace std;

int main() {
  int n, a;
  cin >> n;
  int DP[4010];
  for (int i = 0; i <= n; i++)
    DP[i] = -0xf777777;
  DP[0] = 0;
  for (int i = 0; i < 3; i++) {
    cin >> a;
    for (int i = 0; i <= n; i++)
      if (DP[i] >= 0 and i + a <= n)
        DP[i+a] = max (DP[i+a], 1 + DP[i]);
  }
  cout << DP[n] << endl;
}
