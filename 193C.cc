#include <iostream>
#include <cmath>
#include <cassert>
#include <vector>

using namespace std;

int C[7][6];
int D[6];

const int N = 6;
double A[N][N];
double B[N];
int P[N];
double bp[N];

bool meneito() {
  double detA = 1;
  for (int i = 0; i < N; ++i)
    P[i] = i;
	for(int k=0;k<N;k++) {
		double p=0;
		int nk=k;
		for(int i=k;i<N;i++)
			if(abs(A[i][k])>p) {
					p=abs(A[i][k]);
					nk=i;
			}
		if(p==0)
      return false;
    if(nk!=k) {
      detA*=-1;
      swap(P[k], P[nk]);
      for(int i=0;i<N;i++)
        swap(A[k][i], A[nk][i]);
    }
    for(int i=k+1;i<N;i++) {
      for(int j=k+1;j<N;j++)
        A[i][j]=A[i][j]-A[i][k]*A[k][j]/A[k][k];
      A[i][k]=A[i][k]/A[k][k];
    }
  }
  for(int i=0;i<N;i++) detA*=A[i][i]; // Determinante opcional
  for(int i=0;i<N;i++)
    bp[i]=B[P[i]];
  for(int i=0;i<N;i++) {
    double sus=0;
    for(int j=0;j<i;j++)
      sus+=A[i][j]*B[j];
    B[i]=bp[i]-sus;
  }
  for(int i=N-1;i>=0;i--) {
    double sus=0;
    for(int j=i+1;j<N;j++)
      sus+=A[i][j]*B[j];
    B[i]=(B[i]-sus)/A[i][i];
  }
  return true;
}

int main() {
  for (int mask = 1; mask < 8; ++mask) {
    int id = 0;
    for (int i = 0; i < 4; ++i)
      for (int j = i+1; j < 4; ++j) {
        C[mask-1][id] += (bool)(mask&(1<<i)) != (bool)(mask&(1<<j));
        ++id;
      }
  }
  int limit = 0;
  for (int i = 0; i < 6; ++i) {
    cin >> D[i];
    limit = max(limit, D[i]);
  }
  int mini = 1 << 30, id = -1;
  for (int k = 0; k <= limit; ++k) {
    for (int i = 0; i < 6; ++i)
      B[i] = D[i] - k * C[6][i];
    for (int i = 0; i < 6; ++i)
      for (int j = 0; j < 6; ++j)
        A[i][j] = C[j][i];
    if (meneito()) {
      bool valid = true;
      int sum = k;
      for (int i = 0; valid and i < N; ++i) {
        valid &= (int)B[i] == B[i] and B[i] >= 0;
        sum += B[i];
      }
      if (valid and sum < mini) {
        id = k;
        mini = sum;
      }
    }
  }
  if (id != -1) {
    vector<string> T(4, string(mini, 'b'));
    for (int i = 0; i < 6; ++i)
      B[i] = D[i] - id * C[6][i];
    for (int i = 0; i < 6; ++i)
      for (int j = 0; j < 6; ++j)
        A[i][j] = C[j][i];
    meneito();
    cout << mini << endl;
    /*
    for (int i = 0; i < N; ++i)
      cout << " " << B[i] << endl;
      */
    int pos = 0;
    for (int mask = 1; mask < 8; mask++) {
      for (int k = 0; k < B[mask-1]; ++k) {
        for (int i = 0; i < 4; ++i)
          if (mask & (1<<i))
            T[i][pos] = 'a';
        pos++;
      }
    }
    int mask = 7;
    for (int k = 0; k < id; ++k) {
        for (int i = 0; i < 4; ++i)
          if (mask & (1<<i))
            T[i][pos] = 'a';
        pos++;
    }
    for (int i = 0; i < 4; ++i)
      cout << T[i] << endl;
  } else {
    cout << -1 << endl;
  }
}
