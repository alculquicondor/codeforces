#include <vector>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <set>

using namespace std;

typedef long long type;

template<typename T>
struct p2d {
  T x, y;
  p2d(T x = 0, T y = 0) : x(x), y(y) {}
  p2d(const p2d &a) : x(a.x), y(a.y) {}
  p2d operator - (const p2d &a) const {
    return p2d(x-a.x, y-a.y);
  }
  p2d operator + (const p2d &a) const {
    return p2d(x+a.x, y+a.y);
  }
  p2d operator * (T k) const {
    return p2d(x*k, y*k);
  }
  p2d operator / (T k) const {
    return p2d(x/k, y/k);
  }
  void operator -= (const p2d &a) {
    x -= a.x, y -= a.y;
  }
  void operator += (const p2d &a) {
    x += a.x, y += a.y;
  }
  void operator *= (T k) {
    x *= k, y *= k;
  }
  void operator /= (T k) {
    x /= k, y /= k;
  }
  bool operator == (const p2d &a) const {
    return x == a.x and y == a.y;
  }
  bool operator != (const p2d &a) const {
    return x != a.x or y != a.y;
  }
  bool operator < (const p2d &a) const {
    if (x == a.x)
      return y < a.y;
    return x < a.x;
  }

  T norm2() const {
    return x * x + y * y;
  }
  T norm() const {
    return sqrt(norm2());
  }
  void reduce() {
    T g = __gcd(x, y);
    x /= g, y /= g;
  }
  p2d ort() const {
    return p2d(-y, x);
  }
  T operator % (const p2d &a) const {
    return x * a.y - y * a.x;
  }
  T operator * (const p2d &a) const {
    return x * a.x + y * a.y;
  }
};
typedef p2d<type> pt;

pt A[8];

bool isRect(const vector<int> &B) { 
  set<pt> S;
  pt P[4];
  for (int i = 0; i < 4; ++i) {
    P[i] = A[B[i]];
    S.insert(P[i]);
  }
  for (int i = 0; i < 4; ++i)
    for (int j = 0; j < 4; ++j)
      if (i != j)
        for (int k = 0; k < 4; ++k)
          if (k != i and k != j) {
            pt d1 = P[j] - P[i],
               d2 = P[k] - P[i];
            if (d1 * d2)
              continue;
            if (S.count(P[i]+d1+d2))
              return true;
          }
  return false;
}

bool isSquare(const vector<int> &B) { 
  set<pt> S;
  pt P[4];
  for (int i = 0; i < 4; ++i) {
    P[i] = A[B[i]];
    S.insert(P[i]);
  }
  for (int i = 0; i < 4; ++i)
    for (int j = 0; j < 4; ++j)
      if (i != j) {
        pt d1 = P[j] - P[i],
           d2 = d1.ort();
        if (S.count(P[i] + d2) and S.count(P[j] + d2))
          return true;
        d2 *= -1;
        if (S.count(P[i] + d2) and S.count(P[j] + d2))
          return true;
      }
  return false;
}

void show(const vector<int> &B) {
  cout << B[0] + 1;
  for (int i = 1; i < B.size(); ++i)
    cout << " " << B[i] + 1;
  cout << endl;
}

int main() {
  for (int i = 0; i < 8; ++i)
    cin >> A[i].x >> A[i].y;
  for (int mask = 15; mask < (1<<8); mask++)
    if (__builtin_popcount(mask) == 4) {
      vector<int> U, V;
      for (int i = 0; i < 8; ++i) {
        if (mask & (1<<i))
          U.push_back(i);
        else
          V.push_back(i);
      }
      if (isSquare(U) and isRect(V)) {
        cout << "YES" << endl;
        show(U);
        show(V);
        return 0;
      }
    }
  cout << "NO" << endl;
}
