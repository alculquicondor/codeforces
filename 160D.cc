#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;

#define MAXN 100005

int C[MAXN];
inline int findSet(int i) {
    return (C[i] == i) ? i : (C[i] = findSet(C[i]));
}
inline void unionSet(int i, int j) {
    C[findSet(i)] = findSet(j);
}

struct edge {
  int u, v, w, id;
  bool operator < (const edge &a) const {
    return w < a.w;
  }
} E[MAXN];

int ans[MAXN];
typedef pair<int, int> pii;
struct graph {
  int n, dfsnumbercounter;
  vector<int> dfs_low, dfs_num, dfs_parent;
  vector<vector<pii>> adj;

  graph(int n) : n(n), dfs_low(n), dfs_num(n, -1),
  dfs_parent(n, -1), adj(n) {
    dfsnumbercounter = 0;
  }
  void add_edge(int u, int v, int id) {
    adj[u].push_back({v, id});
    adj[v].push_back({u, id});
  }
  void dfs(int u) {
    //cout << "@ " << u << " " << dfs_parent[u] << endl;
    dfs_low[u] = dfs_num[u] = dfsnumbercounter++;
    int v, id;
    for (int i = 0; i < adj[u].size(); ++i) {
      v = adj[u][i].first;
      id = adj[u][i].second;
      if (dfs_num[v] == -1) {
        dfs_parent[v] = u;
        dfs(v);
        if (dfs_low[v] > dfs_num[u])
          ans[id] = 2;
        dfs_low[u] = min(dfs_low[u], dfs_low[v]);
      } else if (v != dfs_parent[u]) {
        dfs_low[u] = min(dfs_low[u], dfs_num[v]);
      }
    }
  }
  void detect_bridges() {
    for (int i = 0; i < n; ++i)
      if (dfs_num[i] == -1)
        dfs(i);
    for (auto &V : adj) {
      sort(V.begin(), V.end());
      for (int i = 1; i < V.size(); ++i)
        if (V[i].first == V[i-1].first) {
          ans[V[i].second] = 1;
          ans[V[i-1].second] = 1;
        }
    }
  }
};

void go(int ini, int fin) {
  vector<int> V;
  vector<edge> _E;
  V.reserve((fin-ini+1)*2);
  _E.reserve(fin-ini+1);
  int c1, c2;
  for (int i = ini; i < fin; ++i) {
    c1 = findSet(E[i].u);
    c2 = findSet(E[i].v);
    if (c1 != c2) {
      V.push_back(c1);
      V.push_back(c2);
      _E.push_back({c1, c2, E[i].w, E[i].id});
      ans[E[i].id] = 1;
    }
  }
  sort(V.begin(), V.end());
  V.resize(unique(V.begin(), V.end()) - V.begin());
  graph G(V.size());
  for (auto &e : _E) {
    G.add_edge(lower_bound(V.begin(), V.end(), e.u) - V.begin(),
        lower_bound(V.begin(), V.end(), e.v) - V.begin(), e.id);
  }
  G.detect_bridges();
  for (int i = ini; i < fin; ++i)
    unionSet(E[i].u, E[i].v);
}

int main() {
  ios::sync_with_stdio(0);
  int n, m;
  cin >> n >> m;
  for (int i = 0; i < n; ++i)
    C[i] = i;
  int u, v, w;
  for (int i = 0; i < m; ++i) {
    cin >> u >> v >> w;
    E[i] = {u-1, v-1, w, i};
  }
  sort(E, E+m);
  int i = 0, j;
  while (i < m) {
    j = i + 1;
    while (j < m and E[j].w == E[i].w)
      ++j;
    go(i, j);
    i = j;
  }
  string S[] = {"none", "at least one", "any"};
  for (int i = 0; i < m; ++i)
    cout << S[ans[i]] << endl;
}

