#include <iostream>
#include <algorithm>

using namespace std;

int A[101];

int main() {
  int n, d, m;
  cin >> n >> d;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  cin >> m;
  sort(A, A+n);
  int ans = 0;
  for (int i = 0; i < min(n, m); ++i)
    ans += A[i];
  if (m > n)
    ans -= (m-n)*d;
  cout << ans << endl;
}
