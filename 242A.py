x, y, a, b = [int(x) for x in raw_input().split()]
A = []
for i in range(a if a>b else b+1, x+1):
    for j in range(b, min(i, y+1)):
        A.append((i, j))
print len(A)
for x in A:
    print x[0], x[1]
