#include <cstdio>
#include <bits/stl_algobase.h>

using namespace std;

int main() {
  int t, a, b;
  double p;
  scanf("%d", &t);
  while (t--) {
    scanf("%d %d", &a, & b);
    if (b == 0) {
      p = 1;
    } else if (a == 0) {
      p = 0.5;
    } else {
      double q = min((double)b, a/4.);
      p = 0.5 + (a*q-2*q*q)/(2.*a*b);
    }
    printf("%.7f\n", p);
  }
}
