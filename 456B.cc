#include <iostream>
#include <sstream>
#include <cmath>

using namespace std;

int main() {
  string s;
  cin >> s;
  if (s.size() > 2)
    s = s.substr(s.size()-2);
  stringstream ss(s);
  int n, ans;
  ss >> n;
  n %= 4;
  ans = int(pow(1, n) + pow(2, n) + pow(3, n) + pow(4, n)) % 5;
  cout << ans << endl;
}
