n = int(input())

adj = [list() for x in  range(n)]
H = [0] * n
F = [0] * n
FoH = [list() for x in range(n)]
sz = 0
order = [0] * n
pi = [-1] * n

def dfs(u, p = -1):
  global pi, order, sz
  pi[u] = p
  for v in adj[u]:
    if v != p:
      dfs(v, u)
  order[sz] = u
  sz += 1


T1 = [0] * n
T2 = [0] * n
T3 = [0] * n

def solve(u, ac, p = -1):
  global T1, T2, T3, DP
  ans = -1
  if ac >= 2:
    ans = 3 if ac == 2 else 1
    for v in adj[u]:
      if v != p:
        ans *= DP[v][0]
    DP[u][ac] = ans
    return
  nc, sheet, t1, t4 = 0, 0, 1, 1
  for v in adj[u]:
    if v != p:
      T1[nc] = DP[v][0]
      t1 *= T1[nc]
      T2[nc] = DP[v][ac+1]
      if ac == 0:
        T3[nc] = DP[v][3]
      if len(adj[v]) == 1:
        sheet += 1
      else:
        t4 *= T1[nc]
      nc += 1
  ans = (ac+1+sheet) * t4
  if ac == 0:
    for i in range(nc):
      for j in range(i+1, nc):
        ans = max(ans, t1 // (T1[i]*T1[j]) * T3[i]*T3[j]*3)
  ans = max(ans, (ac+1) * t1)
  for i in range(nc):
    ans = max(ans, t1 // T1[i] * T2[i])
  DP[u][ac] = ans

for i in range(1, n):
  u, v = [int(x) for x in input().split()]
  u -= 1
  v -= 1
  adj[u].append(v)
  adj[v].append(u)

dfs(0)
for x in order:
  for j in range(4):
    solve(x, j, pi[x])
print(DP[0][0])
