#include<queue>
#include<algorithm>
#include<iostream>
#include<cstdio>
#include<vector>

using namespace std;

int dx[]={-1,1,0,0,-1,-1,1,1},dy[]={0,0,1,-1,1,-1,1,-1};

bool valid(int x,int y)
{
	return x>=0 and y>=0 and x<8 and y<8;
}

int main()
{
	int mv[8][8],d[8][8];
	char s[3];
	scanf("%s",s);
	int x,y,xf,yf;
	x=s[0]-'a';
	y=s[1]-'1';
	scanf("%s",s);
	xf=s[0]-'a';
	yf=s[1]-'1';
	for(int i=0;i<8;i++)
		for(int j=0;j<8;j++)
			mv[i][j]=-1;
	queue<pair<int,int> > Q;
	Q.push(make_pair(x,y));
	mv[x][y]=9;
	d[x][y]=0;
	while(!Q.empty())
	{
		x=Q.front().first;
		y=Q.front().second;
		Q.pop();
		if(x==xf and y==yf)
			break;
		int xx,yy;
		for(int k=0;k<8;k++)
		{
			xx=x+dx[k];
			yy=y+dy[k];
			if(valid(xx,yy) and mv[xx][yy]==-1)
			{
				mv[xx][yy]=k;
				d[xx][yy]=d[x][y]+1;
				Q.push(make_pair(xx,yy));
			}
		}
	}
	printf("%d\n",d[xf][yf]);
	x=xf;
	y=yf;
	vector<string> v;
	int m;
	while(mv[x][y]!=9)
	{
		m=mv[x][y];
		if(m==0)
		{
			x++;
			v.push_back("L");
		}
		if(m==1)
		{
			x--;
			v.push_back("R");
		}
		if(m==2)
		{
			y--;
			v.push_back("U");
		}
		if(m==3)
		{
			y++;
			v.push_back("D");
		}
		if(m==4)
		{
			y--;
			x++;
			v.push_back("LU");
		}
		if(m==5)
		{
			y++;
			x++;
			v.push_back("LD");
		}
		if(m==6)
		{
			x--;
			y--;
			v.push_back("RU");
		}
		if(m==7)
		{
			y++;
			x--;
			v.push_back("RD");
		}
	}
	reverse(v.begin(),v.end());
	for(int i=0;i<v.size();i++)
		cout<<v[i]<<endl;
}
