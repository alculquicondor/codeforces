n=raw_input()
mini=(1<<31)-1
digits=[0]*10
for x in n:
	digits[int(x)]+=1
for i in range(1,10):
	if digits[i]!=0:
		break
if digits[i]==0:
	mini='0'
else:
	mini=str(i)+'0'*digits[0]
	digits[i]-=1
	while i<10:
		mini+=str(i)*digits[i]
		i+=1
if mini==raw_input():
	print 'OK'
else:
	print 'WRONG_ANSWER'
