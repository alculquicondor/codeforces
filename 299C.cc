#include <iostream>

using namespace std;

int main() {
  int n;
  string a, b;
  int na = 0, nb = 0;
  cin >> n >> a >> b;
  int eq = 0, up = 0, un = 0;
  for (int i = 0; i < 2 * n; i++) {
    if (a[i] == '1' and b[i] == '1')
      eq ++;
    else if (a[i] == '1')
      up ++;
    else if (b[i] == '1')
      un ++;
  }
  bool jug = 0;
  while (eq + up + un) {
    if (jug) {
      if (eq)
        nb ++, eq --;
      else if (un)
        nb ++, un --;
      else
        up --;
    } else {
      if (eq)
        na ++, eq --;
      else if(up)
        na ++, up --;
      else
        un --;
    }
    jug = not jug;
  }
  if (na > nb)
    cout << "First";
  else if (na == nb)
    cout << "Draw";
  else
    cout << "Second";
  cout << endl;
}
