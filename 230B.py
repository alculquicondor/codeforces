from math import sqrt

isprime = []

def criba(n):
    global isprime
    isprime = [True]*n
    isprime[0] = False
    isprime[1] = False
    for i in range(2, int(sqrt(n))+1):
        if isprime[i]:
            for j in range(i*i, n, i):
                isprime[j] = False

def tprime(n):
    t = int(sqrt(n))
    return n == t*t and isprime[t]

criba(1000001)
n = input()
A = [int(x) for x in raw_input().split()]
for x in A:
    print "YES" if tprime(x) else "NO"
