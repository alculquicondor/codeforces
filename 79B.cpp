#include<vector>
#include<cstdio>
#include<algorithm>

using namespace std;

int main()
{
	int n,m,k,t;
	scanf("%d %d %d %d",&n,&m,&k,&t);
	vector<int> Q(n,m%3);
	vector<vector<int> > W(n);
	int f,c,r;
	for(int i=0;i<k;i++)
	{
		scanf("%d %d",&f,&c);
		f--,c--;
		W[f].push_back(c);
		Q[f]=(((Q[f]-1)%3)+3)%3;
	}
	for(auto &v:W)
		sort(v.begin(),v.end());
	for(int i=1;i<n;i++)
		Q[i]=(Q[i]+Q[i-1])%3;
	for(int i=0;i<t;i++)
	{
		bool vale=1;
		scanf("%d %d",&f,&c);
		f--,c--;
		if(f>0)
			r=Q[f-1];
		else
			r=0;
		r+=c;
		for(int x:W[f])
		{
			if(x<c)
				r--;
			else if(x==c)
			{
				vale=0;
				break;
			}
			else
				break;
		}
		if(vale)
		{
			r=((r%3)+3)%3;
			if(r==0)
				puts("Carrots");
			else if(r==1)
				puts("Kiwis");
			else
				puts("Grapes");
		}
		else
			puts("Waste");
	}
}
