#include<cstdio>
#include<cstring>
#include<vector>
#include<iostream>

using namespace std;

vector<bool> mv;
int n,M[201][51][51][2];

int dp(int t,int m,int k,int left)
{
	if(t==mv.size()-1)
		return 1<<30;
	if(M[t][m][k][left]!=-1)
		return M[t][m][k][left];
	int rpta=t+1;
	bool ll=left;
	if((k==0 and left) or (k==n-1 and not left))
		ll=not left;
	if(mv[t])
	{
		//parado
		for(int i=0;i<n;i++)
			if(k+(ll?-1:1)!=i)
				rpta=max(rpta,dp(t+1,i,k+(ll?-1:1),ll));
	}
	else
	{
		if(m!=0 and m-1!=k+(ll?-1:1) and m-1!=k)
			rpta=max(rpta,dp(t+1,m-1,k+(ll?-1:1),ll));
		if(m!=k+(ll?-1:1) and m!=k)
			rpta=max(rpta,dp(t+1,m,k+(ll?-1:1),ll));
		if(m!=n-1 and m+1!=k+(ll?-1:1) and m+1!=k)
			rpta=max(rpta,dp(t+1,m+1,k+(ll?-1:1),ll));
	}
	return M[t][m][k][left]=rpta;
}

int main()
{
	int m,k;
	scanf("%d%d%d",&n,&m,&k);
	string s;
	cin>>s>>s;
	bool left=(s=="head");
	cin>>s;
	for(char u:s)
		mv.push_back(u=='1');
	for(int i=0;i<201;i++)
		for(int j=0;j<51;j++)
			for(int k=0;k<51;k++)
				for(int r=0;r<2;r++)
					M[i][j][k][r]=-1;
	int rpta=dp(0,m-1,k-1,left);
	if(rpta>=1<<30)
		puts("Stowaway");
	else
		printf("Controller %d\n",rpta);
}
