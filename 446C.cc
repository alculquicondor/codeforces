#include <iostream>
#include <cstring>

using namespace std;

typedef long long ll;
const int MOD = 1000000009, MAXN = 300010;

inline ll san(ll x) {
  if (x >= MOD or x <= -MOD)
    x %= MOD;
  return x < 0 ? x + MOD : x;
}

struct Matrix {
  static const int N = 3;
  ll data[N][N];
  Matrix operator = (const Matrix &m) {
    memcpy(data, m.data, sizeof data);
  }
  Matrix operator* (const Matrix &m) const {
    Matrix r;
    for (int i = 0; i < N; ++i)
      for (int j = 0; j < N; ++j) {
        r.data[i][j] = 0;
        for (int k = 0; k < N; ++k)
          r.data[i][j] = san(r.data[i][j] + data[i][k] * m.data[k][j]);
      }
    return r;
  }
};
Matrix M[MAXN];
ll F[MAXN];

void pre() {
  Matrix m0, ac;
  ll data0[] = {1, 1, 0, 1, 0, 0, 1, 0, 1},
      data1[] = {1, 0, 0, 0, 1, 0, 0, 0, 1};
  memcpy(m0.data, data0, sizeof m0.data);
  memcpy(ac.data, data1, sizeof ac.data);
  F[0] = 0;
  for (int i = 0; i < MAXN; ++i) {
    F[i] = ac.data[1][0];
    M[i] = ac;
    ac = ac * m0;
  }
}

typedef pair<ll, ll> pll;

pll & operator += (pll &t, const pll &x) {
  t.first = san(t.first + x.first);
  t.second = san(t.second + x.second);
  return t;
}

const int MAXS = 1048577;
inline int center(int x, int y) {
  return (x + y) >> 1;
}
inline int left(int x) {
  return (x << 1) + 1;
}
int n;
ll st[MAXS];
pll add[MAXS];
inline void push(int idx, int l, int r, int L, int R) {
  int length = r - l + 1;
  if (add[idx].first or add[idx].second) {
    st[idx] = san(st[idx] +
                  san(add[idx].first * M[length].data[2][0]) +
                  san(add[idx].second * M[length].data[2][1]));
    if (l != r) {
      int mid = center(l, r), length = mid - l + 1;
      add[L] += add[idx];
      add[R] += {
        san(add[idx].first * M[length].data[0][0] +
            add[idx].second * M[length].data[0][1]),
        san(add[idx].first * M[length].data[1][0] +
            add[idx].second * M[length].data[1][1])
      };
    }
    add[idx] = {0, 0};
  }
}
void stup(int i, int j, int idx=0, int l=0, int r=n-1) {
  int L = left(idx), R = L + 1;
  if (l >= i and r <= j)
    add[idx] += {F[l-i+1], F[l-i]};
  push(idx, l, r, L, R);
  if ((l >= i and r <= j) or r < i or l > j)
    return;
  int mid = center(l, r);
  stup(i, j, L, l, mid);
  stup(i, j, R, mid+1, r);
  st[idx] = san(st[L] + st[R]);
  return;
}
int stq(int i, int j, int idx=0, int l=0, int r=n-1) {
  int L = left(idx), R = L + 1;
  push(idx, l, r, L, R);
  if (l >= i and r <= j)
    return st[idx];
  if (r < i or l > j)
    return 0;
  int mid = center(l, r);
  return san(stq(i, j, L, l, mid) + stq(i, j, R, mid+1, r));
}

ll A[MAXN];

int main() {
  pre();
  int m;
  cin >> n >> m;
  for (int i = 1; i <= n; ++i) {
    cin >> A[i];
    A[i] = san(A[i] + A[i-1]);
  }
  int op, l, r;
  while (m--) {
    cin >> op >> l >> r;
    --l, --r;
    if (op == 1)
      stup(l, r);
    else {
      cout << san(stq(l, r) + A[r+1] - A[l]) << endl;
    }
  }
  return 0;
}
