#include <iostream>
#include <map>
#include <queue>
#include <cstdlib>
#include <set>

using namespace std;

typedef long long ll;

inline int digits(ll x) {
  int c = 0;
  while (x) {
    ++ c;
    x /= 10;
  }
  return c;
}

typedef pair<ll, ll> pll;

int N;
ll T[17];

ll dijkstra(ll x) {
  set<ll> S;
  map<ll, ll> M;
  priority_queue<pll> Q;
  Q.push(pll(0, x));
  M[x] = 0;
  int n;
  ll t, nx;
  for ( ; ; ) {
    pll p = Q.top();
    Q.pop();
    if (S.find(p.second) != S.end())
      continue;
    S.insert(p.second);
    if (p.second == 0)
      return -p.first;
    n = digits(p.second);
    //cerr << p.second << " " << n << endl;
    t = 0;
    for (int i = n; i < n + 2; ++i) {
      nx = abs(T[i] - p.second);
      if (nx < p.second) {
        if (M.find(nx) == M.end()) {
          M[nx] = -p.first + i;
          Q.push(pll(-M[nx], nx));
        } else if (M[nx] > -p.first + i) {
          M[nx] = -p.first + i;
          Q.push(pll(-M[nx], nx));
        }
      }
    }
  }
}

int main() {
  for (int i = 1; i < 17; ++i)
    T[i] = T[i-1] * 10 + 1;
  ll x;
  cin >> x;
  N = digits(x);
  cout << dijkstra(x) << endl;
}
