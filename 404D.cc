#include <iostream>
#include <cstring>
#define MAXN 1000001
#define MOD 1000000007

using namespace std;

string s;
int M[MAXN][4];
inline void san(int &x) {
  if (x >= MOD)
    x -= MOD;
}

int DP(int p, int op) {
  if (p == s.size())
    return op != 1;
  int &ans = M[p][op];
  if (ans >= 0)
    return ans;
  if (s[p] == '?') {
    ans = 0;
    if (op != 2)
      ans += DP(p+1, 0);
    if (op == 2)
      ans += DP(p+1, 2);
    san(ans);
    if (op != 1)
      ans += DP(p+1, op ? 1 : 2);
    san(ans);
    if (op == 0)
      ans += DP(p+1, 1);
    san(ans);
  } else {
    switch (s[p]) {
      case '*': if (op == 2) ans = 0;
                else ans = DP(p+1, 0); break;
      case '0': if (op <= 1) ans = 0;
                else ans = DP(p+1, 2); break;
      case '1': if (op == 1) ans = 0;
                else ans = DP(p+1, op ? 1 : 2); break;
      case '2': if (op) ans = 0;
                else ans = DP(p+1, 1); break;
    }
  }
  return ans;
}

int main() {
  cin >> s;
  memset(M, -1, sizeof M);
  int ans;
  if (s[0] != '?') {
    ans = s[0] == '2' ? 0 : DP(1, s[0] == '1' ? 1 : (s[0] == '0' ? 2 : 0));
  } else {
    ans = DP(1, 0);
    ans += DP(1, 1);
    san(ans);
    ans += DP(1, 2);
    san(ans);
  }
  cout << ans << endl;
}
