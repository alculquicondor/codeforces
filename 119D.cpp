#include <iostream>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <map>
#include <set>
#define MAXN 1000005
using namespace std;

typedef pair<int, int> pii;
int z[2*MAXN];
void zfunction(string &s, int z[]){
    int sz = s.size();
    for(int i = 1, l = 0, r = 0; i < sz; ++i){
        z[i] = 0;
        if (i <= r)
            z[i] = min(z[i - l], r - i + 1);
        while (i + z[i] < sz && s[z[i]] == s[i + z[i]])
            ++z[i];
        if (i + z[i] - 1 > r){
            l = i;
            r = i + z[i] - 1;
        }
    }
    z[0] = sz;  
}
int pi[MAXN];
void kmpPreprocess(string &P) {
    int i = 0, j = -1, m = P.size();
    pi[0] = -1;
    while(i < m) {
        while(j>=0 and P[i] != P[j])
            j = pi[j];
        i ++, j++;
        pi[i] = j;
    }
}
int kmp[MAXN];
void kmpSearch(string &T, string &P) {
    int i = 0, j = 0, n = T.size(), m = P.size();
    while(i < n) {
        while(j>=0 and T[i]!=P[j])
            j = pi[j];
        kmp[i++] = ++j;
        if(j == m)
            j = pi[j];
    }
}

int main() {
    string a, b;
    getline(cin, a);
    getline(cin, b);
    int n = a.size(), m = b.size();
    if(n!=m) {
        puts("-1 -1");
        return 0;
    }
    string L = b + a, ar = a;
    zfunction(L, z);
    reverse(ar.begin(), ar.end());
    kmpPreprocess(ar);
    kmpSearch(b, ar);
    int i, j=-1;
    for(i=0; i<n-1 and a[i]==b[n-i-1]; i++);
    while(i--) {
        j = max(i+1, n-kmp[n-i-2]);
        if(z[n+i+1]>=j-i-1)
            break;
        j = -1;
    }
    printf("%d %d\n", i, j);
    return 0;
}
