from math import sqrt

def dist(a,b):
	return sqrt((a[0]-b[0])**2+(a[1]-b[1])**2)

n,k=[int(x) for x in raw_input().split()]
ini=[int(x) for x in raw_input().split()]
ans=0
for i in range(n-1):
	t=[int(x) for x in raw_input().split()]
	ans+=dist(ini,t)
	ini=t
print ans*k/50.0
