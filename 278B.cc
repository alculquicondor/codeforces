#include <iostream>
#include <set>

using namespace std;

int main() {
  int n;
  cin >> n;
  set<string> S;
  string s;
  while (n--) {
    cin >> s;
    for (int i = 1; i <= 3; i++)
      for (int j = 0; j + i <= s.size(); j++)
        S.insert(s.substr(j, i));
  }
  string ans = "aaa";
  char x[3];
  for (char a = 'a'; a <= 'z'; a++) {
    x[0] = a, x[1] = 0;
    s = string(x);
    if (S.find(s) == S.end()) {
      ans = s;
      break;
    }
    if (ans == "aaa")
      for (char b = 'a'; b <= 'z'; b++) {
        x[1] = b, x[2] = 0;
        s = string(x);
        if (S.find(s) == S.end()) {
          ans = s;
          break;
        }
      }
  }
  cout << ans << endl;
}
