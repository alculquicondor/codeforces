#include <cstdio>

int x1, y1, z1, x, y, z;

bool cansee(int x0, int y0, int z0) {
    double dx = x0-x1, dy = y0-y1, dz = z0-z1, t, tx, ty, tz;
    //planos z
    if(dz!=0) {
        t = -z1/dz;
        tx = x1+t*dx;
        ty = y1+t*dy;
        if(t<1 and tx>=0 and tx<=x and ty>=0 and ty<=y)
            return false;
        t = (z-z1)/dz;
        tx = x1+t*dx;
        ty = y1+t*dy;
        if(t<1 and tx>=0 and tx<=x and ty>=0 and ty<=y)
            return false;
    }
    //planos y
    if(dy!=0) {
        t = -y1/dy;
        tx = x1+t*dx;
        tz = z1+t*dz;
        if(t<1 and tx>=0 and tx<=x and tz>=0 and tz<=z)
            return false;
        t = (y-y1)/dy;
        tx = x1+t*dx;
        tz = z1+t*dz;
        if(t<1 and tx>=0 and tx<=x and tz>=0 and tz<=z)
            return false;
    }
    //planos x
    if(dx!=0) {
        t = -x1/dx;
        ty = y1+t*dy;
        tz = z1+t*dz;
        if(t<1 and ty>=0 and ty<=y and tz>=0 and tz<=z)
            return false;
        t = (x-x1)/dx;
        ty = y1+t*dy;
        tz = z1+t*dz;
        if(t<1 and ty>=0 and ty<=y and tz>=0 and tz<=z)
            return false;
    }
    return true;
}

int main() {
    scanf("%d %d %d", &x1, &y1, &z1);
    x1 *= 2, y1 *= 2, z1 *= 2;
    scanf("%d %d %d", &x, &y, &z);
    x *= 2, y *= 2, z *= 2;
    int ans = 0, t;
    scanf("%d", &t);
    if(cansee(x/2, 0, z/2))
        ans += t;
    scanf("%d", &t);
    if(cansee(x/2, y, z/2))
        ans += t;
    scanf("%d", &t);
    if(cansee(x/2, y/2, 0))
        ans += t;
    scanf("%d", &t);
    if(cansee(x/2, y/2, z))
        ans += t;
    scanf("%d", &t);
    if(cansee(0, y/2, z/2))
        ans += t;
    scanf("%d", &t);
    if(cansee(x, y/2, z/2))
        ans += t;
    printf("%d\n", ans);
}
