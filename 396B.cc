#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;

typedef long long ll;
struct frac {
  ll num, den;
  void simplify() {
    ll g = __gcd(num, den);
    if (g != 1) {
      num /= g;
      den /= g;
    }
  }
  frac(ll n = 0, ll d = 1) : num(n), den(d) {
    simplify();
  }
  frac operator + (const frac &f) const {
    ll g = __gcd(den, f.den);
    return frac(num * (f.den / g) + f.num * (den / g),
        (den / g) * f.den);
  }
  frac operator - (const frac &f) const {
    ll g = __gcd(den, f.den);
    return frac(num * (f.den / g) - f.num * (den / g),
        (den / g) * f.den);
  }
};

#define MAXN 10000000

bool isP[MAXN];

void pre() {
  memset(isP, true, sizeof isP);
  for (int i = 4; i < MAXN; i += 2)
    isP[i] = false;
  for (int i = 3; i * i < MAXN; i += 2)
    if (isP[i]) {
      for (int j = i * i; j < MAXN; j += i)
        isP[j] = false;
    }
}

inline ll san(const ll &x, const ll &m) {
  return x >= m ? x % m : x;
}

inline ll powmod(ll a, ll e, const ll &m) {
  ll r = 1;
  while (e) {
    if (e & 1)
      r = san(r * a, m);
    e >>= 1;
    a = san(a * a, m);
  }
  return r;
}

ll B[] = {2, 3, 5, 7};
inline bool isprime(ll n) {
  if (n < MAXN)
    return isP[n];
  if ((n & 1) == 0)
    return false;
  int s = __builtin_ctzll(n-1);
  ll d = (n - 1) >> s, x;
  for (int i = 0; i < 4; ++i) {
    x = powmod(B[i], d, n);
    if (x == 1 or x == n-1)
      continue;
    for (int j = 1; j < s; ++j) {
      x = san(x * x, n);
      if (x == 1)
        return false;
      if (x == n-1)
        goto next;
    }
    return false;
next:;
  }
  return true;
}

int main() {
  pre();
  ios::sync_with_stdio(0);
  int tc, n;
  cin >> tc;
  while (tc--) {
    cin >> n;
    ll p, q;
    for (p = n; not isprime(p); --p) {}
    for (q = n + 1; not isprime(q); ++q) {}
    //cout << "# " << p << " " << q << endl;
    frac ans = frac(1, 2) - frac(1, p) + frac(n - p + 1, p * q);
    cout << ans.num << "/" << ans.den << endl;
  }
}
