#include <iostream>

using namespace std;

typedef long long ll;

int solve() {
  ll n;
  cin >> n;
  n--;
  int cant = 5;
  while (n - cant >= 0) {
    n -= cant;
    cant *= 2;
  }
  return n / (cant/5);
}

int main() {
  string name[] = {"Sheldon", "Leonard", "Penny", "Rajesh", "Howard"};
  cout << name[solve()] << endl;
}
