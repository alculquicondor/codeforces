def just47(s):
    for x in s:
        if x != '4' and x != '7' :
            return False
    return True

n = int(raw_input())
s = raw_input()
if just47(s) and sum([int(x) for x in s[:n/2]]) == sum([int(x) for x in s[n/2:]]) :
    print 'YES'
else:
    print 'NO'
