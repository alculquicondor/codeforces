#include <iostream>
#include <vector>
#include <cstdio>
#include <algorithm>

using namespace std;

int n, m;

typedef pair<int, int> pii;

struct tree {
  int x, h, pl, pr;
};
vector<tree> T;
vector<pii> M;

#define MAXN 32800
#define center(x, y) (x + y) >> 1
#define left(x) (x << 1) + 1
float st[MAXN], add[MAXN];

void stinit(int idx = 0, int l = 0, int r = m - 1) {
  add[idx] = 1;
  st[idx] = l == r ? 1 : 0;
  if (l == r)
    return;
  int L = left(idx), R = L + 1, mid = center(l, r);
  stinit(L, l, mid);
  stinit(R, mid+1, r);
}

inline void push(int idx, int l, int r, int L, int R) {
  if (add[idx] != 1) {
    if (l != r) {
      add[L] *= add[idx];
      add[R] *= add[idx];
    } else {
      st[idx] *= add[idx];
    }
    add[idx] = 1;
  }
}

void stupdate(int i, int j, float up,
    int idx = 0, int l = 0, int r = m - 1) {
  int L = left(idx), R = L + 1;
  if (l >= i and r <= j)
    add[idx] *= up;
  push(idx, l, r, L, R);
  if ((l >= i and r <= j) or r < i or l > j)
    return;
  int mid = center(l, r);
  stupdate(i, j, up, L, l, mid);
  stupdate(i, j, up, R, mid + 1, r);
}

float stq(int i, int idx = 0, int l = 0, int r = m - 1) {
  int L = left(idx), R = L + 1;
  push(idx, l, r, L, R);
  if (l >= i and r <= i)
    return st[idx];
  if (r < i or l > i)
    return 0;
  int mid = center(l, r);
  return stq(i, L, l, mid) + stq(i, R, mid+1, r);
}

int main() {
  ios::sync_with_stdio(0);
  cin >> n >> m;
  int x, h, pl, pr, p;
  for (int i = 0; i < n; ++i) {
    cin >> x >> h >> pl >> pr;
    T.push_back({x, h, pl, pr});
  }
  for (int i = 0; i < m; ++i) {
    cin >> x >> p;
    M.push_back({x, p});
  }
  sort(M.begin(), M.end());
  stinit();
  int l, r;
  for (auto &x: T) {
    l = lower_bound(M.begin(), M.end(), pii(x.x+1, 0)) - M.begin();
    r = (int)(lower_bound(M.begin(), M.end(), pii(x.x+x.h+1, 0)) - M.begin()) - 1;
    stupdate(l, r, 1-x.pr/100.);
    l = lower_bound(M.begin(), M.end(), pii(x.x-x.h, 0)) - M.begin();
    r = (int)(lower_bound(M.begin(), M.end(), pii(x.x, 0)) - M.begin()) - 1;
    stupdate(l, r, 1-x.pl/100.);
  }
  float ans = 0;
  for (int i = 0; i < m; ++i)
    ans += M[i].second * stq(i);
  printf("%.5f\n", ans);
}
