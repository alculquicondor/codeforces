#include <iostream>
#define MOD 1000000007
#define MAXN 1000004

using namespace std;
typedef long long ll;

ll san(ll x) {
  if (x >= MOD)
    x %= MOD;
  return x;
}

int a, b;
bool isgood(int x) {
  while (x) {
    if (x % 10 != a and x % 10 != b)
      return false;
    x /= 10;
  }
  return true;
}

ll pow(ll a, int e) {
  ll r = 1;
  while (e) {
    if (e & 1)
      r = san(a*r);
    e >>= 1;
    a = san(a*a);
  }
  return r;
}

inline ll inv(ll x) {
  return pow(x, MOD-2);
}

int C[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> a >> b >> n;
  int ans = 0;
  C[0] = 1;
  for (int i = 1; i <= n; i++)
    C[i] = san(san(C[i-1] * (n-i+1LL))*inv(i));
  for (int i = 0; i <= n; i++)
    if (isgood(a*i+b*(n-i)))
      ans = san(ans+C[i]);
  cout << ans << endl;
}
