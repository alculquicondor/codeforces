#include <iostream>
#include <algorithm>
#define MAXN 500005

using namespace std;

typedef long long ll;

ll A[MAXN], V[MAXN];
int C[MAXN];

int main() {
  std::ios::sync_with_stdio(false);
  int n, sz;
  cin >> n;
  sz = n;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  for (int i = 1; i < n; ++i)
    A[i] += A[i-1];
  ll ans = 0;
  (i)f (n > 2) {
    copy(A, A+n, V);
    sort(V, V+n);
    sz = unique(V, V+n) - V;
    //cerr << sz << endl;
    int p;
    ++C[lower_bound(V, V+sz, A[n-2]) - V];
    for (int i = n - 3; i >= 0; --i) {
      if (A[n-1] == A[i] * 3) {
        p = lower_bound(V, V+sz, A[i] * 2) - V;
        if (p < sz and V[p] == A[i] * 2)
          ans += C[p];
      }
      ++C[lower_bound(V, V+sz, A[i]) - V];
    }
  }
  cout << ans << endl;
}
