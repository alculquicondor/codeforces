#include<cstdio>
#include<bits/stl_algobase.h>

using std::max;

int main()
{
	int n,a,b;
	scanf("%d",&n);
	int cu=0,r=0;
	for(int i=0;i<n;i++)
	{
		scanf("%d %d",&a,&b);
		cu-=a;
		cu+=b;
		r=max(r,cu);
	}
	printf("%d\n",r);
}
