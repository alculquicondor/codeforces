#include <iostream>

using namespace std;

int n;

inline void edge(int u, int v) {
  if (v >= n)
    v -= n;
  cout << u + 1 << " " << v + 1 << endl;
}

int main() {
  ios::sync_with_stdio(0);
  int tc, p, e, id, s;
  cin >> tc;
  while (tc--) {
    cin >> n >> p;
    e = 2 * n + p;
    id = 0, s = 1;
    while (e--) {
      edge(id, id+s);
      if (++id == n) {
        id = 0;
        ++s;
      }
    }
  }
}
