#include <iostream>

using namespace std;

int main() {
  int n;
  string s;
  cin >> n >> s;
  int ans = 0;
  for (int i = n; i < s.size(); i += n)
    ans += s[i-1] == s[i-2] and s[i-2] == s[i-3];
  cout << ans << endl;
}
