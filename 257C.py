from math import atan2, pi

n = int(raw_input())
A = list()
for i in xrange(n):
  x, y = [int(a) for a in raw_input().split()]
  A.append(atan2(x, y) / pi * 180)
A.sort()
ans = A[n-1] - A[0];
for i in xrange(1, n):
  ans = min(ans, A[i-1] + 360 - A[i])
print ans
