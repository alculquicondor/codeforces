#include <iostream>
#include <cstring>
#include <vector>
#define MAXN 200005

using namespace std;
typedef pair<int, bool> pib;
pib pi[MAXN];
int V[MAXN];
bool visit[MAXN];
vector<pib> adj[MAXN];

void dfs(int u, int p = -1) {
  V[u] = 0;
  int v;
  bool c;
  for (int i = 0; i < adj[u].size(); i++) {
    v = adj[u][i].first, c = adj[u][i].second;
    if (v != p) {
      pi[v] = pib(u, 1-c);
      dfs(v, u);
      V[u] += V[v] + c;
    }
  }
}

void act(int u) {
  if (pi[u].first == -1 or visit[u])
    return;
  visit[u] = 1;
  int p = pi[u].first, c = pi[u].second ? 1 : -1;
  act(p);
  V[u] = V[p] + c;
}

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  int u, v;
  for (int i = 0; i < n - 1; i++) {
    cin >> u >> v;
    u--, v--;
    adj[u].push_back(pib(v, 0));
    adj[v].push_back(pib(u, 1));
  }
  pi[0] = pib(-1, -1);
  dfs(0);
  memset(visit, 0, sizeof visit);
  int best = 1 << 30;
  for (int i = 0; i < n; i++) {
    act(i);
    best = min(best, V[i]);
  }
  bool first = true;
  cout << best << endl;
  for (int i = 0; i < n; i++) {
    if (best == V[i]) {
      if (not first)
        cout << " ";
      first = false;
      cout << i + 1;
    }
  }
  cout << endl;
}
