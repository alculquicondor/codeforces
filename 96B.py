def ndigit(n):
	r=0
	while n>0:
		r+=1
		n/=10
	return r

def cnum(n,c):
	r=0
	four=0
	for i in range(c):
		r=r*10
		if((1<<i)&n):
			r+=7
		else:
			r+=4
			four+=1
	if four==c/2:
		return r
	return 0

n=int(raw_input())
c=ndigit(n)
if(c%2):
	print '4'*((c+1)/2)+'7'*((c+1)/2)
	exit(0)

mini=1<<33
for i in range(0,1<<c):
	t=cnum(i,c)
	if(t>=n):
		mini=min(mini,t)

if(mini==1<<33):
	print '4'*((c+2)/2)+'7'*((c+2)/2)
else:
	print mini
