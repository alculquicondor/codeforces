#include <iostream>
#include <vector>
#define MAXN 501

using namespace std;

typedef long long ll;

int V[MAXN];

int main() {
  int n, m;
  ll ansn = 0, ansd = 1;
  cin >> n >> m;
  for (int i = 0; i < n; ++i)
    cin >> V[i];
  int u, v, c;
  for (int i = 0; i < m; ++i) {
    cin >> u >> v >> c;
    if (c * ansn < (V[--u] + V[--v]) * ansd) {
      ansn = V[u] + V[v];
      ansd = c;
    }
  }
  cout << fixed;
  cout.precision(11);
  cout << (double)ansn / ansd << endl;
}

