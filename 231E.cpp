#include <cstdio>
#include <vector>
#include <cstring>
#define MAXN 100001

using namespace std;

int n;
vector<int> adj[MAXN], adj2[MAXN];
int snod[MAXN];
int pi[MAXN], h[MAXN], d[MAXN];
bool visit[MAXN];
int group, sgroups;

void encycle(int v, int u) {
    while(v!=u) {
        snod[v] = group;
        v = pi[v];
    }
    snod[v] = group;
    group ++;
}

void dfs(int u) {
    visit[u] = true;
    for(int v: adj[u]) {
        if(not visit[v]) {
            pi[v] = u;
            dfs(v);
        }
        else if(v!=pi[u] and snod[u]==-1)
            encycle(u, v);
    }
}

vector<int> tojoin;

void dfs1(int u) {
    snod[u] = group;
    visit[u] = true;
    for(int v: adj[u]) {
        if(snod[v]!=-1 and snod[v]<sgroups)
            tojoin.push_back(snod[v]);
        else if(not visit[v])
            dfs1(v);
    }
}

void dfs2(int u) {
    visit[u] = true;
    for(int v: adj2[u])
        if(not visit[v]) {
            h[v] = h[u] + 1;
            d[v] = d[u] + (v<sgroups);
            pi[v] = u;
            dfs2(v);
        }
}

int lca(int u, int v) {
    while(u!=v) {
        if(h[u] < h[v])
            v = pi[v];
        else
            u = pi[u];
    }
    return u;
}

int main() {
    int m;
    scanf("%d %d", &n, &m);
    int u, v;
    for(int i=0; i<m; i++) {
        scanf("%d %d", &u, &v);
        u--, v--;
        adj[u].push_back(v);
        adj[v].push_back(u);
    }
    memset(snod, -1, sizeof snod);
    memset(visit, 0, sizeof visit);
    group = 0;
    dfs(0);
    sgroups = group;
    memset(visit, 0, sizeof visit);
    for (int u=0; u<n; u++)
        if(not visit[u]) {
            if(snod[u]!=-1 and snod[u]<sgroups) {
                for(int v: adj[u])
                    if(snod[v]!=-1 and snod[u]!=snod[v]) {
                        visit[v] = true;
                        adj2[snod[u]].push_back(snod[v]);
                        adj2[snod[v]].push_back(snod[u]);
                    }
            }
            else {
                tojoin.clear();
                dfs1(u);
                for(int v: tojoin) {
                    adj2[v].push_back(group);
                    adj2[group].push_back(v);
                }
                group++;
            }
        }
    memset(visit, 0, sizeof visit);
    d[0] = sgroups>0;
    dfs2(0);
    int k, r, sn;
    scanf("%d", &k);
    int pow[MAXN];
    pow[0] = 1;
    for(int i=1; i<=group; i++)
        pow[i] = ((long long)pow[i-1]*2)%1000000007;
    while(k--) {
        scanf("%d %d", &u, &v);
        u = snod[u-1];
        v = snod[v-1];
        r = lca(u, v);
        sn = d[u]+d[v]-d[r]*2+(r<sgroups);
        printf("%d\n", pow[sn]);
    }
}
