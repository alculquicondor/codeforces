#include <iostream>
#include <algorithm>

using namespace std;
typedef long long ll;

int main() {
  ll a[2], b[2], num, numn, numd, den;
  cin >> a[0] >> a[1] >> b[0] >> b[1];
  den = a[0] * a[1];
  if (a[0] * b[1] < a[1] * b[0]) {
    numn = b[1] * a[0] * a[0];
    numd = b[0];
  } else {
    numn = b[0] * a[1] * a[1];
    numd = b[1];
  }
  den = den * numd;
  num = den - numn;
  ll g = __gcd(num, den);
  num /= g;
  den /= g;
  cout << num << '/' << den << endl;
  return 0;
}
