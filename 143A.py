r = [int(x) for x in raw_input().split()]
c = [int(x) for x in raw_input().split()]
d = [int(x) for x in raw_input().split()]

def valid(a1, a2, b1, b2) :
    if len(set([a1,a2,b1,b2])) != 4:
        return False
    x = a1+a2==r[0] and b1+b2==r[1]
    y = a1+b1==c[0] and a2+b2==c[1]
    z = a1+b2==d[0] and a2+b1==d[1]
    if x and y and z:
        print a1,a2
        print b1,b2
        return True

enc = False

for a1 in range(1,10):
    for a2 in range(1,10):
        for b1 in range(1,10):
            for b2 in range(1,10):
                if valid(a1, a2, b1, b2) :
                    enc = True
                    break
            if enc:
                break
        if enc:
            break
    if enc:
        break
if not enc:
    print -1
