#include <iostream>
#include <vector>
#define MAXN 30001

using namespace std;

bool visit[MAXN];
vector<int> adj[MAXN];
vector<int> topo;

void dfs(int u) {
  visit[u] = true;
  for (auto &v: adj[u]) {
    if (not visit[v])
      dfs(v);
  }
  topo.push_back(u);
}

int main(int argc, const char *argv[]) {
  std::ios::sync_with_stdio(false);
  int n, m, u, v;
  cin >> n >> m;
  while (m--) {
    cin >> u >> v;
    adj[u].push_back(v);
  }
  for (int i = 1; i <= n; ++i)
    if (not visit[i])
      dfs(i);
  cout << topo[0];
  for (int i = 1; i < n; ++i)
    cout << " " << topo[i];
  cout << endl;
  return 0;
}

