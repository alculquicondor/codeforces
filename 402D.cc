#include <iostream>
#include <algorithm>
#define MAXN 5001

using namespace std;

int n, m, sz;
int A[MAXN], B[MAXN], P[MAXN], D[12];

inline int factor(int x) {
  int id = 0, c = 0, beauty = 0;
  sz = 0;
  for (int p = 2; p * p <= x; ++p) {
    while (id < m and p > P[id])
      ++id;
    c = 0;
    while (x % p == 0) {
      x /= p;
      ++c;
    }
    if (id < m and P[id] == p)
      beauty -= c;
    else
      beauty += c;
    if (c)
      D[sz++] = p;
  }
  if (x != 1) {
    D[sz++] = x;
    while (id < m and x > P[id])
      ++id;
    if (id < m and P[id] == x)
      --beauty;
    else
      ++beauty;
  }
  return beauty;
}

int main() {
  ios::sync_with_stdio(0);
  cin >> n >> m;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  for (int i = 0; i < m; ++i)
    cin >> P[i];
  int beauty = 0;
  for (int i = 0; i < n; ++i)
    beauty += factor(A[i]);
  int g = A[0], x, c;
  B[0] = factor(g);
  for (int i = 1; i < n; ++i) {
    g = __gcd(g, A[i]);
    B[i] = 0;
    x = g;
    for (int j = 0; j < sz; ++j) {
      c = 0;
      while (x % D[j] == 0) {
        x /= D[j];
        ++c;
      }
      if (binary_search(P, P+m, D[j]))
        B[i] -= c;
      else
        B[i] += c;
    }
  }
  B[n] = 0;
  int last = n;
  for (int i = n-1; i >= 0; --i)
    if (beauty + (B[last]-B[i]) * (i+1) > beauty) {
      beauty += (B[last]-B[i]) * (i+1);
      last = i;
    }
  cout << beauty << endl;
}

