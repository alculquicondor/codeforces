#include <iostream>
#define MAXN 1000001

using namespace std;

int X[MAXN];

int main() {
  for (int i = 1; i < MAXN; ++i)
    X[i] = X[i-1] ^ i;
  int n, p, ans = 0, t, q;
  ios::sync_with_stdio(0);
  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> p;
    ans ^= p;
  }
  for (int i = 1; i <= n; ++i) {
    ans ^= X[n % i];
    t = n - (n % i);
    if ((t/i) & 1)
      ans ^= X[i-1];
  }
  cout << ans << endl;
}
