A=[int(x) for x in raw_input().split(',')]
A.sort()
i=0
r=''
while(i<len(A)):
	j=i+1
	while j<len(A) and (A[j]==A[j-1] or A[j]==A[j-1]+1):
		j+=1
	j-=1
	if A[j]==A[i]:
		r+=str(A[i])+','
	else:
		r+=str(A[i])+'-'+str(A[j])+','
	i=j+1
print r[:-1]
