n=int(raw_input())

from fractions import gcd

suma=0
for i in range(2,n):
	m=n
	while(m>0):
		suma+=m%i
		m/=i
g=gcd(suma,n-2)
print str(suma/g)+"/"+str((n-2)/g)
