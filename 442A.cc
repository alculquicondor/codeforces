#include <iostream>
#include <algorithm>

using namespace std;

int C[5], N[5];

char S[] = "RGBYW";
inline int val(char c) {
  return find(S, S+5, c) - S;
}

string A[101];

int main() {
  ios::sync_with_stdio(0);
  int n, id;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> A[i];
  }
  sort(A, A+n);
  n = unique(A, A+n) - A;
  for (int i = 0; i < n; ++i) {
    string &s = A[i];
    s[0] = val(s[0]);
    s[1] -= '1';
    C[s[0]] |= 1 << s[1];
    N[s[1]] |= 1 << s[0];
  }
  int ans = 25, tmp1, tmp2;
  for (int m1 = 0; m1 < (1<<5); ++m1)
    for (int m2 = 0; m2 < (1<<5); ++m2) {
      int valid = 0;
      for (int i = 0; i < n; ++i) {
        string &s = A[i];
        tmp1 = C[s[0]] & ~m2;
        tmp2 = N[s[1]] & ~m1;
        valid += ((m1 & (1<<s[0])) and (tmp1 & (tmp1-1)) == 0) or
          ((m2 & (1<<s[1])) and (tmp2 & (tmp2-1)) == 0);
      }
      if (valid >= n - 1)
        ans = min(ans, __builtin_popcount(m1) + __builtin_popcount(m2));
    }
  cout << ans << endl;
}
