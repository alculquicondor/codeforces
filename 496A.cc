#include <set>
#include <iostream>
#include <vector>

using namespace std;

int main() {
  int n;
  cin >> n;
  vector<int> A(n);
  multiset<int> S;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  for (int i = 1; i < n; ++i)
    S.insert(A[i] - A[i-1]);
  int ans = 1000;
  for (int i = 1; i < n - 1; ++i) {
    auto it = S.find(A[i] - A[i-1]);
    S.erase(it);
    it = S.find(A[i+1] - A[i]);
    S.erase(it);
    S.insert(A[i+1] - A[i-1]);
    ans = min(ans, *S.rbegin());
    it = S.find(A[i+1] - A[i-1]);
    S.erase(it);
    S.insert(A[i+1] - A[i]);
    S.insert(A[i] - A[i-1]);
  }
  cout << ans << endl;
  return 0;
}
