#include <iostream>

typedef long long ll;
using namespace std;

#define MAXN 10005

ll A[MAXN], B[MAXN];

int n;
ll go(ll x) {
  ll ans = 0;
  for (int i = 0; i < n; ++i)
    ans += A[i] * x / B[i];
  return ans;
}

int main() {
  ll c;
  cin >> n >> c;
  c -= n;
  if (c < 0) {
    cout << 0 << endl;
  } else {
    ll pl, pr;
    ll am = 0, bm = 1, sup;
    for (int i = 0; i < n; ++i) {
      cin >> A[i] >> B[i];
      if (A[i] * bm >= B[i] * am)
        am = A[i], bm = B[i];
    }
    if (am) {
      sup = (c+1) * bm / am;
      ll lo = 0, hi = sup+1, mid; // [lo, hi[
      while (hi - lo > 1) {
        mid = (hi+lo) >> 1;
        if (go(mid) > c)
          hi = mid;
        else
          lo = mid;
      }
      pr = hi;
      lo = 0, hi = sup+1, mid; // ]lo, hi]
      while (hi - lo > 1) {
        mid = (hi+lo) >> 1;
        if (go(mid) < c)
          lo = mid;
        else
          hi = mid;
      }
      pl = hi;
      //cout << pl << " " << pr << endl;
      cout << pr - pl << endl;
    } else {
      cout << (c ? 0 : -1) << endl;
    }
  }
}

