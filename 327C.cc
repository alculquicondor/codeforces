#include <iostream>
#include <vector>
#define MOD 1000000007LL

using namespace std;
typedef long long ll;

string s;
ll k;

vector<ll> ext_gcd(ll a, ll b) {
  vector<ll> ans(3),tmp;
  if(b==0) {
    ans[0] = 1, ans[1] = 0, ans[2] = a;
    return ans;
  }
  tmp = ext_gcd(b, a%b);
  ans = tmp;
  ans[0] = tmp[1];
  ans[1] = tmp[0]-(a/b)*tmp[1];
  return ans;
}

ll inv(ll x) {
  return (ext_gcd(x, MOD)[0]+MOD) % MOD;
}

ll pow2(ll e) {
  ll ans = 1, ac = 2;
  while (e) {
    if (e & 1)
      ans = (ans * ac) % MOD;
    ac = (ac * ac) % MOD;
    e >>= 1;
  }
  return ans;
}

int main() {
  cin >> s >> k;
  ll ans = 0;
  for (int i = 0; i < s.size(); i++)
    if (s[i] == '5' or s[i] == '0')
      ans = (ans + pow2(i)) % MOD;
  ans = (ans * ((pow2(s.size()*k)+MOD-1) % MOD)) % MOD;
  ans = (ans * inv((pow2(s.size())+MOD-1)%MOD)) % MOD;
  cout << ans << endl;
}
