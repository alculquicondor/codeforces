#include <iostream>
#include <algorithm>
#include <vector>
#define MAXN 10001

using namespace std;

typedef pair<int, int> pii;
int W[MAXN], P[MAXN];
vector<int> adj[MAXN];
bool present[MAXN];

int main() {
  std::ios::sync_with_stdio(false);
  int n, m;
  cin >> n >> m;
  for (int i = 0; i < n; ++i) {
    cin >> W[i];
    P[i] = i;
  }
  sort(P, P+n, [&] (int i, int j) {
      return W[i] > W[j];
  });
  fill(present, present+n, true);
  int u, v;
  while (m--) {
    cin >> u >> v;
    --u, --v;
    adj[u].push_back(v);
    adj[v].push_back(u);
  }
  int ans = 0;
  for (int i = 0; i < n; ++i) {
    int &p = P[i];
    for (int v : adj[p])
      if (present[v])
        ans += W[v];
    present[p] = false;
  }
  cout << ans << endl;
}
