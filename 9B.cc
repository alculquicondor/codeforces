#include <iostream>
#include <algorithm>
#include <cmath>

using namespace std;

template<typename T>
struct p2d {
  T x, y;
  p2d(T x = 0, T y = 0) : x(x), y(y) {}
  p2d(const p2d &a) : x(a.x), y(a.y) {}
  p2d operator - (const p2d &a) const {
    return p2d(x-a.x, y-a.y);
  }
  p2d operator + (const p2d &a) const {
    return p2d(x+a.x, y+a.y);
  }
  p2d operator * (T k) const {
    return p2d(x*k, y*k);
  }
  p2d operator / (T k) const {
    return p2d(x/k, y/k);
  }
  void operator -= (const p2d &a) {
    x -= a.x, y -= a.y;
  }
  void operator += (const p2d &a) {
    x += a.x, y += a.y;
  }
  void operator *= (T k) {
    x *= k, y *= k;
  }
  void operator /= (T k) {
    x /= k, y /= k;
  }
  bool operator == (const p2d &a) const {
    return x == a.x and y == a.y;
  }
  bool operator != (const p2d &a) const {
    return x != a.x or y != a.y;
  }
  bool operator < (const p2d &a) const {
    if (x == a.x)
      return y < a.y;
    return x < a.x;
  }

  T norm2() const {
    return x * x + y * y;
  }
  void reduce() {
    T g = __gcd(x, y);
    x /= g, y /= g;
  }
  p2d ort() const {
    return p2d(-y, x);
  }
  T operator % (const p2d &a) const {
    return x * a.y - y * a.x;
  }
  T operator * (const p2d &a) const {
    return x * a.x + y * a.y;
  }
};
typedef long long ll;
typedef p2d<ll> pt;
ll X[101];

int main() {
    int n, vb, vs;
    cin >> n >> vb >> vs;
    double ans = 1e300, tmp, dist, d;
    int id = -1;
    for (int i = 0; i < n; ++i)
        cin >> X[i];
    pt A;
    cin >> A.x >> A.y;
    for (int i = 1; i < n; ++i) {
        d = sqrt((A - X[i]).norm2());
        tmp = (double)X[i] / vb + d / vs;
        if (tmp < ans) {
            id = i;
            ans = tmp;
            dist = d;
        } else if (tmp == ans and d < dist) {
            id = i;
            dist = d;
        }
    }
    cout << id + 1 << endl;
}

