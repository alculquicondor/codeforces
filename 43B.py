s1=raw_input()
s2=raw_input()
M1=dict()
M2=dict()
for x in s1:
	if x!=' ':
		if(x in M1):
			M1[x]+=1
		else:
			M1[x]=1
for x in s2:
	if x!=' ':
		if(x in M2):
			M2[x]+=1
		else:
			M2[x]=1

for x in M2:
	if(x not in M1 or M2[x]>M1[x]):
		print 'NO'
		exit(0)
print 'YES'

