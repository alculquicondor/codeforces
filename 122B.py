s=raw_input()
i=0
ans='9'
rep=dict()
while i<len(s):
	j=i
	while j<len(s) and (s[j]=='4' or s[j]=='7'):
		j+=1
	for k in range(i,j):
		for r in range(k+1,j+1):
			if s[k:r] in rep:
				rep[s[k:r]]+=1
			else:
				rep[s[k:r]]=1
	i=j+1
try:
	q=rep[max(rep,key=lambda x:rep[x])]
except:
	print -1
	exit(0)
for x in rep:
	if rep[x]==q:
		ans=min(ans,x)
print ans
