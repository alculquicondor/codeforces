#include <iostream>
#include <algorithm>
#define MAXN 1000010

using namespace std;
int prime[MAXN];

void criba() {
  fill(prime, prime+MAXN, 1);
  prime[0] = prime[1] = 0;
  for (int i = 2; i * i <= MAXN; i++)
    if(prime[i])
      for (int j = i * i; j <= MAXN; j += i)
        prime[j] = 0;
  for (int i = 1; i < MAXN; i++)
    prime[i] += prime[i-1];
}

int main() {
  criba();
  int a, b, k;
  cin >> a >> b >> k;
  int lo = 0, hi = b-a+2, l;
  while (hi - lo > 1) {
    bool poss = true;
    l = (hi + lo)/2;
    for (int x = a; poss and x <= b - l + 1; x++)
      poss = prime[x+l-1] - prime[x-1] >= k;
    if (poss)
      hi = l;
    else
      lo = l;
  }
  if (lo < b - a + 1)
    cout << hi << endl;
  else
    cout << -1 << endl;
}
