#include <iostream>
#include <iomanip>

using namespace std;
#define MAXN 2001

bool R[MAXN], C[MAXN];
double M[MAXN][MAXN];
int n;

double F(int fr, int fc) {
  if (fr + fc == 0)
    return 0;
  double &ans = M[fr][fc];
  if (ans >= 0)
    return ans;
  double r = fr, c = fc, n2 = n * n;
  if (fc == 0)
    swap(fr, fc);
  if (fr == 0) {
    ans = 1 + n * fc / n2 * F(0, fc-1);
    ans /= (1 - n * (n-fc) / n2);
    return ans;
  }
  ans = 1 + r * c / n2 * F(fr-1, fc-1) + r * (n-c) / n2 * F(fr-1, fc)
    + (n-r) * c / n2 * F(fr, fc-1);
  ans /= (1 - (n-r) * (n-c) / n2);
  return ans;
}

int main() {
  ios::sync_with_stdio(0);
  int m, r, c;
  cin >> n >> m;
  while (m--) {
    cin >> r >> c;
    --r, --c;
    R[r] = true;
    C[c] = true;
  }
  int fr = 0, fc = 0;
  for (int i = 0; i < n; ++i)
    fr += not R[i];
  for (int i = 0; i < n; ++i)
    fc += not C[i];
  for (int i = 0; i <= n; ++i)
    for (int j = 0; j <= n; ++j)
      M[i][j] = -1;
  cout << fixed << setprecision(6) << F(fr, fc) << endl;
}
