#include <iostream>
#include <cstring>

using namespace std;

bool solve() {
  string s;
  cin >> s;
  int A[26], impar = 0, par = 0;
  memset(A, 0, sizeof A);
  for (int i = 0; i < s.size(); i++)
    A[s[i]-'a'] ++;
  for (int i = 0; i < 26; i++) {
    impar += A[i] & 1;
    par += A[i] >> 1;
  }
  if (impar == 0)
    return 0;
  impar += par-1;
  return (impar+par) & 1;
}

int main() {
  cout << (solve() ? "Second" : "First") << endl;
}

