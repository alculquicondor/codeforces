#include <algorithm>
#include <iostream>

using namespace std;

typedef pair<int, int> pii;

pii A[102];

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> A[i].first >> A[i].second;
  sort(A+1, A+n);
  int last = 0, ans = 0;
  for (int i = 1; i < n; ++i) {
    //cout << A[i].first << " " << A[i].second << " " << last << endl;
    if (A[i].first > last)
      ans += max(min(A[i].first, A[0].second) - max(last, A[0].first),
          0);
    last = max(A[i].second, last);
  }
  if (last < A[0].second)
    ans += A[0].second - max(last, A[0].first);
  cout << ans << endl;
}
