#include <iostream>
#include <algorithm>
#define MAXN 101

using namespace std;

int W[MAXN][MAXN], P[MAXN];
int x[MAXN], y[MAXN], D[MAXN];

int dist(int i, int j) {
  return abs(y[j]-y[i]) + abs(x[j]-x[i]);
}

int main() {
  ios::sync_with_stdio(0);
  int n, d;
  cin >> n >> d;
  P[0] = P[n-1] = 0;
  for (int i = 1; i < n - 1; i++)
    cin >> P[i];
  for (int i = 0; i < n; i++)
    cin >> x[i] >> y[i];
  for (int i = 0; i < n; i++) {
    D[i] = 1 << 30;
    for (int j = 0; j < n; j++) {
      if (i == j)
        W[i][j] = 0;
      else
        W[i][j] = dist(i, j) * d - P[j];
    }
  }
  D[0] = 0;
  for (int i = 0; i < n - 1; i++)
    for (int u = 0; u < n; u++)
      for (int v = 0; v < n; v++)
        if (u != v and D[v] > D[u] + W[u][v])
          D[v] = D[u] + W[u][v];
  cout << (D[n-1] >= 0 ? D[n-1] : 0) << endl;
}
