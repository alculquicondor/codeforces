#include <iostream>
#include <cstring>
#define MAXN 100005

using namespace std;
int g[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n, m, u, v;
  cin >> n >> m;
  memset(g, 0, sizeof g);
  while (m --) {
    cin >> u >> v;
    g[--u]++;
    g[--v]++;
  }
  int n2 = 0, dif, n1 = 0;
  for (int i = 0; i < n; i++) {
    if (g[i] == 2)
      n2++;
    else if (g[i] == 1)
        n1 ++;
  }
  if (n2 == n)
    cout << "ring";
  else if (n2 == n - 2 and n1 == 2)
    cout << "bus";
  else if (n1 == n - 1)
    cout << "star";
  else
    cout << "unknown";
  cout << " topology\n";
}
