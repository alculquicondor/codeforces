#include <iostream>
#include <cstring>
#include <algorithm>
#define MAXN 100002

using namespace std;
int M[MAXN][2], n;
int A[MAXN];
string s;

int DP(int pos, bool top) {
  if (pos == n)
    return 0;
  int &ans = M[pos][top];
  if (ans >= 0)
    return ans;
  ans = 0;
  for (int i = 0; i <= (top?s[pos]:1); ++i)
    ans = max(ans, (i * A[pos]) + DP(pos+1, top and i == s[pos]));
  return ans;
}

int main() {
  ios::sync_with_stdio(0);
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  reverse(A, A+n);
  cin >> s;
  reverse(s.begin(), s.end());
  for (int i = 0; i < n; ++i)
    s[i] -= '0';
  memset(M, -1, sizeof M);
  cout << DP(0, 1) << endl;
}
