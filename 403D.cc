#include <iostream>
#include <cstring>
#define MAXN 1001
#define MOD 1000000007

using namespace std;

int n, k;

int M[47][MAXN][MAXN];
int DP(int id, int ac, int l) {
  if (id == 1)
    return ac >= l;
  if (l > ac)
    return 0;
  int &ans = M[id][ac][l];
  if (ans >= 0)
    return ans;
  ans = DP(id-1, ac-l, l+1) + DP(id, ac, l+1);
  if (ans >= MOD)
    ans -= MOD;
  return ans;
}

typedef long long ll;
ll F[MAXN];
int C[MAXN][MAXN];

void pre() {
  memset(M, -1, sizeof M);
  F[0] = 1;
  C[0][0] = 1;
  for (int i = 1; i < MAXN; ++i) {
    F[i] = F[i-1] * i;
    if (F[i] >= MOD)
      F[i] %= MOD;
    C[i][0] = C[i][i] = 1;
    for (int j = 1; j < i; ++j) {
      C[i][j] = C[i-1][j-1] + C[i-1][j];
      if (C[i][j] >= MOD)
        C[i][j] -= MOD;
    }
  }
}

int main() {
  ios::sync_with_stdio(0);
  pre();
  int tc;
  cin >> tc;
  while (tc--) {
    cin >> n >> k;
    ll ans = 0, t;
    for (int i = (k-1)*k/2; i <= n - k; ++i) {
      t = (ll)DP(k, i, 0) * C[n-i][k];
      if (t >= MOD)
        t %= MOD;
      ans += t;
      if (ans >= MOD)
        ans -= MOD;
    }
    ans *= F[k];
    if (ans >= MOD)
      ans %= MOD;
    cout << ans << endl;
  }
}
