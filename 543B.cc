#include <iostream>
#include <queue>
#include <cstring>
#define MAXN 3001

using namespace std;

int dist[MAXN][MAXN];
bool seen[MAXN];
vector<int> adj[MAXN];

void bfs(int root, int d[]) {
  memset(seen, 0, sizeof seen);
  queue<int> Q;
  Q.push(root);
  d[root] = 0;
  seen[root] = true;
  while (not Q.empty()) {
    int u = Q.front();
    Q.pop();
    for (int v : adj[u]) {
      if (not seen[v]) {
        seen[v] = true;
        d[v] = d[u] + 1;
        Q.push(v);
      }
    }
  }
}

int s[2], t[2], l[2], n, m;
int d[2];

int separate() {
  if (d[0] > l[0] or d[1] > l[1])
    return -1;
  return max(0, m - dist[s[0]][t[0]] - dist[s[1]][t[1]]);
}

int join() {
  int ans = 0, tmp;
  bool valid;
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j) {
      valid = true;
      tmp = m - dist[i][j];
      for (int k = 0; k < 2; ++k) {
        d[k] = min(dist[i][s[k]] + dist[j][t[k]],
                   dist[i][t[k]] + dist[j][s[k]]);
        if (d[k] + dist[i][j] > l[k])
          valid = false;
        tmp -= d[k];
      }
      if (valid)
        ans = max(ans, tmp);
    }
  return ans;
}

int main() {
  cin >> n >> m;
  int u, v;
  for (int i = 0; i < m; ++i) {
    cin >> u >> v;
    --u; --v;
    adj[u].push_back(v);
    adj[v].push_back(u);
  }
  for (int i = 0; i < 2; ++i) {
    cin >> s[i] >> t[i] >> l[i];
    --s[i]; --t[i];
  }
  for (u = 0; u < n; ++u)
    bfs(u, dist[u]);
  for (int i = 0; i < 2; ++i)
    d[i] = dist[s[i]][t[i]];
  int ans = separate();
  if (ans != -1)
    ans = max(ans, join());
  cout << ans << endl;
  return 0;
}

