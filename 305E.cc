#include <iostream>
#include <vector>
#include <bitset>
#define MAXN 5005

using namespace std;

typedef pair<int, int> pii;
int G[MAXN];
bitset<MAXN> B;

int main() {
  string s;
  cin >> s;
  int ini = -1, cnt = 0, maxi = 0;
  vector<pii> A;
  for (int i = 0; i < s.size() - 2; i++) {
    if (s[i] == s[i+2]) {
      if (ini == -1)
        ini = i;
      cnt ++;
    } else if (ini != -1) {
      A.push_back(pii(ini, cnt));
      maxi = max(maxi, cnt);
      ini = -1;
      cnt = 0;
    }
  }
  if (ini != -1) {
    A.push_back(pii(ini, cnt));
    maxi = max(maxi, cnt);
  }
  G[0] = 0;
  for (int n = 1; n <= maxi; n++) {
    B.reset();
    for (int i = 0; i < n; i++) {
      int txor = (i ? G[i-1] : 0) ^ (n-i-1 ? G[n-i-2] : 0);
      if (txor < MAXN)
        B.set(txor);
    }
    int g;
    for (g = 0; B[g]; g++) {}
    G[n] = g;
  }
  int ans = 0;
  for (int i = 0; i < A.size(); i++) {
    //cout << A[i].first << " " << A[i].second << endl;
    ans ^= G[A[i].second];
  }
  if (ans == 0) {
    cout << "Second" << endl;
    return 0;
  }
  cout << "First" << endl;
  int t;
  for (int k = 0; k < A.size(); k++) {
    t = G[A[k].second] ^ ans;
    int n = A[k].second;
    for (int i = 0; i < n; i++)
      if(((i ? G[i-1] : 0) ^ (n-i-1 ? G[n-i-2] : 0)) == t) {
        cout << A[k].first + i + 2 << endl;
        return 0;
      }
  }
}
