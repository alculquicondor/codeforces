#include <cstdio>

int main() {
  int n, m, h, gh, x, sum = 0;
  scanf("%d %d %d", &n, &m, &h);
  h --;
  for (int i = 0; i < m; i++) {
    scanf("%d", &x);
    sum += x;
    if (i == h)
      gh = x;
  }
  sum --;
  gh --;
  n --;
  if (sum < n) {
    puts("-1");
  } else {
    double ans = 1;
    for (int i = 0; i < n; i++) {
      ans *= sum - gh - i;
      ans /= sum - i;
    }
    printf("%.9f\n", 1 - ans);
  }
}
