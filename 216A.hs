import Control.Applicative
tri a b c x = sum [a+(min (min (b-1) (c-1)) i) | i <- [0..x]]
hex a b c = (tri a b c (b-1)) + (tri a b c (c-2)) 

main = do
    [a, b, c] <- (map read . words) <$> getLine
    print $ hex a b c
