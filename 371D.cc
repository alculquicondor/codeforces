#include <cstdio>
#define MAXN 200005

using namespace std;

int C[MAXN], L[MAXN], N[MAXN];
inline int findSet(int i) {
    return (N[i] == i) ? i : (N[i] = findSet(N[i]));
}
inline void unionSet(int i, int j) {
    N[findSet(i)] = findSet(j);
}

int main() {
  int n, m;
  scanf("%d", &n);
  for (int i = 0; i < n; ++i) {
    scanf("%d", C+i);
    N[i] = i;
  }
  N[n] = n;
  scanf("%d", &m);
  int op, k, a;
  while (m--) {
    scanf("%d %d", &op, &k);
    if (op == 1) {
      scanf("%d", &a);
      --k;
      while (k < n and a) {
        if (a > C[k] - L[k]) {
          a -= C[k] - L[k];
          L[k] = C[k];
          unionSet(k, k+1);
          k = findSet(k);
        } else {
          L[k] += a;
          a = 0;
        }
      }
    } else {
      printf("%d\n", L[--k]);
    }
  }
}

