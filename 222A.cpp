#include <cstdio>

int main() {
    int n, k;
    scanf("%d %d", &n, &k);
    int A[100001];
    for(int i=0; i<n; i++)
        scanf("%d", &A[i]);
    bool poss = true;
    for(int i=k; i<n; i++)
        if(A[i]!=A[i-1]) {
            puts("-1");
            return 0;
        }
    int i;
    for(i = k-2; i>=0 and A[i]==A[k-1]; i--);
    printf("%d\n", i+1);
}
