#include <iostream>
#include <algorithm>
#define MAXN 500001

using namespace std;

int A[MAXN], n;

int main() {
  std::ios::sync_with_stdio(false);
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  sort(A, A+n);
  int q = n / 2, ans = n;
  for (int i = n - 1; q; --i) {
    while (q and A[i] < A[q-1] * 2)
      --q;
    if (q) {
      --ans;
      --q;
    }
  }
  cout << ans << endl;
}

