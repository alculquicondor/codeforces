#include <iostream>

using namespace std;

int solve(string s, int &hs) {
  int op = 0, cl = 0;
  hs = 0;
  for (char c : s) {
    if (c == '(')
      ++op;
    else if (c == ')')
      ++cl;
    else
      ++hs;
  }
  int dif = op - cl;
  if (dif < hs)
    return 0;
  op = 0;
  cl = dif;
  int q = hs;
  for (char c : s) {
    if (c == '(') {
      ++op;
    } else if (c == ')') {
      --op;
    } else {
      if (--q)
        --op;
      else
        op -= dif;
      --dif;
    }
    if (op < 0)
      return 0;
  }
  return cl;
}

int main() {
  string s;
  cin >> s;
  int hs, t = solve(s, hs);
  if (t) {
    for (int i = 1; i < hs; ++i)
      cout << 1 << endl;
    cout << t + 1 - hs << endl;
  } else {
    cout << -1 << endl;
  }
  return 0;
}
