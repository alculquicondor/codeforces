#include <iostream>
#define MAXN 2002

using namespace std;
typedef pair<int, int> pii;
typedef long long ll;

int M[MAXN][MAXN];
ll C[MAXN<<1], R[MAXN<<1], n;

inline ll cnt(int i, int j) {
  return R[i-j+n] + C[i+j] - M[i][j];
}

pii solve(bool odd) {
  ll ans = -1, t;
  int ii, jj;
  for (int i = 0; i < n; ++i)
    for (int j = (i & 1) ^ odd; j < n; j += 2) {
      t = cnt(i, j);
      if (ans < t) {
        ans = t;
        ii = i;
        jj = j;
      }
    }
  return pii(ii, jj);
}

int main() {
  std::ios::sync_with_stdio(false);
  cin >> n;
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j) {
      cin >> M[i][j];
      R[i-j+n] += M[i][j];
      C[i+j] += M[i][j];
    }
  pii a = solve(false), b = solve(true);
  cout << cnt(a.first, a.second) + cnt(b.first, b.second) << endl;
  cout << a.first+1 << ' ' << a.second+1 << ' ' << b.first+1 << ' ' << b.second+1
    << endl;
}

