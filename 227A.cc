#include <iostream>

using namespace std;

template<typename T>
struct p2d {
  T x, y;
  p2d(T x, T y) : x(x), y(y) {}
  p2d() {}

  p2d operator - (const p2d &a) {
    return p2d(x-a.x, y-a.y);
  }

  T operator % (const p2d &a) {
    return x * a.y - y * a.x;
  }

  T operator * (const p2d &a) {
    return x * a.x + y * a.y;
  }
};

int main() {
  p2d<long long> a, b, c;
  cin >> a.x >> a.y;
  cin >> b.x >> b.y;
  cin >> c.x >> c.y;
  long long t = (c-b) % (a-b);
  if (t == 0)
    cout << "TOWARDS";
  else if (t > 0)
    cout << "LEFT";
  else
    cout << "RIGHT";
  cout << endl;
}
