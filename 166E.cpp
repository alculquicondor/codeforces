#include<cstdio>
#define MOD 1000000007LL

using namespace std;

int M[10000001][2];

int main() {
    int n;
    scanf("%d",&n);
    M[0][0]=1;
    M[0][1]=0;
    for(int i=1;i<=n;i++)
    {
        M[i][0]=(3LL*M[i-1][1])%MOD;
        M[i][1]=(2LL*M[i-1][1]+M[i-1][0])%MOD;
    }
    printf("%d\n",M[n][0]);
}
