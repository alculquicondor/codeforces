#include <cstdio>

typedef long long ll;

#define MAXN 524288
#define center(x, y) (x + y) >> 1
#define left(x) (x << 1) + 1

int n, A[100005];
namespace sum {
ll st[MAXN];

void stupdate(int i, ll up,
    int idx = 0, int l = 0, int r = n - 1) {
  int L = left(idx), R = L + 1;
  if (r < i or l > i)
    return;
  if (l == r) {
    st[idx] = up;
    return;
  }
  int mid = center(l, r);
  stupdate(i, up, L, l, mid);
  stupdate(i, up, R, mid + 1, r);
  st[idx] = st[L] + st[R];
}

ll stq(int i, int j, int idx = 0, int l = 0, int r = n - 1) {
  int L = left(idx), R = L + 1;
  if (l >= i and r <= j)
    return st[idx];
  if (r < i or l > j)
    return 0;
  int mid = center(l, r);
  return stq(i, j, L, l, mid) + stq(i, j, R, mid+1, r);
}

void stbuild(int idx = 0, int l = 0, int r = n - 1) {
  int L = left(idx), R = L + 1;
  if (l == r) {
    st[idx] = A[l];
    return;
  }
  int mid = center(l, r);
  stbuild(L, l, mid);
  stbuild(R, mid+1, r);
  st[idx] = st[L] + st[R];
}
}

namespace maxi {
int st[MAXN];

void stupdate(int i,
    int idx = 0, int l = 0, int r = n - 1) {
  int L = left(idx), R = L + 1;
  if (r < i or l > i)
    return;
  if (l == r) {
    return;
  }
  int mid = center(l, r);
  stupdate(i, L, l, mid);
  stupdate(i, R, mid + 1, r);
  st[idx] = A[st[L]] > A[st[R]] ? st[L] : st[R];
}

int stq(int i, int j, int idx = 0, int l = 0, int r = n - 1) {
  int L = left(idx), R = L + 1;
  if (l >= i and r <= j)
    return st[idx];
  if (r < i or l > j)
    return -1;
  int mid = center(l, r);
  int q1 = stq(i, j, L, l, mid),
      q2 = stq(i, j, R, mid+1, r);
  if (q1 != -1 and q2 != -1)
    return A[q1] > A[q2] ? q1 : q2;
  return q1 != -1 ? q1 : q2;
}

void stbuild(int idx = 0, int l = 0, int r = n -1) {
  int L = left(idx), R = L + 1;
  if (l == r) {
    st[idx] = l;
    return;
  }
  int mid = center(l, r);
  stbuild(L, l, mid);
  stbuild(R, mid+1, r);
  st[idx] = A[st[L]] > A[st[R]] ? st[L] : st[R];
}
}

int main() {
  int m;
  scanf("%d %d", &n, &m);
  for (int i = 0; i < n; ++i)
    scanf("%d", A+i);
  sum::stbuild();
  maxi::stbuild();
  int op, l, r, x;
  while (m--) {
    scanf("%d", &op);
    if (op < 3) {
      scanf("%d %d", &l, &r);
      --l, --r;
      if (op == 1) {
        printf("%I64d\n", sum::stq(l, r));
      } else {
        scanf("%d", &x);
        int m = maxi::stq(l, r);
        while (A[m] >= x) {
          A[m] %= x;
          sum::stupdate(m, A[m]);
          maxi::stupdate(m);
          m = maxi::stq(l, r);
        }
      }
    } else {
      scanf("%d %d", &l, &x);
      A[--l] = x;
      sum::stupdate(l, x);
      maxi::stupdate(l);
    }
  }
  return 0;
}
