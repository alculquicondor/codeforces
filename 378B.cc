#include <iostream>
#include <vector>

using namespace std;

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  vector<int> A(n), B(n);
  for (int i = 0; i < n; ++i)
    cin >> A[i] >> B[i];
  int j = 0;
  for (int i = 0; i < n; ++i) {
    while (j < n and B[j] < A[i])
      ++j;
    cout << (i + 1 + min(i+1, j) <= n);
  }
  cout << endl;
  j = 0;
  for (int i = 0; i < n; ++i) {
    while (j < n and A[j] < B[i])
      ++j;
    cout << (i + 1 + min(i+1, j) <= n);
  }
  cout << endl;
}
