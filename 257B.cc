#include <iostream>

using namespace std;
typedef pair<int, int> pii;

pii play(int n, int m, bool l) {
  int A[] = {n, m}, a = 0, b = 0;
  bool jug = 1;
  while (A[0] + A[1]) {
    if (jug) {
      if (A[not l]) {
        A[not l] --;
        l = not l;
        b ++;
      } else {
        A[l] --;
        a ++;
      }
    } else {
      if (A[l]) {
        A[l] --;
        a ++;
      } else {
        A[not l] --;
        l = not l;
        b ++;
      }
    }
    jug = not jug;
  }
  return pii(a, b);
}

int main() {
  int n, m;
  cin >> n >> m;
  pii ans = max(play(n-1, m, 0), play(n, m-1, 1));
  cout << ans.first << " " << ans.second << endl;
}
