#include<queue>
#include<cstdio>

using namespace std;
int n,l;
vector<pair<int,int> > adj[100001],adj2[100001];
int d[100001];

int BFS(int s)
{
	queue<int> Q;
	int pi[n],visit[n];
	for(int i=0;i<n;i++)
		d[i]=1<<30,visit[i]=0;;
	d[s]=0;
	Q.push(s);
	int ans=0;
	while(not Q.empty())
	{
		int u=Q.front(),v,w;
		Q.pop();
		if(visit[u])
			continue;
		visit[u]=1;
		if(d[u]==l)
			ans++;
		for(auto pp:adj[u])
		{
			v=pp.first;
			w=pp.second;
			if(d[v]>d[u]+w)
			{
				d[v]=d[u]+w;
				pi[v]=u;
				Q.push(v);
			}
		}
	}
	return ans;
}

int main()
{
	int m,s;
	scanf("%d %d %d",&n,&m,&s);
	s--;
	int u,v,w;
	for(int i=0;i<m;i++)
	{
		scanf("%d %d %d",&u,&v,&w);
		u--,v--;
		adj[u].push_back(make_pair(v,w));
		adj[v].push_back(make_pair(u,w));
		adj2[u].push_back(make_pair(v,w));
	}
	scanf("%d",&l);
	int ans=BFS(s);
	//printf("#%d\n",ans);
	for(int u=0;u<n;u++)
		for(auto pp:adj2[u])
		{
			v=pp.first;
			w=pp.second;
			bool a1,a2;
			if(a1=(l>d[u] and l<d[u]+w))
				ans++;
			if(a2=(l>d[v] and l<d[v]+w))
				ans++;
			if(a1 and a2 and l-d[u]+l-d[v]==w)
					ans--;
		}
	printf("%d\n",ans);
}
