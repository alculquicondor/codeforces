#include <iostream>
#include <algorithm>

using namespace std;

typedef long long ll;

void minall(ll l, ll r, int k) {
  ll ans = 1LL << 60, m;
  for (int mask = 1; mask < (1 << (r-l+1)); ++mask)
    if (__builtin_popcount(mask) <= k) {
      ll tmp = 0;
      for (int i = 0; l + i <= r; ++i)
        if (mask & (1<<i))
          tmp ^= l + i;
      if (tmp < ans) {
        ans = tmp;
        m = mask;
      }
    }
  cout << ans << endl << __builtin_popcount(m) << endl;
  bool first = true;
  for (int i = 0; l + i <= r; ++i)
    if (m& (1<<i)) {
      if (not first)
        cout << ' ';
      first = false;
      cout << l + i;
    }
  cout << endl;
}

void search3(ll l, ll r) {
  int k = 0;
  ll x, y, z;
  for (x = 1; x <= r; x = (x << 1) | 1, ++k) {
    if (x >= l) {
      z = (3LL << k);
      y = z - 1;
      if (z <= r) {
        cout << 0 << endl << 3 << endl;
        cout << x << ' ' << y << ' ' << z << endl;
        return;
      }
    }
  }
  minall(l, l+4, 3);
}

void solve(ll l, ll r, int k) {
  if (r - l + 1 < 5) {
    minall(l, r, k);
    return;
  }
  if (k == 3)
    search3(l, r);
  else
    minall(l, l+4, k);
}

int main() {
  ll l, r;
  int k;
  cin >> l >> r >> k;
  solve(l, r, k);
}
