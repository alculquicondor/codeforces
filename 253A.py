import sys
sys.stdin=open('input.txt','r')
sys.stdout=open('output.txt','w')
n, m = [int(x) for x in input().split()]
print('BG' * m + 'B' * (n-m) if n > m else 'GB' * n + 'G' * (m-n))
