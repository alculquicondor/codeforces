#include <iostream>
#include <vector>

using namespace std;

int k, d;
int dp(int n, vector<int> & tab, bool ok,int &r)
{
  if (tab[n])
  {
    if (ok) {
      r += tab[n];
    }
  }
  else
  {
    int ac = 0;
    for (int i = 1; i <= k && n - i >= 0; i++)
    {
      ac += dp(n - i, tab, i >= d, r);
    }
    tab[n] = ac;
  }
  return tab[n];
}


int main()
{
  int n,res=0;
  cin >> n;
  cin >> k;
  cin >> d;

  vector<int> v(n+1, 0);
  v[0] = 1;
  dp(n, v, false,res);
  cout << res << endl;
  return 0;
}
