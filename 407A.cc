#include <iostream>
#include <algorithm>

using namespace std;

typedef long long ll;
ll sqrt(ll n) {
  ll x = n, y = (n+1) / 2;
  while (x > y) {
    x = y;
    y = (x*x+n) / (2*x);
  }
  return x;
}

void print(int x, int y) {
  cout << x << " " << y << endl;
}

int main() {
  int a, b, u, v;
  cin >> a >> b;
  bool found = false;
  for (int i = 1; not found and i < a; ++i) {
    int t = a * a - i * i, x = sqrt(t), g;
    if (x * x == t) {
      g = __gcd(x, i);
      u = x / g, v = i / g;
      int m2 = u * u + v * v, m = sqrt(m2);
      if (m * m == m2 and b % m == 0 and i != u * b / m) {
        found = true;
        cout << "YES" << endl;
        print(0, 0);
        print(x, i);
        print(-v * b / m, u * b / m);
      }
    }
  }
  if (not found)
    cout << "NO" << endl;
}
