#include <iostream>
#include <algorithm>
#define MAXN 100004

using namespace std;

int LIS[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  int ans = 0, x, p;
  for (int i = 0; i < n; i++) {
    cin >> x;
    p = lower_bound(LIS, LIS+ans, x) - LIS;
    if (p == ans)
      ans ++;
    LIS[p] = x;
  }
  cout << ans << endl;
}
