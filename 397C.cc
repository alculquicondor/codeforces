#include <iostream>
#include <map>
#define MAXN 15550
#define MOD 1000000007

using namespace std;

typedef long long ll;
map<int, int> F;

void factor(int x) {
  for (int i = 2; i * i <= x; ++i) {
    while (x % i == 0) {
      ++F[i];
      x /= i;
    }
  }
  if (x > 1)
    ++F[x];
}

ll f[MAXN];

inline void san(ll &x) {
  if (x >= MOD)
    x %= MOD;
}

inline ll potmod(ll a, int e) {
  ll r = 1;
  while (e) {
    if (e & 1) {
      r *= a;
      san(r);
    }
    e >>= 1;
    a *= a;
    san(a);
  }
  return r;
}

inline ll inv(ll x) {
  return potmod(x, MOD-2);
}

ll comb(int n, int m) {
  ll div = f[m] * f[n-m];
  san(div);
  return (f[n] * inv(div)) % MOD;
}

int main() {
  std::ios::sync_with_stdio(false);
  f[0] = 1;
  for (int i = 1; i < MAXN; ++i) {
    f[i] = f[i-1] * i;
    if (f[i] >= MOD)
      f[i] %= MOD;
  }
  int n, x;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> x;
    factor(x);
  }
  ll ans = 1;
  for (auto x: F) {
    ans *= comb(x.second+n-1, x.second);
    san(ans);
  }
  cout << ans << endl;
}
