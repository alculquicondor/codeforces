#include <iostream>
#include <vector>

using namespace std;

inline bool operator==(vector<int> &a, vector<int> &b) {
    if(a.size()!=b.size())
        return false;
    for(int i=0; i<a.size(); i++)
        if(a[i]!=b[i])
            return false;
    return true;
}

int main() {
    ios_base::sync_with_stdio(false);
    int n, K;
    cin>>n>>K;
    vector<int> q(n), s(n), A(n);
    for(int i=0; i<n; i++) {
        A[i] = i+1;
        cin>>q[i];
    }
    for(int i=0; i<n; i++)
        cin>>s[i];
    if(s==A) {
        cout<<"NO\n";
        return 0;
    }
    if(K>1) {
        vector<int> B(n);
        for(int i=0; i<n; i++)
            B[i] = A[q[i]-1];
        bool direct = s==B;
        for(int i=0; i<n; i++)
            B[q[i]-1] = A[i];
        bool inverse = s==B;
        if(direct and inverse) {
            cout<<"NO\n";
            return 0;
        }
    }
    //direct
    for(int k=1; k<=K; k++) {
        vector<int> B(n);
        for(int i=0; i<n; i++)
            B[i] = A[q[i]-1];
        A = B;
        if((K-k)%2==0 and s==A) {
            //cerr<<"1: "<<k<<"\n";
            cout<<"YES\n";
            return 0;
        }
    }
    //inverse
    for(int i=0; i<n; i++)
        A[i] = i+1;
    for(int k=1; k<=K; k++) {
        vector<int> B(n);
        for(int i=0; i<n; i++)
            B[q[i]-1] = A[i];
        A = B;
        if((K-k)%2==0 and s==A) {
            //cerr<<"2: "<<k<<"\n";
            cout<<"YES\n";
            return 0;
        }
    }
    cout<<"NO\n";
}
