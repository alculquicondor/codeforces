#include <cstdio>
#include <vector>
#include <algorithm>
#include <cstring>
#define MAXN 200002

using namespace std;
typedef pair<int, int> pii;
typedef long long ll;

int n;
vector<int> E[MAXN];

int P[MAXN], Q[MAXN], A[MAXN];
ll ans[MAXN];
pii lr[MAXN];

bool order(int i, int j) {
  return lr[i].second < lr[j].second;
}

ll BIT[MAXN];
inline ll bitq(int idx) {
  ll ans = 0;
  while (idx) {
    ans += BIT[idx];
    idx -= idx&-idx;
  }
  return ans;
}
inline ll bitq(int a, int b) {
  return bitq(b) - bitq(a-1);
}
inline void bitup(int idx, int add = 1) {
  while (idx <= n) {
    BIT[idx] += add;
    idx += idx & -idx;
  }
}

void update(int i) {
  int x = A[i], p;
  for (int j = 0; j < E[x].size(); j++) {
    p = P[E[x][j]];
    if (p <= i)
      bitup(p);
  }
}

void pre() {
  for (int i = 1; i <= n; i++) {
    for (int j = 2 * i; j <= n; j+= i) {
      E[i].push_back(j);
      E[j].push_back(i);
    }
    E[i].push_back(i);
  }
}

int main() {
  int q;
  scanf("%d %d", &n, &q);
  pre();
  for (int i = 1; i <= n; i++) {
    scanf("%d", &A[i]);
    P[A[i]] = i;
  }
  int nowr, nowl, lastr = 0;
  memset(BIT, 0, sizeof BIT);
  for (int i = 0; i < q; i++) {
    scanf("%d %d", &lr[i].first, &lr[i].second);
    Q[i] = i;
  }
  sort(Q, Q+q, order);
  for (int i = 0; i < q; i++) {
    nowl = lr[Q[i]].first, nowr = lr[Q[i]].second;
    for (int j = lastr + 1; j <= nowr; j++)
      update(j);
    ans[Q[i]] = bitq(nowl, nowr);
    lastr = nowr;
  }
  for (int i = 0; i < q; i++)
    printf("%I64d\n", ans[i]);
}
