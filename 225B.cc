#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
typedef long long ll;

int main() {
  ll F[50];
  int n, k, t = 1;
  cin >> n >> k;
  F[0] = 1;
  for (int i = 1; true; i++) {
    F[i] = 0;
    for (int j = i - 1; j >= max(0, i-k); j--)
      F[i] += F[j];
    if (F[i] > n)
      break;
    t ++;
  }
  vector<int> ans;
  while (n) {
    int lo = 0, hi = t, mid;
    while (hi - lo > 1) {
      mid = (lo+hi) / 2;
      if (F[mid] > n)
        hi = mid;
      else
        lo = mid;
    }
    ans.push_back(F[lo]);
    n -= F[lo];
  }
  if (ans.size() == 1) {
    cout << 2 << endl;
    cout << "0 " << ans[0] << endl;
    return 0;
  }
  cout << ans.size() << endl;
  cout << ans[0];
  for (int i = 1; i < ans.size(); i++)
    cout << " " << ans[i];
  cout << endl;
}
