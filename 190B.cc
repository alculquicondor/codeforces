#include <vector>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <cstdio>

using namespace std;

typedef long long type;

template<typename T>
struct p2d {
  T x, y;
  p2d(T x = 0, T y = 0) : x(x), y(y) {}
  p2d(const p2d &a) : x(a.x), y(a.y) {}
  p2d operator - (const p2d &a) const {
    return p2d(x-a.x, y-a.y);
  }
  p2d operator + (const p2d &a) const {
    return p2d(x+a.x, y+a.y);
  }
  p2d operator * (T k) const {
    return p2d(x*k, y*k);
  }
  p2d operator / (T k) const {
    return p2d(x/k, y/k);
  }
  void operator -= (const p2d &a) {
    x -= a.x, y -= a.y;
  }
  void operator += (const p2d &a) {
    x += a.x, y += a.y;
  }
  void operator *= (T k) {
    x *= k, y *= k;
  }
  void operator /= (T k) {
    x /= k, y /= k;
  }
  bool operator == (const p2d &a) const {
    return x == a.x and y == a.y;
  }
  bool operator != (const p2d &a) const {
    return x != a.x or y != a.y;
  }
  bool operator < (const p2d &a) const {
    if (x == a.x)
      return y < a.y;
    return x < a.x;
  }

  T norm2() const {
    return x * x + y * y;
  }
  T norm() const {
    return sqrt(norm2());
  }
  void reduce() {
    T g = __gcd(x, y);
    x /= g, y /= g;
  }
  p2d ort() const {
    return p2d(-y, x);
  }
  T operator % (const p2d &a) const {
    return x * a.y - y * a.x;
  }
  T operator * (const p2d &a) const {
    return x * a.x + y * a.y;
  }
};
typedef p2d<type> pt;

int main() {
    pt A[2];
    type r[2];
    for (int i = 0; i < 2; ++i)
        cin >> A[i].x >> A[i].y >> r[i];
    type d2 = (A[0]-A[1]).norm2(), sr = r[0] + r[1];
    if (d2 > sr*sr) {
        double ans = sqrt((double)d2) - sr;
        printf("%.7f\n", ans / 2);
    } else if (sqrt((double)d2)+min(r[0], r[1]) < max(r[0], r[1])) {
        double ans = abs(r[0]-r[1]) - sqrt((double)d2);
        printf("%.7f\n", ans / 2);
    } else {
        cout << 0 << endl;
    }
}
