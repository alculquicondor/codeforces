#include <iostream>
#include <algorithm>
#include <vector>

using namespace std;
typedef long long ll;

ll sum(vector<int> &A) {
  ll ans = 0;
  for (int i = 0; i < A.size(); i++)
    ans += A[i];
  return ans;
}

int main() {
  ios::sync_with_stdio(0);
  int n, neg = 0;
  cin >> n;
  vector<int> A(2*n-1);
  for (int i = 0; i < 2*n-1; i++) {
    cin >> A[i];
    if (A[i] < 0) {
      neg ++;
      A[i] *= -1;
    }
  }
  sort(A.begin(), A.end());
  ll ans = sum(A);
  if (n % 2 == 0 and neg % 2 == 1)
    ans -= 2 * A[0];
  cout << ans << endl;
}
