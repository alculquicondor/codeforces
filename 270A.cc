#include <iostream>

using namespace std;

int main() {
  int tc, a;
  cin >> tc;
  while (tc --) {
    cin >> a;
    if (360 % (180-a) == 0)
      cout << "YES";
    else
      cout << "NO";
    cout << endl;
  }
}
