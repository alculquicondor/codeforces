#include <iostream>

using namespace std;
typedef long long ll;

int main() {
  ll x1, x2, a, b;
  cin >> x1 >> x2 >> a >> b;
  string ans;
  ll x0;
  if (x1 + a <= x2 and x2 <= x1 + b) {
    ans = "FIRST";
    x0 = x2;
  } else if (a <= 0 and 0 <= b) {
    ans = "DRAW";
  } else {
    bool change = false;
    if (x1 > x2) { // correction
      swap(x1, x2);
      ll t = b;
      b = -a;
      a = -t;
      change = true;
    }
    if (b < 0) {
      ans = "DRAW";
    } else {
      ll dist = (x2-x1) % (a+b);
      if (dist == 0) {
        ans = "SECOND";
      } else if (dist < a or dist > b) {
        ans = "DRAW";
      } else {
        ans = "FIRST";
        if (change)
          x0 = x2 - dist;
        else
          x0 = x1 + dist;
      }

      /*
         ll dist = x2 - x1,
         k1 = (dist+a+b-1) / (a+b),
         k2 = (dist+a-1) / (a+b);
         if (k1*(b+a) < k2*(b+a)+b) {
         ans = "SECOND";
         } else {
         ans = "FIRST";
         x0 = max(x2-(k1-1)*(b+a), x1+a);
         if (change)
         x0 = x2 - (x0 - x1);
         }
         */
    }
  }
  cout << ans << endl;
  if (ans == "FIRST")
    cout << x0 << endl;
}

