#include <iostream>
#include <cstring>
#include <queue>
#include <algorithm>

using namespace std;
typedef long long ll;

int lcm(int k) {
  int ans = 1;
  for (int i = 2; i <= k; i++)
    ans *= i / __gcd(i, ans);
  return ans;
}
#define MAXN 360370
int d[MAXN];
bool visit[MAXN];
int k;

int bfs(int a, int b) {
  int v;
  visit[a] = true;
  d[a] = 0;
  queue<int> Q;
  Q.push(a);
  while (not Q.empty()) {
    a = Q.front();
    Q.pop();
    v = a - 1;
    if (not visit[v] and v >= 0) {
      visit[v] = true;
      d[v] = d[a] + 1;
      Q.push(v);
    }
    for (int i = 2; i <= k; i++) {
      v = a - a % i;
      if (not visit[v] and v >= 0) {
        visit[v] = true;
        d[v] = d[a] + 1;
        Q.push(v);
      }
    }
  }
  return d[b];
}

int main() {
  ll a, b;
  cin >> a >> b >> k;
  int inter = lcm(k);
  ll ans;
  memset(visit, 0, sizeof visit);
  if (a / inter == b / inter) {
    ans = bfs(a % inter, b % inter);
  } else {
    ll desp = (a/inter)-(b/inter);
    ans += (desp-1) * bfs(inter-1, 0) + desp;
    ans += d[b % inter];
    memset(visit, 0, sizeof visit);
    ans += bfs(a % inter, 0);
  }
  cout << ans << endl;
}
