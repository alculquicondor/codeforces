#include <cstdio>
#include <set>
using namespace std;

int main() {
    int n, x;
    scanf("%d", &n);
    set<int> ans, now, next;
    while(n--) {
        scanf("%d", &x);
        next.clear();
        ans.insert(x);
        next.insert(x);
        for(int v: now) {
            ans.insert(v|x);
            next.insert(v|x);
        }
        now = next;
    }
    printf("%d\n", ans.size());
}
