#include <iostream>
#include <algorithm>
#include <cstring>
#include <stack>
#include <vector>
#define MAXN 100005

using namespace std;

struct node {
  int id, v;
  bool operator < (const node &x) const {
    return v < x.v;
  }
  bool operator < (int v) const {
    return this->v < v;
  }
} A[MAXN];
bool operator < (int v, const node & x) {
  return v < x.v;
}

typedef pair<int, bool> pib;
vector<pib> adj[MAXN];
int degree[MAXN], color[MAXN], n;

bool solve_equal(int a) {
  memset(color, 0, sizeof color);
  for (int i = 0; i < n; ++i)
    if (not binary_search(A, A+n, a-A[i].v))
      return false;
  return true;
}

bool solve_diff(int a, int b) {
  node *p;
  for (int i = 0; i < n; ++i) {
    p = lower_bound(A, A+n, a-A[i].v);
    if (p < A + n and p->v == a-A[i].v) {
      adj[A[i].id].push_back({p->id, 0});
      ++degree[p->id];
    }
    p = lower_bound(A, A+n, b-A[i].v);
    if (p < A + n and p->v == b-A[i].v) {
      adj[A[i].id].push_back({p->id, 1});
      ++degree[p->id];
    }
  }
  stack<int> S;
  for (int i = 0; i < n; ++i)
    if (degree[i] == 1)
      S.push(i);
  while (not S.empty()) {
    int u = S.top(), v = -1;
    bool c;
    S.pop();
    if (color[u] != -1)
      continue;
    for (pib t : adj[u])
      if (color[t.first] == -1) {
        v = t.first;
        c = t.second;
        break;
      }
    if (v == -1)
      return false;
    color[u] = color[v] = c;
    for (pib t : adj[v])
      if (--degree[t.first] == 1)
        S.push(t.first);
  }
  for (int i = 0; i < n; ++i)
    if (color[i] == -1)
      return false;
  return true;
}

int main() {
  std::ios::sync_with_stdio(false);
  memset(color, -1, sizeof color);
  int a, b;
  cin >> n >> a >> b;
  for (int i = 0; i < n; ++i) {
    A[i].id = i;
    cin >> A[i].v;
  }
  sort(A, A+n);
  if (a == b ? solve_equal(a) : solve_diff(a, b)) {
    cout << "YES" << endl;
    cout << color[0];
    for (int i = 1; i < n; ++i)
      cout << ' ' << color[i];
  } else {
    cout << "NO";
  }
  cout << endl;
  return 0;
}

