#include <iostream>
#define MAXN 101

using namespace std;

int main() {
  int n, m, x, ming = 100, maxg = 0, minf = 100;
  cin >> n >> m;
  for (int i = 0; i < n; i++) {
    cin >> x;
    ming = min(ming, x);
    maxg = max(maxg, x);
  }
  for (int i = 0; i < m; i++) {
    cin >> x;
    minf = min(minf, x);
  }
  int ans = max(2*ming, maxg);
  if (ans < minf)
    cout << ans;
  else
    cout << -1;
  cout << endl;
}
