#include <iostream>
#define MAXN 100001

using namespace std;

int main() {
  ios::sync_with_stdio(0);
  int n, k;
  cin >> n >> k;
  if (n == k) {
    cout << -1 << endl;
    return 0;
  }
  k = n - k;
  cout << k;
  for (int i = 1; i < k; ++i)
    cout << " " << i;
  for (int i = k; i < n; ++i)
    cout << " " << i+1;
  cout << endl;
}
