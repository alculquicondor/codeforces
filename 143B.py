s = raw_input().split('.')
if len(s)==1:
    intg = s[0]
    dcml = ''
else:
    intg = s[0]
    dcml = s[1]
minus = 0
if intg[0] == '-':
    intg = intg[1:]
    minus = 1
dcml = dcml[:2]
if len(dcml)<2:
    dcml += '0'*(2-len(dcml))

tmp = intg[:len(intg)%3]
for i in range(len(intg)%3,len(intg),3):
    tmp += ','+intg[i:i+3]
if tmp[0] == ',':
    intg = tmp[1:]
else:
    intg = tmp

ans = ''
if minus:
    ans += '('
ans += '$'+intg+'.'+dcml
if minus:
    ans += ')'
print ans
