#include<cstdio>
#include<set>
#include<vector>
#include<map>

using namespace std;

int main()
{
	int n,c,t;
	scanf("%d",&n);
	vector<set<int> > uni(n*(n-1)/2);
	for(int i=0;i<n*(n-1)/2;i++)
	{
		scanf("%d",&c);
		for(int j=0;j<c;j++)
		{
			scanf("%d",&t);
			uni[i].insert(t);
		}
	}
	c=0;
	vector<vector<int> > conj(n);
	map<int,int> M;
	set<int> solved;
	for(int i=0;c<n && i<n*(n-1)/2;i++)
	{
		vector<int> v1,v2;
		int id1;
		for(set<int>::iterator it=uni[i].begin();it!=uni[i].end();it++)
			if(solved.find(*it)==solved.end())
			{
				if(M.find(*it)!=M.end())
				{
					if(v1.empty() and v2.empty())
					{
						id1=M[*it];
						v1.push_back(*it);
						solved.insert(*it);
						M.erase(*it);
					}
					else
					{
						if(id1==M[*it])
							v1.push_back(*it);
						else
							v2.push_back(*it);
						solved.insert(*it);
						M.erase(*it);
					}
				}
				else
					M[*it]=i;
			}
		if(!v1.empty())
			conj[c++]=v1;
		if(!v2.empty())
			conj[c++]=v2;
		v1.clear();
		v2.clear();
	}
	if(n==2)
	{
		conj[0].push_back(*uni[0].begin());
		set<int>::iterator it=uni[0].begin();
		it++;
		conj[1]=vector<int>(it,uni[0].end());
	}
	for(int i=0;i<n;i++)
	{
		printf("%d",conj[i].size());
		for(int j=0;j<conj[i].size();j++)
			printf(" %d",conj[i][j]);
		printf("\n");
	}
	return 0;
}
