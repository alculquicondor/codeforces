#include<vector>
#include<cstdio>
#include<algorithm>

using namespace std;

int main()
{
	int n,t;
	scanf("%d %d",&n,&t);
	vector<pair<int,int> > ho(n);
	for(int i=0;i<n;i++)
		scanf("%d %d",&ho[i].first,&ho[i].second);
	sort(ho.begin(),ho.end(),[](pair<int,int> a,pair<int,int> b)
		{
			return a.first<b.first;
		});
	int ans=2,d;
	for(int i=0;i<n-1;i++)
	{
		d=(ho[i+1].first-ho[i].first)*2-(ho[i+1].second+ho[i].second);
		if(d==2*t)
			ans++;
		else if (d>2*t)
			ans+=2;
	}
	printf("%d\n",ans);
}
