#include <iostream>
#include <queue>
#define MAXN 201

using namespace std;

typedef priority_queue<int, vector<int>, greater<int> > min_heap;
typedef priority_queue<int, vector<int>, less<int> > max_heap;

int n, k, A[MAXN];

int solve(max_heap M1, max_heap M2, min_heap m, int l) {
  int limit = min(k, l);
  limit = min(limit, n - l);
  //cerr << "# " << limit << ": ";
  int ans = 0, lans = 0;
  for (int i = 0; i < limit; ++i) {
    ans -= m.top();
    m.pop();
    if (M1.empty()) {
      ans += M2.top();
      M2.pop();
    } else if (M2.empty()) {
      ans += M1.top();
      M1.pop();
    } else {
      if (M2.top() > M1.top()) {
        ans += M2.top();
        M2.pop();
      } else {
        ans += M1.top();
        M1.pop();
      }
    }
    if (ans < lans)
      break;
    lans = ans;
    //cerr << " " << ans;
  }
  //cerr << endl;
  return lans;
}

int main() {
  std::ios::sync_with_stdio(false);
  cin >> n >> k;
  int ans = -(1<<30);
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  max_heap M1;
  int sum;
  for (int i = 0; i < n; ++i) {
    max_heap M2;
    for (int j = n - 1; j >= i; --j) {
      sum = 0;
      min_heap m;
      for (int k = i; k <= j; ++k) {
        m.push(A[k]);
        sum += A[k];
      }
      //cout << i << " " << j << " " << sum << " " <<
        //sum + solve(M1, M2, m, j-i+1) << endl;
      ans = max(ans, sum + solve(M1, M2, m, j-i+1));
      M2.push(A[j]);
    }
    M1.push(A[i]);
  }
  cout << ans << endl;
}
