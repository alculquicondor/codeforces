#include <cstdio>
#include <vector>
#include <algorithm>
#include <iostream>
#include <set>
#define MAXN 100005

using namespace std;

int C[MAXN], S[MAXN];
inline int findSet(int i) {
    return (C[i] == i) ? i : (C[i] = findSet(C[i]));
}
inline void unionSet(int i, int j) {
  if (findSet(i) != findSet(j)) {
    S[findSet(j)] += S[findSet(i)];
    C[findSet(i)] = findSet(j);
  }
}

int A[MAXN], pi[MAXN];
vector<int> adj[MAXN];

int main() {
  int n, m;
  scanf("%d %d", &n, &m);
  for (int i = 0; i < n; ++i) {
    scanf("%d", A+i);
    pi[i] = i;
  }
  sort(pi, pi+n, [](int i, int j) {
      return A[i] > A[j];
    });
  int u, v;
  for (int i = 0; i < m; ++i) {
    C[i] = i;
    scanf("%d %d", &u, &v);
    adj[--u].push_back(i);
    adj[--v].push_back(i);
  }
  double ans = 0, div = n * (n-1.) / 2., sum, sum2, x;
  for (int i = 0; i < n; ++i) {
    u = pi[i];
    sum = sum2 = 0;
    set<int> J;
    //cerr << u+1 << " " << A[u] << ":";
    for (int v: adj[u])
      J.insert(findSet(v));
    for (int j: J) {
      x = S[findSet(j)];
      //cerr << " " << findSet(j)+1 << "," << x;
      sum += x;
      sum2 += x/div * x;
    }
    double q = sum * (sum+1) - sum2;
    //cerr << " -> " << q;
    ans += A[u] * (sum / div * (sum/2+1) - sum2 / 2);
    for (int j: J)
      unionSet(adj[u][0], j);
    ++S[findSet(adj[u][0])];
    //cerr << "  " << findSet(adj[u][0])+1 << "," << S[findSet(adj[u][0])] << endl;
  }
  printf("%.6f\n", ans);
}
