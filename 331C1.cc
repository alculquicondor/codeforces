#include <iostream>
#include <vector>

using namespace std;

int maxdigit(int x) {
  int ans = 0;
  while (x) {
    ans = max(ans, x % 10);
    x /= 10;
  }
  return ans;
}

int DP[1000001];

int main() {
  int n, ans = 0;
  cin >> n;
  while (n) {
    n -= maxdigit(n);
    ans++;
  }
  cout << ans << endl;
}
