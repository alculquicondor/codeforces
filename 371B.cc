#include <iostream>
#include <algorithm>

using namespace std;

int parts(int x) {
  int d[] = {2, 3, 5}, c = 0;
  for (int i = 0; i < 3; ++i) {
    while (x % d[i] == 0) {
      x /= d[i];
      ++c;
    }
  }
  return x == 1 ? c : -1;
}

int main() {
  int a, b, g;
  cin >> a >> b;
  g = __gcd(a, b);
  int c1 = parts(a / g), c2 = parts(b / g);
  cout << (c1 == -1 or c2 == -1 ? -1 : c1 + c2) << endl;
}
