import string
s=raw_input()
r=''
i=0
def nexti(i):
	if(i==len(s)-1 or s[i+1] in string.digits):
		return ''
	i+=1
	while(i<len(s) and s[i]==' '):
		i+=1
	if(i==len(s)):
		return ''
	return s[i]

while(i<len(s)):
	if(s[i]==' '):
		i+=1
		continue
	if(s[i]=='.'):
		c=0
		if(len(r)>0 and r[len(r)-1]!=' '):
			r+=' '
		while(c<3 and i<len(s) and s[i]=='.'):
			i+=1
			c+=1
		r+='...'
		continue
	r+=s[i]
	ss=nexti(i)
	if(s[i]==',' or (s[i] in string.digits and ss!='' and (ss=='.' or ss in string.digits))):
		r+=' '
		i+=1
		continue
	i+=1
	
if(r[len(r)-1]==' '):
	r=r[:-1]
print r
