#include <bits/stdc++.h>
using namespace std;

typedef long long ll;
ll c, h[2], w[2], q[2];

ll calc(int qi, int i) {
  int qj = (c - w[i] * qi) / w[1-i];
  return h[i] * qi + h[1-i] * qj;
}

int main() {
  ios::sync_with_stdio(false);
  cin >> c >> h[0] >> h[1] >> w[0] >> w[1];
  int id = h[1] * w[0] < h[0] * w[1];
  ll ans = 0;
  if (w[1-id] < c / w[1-id]) {
    for (int i = 0; i <= min(w[1-id], c / w[id]); ++i)
      ans = max(ans, calc(i, id));
  } else {
    for (int i = 0; i <= c / w[1-id]; ++i)
      ans = max(ans, calc(i, 1-id));
  }
  cout << ans << endl;
  return 0;
}

