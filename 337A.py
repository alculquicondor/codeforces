n, m = [int(x) for x in input().split()]
A = [int(x) for x in input().split()]
A.sort()
ans = (1<<30)
for i in range(m-n+1):
  ans = min(ans, A[i+n-1]-A[i])
print(ans)
