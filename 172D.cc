#include <iostream>
#include <algorithm>
#define MAXN 10000002

using namespace std;
typedef long long ll;

int F[MAXN];
void criba() {
  fill(F, F+MAXN, 1);
  for (int i = 2; i < MAXN; i++)
    if(F[i] == 1) {
      F[i] = i;
      for (ll j = (ll) i * i; j < MAXN; j+= i)
        F[j] = i;
    }
}

int main() {
  criba();
  int a, n;
  cin >> a >> n;
  ll ans = 0;
  while (n--) {
    int t = a, f, c, m = 1;
    while (t > 1) {
      f = F[t], c = 0;
      while (t % f == 0)
        c++, t /= f;
      if (c & 1)
        m *= f;
    }
    ans += m;
    a ++;
  }
  cout << ans << endl;
}
