#include <cstdio>
#include <algorithm>
#include <queue>
#include <cstring>
#include <cmath>

using namespace std;

// Begin of MCMF

#define REP(i,n) for((i)=0;(i)<(int)(n);(i)++)
typedef int F;
typedef double C;
#define F_INF (1<<29)
#define C_INF 1.0E+9
#define MAXV 805
#define MAXE 648025

int V,E;
F cap[MAXE];
C cost[MAXE],dist[MAXV],pot[MAXV];
int to[MAXE],prev[MAXE],last[MAXV],path[MAXV];
bool used[MAXV];
priority_queue <pair <C, int> > q;

class MinCostFlow{
  public:

    MinCostFlow(int n){
      int i;
      V = n; E = 0;
      REP(i,V) last[i] = -1;
    }

    void add_edge(int x, int y, F w = 1, C c = 0){
      cap[E] = w; cost[E] = c; to[E] = y; prev[E] = last[x]; last[x] = E; E++;
      cap[E] = 0; cost[E] = -c; to[E] = x; prev[E] = last[y]; last[y] = E; E++;
    }

    pair <F, C> search(int s, int t){
      F ansf=0; C ansc=0;
      int i;

      REP(i,V) used[i] = false;
      REP(i,V) dist[i] = C_INF;

      dist[s] = 0; path[s] = -1; q.push(make_pair(0,s));
      while(!q.empty()){
        int x = q.top().second; q.pop();
        if(used[x]) continue; used[x] = true;
        for(int e=last[x];e>=0;e=prev[e]) if(cap[e] > 0){
          C tmp = dist[x] + cost[e] + pot[x] - pot[to[e]];
          if(tmp < dist[to[e]] && !used[to[e]]){
            dist[to[e]] = tmp;
            path[to[e]] = e;
            q.push(make_pair(-dist[to[e]],to[e]));
          }
        }
      }

      REP(i,V) pot[i] += dist[i];

      if(used[t]){
        ansf = F_INF;
        for(int e=path[t];e>=0;e=path[to[e^1]]) ansf = min(ansf,cap[e]);
        for(int e=path[t];e>=0;e=path[to[e^1]]) {
          ansc += cost[e] * ansf; cap[e] -= ansf; cap[e^1] += ansf;}
      }

      return make_pair(ansf,ansc);
    }

    pair <F, C> mincostflow(int s, int t){
      F ansf=0; C ansc=0;
      int i;
      while(1){
        pair <F, C> p = search(s,t);
        if(!used[t]) break;
        ansf += p.first; ansc += p.second;
      }
      return make_pair(ansf,ansc);
    }
};

// End of MCMF

typedef pair<int, int> pii;
pii P[401];

double hipo(int i, int j) {
  int dx = P[i].first - P[j].first, dy = P[i].second - P[j].second;
  return sqrt(dx*dx + dy*dy);
}

int main() {
  int n;
  scanf("%d", &n);
  MinCostFlow mcf(2*n+2);
  for (int i = 0; i < n ; i++)
    scanf("%d %d", &P[i].first, &P[i].second);
  for (int i = 0; i < n; i++) {
    mcf.add_edge(0, i+1, 2);
    for (int j = 0; j < n; j++)
      if (P[i].second > P[j].second)
        mcf.add_edge(i+1, j + n + 1, 1, hipo(i, j));
    mcf.add_edge(i+n+1, 2*n+1);
  }
  pair<int, double> p = mcf.mincostflow(0, 2*n+1);
  if (p.first == n - 1)
    printf("%.7f\n", p.second);
  else
    puts("-1");
}

