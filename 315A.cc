#include <iostream>
#include <cstring>
#define MAXN 101

using namespace std;

int A[MAXN], B[MAXN];
bool can[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  for (int i = 0; i < n; i++)
    cin >> A[i] >> B[i];
  memset(can, 0, sizeof can);
  int pos = 0;
  for (int i = 0; i < n; i++)
    for (int j = 0; j < n; j++)
      if (j != i and B[i] == A[j])
        can[j] = true;
  for (int i = 0; i < n; i++)
    pos += can[i];
  cout << n - pos << endl;
}

