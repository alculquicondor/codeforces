#include<cstdio>
#include<cstring>

int n, l;
int M[101][20002];
int R[101][2002];

bool DP(int i, int d) {
    if(i==n-1) {
        R[i][d+10000] = d;
        return d>0 and d<=l;
    }
    int &ans = M[i][d+10000];
    if(ans>=0)
        return ans;
    for(int j=1; j<=l; j++)
        if(DP(i+1, j-d)) {
            R[i][d+10000] = j;
            return ans = true;
        }
    return ans = false;
}

int main() {
    int d;
    scanf("%d %d %d", &n, &d, &l);
    memset(M, -1, sizeof M);
    bool hay = DP(0, d);
    if(hay) {
        printf("%d", R[0][d+10000]);
        d = R[0][d+10000]-d;
        for(int i=1; i<n; i++) {
            printf(" %d", R[i][d+10000]);
            d = R[i][d+10000]-d;
        }
        puts("");
    }
    else
        puts("-1");
}
