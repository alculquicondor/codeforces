#include <iostream>
#include <cmath>
#include <cstdio>

using namespace std;

inline void print(int x, int y) {
  printf("%.4f %.4f\n", x / 10000., y / 10000.);
}

int main() {
  int a, d, n;
  double aa, dd;
  cin >> aa >> dd >> n;
  a = round(aa * 10000);
  d = round(dd * 10000);
  long long L = 4LL * a, l = d % L;
  int t, x;
  for (int i = 0; i < n; ++i) {
    t = l / a;
    x = l % a;
    if (t == 0)
      print(x, 0);
    else if (t == 1)
      print(a, x);
    else if (t == 2)
      print(a - x, a);
    else
      print(0, a-x);
    l += d;
    if (l >= L)
      l %= L;
  }
}
