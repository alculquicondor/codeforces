import Control.Applicative
import Data.List

solve [] = 0
solve a = pi*(last a)^2 - solve (init a)

main = getLine >> do
    rs <- map read . words <$> getLine
    print $ solve (sort rs)
