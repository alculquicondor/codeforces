#include <iostream>

using namespace std;
typedef long long ll;

int main() {
  int tc;
  cin >> tc;
  ll n, l, r, q;
  while (tc--) {
    cin >> n >> l >> r;
    q = n / l;
    cout << (r * q >= n ? "Yes" : "No") << endl;
  }
}
