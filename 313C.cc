#include <iostream>
#include <algorithm>
#define MAXN 2000004

using namespace std;
typedef long long ll;

int A[MAXN];

int main() {
  ios::sync_with_stdio(0);
  int n;
  cin >> n;
  for (int i = 0; i < n; i++)
    cin >> A[i];
  ll p = 1, ans = 0, s = 1;
  while (s < n) {
    s <<= 2;
    p++;
  }
  s = 1;
  sort(A, A+n, greater<int>());
  for (int i = 0; i < n; i++) {
    if (i == s) {
      p --;
      s *= 4;
    }
    ans += A[i] * p;
  }
  cout << ans << endl;
}
