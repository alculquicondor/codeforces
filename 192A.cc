#include <iostream>
#include <algorithm>
#define MAXN 45000

using namespace std;

int A[MAXN];
/* alculquicondor */

int main() {
  int n;
  for (int i = 0; i < MAXN; ++i)
    A[i] = i * (i+1) >> 1;
  cin >> n;
  bool found = false;
  for (int i = 1; not found and A[i] < n; ++i)
    found = binary_search(A, A+MAXN, n - A[i]);
  cout << (found ? "YES" : "NO") << endl;
}
