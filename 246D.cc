#include <iostream>
#include <vector>
#include <set>
#include <cstring>
#define MAXN 100002

using namespace std;
bool visit[MAXN];
bool vale[MAXN];
int color[MAXN];
set<int> neig[MAXN];
vector<int> adj[MAXN];

void dfs(int u) {
  int v;
  visit[u] = true;
  for (int i = 0; i < adj[u].size(); i++) {
    v = adj[u][i];
    if (color[u] != color[v]) {
      neig[color[u]].insert(color[v]);
      neig[color[v]].insert(color[u]);
    }
    if (not visit[v])
      dfs(v);
  }
}

int main() {
  ios::sync_with_stdio(0);
  int n, m, u, v;
  cin >> n >> m;
  memset(vale, 0, sizeof vale);
  for (int i = 0; i < n; i++) {
    cin >> color[i];
    vale[color[i]] = true;
  }
  while (m--) {
    cin >> u >> v;
    adj[--u].push_back(--v);
    adj[v].push_back(u);
  }
  memset(visit, 0, sizeof visit);
  for (int i = 0; i < n; i++)
    if (not visit[i])
      dfs(i);
  int ans, has = -1;
  for (int i = 0; i < MAXN; i++)
    if (vale[i] and (int)neig[i].size() > has) {
      ans = i;
      has = neig[i].size();
    }
  cout << ans << endl;
}
