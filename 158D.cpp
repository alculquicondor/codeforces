#include<cstdio>
#include<cmath>
#include<bits/stl_algobase.h>

using std::max;

int n,A[20001];

int calc(int lados)
{
    int ans=-1000*n,tmp;
    if(lados<=2)
        return ans;
    for(int i=0;i<n/lados;i++)
    {
        tmp=0;
        for(int j=i;j<n;j+=n/lados)
            tmp+=A[j];
        ans=max(ans,tmp);
    }
    return ans;
}

int main()
{
    scanf("%d",&n);
    for(int i=0;i<n;i++)
        scanf("%d",&A[i]);
    int ans=calc(n);
    for(int i=2;i<=sqrt(n);i++)
        if(n%i==0)
        {
            ans=max(calc(i),ans);
            ans=max(calc(n/i),ans);
        }
    printf("%d\n",ans);
}
