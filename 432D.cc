#include <iostream>
#include <vector>
#define MAXN 100005

using namespace std;

int pi[MAXN], C[MAXN];

void kmpPreprocess(const string &P) {
  int i = 0, j = -1, m = P.size();
  pi[0] = -1;
  while(i < m) {
    while(j>=0 and P[i] != P[j])
      j = pi[j];
    i ++, j++;
    pi[i] = j;
    ++C[j];
  }
}

typedef pair<int, int> pii;
vector<pii> ans;

int main() {
  string s;
  cin >> s;
  kmpPreprocess(s);
  int n = s.size();
  C[n] = 1;
  for (int i = n; i; --i)
    C[pi[i]] += C[i];
  while (n) {
    ans.push_back(pii(n, C[n]));
    n = pi[n];
  }
  cout << ans.size() << endl;
  for (int i = ans.size()-1; i >= 0; --i)
    cout << ans[i].first << " " << ans[i].second << endl;
}
