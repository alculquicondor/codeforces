#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>
#define MAXN 200000
#define left(x) (x<<1) + 1
#define center(l, r) (l+r)>>1

using namespace std;
typedef long long ll;
ll st[4*MAXN], add[4*MAXN];
int n;

void stupdate(int i, int j, int idx = 0, int l = 0, int r = n - 1) {
  int L = left(idx), R = L + 1, mid = center(l, r);
  if (l >= i and r <= j)
    add[idx] ++;
  if (add[idx]) {
    st[idx] += (r-l+1) * add[idx];
    if (l != r) {
      add[L] += add[idx];
      add[R] += add[idx];
    }
    add[idx] = 0;
  }
  if (l >= i and r <= j)
    return;
  if (l > j or r < i)
    return;
  stupdate(i, j, L, l, mid);
  stupdate(i, j, R, mid+1, r);
  st[idx] = st[L] + st[R];
}

ll stq(int i, int idx = 0, int l = 0, int r = n - 1) {
  int L = left(idx), R = L + 1, mid = center(l, r);
  if (add[idx]) {
    st[idx] += (r-l+1) * add[idx];
    if (l != r) {
      add[L] += add[idx];
      add[R] += add[idx];
    }
    add[idx] = 0;
  }
  if (l >= i and r <= i)
    return st[idx];
  if (l > i or r < i)
    return 0;
  return stq(i, L, l, mid) + stq(i, R, mid+1, r);
}

int main() {
  memset(st, 0, sizeof st);
  memset(add, 0, sizeof add);
  int q, a, b;
  cin >> n >> q;
  vector<int> A(n);
  for (int i = 0; i < n; i++)
    cin >> A[i];
  sort(A.begin(), A.end());
  while (q--) {
    cin >> a >> b;
    a --, b--;
    stupdate(a, b);
  }
  vector<ll> B(n);
  for (int i = 0; i < n; i++)
    B[i] = stq(i);
  sort(B.begin(), B.end());
  ll ans = 0;
  for (int i = 0; i < n; i++)
    ans += A[i] * B[i];
  cout << ans << endl;
}
