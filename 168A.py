from math import ceil
n, x, y = [int(x) for x in raw_input().split()]

print max(int(ceil((y*n-100*x)/100.0)), 0)
