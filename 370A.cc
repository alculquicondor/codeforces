#include <iostream>
#include <cmath>

using namespace std;

int main() {
  int r1, c1, r2, c2;
  cin >> r1 >> c1 >> r2 >> c2;
  int tower = r1 == r2 or c1 == c2 ? 1 : 2;
  int bishop;
  bool bl1 = (r1 % 2 == 1) ^ (c1 % 2 == 1),
       bl2 = (r2 % 2 == 1) ^ (c2 % 2 == 1);
  if (bl1 ^ bl2) {
    bishop = 0;
  } else {
    int d1 = r2-r1;
    bishop = c2 == c1 + d1 or c2 == c1 - d1 ? 1 : 2;
  }
  int king = max(abs(r1-r2), abs(c1-c2));
  cout << tower << " " << bishop << " " << king << endl;
}
