#include <iostream>
#include <vector>
#include <cstring>
#define MAXN 50005
#define MAXK 505

using namespace std;
typedef long long ll;

ll A[MAXN][MAXK];
vector<int> adj[MAXN];
int pi[MAXN];
int n, k;
bool utd[MAXN];

int dfs(int u, int p = -1) {
  pi[u] = p;
  for (int i = 0; i <= k; i++)
    A[u][i] = 0;
  A[u][0] = 1;
  for (int i = 0, v; i < adj[u].size(); i++) {
    v = adj[u][i];
    if (v != p) {
      dfs(v, u);
      for (int j = 0; j < k; j++)
        A[u][j+1] += A[v][j];
    }
  }
}

void refresh(int u) {
  if (pi[u] == -1 or utd[u])
    return;
  int p = pi[u];
  refresh(p);
  for (int i = k; i > 1; i--)
    A[u][i] += A[p][i-1] - A[u][i-2];
  A[u][1] ++;
  utd[u] = true;
}

int main() {
  ios::sync_with_stdio(0);
  int u, v;
  cin >> n >> k;
  for (int i = 0; i < n - 1; i++) {
    cin >> u >> v;
    u --, v--;
    adj[u].push_back(v);
    adj[v].push_back(u);
  }
  dfs(0);
  memset(utd, false, sizeof utd);
  ll ans = 0;
  for (int i = 0; i < n; i++) {
    refresh(i);
    ans += A[i][k];
  }
  cout << (ans/2) << endl;
}
