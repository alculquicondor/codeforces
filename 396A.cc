#include <iostream>
#include <map>

using namespace std;

typedef long long ll;

#define MOD 1000000007

#define inv(x) powmod(x, MOD-2)

inline ll san(ll x) {
  return x >= MOD ? x % MOD : x;
}

inline ll powmod(ll a, int e) {
  ll r = 1;
  while (e) {
    if (e & 1)
      r = san(r * a);
    e >>= 1;
    a = san(a * a);
  }
  return r;
}

int C(int n, int m) {
  int c = 1;
  for (int i = 1; i <= m; ++i) {
    c = san(c * (n-i+1LL));
    c = san(c * inv(i));
  }
  return c;
}

map<int, int> M;

int main() {
  ios::sync_with_stdio(0);
  int n, x;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> x;
    for (int j = 2; j * j <= x; ++j) {
      int c = 0;
      while (x % j == 0) {
        x /= j;
        ++c;
      }
      if (c)
        M[j] += c;
    }
    if (x > 1)
      ++M[x];
  }
  ll ans = 1;
  for (map<int, int>::iterator it = M.begin(); it != M.end(); ++it) {
    ans *= C(it->second + n - 1, min(it->second, n-1));
    if (ans >= MOD)
      ans %= MOD;
  }
  cout << ans << endl;
}
