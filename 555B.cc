#include <bits/stdc++.h>
using namespace std;

#define MAXN 200005
typedef long long ll;
typedef pair<ll, int> pii;
struct Need {
  long long mini, maxi;
  int id;
  bool operator < (const Need &o) const {
    return maxi < o.maxi;
  }
};
Need needed[MAXN];
pii have[MAXN];
int ans[MAXN];
int n, m;

bool solve() {
  if (m < n)
    return false;
  sort(needed, needed + n);
  set<pii> S(have, have + m);
  for (int i = 0; i < n; ++i) {
    set<pii>::iterator it = S.lower_bound(pii(needed[i].mini, 0));
    if (it == S.end() or it->first > needed[i].maxi)
      return false;
    ans[needed[i].id] = it->second;
    S.erase(it);
  }
  return true;
}

int main() {
  ios::sync_with_stdio(false);
  cin >> n >> m;
  ll l, r, nl, nr;
  cin >> l >> r;
  --n;
  for (int i = 0; i < n; ++i) {
    cin >> nl >> nr;
    needed[i] = Need{nl - r, nr - l, i};
    l = nl;
    r = nr;
  }
  for (int i = 0; i < m;++i) {
    cin >> l;
    have[i] = pii(l, i + 1);
  }
  if (solve()) {
    cout << "Yes" << endl;
    cout << ans[0];
    for (int i = 1; i < n; ++i)
      cout << ' ' << ans[i];
    cout << endl;
  } else {
    cout << "No" << endl;
  }
  return 0;
}

