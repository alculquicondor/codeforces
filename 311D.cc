#include <iostream>
#include <valarray>
#include <cstring>
#define MOD 95542721

using namespace std;

typedef long long ll;

inline ll san(ll x) {
  if (x >= MOD or x <= -MOD)
    x %= MOD;
  if (x < 0)
    x += MOD;
  return x;
}

#define MAXN 300000
#define center(x, y) (x + y) >> 1
#define left(x) (x << 1) + 1
typedef valarray<ll> vll;
vll st[MAXN];
int add[MAXN];
int n;

void swift(vll &v, int d) {
  vll t(v);
  for (int i = 0; i < v.size(); i++)
    v[i] = t[(i+d)%v.size()];
}

inline void san(vll &x) {
  for (int i = 0; i < x.size(); i++)
    x[i] = san(x[i]);
}

inline void push(int idx, int l, int r, int L, int R) {
  if (add[idx]) {
    swift(st[idx], add[idx]);
    if (l != r) {
      add[L] += add[idx];
      add[R] += add[idx];
    }
    add[idx] = 0;
  }
}

void stupdate(int i, int j, 
    int idx = 0, int l = 0, int r = n - 1) {
  int L = left(idx), R = L + 1;
  if (l >= i and r <= j)
    add[idx] ++;
  push(idx, l, r, L, R);
  if ((l >= i and r <= j) or r < i or l > j)
    return;
  int mid = center(l, r);
  stupdate(i, j, L, l, mid);
  stupdate(i, j, R, mid + 1, r);
  st[idx] = st[L] + st[R];
  san(st[idx]);
}

ll stq(int i, int j, int idx = 0, int l = 0, int r = n - 1) {
  int L = left(idx), R = L + 1;
  push(idx, l, r, L, R);
  if (l >= i and r <= j)
    return st[idx][0];
  if (r < i or l > j)
    return 0;
  int mid = center(l, r);
  return san(stq(i, j, L, l, mid) + stq(i, j, R, mid+1, r));
}

int A[100004];
inline ll cube(ll x) {
  return san(x * san(x * x));
}

void stbuild(int idx = 0, int l = 0, int r = n - 1) {
  if (l == r) {
    st[idx] = vll (48);
    st[idx][0] = A[l];
    for (int i = 1; i < 48; i++)
      st[idx][i] = cube(st[idx][i-1]);
    return;
  }
  int L = left(idx), R = L + 1, mid = center(l, r);
  stbuild(L, l, mid);
  stbuild(R, mid+1, r);
  st[idx].resize(48);
  st[idx] = st[L] + st[R];
}

int main() {
  ios::sync_with_stdio(0);
  cin >> n;
  for (int i = 0; i < n; i++)
    cin >> A[i];
  stbuild();
  memset(add, 0, sizeof add);
  int q, op, a, b;
  cin >> q;
  while (q--) {
    cin >> op >> a >> b;
    if (op == 1)
      cout << stq(a-1, b-1) << endl;
    else
      stupdate(a-1, b-1);
  }
}

