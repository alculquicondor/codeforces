def rank(s):
	if(s=='6'):
		return 0
	if(s=='7'):
		return 1
	if(s=='8'):
		return 2
	if(s=='9'):
		return 3
	if(s=='T'):
		return 4
	if(s=='J'):
		return 5
	if(s=='Q'):
		return 6
	if(s=='K'):
		return 7
	return 8

def beat(a,b):
	if(rank(a)<rank(b)):
		return 'NO'
	return 'YES'

tr=raw_input()
s=raw_input().split()
c1=s[0]
c2=s[1]
if(c1[1]==tr):
	if(c2[1]==tr):
		print beat(c1[0],c2[0])
	else:
		print 'YES'
else:
	if(c1[1]!=c2[1]):
		print 'NO'
	else:
		print beat(c1[0],c2[0])
