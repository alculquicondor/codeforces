#include <iostream>
#include <vector>
#define MAXN 100010
#define MOD 1000000007

using namespace std;


using namespace std;

int pi[MAXN], DP[MAXN], AAC[MAXN];

void kmpPreprocess(const string &P) {
  int i = 0, j = -1, m = P.size();
  pi[0] = -1;
  while(i < m) {
    while(j>=0 and P[i] != P[j])
      j = pi[j];
    i ++, j++;
    pi[i] = j;
  }
}

vector<int> kmpSearch(const string &T, const string &P) {
  vector<int> ans;
  int i = 0, j = 0, n = T.size(), m = P.size();
  while(i < n) {
    while(j>=0 and T[i]!=P[j])
      j = pi[j];
    i ++, j++;
    if(j == m) {
      ans.push_back(i-j);
      j = pi[j];
    }
  }
  return ans;
}

int main() {
  string s, t;
  cin >> s >> t;
  kmpPreprocess(t);
  vector<int> idx = kmpSearch(s, t);
  int j = idx.size(), p;
  for (int i = s.size() - 1; i >= 0; --i) {
    while (j and idx[j-1] >= i)
      --j;
    if (j < (int)idx.size()) {
      p = idx[j] + t.size();
      DP[i] = AAC[p] + (s.size() - p + 1);
      DP[i] %= MOD;
    }
    DP[i] = (DP[i] + DP[i+1]) % MOD;
    AAC[i] = (DP[i] + AAC[i+1]) % MOD;
  }
  cout << DP[0] << endl;
  return 0;
}
