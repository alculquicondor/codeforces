#include <iostream>
#define MOD 1000000007

using namespace std;
typedef long long ll;

inline ll san(ll x) {
  if (x >= MOD)
    x %= MOD;
  return x;
}

int pow(ll a, int e) {
  int r = 1;
  while (e) {
    if (e & 1)
      r = san(r*a);
    e >>= 1;
    a = san(a*a);
  }
  return r;
}

int main() {
  int n, m, k;
  cin >> n >> m >> k;
  int ans = 0;
  if (k == 1) {
    ans = pow(m, n);
  } else if (k > n) {
    ans = pow(m, n);
  } else if (k == n) {
    ans = pow(m, n/2+n%2);
  } else {
    ans = m;
    if (k & 1)
      ans = san(ans + m*(m-1));
  }
  cout << ans << endl;
}
