#include <iostream>
#include <fstream>
#include <algorithm>
#include <cstring>

using namespace std;
typedef pair<int, int> pii;
typedef pair<pii, pii> ppp;

istream& operator >> (istream& is, ppp &p) {
  is >> p.first.first >> p.first.second >>
    p.second.first >> p.second.second;
  return is;
}

ppp P[5];
#define MAXN 31405
int n, X[MAXN];

bool solve() {
  bool first = true;
  int x, y;
  memset(X, -1, sizeof X);
  for (int i = 0; i < n; i++) {
    if (first) {
      x = P[i].first.first;
      y = P[i].first.second;
      first = false;
    }
    if (P[i].first.first == x) {
      for (int j = P[i].first.second; j < P[i].second.second; j++)
        X[j] = P[i].second.first;
    } else {
      for (int j = P[i].first.second; j < P[i].second.second; j++) {
        if (X[j] == -1 or X[j] < P[i].first.first)
          return false;
        X[j] = P[i].second.first;
      }
    }
  }
  int lado = X[y] - x;
  if (y + lado > MAXN)
    return false;
  for (int i = y + 1; i < y + lado; i++)
    if (X[i] != X[y])
      return false;
  for (int i = y + lado; i < MAXN; i++)
    if (X[i] != -1)
      return false;
  return true;
}

int main() {
  ios::sync_with_stdio(false);
  cin >> n;
  for (int i = 0; i < n; i++)
    cin >> P[i];
  sort(P, P+n);
  cout << (solve() ? "YES" : "NO") << endl;
}
