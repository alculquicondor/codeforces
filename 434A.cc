#include <iostream>
#include <vector>
#include <algorithm>
#define MAXN 100001

using namespace std;
typedef long long ll;
vector<int> L[MAXN];
ll A[MAXN], C[MAXN];

int main() {
  std::ios::sync_with_stdio(false);
  int n, m;
  cin >> n >> m;
  for (int i = 0; i < m; ++i)
    cin >> A[i];
  --m;
  if (m and A[1] != A[0])
    L[A[0]].push_back(A[1]);
  for (int i = 1; i < m; ++i) {
    if (A[i-1] != A[i])
      L[A[i]].push_back(A[i-1]);
    if (A[i+1] != A[i])
      L[A[i]].push_back(A[i+1]);
  }
  if (m and A[m] != A[m-1])
    L[A[m]].push_back(A[m-1]);
  ll total = 0;
  for (int i = 1; i <= n; ++i) {
    A[i] = 0;
    for (auto &x: L[i]) {
      A[i] += abs(x-i);
      C[i] += x;
    }
    total += A[i];
    sort(L[i].begin(), L[i].end());
  }
  ll ans = total >>= 1;
  for (int i = 1; i <= n; ++i) {
    ll sum = 0, tmp;
    for (int j = 0; j < L[i].size(); ++j) {
      tmp = (ll)L[i][j] * j - sum +
        (C[i] - sum) - (ll)L[i][j] * (L[i].size()-j);
      //cerr << i << " " << L[i][j] << ": " << tmp << endl;
      ans = min(ans, total - A[i] + tmp);
      sum += L[i][j];
    }
  }
  cout << ans << endl;
}

