#include <iostream>

using namespace std;

int n;
string s;

string solve(int p) {
  int r = 58 - s[p];
  string ans;
  for (int i = 0; i < n; ++i)
    ans.push_back('0' + (s[(p+i)%n] - '0' + r) % 10);
  return ans;
}

int main() {
  cin >> n >> s;
  string ans(n, '9');
  for (int i = 0; i < n; ++i)
    ans = min(ans, solve(i));
  cout << ans << endl;
  return 0;
}
