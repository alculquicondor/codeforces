n=int(raw_input())
S=set([int(x) for x in raw_input().split()])
A=[x for x in S]
A.sort()
t=len(A)-1
for i in range(t):
	if(A[i]>1 and A[i]*2>A[i+1] and A[i]!=A[t]):
		print 'YES'
		exit(0)
print 'NO'
