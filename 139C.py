k=0

def isvowel(s):
	return s=='a' or s=='e' or s=='i' or s=='o' or s=='u'

def qsch(A):
	for j in range(len(A)):
		x=A[j]
		i=len(x)-1
		c=0
		while i>=0:
			if isvowel(x[i]):
				c+=1
			if(c==k):
				break
			i-=1
		if(c<k):
			return 'NO'
		A[j]=x[i:]
	if(A[0]==A[1]==A[2]==A[3]):
		return 'aaaa'
	if(A[0]==A[1] and A[2]==A[3]):
		return 'aabb'
	if(A[0]==A[2] and A[1]==A[3]):
		return 'abab'
	if(A[0]==A[3] and A[1]==A[2]):
		return 'abba'
	return 'NO'

n,k=[int(x) for x in raw_input().split()]
ans='aaaa'
for i in range(n):
	A=[]
	for j in range(4):
		A.append(raw_input())
	tt=qsch(A)
	if ans=='aaaa':
		ans=tt
	elif ans!=tt and tt!='aaaa':
		ans='NO'
		break
print ans
