#include <iostream>
#define MAXN 51

using namespace std;

int C[MAXN];
inline int findSet(int i) {
    return (C[i] == i) ? i : (C[i] = findSet(C[i]));
}
inline void unionSet(int i, int j) {
    C[findSet(i)] = findSet(j);
}

int T[MAXN];

int main() {
  std::ios::sync_with_stdio(false);
  int n, m, u, v;
  cin >> n >> m;
  for (int i = 0; i < n; ++i)
    C[i] = i;
  for (int i = 0; i < m; ++i) {
    cin >> u >> v;
    --u, --v;
    unionSet(u, v);
  }
  for (int i = 0; i < n; ++i)
    ++T[findSet(i)];
  long long ans = 1;
  for (int i = 0; i < n; ++i)
    if (T[i] > 1)
      ans <<= T[i]-1;
  cout << ans << endl;
}
