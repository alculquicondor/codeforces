#include <cstdio>
#include <cstring>
#include <iostream>
#define MOD 1000000007

using namespace std;

typedef long long ll;

ll C[101][101][3], M[10001][101], n, m;

ll powmod(ll a, ll e) {
    ll p = 1;
    while(e) {
        if(e&1)
            p = (p*a)%MOD;
        a = (a*a)%MOD;
        e>>=1;
    }
    return p;
}

inline ll ceil(ll m, ll n) {
    ll q = m/n;
    if(n*q == m)
        return q;
    return q+1;
}

void pre() {
    for(int i=0; i<3; i++)
        C[0][0][i] = 1;
    for(int i=1; i<=100; i++) {
        for(int j=0; j<3; j++)
            C[i][0][j] = 1LL;
        for(int j=1; j<i; j++) {
            C[i][j][0] = (C[i-1][j-1][0]+C[i-1][j][0])%MOD;
            C[i][j][1] = powmod(C[i][j][0], ceil(m, n));
            C[i][j][2] = powmod(C[i][j][0], ceil(m, n)-1);
        }
        for(int j=0; j<3; j++)
            C[i][i][j] = 1LL;
    }
}

ll DP(int f, int p) {
    ll &ans = M[f][p];
    if(ans>=0)
        return ans;
    if(f==0)
        return ans = 1LL;
    if((n-p)*n<f)
        return ans = 0LL;
    //cout<<"# "<<f<<" "<<p<<endl;
    ans = 0;
    for(int i=0; i<=f and i<=n; i++)
        ans = (ans+(C[n][i][(ceil(m-p,n)==ceil(m,n))?1:2]*DP(f-i, p+1))%MOD)%MOD;
    return ans;
}

int main() {
    int k;
    cin>>n>>m>>k;
    pre();
    memset(M, -1, sizeof M);
    cout<<DP(k, 0)<<endl;
}
