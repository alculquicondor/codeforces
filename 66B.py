n=int(raw_input())
A=[int(x) for x in raw_input().split()]
L=[-1]*n
R=[-1]*n

def left(i):
	if(L[i]!=-1):
		return L[i]
	if i>0 and A[i-1]<=A[i]:
		L[i]=1+left(i-1)
	else:
		L[i]=1
	return L[i]

def right(i):
	if(R[i]!=-1):
		return R[i]
	if i<n-1 and A[i+1]<=A[i]:
		R[i]=1+right(i+1)
	else:
		R[i]=1
	return R[i]

print max([left(i)+right(i)-1 for i in range(n)])
