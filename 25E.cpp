#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
#define MAXN 300005
#define foreach(it,c) for(typeof((c).begin()) it=(c).begin();it!=(c).end();++it)

int pi[MAXN];
void kmpPreprocess(string &P) {
    int i = 0, j = -1, m = P.size();
    pi[0] = -1;
    while(i < m) {
        while(j>=0 and P[i] != P[j])
            j = pi[j];
        i ++, j++;
        pi[i] = j;
    }
}
int kmpSearch(string &T, string &P) {
    vector<int> ans;
    int i = 0, j = 0, n = T.size(), m = P.size();
    while(i < n) {
        while(j>=0 and T[i]!=P[j])
            j = pi[j];
        i ++, j++;
        if(i==n)
            return j;
        if(j == m) {
            return j;
            j = pi[j];
        }
    }
}

int main() {
    vector<string> A(3);
    cin>>A[0]>>A[1]>>A[2];
    sort(A.begin(), A.end());
    string ans;
    int t, rpta = MAXN;
    do {
        kmpPreprocess(A[1]);
        t = kmpSearch(A[0], A[1]);
        ans = A[0] + A[1].substr(t);
        kmpPreprocess(A[2]);
        t = kmpSearch(ans, A[2]);
        rpta = min(rpta, (int)ans.size()+(int)A[2].size()-t);
    } while(next_permutation(A.begin(), A.end()));
    cout<<rpta<<endl;
}
