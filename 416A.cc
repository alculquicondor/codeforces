#include <iostream>
#include <map>
#define INF 1000000001

using namespace std;

map<string, int> M;

int main() {
  M[">="] = 0;
  M["<"] = 1;
  M["<="] = 2;
  M[">"] = 3;
  int n;
  cin >> n;
  string op, res;
  int opc, x, l = -INF, r = INF;
  for (int i = 0; i < n; ++i) {
    cin >> op >> x >> res;
    opc = M[op];
    if (res == "N")
      opc ^= 1;
    if (opc == 0 or opc == 3)
      l = max(l, opc == 0 ? x : x + 1);
    else
      r = min(r, opc == 2 ? x : x - 1);
  }
  if (l <= r)
    cout << l << endl;
  else
    cout << "Impossible" << endl;
}
