#include <iostream>
#include <algorithm>
#include <cstring>
#define MAXN 100001

using namespace std;

short A[MAXN];
int dp1[3], dp2[3];
bool go[MAXN][3];

int main() {
  int n;
  ios::sync_with_stdio(0);
  cin >> n;
  for (int i = 0; i < n; i++)
    cin >> A[i];
  sort(A, A + n, greater<int>());
  if (A[n-1] != 0) {
    cout << -1 << endl;
    return 0;
  }
  memset(go, 0, sizeof go);
  fill(dp1, dp1+3, -(1 << 30));
  dp1[0] = 0;
  for (int i = n - 1; i >= 0; i--) {
    for (int j = 0; j < 3; j++) {
      dp2[j] = 1+dp1[(j+A[i])%3];
      go[i][j] = 1;
      if (dp1[j] > dp2[j]) {
        dp2[j] = dp1[j];
        go[i][j] = 0;
      }
    }
    copy(dp2, dp2+3, dp1);
  }
  int m = 0;
  if (dp1[0] >= 0) {
    bool first = true;
    for (int i = 0; i < n; i++)
      if (go[i][m]) {
        cout << A[i];
        m = (m+A[i])%3;
        if (first and A[i] == 0)
          break;
        first = false;
      }
    cout << endl;
  } else {
    cout << -1 << endl;
  }
}
