#include <iostream>

typedef long long ll;

using namespace std;

int main() {
  std::ios::sync_with_stdio(false);
  ll n, sum = 0, x;
  cin >> n;
  for (int i = 1; i < n; ++i) {
    cin >> x;
    sum += x;
  }
  cout << n * (n+1) / 2 - sum << endl;
}

