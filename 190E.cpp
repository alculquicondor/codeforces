#include <cstdio>
#include<vector>
#include<algorithm>
#include<map>

using namespace std;

#define MAXN 500001
#define foreach(it,c) for(typeof((c).begin()) it=(c).begin();it!=(c).end();++it)
#define left(i) (i<<1)+1
#define center(i,j) (i+j)>>1

vector<int> A[MAXN];
int C[MAXN];
map<int,vector<int> > MV;
int V;

inline int findSet(int i) {
    return (C[i] == i) ? i : (C[i] = findSet(C[i]));
}
inline void unionSet(int i, int j) {
    i = findSet(i);
    j = findSet(j);
    C[min(i, j)] = max(i, j);
}
inline void unionRange(int i, int j) {
    if(j<i)
        return;
    i = findSet(i);
    while(i<j) {
        unionSet(i, i+1);
        i = findSet(i+1);
    }
}

int main() {
    int E;
    int u, v;
    scanf("%i %i", &V, &E);
    for(int i=0;i<V;++i) {
        C[i] = next[i] = i;
        A[i].reserve(100);
    }
    for(int i=0;i<E;++i) {
        scanf("%i %i", &u, &v);
        --u, --v;
        A[min(u,v)].push_back(max(u,v));
    }
    int sz;
    for(int i=0;i<V;++i) {
        A[i].push_back(i);
        A[i].push_back(V);
        sort(A[i].begin(), A[i].end());
        sz = A[i].size();
        for(int j=0;j<sz-1;++j)
            if(A[i][j]+1<A[i][j+1]) {
                unionSet(i, A[i][j]+1);
                unionRange(A[i][j]+1, A[i][j+1]-1);
            }
    }
    for(int i=0;i<V;++i) {
        MV[findSet(i)].push_back(i+1);
    }
    printf("%i\n", MV.size());
    foreach(it, MV) {
        sz = it->second.size();
        printf("%i", sz);
        for(int i=0;i<sz;++i)
            printf(" %i", it->second[i]);
        printf("\n");
    }
}
