#include <iostream>
#include <cstring>
#define MAXN 100001

using namespace std;

typedef long long ll;

ll L[MAXN], R[MAXN], U[MAXN], Q[MAXN];
ll A[MAXN];

int main() {
  int n, m, k;
  cin >> n >> m >> k;
  for (int i = 0; i < n; ++i)
    cin >> A[i];
  for (int i = 0; i < m; ++i)
    cin >> L[i] >> R[i] >> U[i];
  int x, y;
  while (k--) {
    cin >> x >> y;
    Q[x-1] ++;
    Q[y] --;
  }
  U[0] *= Q[0];
  for (int i = 1; i < m; ++i) {
    Q[i] += Q[i-1];
    U[i] *= Q[i];
  }
  memset(Q, 0, sizeof Q);
  for (int i = 0; i < m; ++i) {
    Q[L[i]-1] += U[i];
    Q[R[i]] -= U[i];
  }
  for (int i = 1; i < n; ++i)
    Q[i] += Q[i-1];
  cout << A[0] + Q[0];
  for (int i = 1; i < n; ++i)
    cout << " " << A[i] + Q[i];
  cout << endl;
}
