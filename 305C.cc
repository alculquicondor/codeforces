#include <iostream>
#include <map>

using namespace std;

map<int, int> M;

int main() {
  ios::sync_with_stdio(0);
  int n, x;
  cin >> n;
  for (int i = 0; i < n; i++) {
    cin >> x;
    M[x]++;
  }
  int ct = 0;
  int v, c;
  for (map<int, int>::iterator it = M.begin(); it != M.end(); it++) {
    v = it->first, c = it->second;
    if (c & 1)
      ct ++;
    for (int i = 1; c >= (1<<i); i++)
      if (c & (1<<i))
        M[v+i] ++;
  }
  cout << v - ct + 1 << endl;
}
