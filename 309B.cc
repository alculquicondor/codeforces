#include <iostream>
#include <vector>
#define MAXN 1000005

using namespace std;

string S[MAXN];
int A[MAXN];
int son[MAXN][20];
vector<int> D;

int decom(int x) {
  int i;
  for (i = 0; x; i++) {
    if (x & 1)
      D.push_back(i);
    x >>= 1;
  }
  return i;
}

int getson(int id) {
  for (int k = 0; k < D.size(); k++)
    id = son[id][D[k]];
  return id;
}

int main() {
  int n, row, col;
  cin >> n >> row >> col;
  col ++;
  int log = decom(row);
  for (int i = 0; i < n; i++) {
    cin >> S[i];
    A[i] = S[i].size() + 1;
    //cin >> A[i];
  }
  for (int k = 0; k < log; k++)
    son[n][k] = n;
  int j = n, ac = 0;
  for (int i = n - 1; i >= 0; i--) {
    ac += A[i];
    while (ac > col)
      ac -= A[--j];
    if (ac == 0) {
      for (int k = 0; k < log; k++)
        son[i][k] = i;
    } else {
      son[i][0] = j;
      for (int k = 1; k < log; k++)
        son[i][k] = son[son[i][k-1]][k-1];
    }
  }
  int ans = 0, ss, ini;
  for (int i = 0; i < n; i++) {
    ss = getson(i);
    if (ss - i > ans) {
      ans = ss - i;
      ini = i;
    }
  }
  int i = ini;
  while (i - ini < ans) {
    j = son[i][0];
    cout << S[i];
    for (int k = i + 1; k < j; k++)
      cout << " " << S[k];
    cout << endl;
    i = j;
  }
}

