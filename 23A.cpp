#include<iostream>
#include<cstdio>

using namespace std;

string s;

bool instr(int ini,int fin)
{
	int t=s.size()-fin+ini+1;
	int r=0;
	for(int i=0;i<t;i++)
	{
		bool hay=true;
		for(int j=0;j<fin-ini;j++)
			if(s[i+j]!=s[ini+j])
			{
				hay=false;
				break;
			}
		if(hay)
			r++;
		if(r==2)
			return true;
	}
	return false;
}

int main()
{
	cin>>s;
	int rpta=0;
	for(int i=0;i<s.size();i++)
		for(int j=s.size();j>i;j--)
			if(instr(i,j))
				rpta=max(rpta,j-i);
	printf("%d\n",rpta);
}
