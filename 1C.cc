#include <vector>
#include <cmath>
#include <iostream>
#include <cstdio>

using namespace std;

template<typename T>
struct p2d {
  T x, y;
  p2d(T x = 0, T y = 0) : x(x), y(y) {}
  p2d(const p2d &a) : x(a.x), y(a.y) {}
  p2d operator - (const p2d &a) const {
    return p2d(x-a.x, y-a.y);
  }
  p2d operator + (const p2d &a) const {
    return p2d(x+a.x, y+a.y);
  }
  p2d operator * (T k) const {
    return p2d(x*k, y*k);
  }
  p2d operator / (T k) const {
    return p2d(x/k, y/k);
  }
  void operator -= (const p2d &a) {
    x -= a.x, y -= a.y;
  }
  void operator += (const p2d &a) {
    x += a.x, y += a.y;
  }
  void operator *= (T k) {
    x *= k, y *= k;
  }
  void operator /= (T k) {
    x /= k, y /= k;
  }
  bool operator == (const p2d &a) const {
    return x == a.x and y == a.y;
  }
  bool operator != (const p2d &a) const {
    return x != a.x or y != a.y;
  }
  bool operator < (const p2d &a) const {
    if (x == a.x)
      return y < a.y;
    return x < a.x;
  }

  T norm2() const {
    return x * x + y * y;
  }
  T norm() const {
      return sqrt(norm2());
  }
  p2d ort() const {
    return p2d(-y, x);
  }
  T operator % (const p2d &a) const {
    return x * a.y - y * a.x;
  }
  T operator * (const p2d &a) const {
    return x * a.x + y * a.y;
  }
};
typedef long double ldouble;
typedef p2d<ldouble> pt;

pt A[3];

pt circuncenter(const pt &a, const pt &b, const pt &c) {
    pt c1 = (a+b) / 2, d1 = (b-a).ort();
    pt c2 = (a+c) / 2, d2 = (c-a).ort();
    ldouble det = -d1.x*d2.y + d2.x*d1.y, v1 = c2.x - c1.x,
           v2 = c2.y - c1.y, t = (-d2.y*v1 + d2.x*v2) / det;
    return c1 + d1 * t;
}

const ldouble eps = 5e-6, pi = 4 * atan(1);
bool equal(ldouble a, ldouble b) {
    return a > b - eps and a < b + eps;
}

inline ldouble validate(ldouble a) {
    if (a < -1)
        a = -1;
    if (a > 1)
        a = 1;
    return a;
}

int main() {
    for (int i = 0; i < 3; ++i)
        cin >> A[i].x >> A[i].y;
    pt c = circuncenter(A[0], A[1], A[2]);
    ldouble radio;
    for (int i = 0; i < 3; ++i) {
        A[i] = A[i] - c;
        radio = A[i].norm();
        A[i] /= radio;
    }
    ldouble a1 = acos(validate(A[0] * A[1])),
           a2 = acos(validate(A[0] * A[2]));
    for (int n = 3; n <= 100; ++n) {
        ldouble ang = 2*pi/n, k = a1 / ang, r = a2 / ang;
        /*
        cout << ang << endl;
        cout << k << " " << round(k) << ", " << r << " " << round(r) << endl;
        */
        if (equal(round(k), k) and equal(round(r), r)) {
            ldouble ans = radio*radio*sin(pi/n)*cos(pi/n)*n;
            printf("%.6f\n", (double)ans);
            return 0;
        }
    }
    puts(":/");
}

