#include <iostream>
#include <cassert>
#define MAXN 300001

using namespace std;
int C[MAXN], next[MAXN];
int findNext(int i) {
    return (next[i] == i) ? i : (next[i] = findNext(next[i]));
}

inline bool setWinner(int l, int r, int p) {
  int x = findNext(l);
  while (x < r) {
    if (x == p) {
      x++;
    } else {
      C[x] = p;
      next[x] = x+1;
    }
    x = findNext(x);
  }
  return true;
}

int main() {
  int n, m;
  cin >> n >> m;
  for (int i = 0; i <= n; ++i) {
    C[i] = -1;
    next[i] = i;
  }
  int l, r, p;
  for (int i = 0; i < m; ++i) {
    cin >> l >> r >> p;
    l--, p--;
    setWinner(l, r, p);
  }
  cout << C[0] + 1;
  for (int i = 1; i < n; ++i)
    cout << " " << C[i] + 1;
  cout << endl;
}

