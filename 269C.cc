#include <iostream>
#include <vector>
#include <cstring>
#include <queue>
#define MAXN 200005

using namespace std;

struct edge {
  int v, id, f;
  bool dir;
  edge(int v, int id, int f, bool dir) :
    v(v), id(id), f(f), dir(dir) {}
  edge() {}
};

bool visit[MAXN];
bool ans[MAXN];
int In[MAXN], Out[MAXN];
vector<edge> adj[MAXN];

int n, m;

int main() {
  ios::sync_with_stdio(0);
  cin >> n >> m;
  memset(visit, 0, sizeof visit);
  memset(In, 0, sizeof In);
  memset(Out, 0, sizeof Out);
  int u, v, f;
  for (int i = 0; i < m; i++) {
    cin >> u >> v >> f;
    u --, v--;
    adj[u].push_back(edge(v, i, f, 0));
    adj[v].push_back(edge(u, i, f, 1));
    Out[u] += f;
    Out[v] += f;
  }
  queue<int> Q;
  Q.push(0);
  while (not Q.empty()) {
    int u = Q.front();
    Q.pop();
    if (visit[u])
      continue;
    visit[u] = true;
    for (int i = 0; i < adj[u].size(); i++) {
      edge &e = adj[u][i];
      if (not visit[e.v]) {
        In[e.v] += e.f;
        Out[e.v] -= e.f;
        ans[e.id] = e.dir;
        if (e.v != n - 1 and In[e.v] == Out[e.v])
          Q.push(e.v);
      }
    }
  }
  for (int i = 0; i < m; i++)
    cout << ans[i] << endl;
}
