#include <bits/stdc++.h>
using namespace std;


typedef int ll;

#define MAXN 1048576
#define center(x, y) (x + y) >> 1
#define left(x) (x << 1) + 1
ll st[MAXN];
int n;

void stupdate(int i, ll up,
    int idx = 0, int l = 0, int r = n - 1) {
  int L = left(idx), R = L + 1;
  if (r < i or l > i)
    return;
  if (l == r) {
    st[idx] = max(st[idx], up);
    return;
  }
  int mid = center(l, r);
  stupdate(i, up, L, l, mid);
  stupdate(i, up, R, mid + 1, r);
  st[idx] = max(st[L], st[R]);
}

ll stq(int i, int j, int idx = 0, int l = 0, int r = n - 1) {
  int L = left(idx), R = L + 1;
  if (l >= i and r <= j)
    return st[idx];
  if (r < i or l > j)
    return 0;
  int mid = center(l, r);
  return max(stq(i, j, L, l, mid), stq(i, j, R, mid+1, r));
}

typedef pair<int, int> pii;
pii A[2000000];
int V[MAXN];

inline int pos(int x) {
  return lower_bound(V, V+n, x) - V;
}

int main() {
  ios::sync_with_stdio(false);
  int n; 
  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> A[i].first >> A[i].second;
    V[i] = A[i].first - A[i].second;
  }
  sort(A, A+n);
  sort(V, V+n);
  ::n = unique(V, V+n) - V;
  int ans = 0, curr;
  for (int i = n - 1; i >= 0; --i) {
    curr = stq(pos(A[i].first + A[i].second), ::n-1);
    ans = max(ans, curr + 1);
    stupdate(pos(A[i].first - A[i].second), curr + 1);
  }
  cout << ans << endl;
  return 0;
}

