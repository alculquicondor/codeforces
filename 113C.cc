#include <bitset>
#include <cstdio>

std::bitset<150000002> P;

int main() {
  int l, r, i, j;
  scanf("%d%d", &l, &r);
  P.set();
  P.reset(0);
  int ans = 0;
  for (i = 9; i <= r; i += 6)
    P.reset(i >> 1);
  for (i = 25; i <= r; i += 10)
    P.reset(i >> 1);
  int w[] = {4, 2, 4, 2, 4, 6, 2, 6}, cur;
  for (i = 7, cur = 0; (j = i * i) <= r; i += w[cur++&7])
    if (P.test(i >> 1)) {
      for ( ; j <= r; j += i << 1)
        P.reset(j >> 1);
    }
  i = ((l >> 2) << 2) + 1;
  while (i < l)
    i += 4;
  for ( ; i <= r; i += 4)
    ans += P.test(i >> 1);
  ans += (l <= 2 and 2 <= r);
  printf("%d\n", ans);
  return 0;
}

