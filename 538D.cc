#include <iostream>
#include <cstring>
#define MAXN 105

using namespace std;

char T[MAXN][MAXN];
char R[MAXN][MAXN];
bool att[MAXN][MAXN];

int n;
bool validp(int x, int y) {
  return x >= 0 and x < n and y >= 0 and y < n;
}

int main() {
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> T[i];
  for (int dx = -(n-1); dx <= n-1; ++dx) {
    for (int dy = -(n-1); dy <= n-1; ++dy) {
      bool go = true;
      for (int i = 0; go and i < n; ++i)
        for (int j = 0; go and j < n; ++j)
          if (T[i][j] == 'o') {
            int ii = i + dx,
                jj = j + dy;
            if (validp(ii, jj) and T[ii][jj] == '.')
              go = false;
          }
      R[dx+n-1][dy+n-1] = go ? 'x' : '.';
    }
    R[dx+n-1][2*n-1] = '\0';
  }
  R[n-1][n-1] = 'o';
  bool valid = true;
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      if (T[i][j] == 'o') {
        for (int dx = -(n-1); dx <= n-1; ++dx)
          for (int dy = -(n-1); dy <= n-1; ++dy)
            if (R[dx+n-1][dy+n-1] == 'x') {
              int ii = i + dx,
                  jj = j + dy;
              if (validp(ii, jj))
                att[ii][jj] = true;
            }
      }
  for (int i = 0; valid and i < n; ++i)
    for (int j = 0; valid and j < n; ++j)
      if (T[i][j] == 'x' and not att[i][j])
        valid = false;
  if (valid) {
    cout << "YES" << endl;
    for (int i = 0; i < 2 * n - 1; ++i)
      cout << R[i] << endl;
  } else {
    cout << "NO" << endl;
  }
  return 0;
}

