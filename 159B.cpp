#include<cstdio>
#include<algorithm>

using namespace std;
typedef pair<int,int> pii;
pii mark[100001],tap[100001];
int n,m;

pii operator+(pii a,pii b)
{
    a.first+=b.first;
    a.second+=b.second;
    return a;
}

pii operate(int ini1,int fin1,int ini2,int fin2)
{
//    printf("%d %d %d %d: %d\n",ini1,fin1,ini2,fin2,ini1<n?mark[ini1].first:-1);
    pii ans(0,0);
    ans.first=min(fin1-ini1,fin2-ini2);
    int i=ini1,j=ini2;
    while(i<fin1 and j<fin2)
    {
//        printf(">%d %d\n",mark[i].second,tap[j].second);
        if(mark[i].second==tap[j].second)
            ans.second++,i++,j++;
        else if(mark[i].second<tap[j].second)
            i++;
        else
            j++;
    }
    return ans;
}

int main()
{
    scanf("%d %d",&n,&m);
    int t1,t2;
    pii ans(0,0);
    for(int i=0;i<n;i++)
    {
        scanf("%d %d",&t2,&t1);
        mark[i]=pii(t1,t2);
    }
    sort(mark,mark+n);
    for(int i=0;i<m;i++)
    {
        scanf("%d %d",&t2,&t1);
        tap[i]=pii(t1,t2);
    }
    sort(tap,tap+m);
    int last1=0,last2=0;
    int i=0,j=0;
    while(i<n and j<m)
    {
        while(i<n and mark[i].first==mark[last1].first)
            i++;
        while(j<m and tap[j].first<mark[last1].first)
            j++;
        last2=j;
        while(j<m and tap[j].first==mark[last1].first)
            j++;
        ans=ans+operate(last1,i,last2,j);
        last1=i;
        last2=j;
    }
    printf("%d %d\n",ans.first,ans.second);
}
