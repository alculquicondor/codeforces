#include <iostream>
#include <algorithm>
#define MAXN 1001

using namespace std;

const int INF = 1 << 30;
int n, m, x, y;
int W[MAXN];
int dp1[2][MAXN], dp2[2][MAXN];

inline int getcost(int id, bool c) {
  return c ? W[id] : n - W[id];
}

int main() {
  ios::sync_with_stdio(0);
  string s;
  cin >> n >> m >> x >> y;
  for (int i = 0; i < n; i++) {
    cin >> s;
    for (int j = 0; j < m; j++)
      W[j] += s[j] == '.';
  }
  for (int c = 0; c < 2; c++) {
    fill(dp1[c], dp1[c]+x, INF);
    fill(dp1[c]+x, dp1[c]+y+1, 0);
    dp1[c][y+1] = INF;
  }
  for (int i = m - 1; i >= 0; i --) {
    for (int c = 0; c < 2; c++) {
      for (int j = 0; j < x; j++)
        dp2[c][j] = getcost(i, c) + dp1[c][j+1];
      for (int j = x; j <= y; j++)
        dp2[c][j] = min(getcost(i, c) + dp1[c][j+1],
            getcost(i, 1-c) + dp1[1-c][1]);
      dp2[c][y+1] = INF;
    }
    for (int c = 0; c < 2; c++)
      copy(dp2[c], dp2[c]+m+1, dp1[c]);
  }
  cout << min(dp1[0][0], dp1[1][0]) << endl;
}
