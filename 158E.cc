#include <iostream>
#include <algorithm>
#define MAXN 4005

using namespace std;

int ini[MAXN], len[MAXN];
int DP[MAXN][MAXN];

int main() {
  int n, k;
  cin >> n >> k;
  for (int i = 0; i < n; i++) {
    cin >> ini[i] >> len[i];
    ini[i] --; 
  }
  for (int i = 0; i <= k; i++)
    DP[0][k] = 0;
  for (int i = 1; i <= n; i++) {
    DP[i][0] = max(DP[i-1][0], ini[i-1]) + len[i-1];
    for (int j = 1; j <= k; j++)
      DP[i][j] = min(DP[i-1][j-1],
          max(DP[i-1][j], ini[i-1])+len[i-1]);
  }
  int ans = 0;
  for (int i = 0; i < n; i++)
    ans = max(ans, ini[i]-DP[i][k]);
  ans = max(ans, 86400-DP[n][k]);
  cout << ans << endl;
}
