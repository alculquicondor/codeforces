#include <iostream>
#include <cstdlib>
#define MAXN 50001

using namespace std;

typedef long long ll;

int A[MAXN];

int main() {
  std::ios::sync_with_stdio(false);
  int n;
  ll h = 0, ans = 0, s = 0;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> A[i];
    s += A[i];
  }
  s /= n;
  for (int i = 0; i < n; ++i) {
    h += s - A[i];
    ans += abs(h);
  }
  cout << ans << endl;
}
