#include <iostream>
#define MAXN 1000002
#define MOD 1000000007

using namespace std;
typedef long long ll;
int A[MAXN];
ll pow[MAXN];

inline ll san(ll x) {
  if (x >= MOD)
    x -= MOD;
  return x;
}

int main() {
  ios::sync_with_stdio(0);
  int n, m, k, sz = 0;
  cin >> n >> m >> k;
  int u, v;
  for (int i = 0; i < m; i++) {
    cin >> u >> v;
    if (v - u == 1) {
    } else if (v - u == k + 1) {
      A[sz++] = u;
    } else {
      cout << 0 << endl;
      return 0;
    }
  }
  pow[0] = 1;
  for (int i = 1; i <= k; i++)
    pow[i] = san(pow[i-1] * 2);
  ll ans = 0;
  if (sz == 0) {
    ans = 1;
    for (int i = 1; i < n - k; i++)
      ans = san(ans + pow[min(i+k, n-k-1)-i]);
  } else if (A[sz-1] - A[0] <= k) {
    for (int i = max(A[sz-1] - k, 1); i < A[0]; i++)
      ans = san(ans + pow[min(i+k, n-k-1)-i-sz]);
    ans = san(ans + pow[min(A[0]+k, n-k-1)-A[0]-sz+1]);
  }
  cout << ans << endl;
}

