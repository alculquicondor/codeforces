#include <iostream>

using namespace std;

typedef long long ll;

bool F(ll a, ll b) {
  if (a == 0)
    return false;
  ll m = b % a;
  if (not F(m, a))
    return true;
  ll v = b / a - 1;
  if (a & 1)
    return v & 1;
  ll p = v % (a+1);
  return p < a ? p & 1 : true;
}

int main() {
  ios::sync_with_stdio(0);
  int tc;
  cin >> tc;
  ll a, b;
  while (tc--) {
    cin >> a >> b;
    if (a > b)
      swap(a, b);
    cout << (F(a, b) ? "First" : "Second") << endl;
  }
  return 0;
}

