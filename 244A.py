n, k = [int(x) for x in raw_input().split()]
U = [False]*(n*k)
A = [int(x)-1 for x in raw_input().split()]
for x in A:
    U[x] = True
for i in range(k):
    j = 0
    for x in range(n*k):
        if not U[x]:
            U[x] = True
            print x+1,
            j += 1
            if j == n-1:
                break
    print A[i]+1
