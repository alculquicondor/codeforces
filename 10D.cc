#include <iostream>
#include <cstring>
#include <algorithm>
#include <set>
#include <map>
#include <vector>
#define MAXN 1000

using namespace std;

typedef pair<int, int> pii;
int n, m;
int A[MAXN], B[MAXN];
vector<int> next[MAXN];
vector<int> pos[MAXN];
map<int, int> M;
int dp[MAXN][MAXN];
pii w[MAXN][MAXN];

int main() {
    ios::sync_with_stdio(0);
    cin >> n;
    for(int i=0; i<n; i++)
        cin >> A[i];
    cin >> m;
    for(int i=0; i<m; i++)
        cin >> B[i];
    for(int i=0; i<n; i++) {
        set<int> S;
        for(int j=i+1; j<n; j++)
            if(A[j] > A[i] and S.find(A[j])==S.end()) {
                S.insert(A[j]);
                next[i].push_back(j);
            }
    }
    int id = 1;
    for(int i=0; i<m; i++) {
        if(M.find(B[i]) == M.end())
            M[B[i]] = id++;
        pos[M[B[i]]].push_back(i);
    }
    int ans = 0;
    pii ini(-1, -1);
    memset(dp, 0, sizeof dp);
    vector<int>::iterator it;
    for(int i=n-1; i>=0; --i)
        for(int j=m-1; j>=0; --j)
            if(A[i] == B[j]) {
                int &now = dp[i][j];
                now = 1;
                w[i][j] = pii(-1, -1);
                for(int k=0; k<next[i].size(); k++) {
                    int se = A[next[i][k]];
                    vector<int> &V = pos[M[se]];
                    it = upper_bound(V.begin(), V.end(), j);
                    int tmp;
                    if(it!=V.end() and now < (tmp=1+dp[next[i][k]][*it])) {
                        now = tmp; 
                        w[i][j] = pii(next[i][k], *it);
                    }
                }
                if(ini == pii(-1, -1) or ans < now) {
                    ans = now;
                    ini = pii(i, j);
                }
            }
    cout << ans << endl;
    if(ans) {
        cout << A[ini.first];
        ini = w[ini.first][ini.second];
        while(ini != pii(-1, -1)) {
            cout << " " << A[ini.first];
            ini = w[ini.first][ini.second];
        }
    }
    cout << endl;
}
