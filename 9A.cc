#include <iostream>
#include <algorithm>

using namespace std;

int main() {
  int a, b;
  cin >> a >> b;
  a = max(a, b);
  int num = 6 - a + 1, den = 6, g = __gcd(num, den);
  cout << num/g << "/" << den/g << endl;
}
