n, m = [int(x) for x in raw_input().split()]
ans = 0
for a in range(n+1):
    b = n-a**2
    ans += b>=0 and a+b**2 == m
print ans
