#include <iostream>
#include <vector>
#define MAXN 300005

using namespace std;

typedef pair<int, int> pii;

vector<pii> E[MAXN];
int A[MAXN], T[MAXN];
int wmax, n;

int solve() {
  for (int w = wmax; w; --w) {
    for (auto &p : E[w])
      T[p.second] = A[p.second];
    for (auto &p : E[w])
      T[p.second] = max(T[p.second], A[p.first] + 1);
    for (auto &p : E[w])
      A[p.second] = T[p.second];
  }
  int ans = 0;
  for (int i = 0; i < n; ++i)
    ans = max(ans, A[i]);
  return ans;
}

int main() {
  std::ios::sync_with_stdio(false);
  int m, u, v, w;
  cin >> n >> m;
  while (m--) {
    cin >> u >> v >> w;
    E[w].push_back(pii(v, u));
    wmax = max(wmax, w);
  }
  cout << solve() << endl;
  return 0;
}
