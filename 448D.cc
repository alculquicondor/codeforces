#include <iostream>

using namespace std;

typedef long long ll;

int n, m;

ll go(ll x) {
  ll ans = 0;
  for (int i = 1; i <= n; ++i)
    ans += min(x / i, (ll)m);
  return ans;
}

int main() {
  ll k;
  cin >> n >> m >> k;
  ll lo = 0, hi = k + 1, mid;
  while (hi - lo > 1) {
    mid = (hi + lo) >> 1;
    if (go(mid) < k)
      lo = mid;
    else
      hi = mid;
  }
  cout << hi << endl;
}

