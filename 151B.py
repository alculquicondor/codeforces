def allequal(s):
    c = s[0]
    for x in s[1:]:
        if x == '-':
            pass
        elif x != c:
            return False
    return True

def decre(s):
    c = s[0]
    for x in s[1:]:
        if x == '-':
            continue
        elif x >= c:
            return False
        c = x
    return True

class PhBook(object):
    def __init__(self,name,L):
        self.tx = 0
        self.pz = 0
        self.gl = 0
        self.name = name
        for x in L:
            if allequal(x):
                self.tx += 1
            elif decre(x):
                self.pz += 1
            else:
                self.gl += 1
    def __str__(self):
        return self.name

n = int(raw_input())
P = list()
for i in range(n):
    s = raw_input().split()
    c = int(s[0])
    name = s[1]
    L = list()
    for j in range(c):
        L.append(raw_input())
    P.append(PhBook(name,L))

def liststr(L):
    s = L[0]
    for i in range(1,len(L)):
        s += ', '+L[i]
    return s

tx = max(P, key = lambda x: x.tx)
print 'If you want to call a taxi, you should call: '+liststr([str(x) for x in P if x.tx==tx.tx])+'.'
pz = max(P, key = lambda x: x.pz)
print 'If you want to order a pizza, you should call: '+liststr([str(x) for x in P if x.pz==pz.pz])+'.'
gl = max(P, key = lambda x: x.gl)
print 'If you want to go to a cafe with a wonderful girl, you should call: '+liststr([str(x) for x in P if x.gl==gl.gl])+'.'
