#include <iostream>

using namespace std;

inline int ceil(int y, int k) {
  int c = y / k;
  return c + (c * k != y);
}

int main() {
  ios::sync_with_stdio(0);
  int y, k, n;
  cin >> y >> k >> n;
  int l = ceil(y, k), r = n/k;
  if ( y == k * l)
    l ++;
  if (r < l) {
    cout << -1 << endl;
    return 0;
  }
  cout << l*k - y;
  for (int i = l + 1; i <= r; i++)
    cout << " " << i*k-y;
  cout << endl;
}
