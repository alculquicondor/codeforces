#include <iostream>
#include <algorithm>

using namespace std;
typedef long long ll;

int main() {
  ll n;
  cin >> n;
  if (n <= 2) {
    cout << n << endl;
    return 0;
  }
  if (n % 2) {
    cout << n * (n-1) * (n-2) << endl;
    return 0;
  }
  ll ans = (n-1) * (n-3), t = 0;
  for (int i = n; not t; i--)
    if (__gcd((ll)i, ans) == 1)
      t = i;
  cout << (ans * t) << endl;
}
