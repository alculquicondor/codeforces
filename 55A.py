n=int(raw_input())
i=1%n
k=2%n
v=[set() for j in range(n)]
v[0].add(1)
while (k not in v[i]):
	v[i].add(k)
	i=(i+k)%n
	k=(k+1)%n

for x in v:
	if len(x)==0:
		print 'NO'
		exit(0)
print 'YES'
