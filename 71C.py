def criba(n):
	factor=[1 for i in range(n)]
	for i in range(2,n):
		if factor[i]==1:
			for j in range(i,n,i):
				factor[j]=i
	return factor

n=int(raw_input())
A=[int(x) for x in raw_input().split()]
factor=criba(n+1)
m=n
while(m>1):
	f=factor[m]
	if f==2:
		if n%4==0:
			f=4
		else:
			break
	for i in range(n/f):
		vale=1
		for j in range(i,n,n/f):
			if not A[j]:
				vale=0
				break
		if vale:
			print 'YES'
			exit(0)
	m/=f
print 'NO'
