#include <iostream>
#include <cstdio>

using namespace std;

int n, A;
int L[10], P[10];
double ans;

void test() {
  double p = 0;
  for (int mask = 0; mask < (1 << n); mask++) {
    double t = 1;
    int B = 0;
    for (int i = 0; i < n; i++)
      t *= (mask & (1<<i) ? P[i] : (B+=L[i], 100-P[i]))/100.;
    if (__builtin_popcount(mask) <= n/2)
      t *= (double)A/(A+B);
    p += t;
  }
  ans = max(ans, p);
}

void bt(int id, int k) {
  if (id == n) {
    test();
    return;
  }
  bt(id + 1, k);
  int x = P[id];
  for (int i = 1; P[id] < 100 and i <= k; i++) {
    P[id] += 10;
    bt(id + 1, k - i);
  }
  P[id] = x;
}

int main() {
  int k;
  cin >> n >> k >> A;
  for (int i = 0; i < n; i++)
    cin >> L[i] >> P[i];
  ans = 0;
  bt(0, k);
  printf("%.8f\n", ans);
}
