A=[int(x) for x in raw_input().split()]

def trid(a,b,c):
	return a==b+c or b==a+c or c==a+b

def tri(a,b,c):
	return a<b+c and a>abs(b-c) and c<a+b and c>(b-a)

if tri(A[0],A[1],A[2]) or tri(A[0],A[1],A[3]) or tri(A[0],A[2],A[3]) or tri(A[1],A[2],A[3]) :
	print 'TRIANGLE'
	exit(0)

if trid(A[0],A[1],A[2]) or trid(A[0],A[1],A[3]) or trid(A[0],A[2],A[3]) or trid(A[1],A[2],A[3]) :
	print 'SEGMENT'
	exit(0)

print 'IMPOSSIBLE'
