#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

typedef long long ll;
const int MOD = 1000000007;

inline ll san(ll x) {
  if (abs(x) >= MOD)
    x %= MOD;
  if (x < 0)
    x += MOD;
  return x;
}

class Matrix {
  private:
    int n;
    vector<ll> M;
  public:
    Matrix(int n) : n(n) {
      M.assign(n*n, 0);
    }
    inline ll& operator() (int i, int j) {
      return M[i*n + j];
    }
    inline ll operator() (int i, int j) const {
      return M[i*n + j];
    }
    static Matrix identity(int n) {
      Matrix id(n);
      for (int i = 0; i < n; i++)
        id(i, i) = 1;
      return id;
    }
    Matrix operator*(Matrix &b) const {
      Matrix ans(n);
      for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
          for (int k = 0; k < n; k++) {
            ll &x = ans(i, j);
            x = san(x + (*this)(i, k)*b(k, j));
          }
      return ans;
    }
    Matrix exp(ll e) const {
      Matrix a = *this;
      Matrix p = identity(n);
      while (e) {
        if (e & 1)
          p = a * p;
        e >>= 1;
        a = a * a;
      }
      return p;
    }
};

int main() {
  Matrix M(2);
  int a, b, n;
  cin >> a >> b >> n;
  M(0, 0) = 1;
  M(0, 1) = -1;
  M(1, 0) = 1;
  M = M.exp(n-1);
  cout << san(M(1, 0) * b + M(1, 1) * a) << endl;
}
