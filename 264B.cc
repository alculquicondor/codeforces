#include <iostream>
#include <algorithm>
#include <vector>
#define MAXN 100002

using namespace std;

int F[MAXN];
void criba() {
  fill(F, F + MAXN, 1);
  for (int i = 2; i < MAXN; i++)
    if (F[i] == 1) {
      F[i] = i;
      for (long long j = (long long)i * i; j < MAXN; j += i)
        F[j] = i;
    }
}

int best[MAXN];

int main() {
  criba();
  int n, x, f, now, ans = 0;
  cin >> n;
  fill(best, best+MAXN, 0);
  for (int i = 0; i < n; i++) {
    vector<int> fac;
    cin >> x;
    while (x > 1) {
      f = F[x];
      while (x % f == 0)
        x /= f;
      fac.push_back(f);
    }
    now = 1;
    for (int j = 0; j < fac.size(); j++) {
      f = fac[j];
      now = max(now, best[f] + 1);
    }
    for (int j = 0; j < fac.size(); j++) {
      f = fac[j];
      best[f] = max(best[f], now);
    }
    ans = max(ans, now);
  }
  cout << ans << endl;
}
