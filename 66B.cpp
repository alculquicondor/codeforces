#include<cstdio>
#include<bits/stl_algobase.h>

using std::max;

int n;
int A[1001],L[1001],R[1001];

int left(int i)
{
	if(L[i]!=-1)
		return L[i];
	if(i>0 and A[i-1]<=A[i])
		L[i]=1+left(i-1);
	else
		L[i]=1;
	return L[i];
}

int right(int i)
{
	if(R[i]!=-1)
		return R[i];
	if(i<n-1 and A[i+1]<=A[i])
		R[i]=1+right(i+1);
	else
		R[i]=1;
	return R[i];
}

int main()
{
	scanf("%d",&n);
	for(int i=0;i<n;i++)
	{
		scanf("%d",&A[i]);
		L[i]=R[i]=-1;
	}
	int mm=0;
	for(int i=0;i<n;i++)
		mm=max(mm,left(i)+right(i)-1);
	printf("%d\n",mm);
}
