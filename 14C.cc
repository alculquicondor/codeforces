#include <iostream>
#include <algorithm>

using namespace std;

typedef long long type;

template<typename T>
struct p2d {
  T x, y;
  p2d(T x = 0, T y = 0) : x(x), y(y) {}
  p2d(const p2d &a) : x(a.x), y(a.y) {}
  p2d operator - (const p2d &a) const {
    return p2d(x-a.x, y-a.y);
  }
  p2d operator + (const p2d &a) const {
    return p2d(x+a.x, y+a.y);
  }
  p2d operator * (T k) const {
    return p2d(x*k, y*k);
  }
  p2d operator / (T k) const {
    return p2d(x/k, y/k);
  }
  void operator -= (const p2d &a) {
    x -= a.x, y -= a.y;
  }
  void operator += (const p2d &a) {
    x += a.x, y += a.y;
  }
  void operator *= (T k) {
    x *= k, y *= k;
  }
  void operator /= (T k) {
    x /= k, y /= k;
  }
  bool operator == (const p2d &a) const {
    return x == a.x and y == a.y;
  }
  bool operator != (const p2d &a) const {
    return x != a.x or y != a.y;
  }
  bool operator < (const p2d &a) const {
    if (x == a.x)
      return y < a.y;
    return x < a.x;
  }
  T operator * (const p2d &a) const {
    return x * a.x + y * a.y;
  }
};
typedef p2d<type> pt;
typedef pair<pt, pt> ppp;
ppp A[4];

bool valid() {
  pt p0 = A[0].first, p1 = A[0].second, d = p1 - p0;
  if (not(not d.x ^ not d.y))
    return false;
  for (int i = 1; i < 4; ++i) {
    if ((A[i].second - A[i].first) * d)
      return false;
    if (p1 == A[i].first) {
      d = A[i].second - p1;
      p1 = A[i].second;
    } else if (p1 == A[i].second) {
      d = A[i].first - p1;
      p1 = A[i].first;
    } else {
      return false;
    }
    if (not(not d.x ^ not d.y))
      return false;
  }
  return p1 == p0;
}

int main() {
  for (int i = 0; i < 4; ++i) {
    cin >> A[i].first.x >> A[i].first.y;
    cin >> A[i].second.x >> A[i].second.y;
  }
  sort(A, A+4);
  do {
    if (valid()) {
      cout << "YES" << endl;
      return 0;
    }
  } while (next_permutation(A, A+4));
  cout << "NO" << endl;
}
