#include <iostream>

using namespace std;

int main() {
  std::ios::sync_with_stdio(false);
  int n, k;
  cin >> n >> k;
  if (n == 1 and k == 0) {
    cout << 1 << endl;
    return 0;
  }
  int m = n / 2 - 1;
  k -= m;
  if (m < 0 or k <= 0) {
    cout << - 1 << endl;
    return 0;
  }
  for (int i = 0; i < m; ++i)
    cout << 2*i+1 << " " << 2*i+2 << " ";
  int x = 2 * m / k + 1;
  cout << x * k << " " << (x+1) * k;
  if (n & 1)
    cout << " " << (x+1)*k+1;
  cout << endl;
  return 0;
}
