#include <iostream>

using namespace std;

inline char val(int c) {
  return c + 'A';
}

#define N 4
int A[N];
int main() {
  string s;
  for (int i = 0; i < N; ++i) {
    cin >> s;
    A[i] = s.size()-2;
  }
  int mini = 0, maxi = 0;
  for (int i = 1; i < N; ++i) {
    if (A[i] < A[mini])
      mini = i;
    if (A[i] > A[maxi])
      maxi = i;
  }
  bool valid = true;
  for (int i = 0; i < N; ++i)
    if (i != mini and A[mini] > A[i] / 2)
      valid = false;
  if (not valid)
    mini = -1;
  valid = true;
  for (int i = 0; i < N; ++i)
    if (i != maxi and A[maxi] < A[i] * 2)
      valid = false;
  if (not valid)
    maxi = -1;
  if (mini != -1) {
    if (maxi == -1)
      cout << val(mini);
    else
      cout << 'C';
  } else {
    if (maxi != -1)
      cout << val(maxi);
    else
      cout << 'C';
  }
  cout << endl;
  return 0;
}
