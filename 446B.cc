#include <iostream>
#include <queue>
#define MAXN 1000005

using namespace std;

typedef long long ll;

ll R[MAXN], C[MAXN];

int p, k;

void solve(ll A[], int n, int m) {
  priority_queue<ll> Q;
  for (int i = 0; i < n; ++i)
    Q.push(A[i]);
  A[0] = 0;
  ll ans = 0;
  ll x;
  for (int i = 0; i < k; ++i) {
    x = Q.top();
    Q.pop();
    ans += x;
    Q.push(x-(ll)p*m);
    A[i+1] = ans;
  }
}

int main() {
  std::ios::sync_with_stdio(false);
  int n, m, x;
  cin >> n >> m >> k >> p;
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j) {
      cin >> x;
      R[i] += x;
      C[j] += x;
    }
  solve(R, n, m);
  solve(C, m, n);
  ll ans = - (1LL << 62);
  for (int i = 0; i <= k; ++i)
    ans = max(ans, R[i] + C[k-i] - i * (ll)(k-i) * p);
  cout << ans << endl;
}

