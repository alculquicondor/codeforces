#include <iostream>
#include <algorithm>
#include <cstring>

using namespace std;

typedef unsigned long long ll;

template<typename T>
struct Frac {
  T num, den;
  inline void simplify() {
    T g = __gcd(num, den);
    num /= g; den /= g;
  }
  Frac(T n = 0, T d = 1) : num(n), den(d) {
    simplify();
  }
  Frac operator + (const Frac &b) {
    T g = __gcd(den, b.den);
    return Frac(b.den / g * num + den / g * b.num,
        den / g * b.den);
  }
  Frac operator - (const Frac &b) {
    T g = __gcd(den, b.den);
    return Frac(b.den / g * num - den / g * b.num,
        den / g * b.den);
  }
};

#define MAXN 1000000
bool isP[MAXN];
void criba() {
  memset(isP, true, sizeof isP);
  for (int i = 2; i * i < MAXN; ++i)
    if (isP[i])
      for (int j = i * i; j < MAXN; j += i)
        isP[j] = false;
}

inline ll mulmod(ll a, ll e, const ll &m) {
  if (a < MAXN) {
    ll r = a * e;
    return r >= m ? r % m : r;
  }
  ll r = 0;
  while (e) {
    if (e & 1) {
      r += a;
      if (r >= m)
        r -= m;
    }
    e >>= 1;
    a += a;
    if (a >= m)
      a -= m;
  }
  return r;
}

inline ll powmod(ll a, ll e, const ll &m) {
  ll r = 1;
  while (e) {
    if (e & 1)
      r = mulmod(r, a, m);
    e >>= 1;
    a = mulmod(a, a, m);
  }
  return r;
}

ll B[] = {2, 3, 5, 7};
inline bool isprime(ll n) {
  if (n < MAXN)
    return isP[n];
  if ((n & 1) == 0)
    return false;
  int s = __builtin_ctzll(n-1);
  ll d = (n - 1) >> s, x;
  for (int i = 0; i < 4; ++i) {
    x = powmod(B[i], d, n);
    if (x == 1 or x == n-1)
      continue;
    for (int j = 1; j < s; ++j) {
      x = mulmod(x, x, n);
      if (x == 1)
        return false;
      if (x == n-1)
        goto next;
    }
    return false;
next:;
  }
  return true;
}

typedef Frac<ll> F;
int main() {
  std::ios::sync_with_stdio(false);
  criba();
  int tc, n;
  ll l, r;
  cin >> tc;
  while (tc--) {
    cin >> n;
    for (l = n; not isprime(l); --l) {}
    for (r = n+1; not isprime(r); ++r) {}
    //cerr << l << " " << r << endl;
    F ans = F(1, 2) - F(1, r);
    //cerr << ans.num << "/" << ans.den << endl;
    if (r-1 != n)
      ans = ans - F(r - n - 1, l * r);
    cout << ans.num << "/" << ans.den << endl;
  }
}
