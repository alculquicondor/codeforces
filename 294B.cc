#include <iostream>
#include <cstring>
#define MAXN 101

using namespace std;

const int INF = 1 << 30;
int n, x[MAXN], y[MAXN];
int M[MAXN][(MAXN+2)*MAXN];

int DP(int pos, int sum) {
  if (pos == n)
    return sum >= 0 ? 0 : INF;
  int &ans = M[pos][sum+MAXN*MAXN];
  if (ans >= 0)
    return ans;
  return ans = min(x[pos]+DP(pos+1, sum+x[pos]),
      DP(pos+1, sum-y[pos]));
}

int main() {
  cin >> n;
  for (int i = 0; i < n; i++)
    cin >> x[i] >> y[i];
  memset(M, -1, sizeof M);
  cout << DP(0, 0) << endl;
}
