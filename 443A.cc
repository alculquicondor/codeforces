#include <iostream>
#include <set>
#include <sstream>

using namespace std;

int main() {
  set<char> S;
  string s;
  getline(cin, s);
  stringstream ss(s);
  while (ss >> s) {
    for (int x: s)
      if (x >= 'a' and x <= 'z')
        S.insert(x);
  }
  cout << S.size() << endl;
}
