#include <cstdio>
#include <algorithm>
#include <cmath>

using namespace std;

typedef int type;

template<typename T>
struct p2d {
  T x, y;
  p2d(T x = 0, T y = 0) : x(x), y(y) {}
  p2d(const p2d &a) : x(a.x), y(a.y) {}
  p2d operator - (const p2d &a) const {
    return p2d(x-a.x, y-a.y);
  }
  bool operator == (const p2d &a) const {
    return x == a.x and y == a.y;
  }
  bool operator < (const p2d &a) const {
    if (x == a.x)
      return y < a.y;
    return x < a.x;
  }
  p2d direction() const {
    T g = __gcd(abs(x), abs(y));
    return p2d(x / g, y / g);
  }
  T operator % (const p2d &a) const {
    return x * a.y - y * a.x;
  }
  double pseudo_angle() const {
    return copysign(1. - x / (fabs(x) + fabs(y)), y);
  }
};
typedef p2d<type> pt;

#define MAXN 2000
pt P[MAXN];
pt A[MAXN];

int counts(int n) {
  int i = 0, j = 1, ans = 0;
  while (i < n) {
    int k = 1;
    while (i + k < n and P[i+k] == P[i])
      ++k;
    if (i + k > j)
      j = i + k;
    while (j < i + n and P[i] % P[j % n] > 0)
      ++j;
    i += k;
    ans += (j - i) * k;
  }
  return ans;
}

int main() {
  int n;
  scanf("%d", &n);
  for (int i = 0; i < n; ++i)
    scanf("%d %d", &A[i].x, &A[i].y);
  long long ans = 0;
  for (int i = 0; i < n; ++i) {
    for (int j = 0, k = 0; j < n; ++j)
      if (i != j)
        P[k++] = (A[j] - A[i]).direction();
    sort(P, P + n - 1, [] (const pt &a, const pt &b) {
        return a.pseudo_angle() < b.pseudo_angle();
      });
    ans += counts(n - 1);
  }
  printf("%ld\n", ans / 3);
}
