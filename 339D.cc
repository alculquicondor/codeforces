#include <iostream>
#define MAXN 262200

using namespace std;

int A[MAXN], N;

#define center(x, y) (x + y) >> 1
#define left(x) (x << 1) + 1

void update(bool f, int i, int up,
    int idx = 0, int l = 0, int r = N - 1) {
  if (r < i or l > i)
    return;
  if (l == r) {
    A[idx] = up;
    return;
  }
  int L = left(idx), R = L + 1, mid = center(l, r);
  update(not f, i, up, L, l, mid);
  update(not f, i, up, R, mid + 1, r);
  A[idx] = f ? A[L] | A[R] : A[L] ^ A[R];
}

void build(bool f, int idx = 0, int l = 0, int r = N -1) {
  if (l == r)
    return;
  int L = left(idx), R = L + 1, mid = center(l, r);
  build(not f, L, l, mid);
  build(not f, R, mid+1, r);
  A[idx] = f ? A[L] | A[R] : A[L] ^ A[R];
}

int main() {
  ios::sync_with_stdio(0);
  int n, q;
  cin >> n >> q;
  N = 1 << n;
  for (int i = 0; i < N; ++i)
    cin >> A[N+i-1];
  build(n & 1);
  int id, v;
  while (q--) {
    cin >> id >> v;
    update(n & 1, id-1, v);
    cout << A[0] << endl;
  }
  return 0;
}
