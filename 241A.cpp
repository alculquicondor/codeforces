#include <iostream>
#define MAXN 1001

using namespace std;

int main() {
    int n, k, d[MAXN], s[MAXN], m[MAXN];
    cin>>n>>k;
    for(int i=0; i<n; i++)
        cin>>d[i];
    for(int i=0; i<n; i++)
        cin>>s[i];
    m[0] = s[0];
    for(int i=1; i<n; i++)
        m[i] = max(m[i-1], s[i]);
    long long f = 0, ans = 0;
    int r;
    for(int i=0; i<n; i++) {
        f += s[i];
        while(f<d[i]) {
            ans += k;
            f += m[i];
        }
        ans += d[i];
        f -= d[i];
    }
    cout<<ans<<endl;
}
