#include <iostream>

using namespace std;

typedef long long ll;
typedef long double ld;

ll A, B;
ll calc(int i) {
  return A + i * B;
}
ll sumup(int l, ll q) {
  if (max(ld(q) * calc(l), B * ld(q * (q-1) / 2)) > 1e13)
    return 1e13;
  return q * calc(l) + B * q * (q - 1) / 2;
}

int solve(int l, ll t, ll m) {
  ll lo = 0, hi = t + 1, mid;
  while (hi - lo > 1) {
    mid = (hi + lo) >> 1;
    if (calc(l + mid - 1) > t or sumup(l, mid) > m * t)
      hi = mid;
    else
      lo = mid;
  }
  return lo ? l + lo : -1;
}

int main() {
  ios::sync_with_stdio(false);
  int n;
  cin >> A >> B >> n;
  for (int i = 0; i < n; ++i) {
    int l, t, m;
    cin >> l >> t >> m;
    cout << solve(l-1, t, m) << endl;
  }
  return 0;
}

