#include <iostream>
#include <vector>
#include <cstring>
#define MAXN 102

using namespace std;
vector<int> lang[MAXN];
bool C[MAXN][MAXN], visit[MAXN];
int n;

void dfs(int u) {
  visit[u] = true;
  for (int v = 0; v < n; v++)
    if (C[u][v] and not visit[v])
      dfs(v);
}

int main() {
  ios::sync_with_stdio(0);
  int m, c, l, zr = 0;
  cin >> n >> m;
  memset(visit, 0, sizeof visit);
  for (int i = 0; i < n; i++) {
    cin >> c;
    if (not c) {
      zr ++;
      visit[i] = 1;
    }
    while (c--) {
      cin >> l;
      lang[--l].push_back(i);
    }
  }
  memset(C, 0, sizeof C);
  int u, v;
  for (int k = 0; k < m; k++) {
    for (int i = 0; i < lang[k].size(); i++)
      for (int j = i + 1; j< lang[k].size(); j++) {
        u = lang[k][i], v = lang[k][j];
        C[u][v] = C[v][u] = 1;
      }
  }
  int comp = 0;
  for (int i = 0; i < n; i++)
    if (not visit[i]) {
      comp ++;
      dfs(i);
    }
  cout << (zr + comp - (bool)comp) << endl;
}
