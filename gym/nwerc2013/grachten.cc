#include <iostream>
#include <algorithm>

using namespace std;

int main() {
  int a, b, c, n, d, g;
  while (cin >> a >> b >> c) {
    n = a * b;
    d = c - b;
    g = __gcd(n, d);
    n /= g;
    d /= g;
    cout << n << "/" << d << endl;
  }
}
