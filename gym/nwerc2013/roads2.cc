#include <iostream>
#include <algorithm>
#define MAXN 2001

using namespace std;
int C[MAXN];
inline int findSet(int i) {
    return (C[i] == i) ? i : (C[i] = findSet(C[i]));
}
inline void unionSet(int i, int j) {
    C[findSet(i)] = findSet(j);
}

int dist[MAXN][MAXN], W[MAXN][MAXN];
vector<int> adj[MAXN];
struct edge {
  int w;
  size_t u, v;
  bool operator < (const edge &x) const {
    return w < x.w;
  }
};

void dfs(int u, int p, int d[]) {
  for (int v : adj[u])
    if (v != p) {
      d[v] = d[u] + W[u][v];
      dfs(v, u, d);
    }
}

int main() {
  std::ios::sync_with_stdio(false);
  size_t n;
  bool first = true;
  while (cin >> n) {
    if (not first)
      cout << endl;
    first = false;
    vector<edge> E;
    vector<int> used;
    for (size_t i = 0; i < n; ++i) {
      C[i] = i;
      adj[i].clear();
      for (size_t j = 0; j < n; ++j) {
        cin >> W[i][j];
        if (i < j)
          E.push_back({W[i][j], i, j});
      }
    }
    sort(E.begin(), E.end());
    int i = 0;
    for (edge &x : E) {
      if (findSet(x.u) != findSet(x.v)) {
        used.push_back(i);
        adj[x.u].push_back(x.v);
        adj[x.v].push_back(x.u);
      }
      unionSet(x.u, x.v);
      ++i;
    }
    for (size_t u = 0; u < n; ++u) {
      dist[u][u] = 0;
      dfs(u, -1, dist[u]);
    }
    i = 0;
    for (edge &x : E) {
      if (W[x.u][x.v] < dist[x.u][x.v]) {
        used.push_back(i);
        break;
      }
      ++i;
    }
    if (used.size() < n)
      used.push_back(0);
    for (int x : used) {
      edge &t = E[x];
      cout << t.u+1 << ' ' << t.v+1 << ' ' << t.w << endl;
    }
  }
}
