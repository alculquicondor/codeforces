#include <iostream>
#include <vector>
#define MAXN 1005

using namespace std;

typedef pair<int, int> pii;
const int inf = 10000;
int M[MAXN<<1][MAXN], used[MAXN<<1][MAXN], caso = 0;
struct Jingle {
  string s;
  int p;
  vector<pii> tree;
  vector<int> ball;
  vector<int> empty;
  bool valid;
  Jingle(const string &s) : s(s) {
    tree.reserve(s.size()>>1);
    ball.reserve(s.size()>>1);
    empty.reserve(s.size()>>1);
    p = 1;
    parse();
    valid = true;
  }
  void parse() {
    int id = tree.size();
    tree.push_back(pii(-1, -1));
    ball.push_back(0);
    empty.push_back(0);
    for (int j = 0; s[p] == '('; ++j) {
      ++p;
      if (j)
        tree[id].second = tree.size();
      else
        tree[id].first = tree.size();
      parse();
      ++p;
    }
    if (s[p] == 'B') {
      ball[id] = 1;
      ++p;
    }
  }
  void dfs_pre(int id = 0) {
    pii &t = tree[id];
    if (t.first == -1) {
      if (not ball[id])
        empty[id] = 1;
      return;
    }
    dfs_pre(t.first);
    dfs_pre(t.second);
    ball[id] = ball[t.first] + ball[t.second];
    empty[id] = empty[t.first] + empty[t.second];
  }
  int dfs(int id = 0, int d = 0) {
    pii &t = tree[id];
    int &ans = M[id][d+500];
    if (used[id][d+500] == caso)
      return ans;
    used[id][d+500] = caso;
    if (t.first == -1) {
      bool valid = d == 0 or (ball[id] ? d == -1 : d == 1);
      return ans = valid ? d != 0 : inf;
    }
    int x = (ball[t.second] - ball[t.first] + d) >> 1, y = d - x;
    int t1 = dfs(t.first, x) + dfs(t.second, y);
    y = (ball[t.first] - ball[t.second] + d) >> 1, x = d - y;
    int t2 = dfs(t.first, x) + dfs(t.second, y);
    return ans = min(min(t1, t2), inf);
  }
};

int main() {
  std::ios::sync_with_stdio(false);
  string s;
  while (cin >> s) {
    ++caso;
    Jingle x(s);
    x.dfs_pre();
    int ans = x.dfs();
    if (ans < inf)
      cout << (ans >> 1);
    else
      cout << "impossible";
    cout << endl;
  }
}
