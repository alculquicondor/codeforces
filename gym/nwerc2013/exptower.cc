#include <iostream>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <map>
#include <vector>

using namespace std;
typedef map<int, int> mii;
typedef long long ll;

int qpow(int a, int e) {
  int r = 1;
  while (e) {
    if (e & 1)
      r = r * a;
    e >>= 1;
    a = a * a;
  }
  return r;
}

int maxpot(int a, int l = 14) {
  if (a == 1)
    return 1;
  int r = 1, t;
  for (int i = 2; i < l; ++i) {
    t = pow(a, 1./i);
    if (a == qpow(t, i) or a == qpow(t+1, i))
      r = i;
  }
  return r;
}

mii factor(int x) {
  mii M;
  int c;
  for (int i = 2; i * i <= x; ++i) {
    c = 0;
    while (x % i == 0) {
      x /= i;
      ++c;
    }
    if (c)
      M[i] = c;
  }
  if (x > 1)
    M[x] ++;
  return M;
}

ll M[124618];
ll recu(int x) {
  ll &ans = M[x];
  if (ans)
    return ans;
  ans = 1;
  int p = maxpot(x, 17);
  for (int i = 1; i < p; ++i)
    if (p % i == 0)
      ans += recu(p / i);
  //cerr << "# " << x << ": " << ans << endl;
  return ans;
}

int main() {
  //cout << recu(65536) << endl;
  int a, b, c;
  while (scanf("%d^%d^%d", &a, &b, &c) != EOF) {
    int p = maxpot(a);
    mii F = factor(b);
    for (auto &it : F)
      it.second *= c;
    mii T = factor(p);
    for (auto &it : T)
      F[it.first] += it.second;
    int maxi = 0;
    vector<int> E;
    for (auto &it : F) {
      maxi = max(it.second, maxi);
      E.push_back(it.second);
    }
    ll ans = 0, t;
    for (int i = 2; i <= maxi; ++i) {
      t = 1;
      for (int &x : E)
        t *= (x / i + 1);
      ans += (t-1) * recu(i);
    }
    cout << ans << endl;
  }
}
