#include <cstdio>
#include <algorithm>
#include <vector>
#define MAXN 2005

using namespace std;
int C[MAXN];
inline int findSet(int i) {
    return (C[i] == i) ? i : (C[i] = findSet(C[i]));
}
inline void unionSet(int i, int j) {
    C[findSet(i)] = findSet(j);
}

int dist[MAXN][MAXN], W[MAXN][MAXN];
struct edge {
  int w, u, v;
  bool operator < (const edge &x) const {
    return w < x.w;
  }
};
int n;

int ans[MAXN], sz;
vector<edge> E;
inline void print(const edge &x) {
  printf("%d %d %d\n", x.u+1, x.v+1, x.w);
}
vector<int> adj[MAXN];
void kruskal() {
  for (int i = 0; i < n; ++i) {
    C[i] = i;
    adj[i].clear();
  }
  sz = 0;
  int i = 0;
  for (edge &x : E) {
    if (findSet(x.u) != findSet(x.v)) {
      unionSet(x.u, x.v);
      adj[x.u].push_back(x.v);
      adj[x.v].push_back(x.u);
      ans[sz++] = i;
    }
    ++i;
  }
}

void dfs(int u, int p, int d[]) {
  for (int v : adj[u])
    if (v != p) {
      d[v] = d[u] + W[u][v];
      dfs(v, u, d);
    }
}

void floyd() {
  for (int k = 0; k < n; ++k) {
    dist[k][k] = 0;
    dfs(k, -1, dist[k]);
  }
}

int main() {
  bool first = true;
  while (scanf("%d", &n) != EOF) {
    if (not first)
      printf("\n");
    first = false;
    E.clear();
    for (int i = 0; i < n; ++i)
      for (int j = 0; j < n; ++j) {
        scanf("%d", &W[i][j]);
        if (i > j)
          E.push_back({W[i][j], i, j});
      }
    sort(E.begin(), E.end());
    kruskal();
    floyd();
    int i = 0;
    for (edge &x : E) {
      if (x.w != dist[x.u][x.v]) {
        ans[sz++] = i;
        break;
      }
      ++i;
    }
    if (i == (int)E.size())
      ans[sz++] = 0;
    for (int i = 0; i < sz; ++i)
      print(E[ans[i]]);
  }
}
