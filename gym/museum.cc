#include <cstdio>
#include <algorithm>
#include <vector>
#include <cstring>
#include <iostream>
#define MAXV 100

using namespace std;
typedef pair<int, int> pii;

// Flow Algorithm
int C[MAXV][MAXV], F[MAXV][MAXV];
int visit[MAXV], pi[MAXV];
int iter, N;
vector<int> adj[MAXV];

bool dfs(int u) {
  visit[u] = iter;
  if (u == N-1) {
    while (u) {
      F[u][pi[u]] = -(++F[pi[u]][u]);
      u = pi[u];
    }
    return true;
  }
  for (int v : adj[u]) {
    if (C[u][v] - F[u][v] and visit[v] != iter) {
      pi[v] = u;
      if (dfs(v))
        return true;
    }
  }
  return false;
}
int maxflow() {
  int flow = 0;
  ++iter;
  while (dfs(0)) {
    ++flow;
    ++iter;
  }
  return flow;
}
// End of Flow Algorithm

int n;

int T[1440];
void get_capacity(int id, vector<pii> &V) {
  memset(T, 0, sizeof T);
  for (pii &p : V)
    for (int m = p.first; m < p.second; ++m)
      T[m] = 1;
  for (int m = 1; m < 1440; ++m)
    T[m] += T[m-1];
  for (int m = 0; m < 48; ++m)
    if (T[(m+1)*30-1] - (m ? T[m*30-1] : 0) == 30) {
      C[id][m+n+1] = 1;
      adj[id].push_back(m+n+1);
      adj[m+n+1].push_back(id);
    }
}

bool go(int c) {
  memset(F, 0, sizeof F);
  for (int m = 0; m < 48; ++m)
    C[m+n+1][n+49] = c;
  return maxflow() == c * 48;
}

int main() {
  int k, h, m, m1, m2, c;
  while (scanf("%d", &n) != EOF and n) {
    N = n + 50;
    for (int i = 1; i <= n; ++i) {
      scanf("%d %d", &k, &c);
      C[0][i] = c / 30;
      adj[0].push_back(i);
      vector<pii> free;
      free.reserve(k);
      while (k--) {
        scanf("%d:%d", &h, &m);
        m1 = h * 60 + m;
        scanf("%d:%d", &h, &m);
        m2 = h * 60 + m;
        if (m1 < m2) {
          free.push_back({m1, m2});
        } else if (m1 == m2) {
          free.push_back({0, 1440});
        } else {
          free.push_back({m1, 1440});
          free.push_back({0, m2});
        }
      }
      get_capacity(i, free);
    }
    for (int m = 0; m < 48; ++m)
      adj[m+n+1].push_back(n+49);
    int lo = 0, hi = n + 1, mid;
    while (hi - lo > 1) {
      mid = (hi + lo) >> 1;
      if (go(mid))
        lo = mid;
      else
        hi = mid;
    }
    printf("%d\n", lo);
    memset(C, 0, sizeof C);
    for (int i = 0; i < N; ++i)
      adj[i].clear();
  }
}
