#include <iostream>
#include <vector>

using namespace std;

struct event {
  int rel;
  vector<int> P;
};

vector<int> A;
vector<event> E;
const int inf = 1000000000;

int main() {
  ios::sync_with_stdio(0);
  int n, m;
  cin >> n >> m;
  A.assign(m+1, inf);
  E.resize(n);
  for (event &e : E) {
    int c;
    cin >> c >> e.rel;
    e.P.resize(c);
    for (int &p : e.P) {
      cin >> p;
      A[p] = min(A[p], e.rel);
    }
  }
  bool valid = true;
  for (event &e : E) {
    int x = 0;
    for (int &p : e.P)
      x = max(x, A[p]);
    valid &= x == e.rel;
  }
  if (valid) {
    cout << "Good memory" << endl;
    for (int i = 1; i <= m; ++i)
      cout << A[i] << ' ';
    cout << endl;
  } else {
    cout << "Poor memory" << endl;
  }
}
