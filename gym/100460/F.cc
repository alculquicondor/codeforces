#include <cstdio>
#include <iostream>
#include <utility>
#include <vector>
#include <algorithm>
#define M 10000
#define F first
#define S second
#define INF 0x3f3f3f3f
using namespace std;

typedef long long LL;
typedef pair<LL, LL> pii;

LL gcd(LL a, LL b) {
  if (!b) return a;
  return gcd(b, a%b);
}

pii in[M];
int m;
vector<pii> get_next() {
  for (int i = 0; i < m; ++i)
    cin >> in[i].F >> in[i].S;

  LL minx = INF, miny = INF;
  for (int i = 0; i < m; ++i) {
    minx = min(minx, in[i].F);
    miny = min(miny, in[i].S);
  }
  vector<pii> ans;
  int fac = -1;
  bool ini = 0;
  for (int i = 0; i < m; ++i) {
    ans.push_back(pii(in[i].F - minx, in[i].S - miny));
    if (ans[i].F != 0) {
      if (ini) {
        fac = gcd(fac, ans[i].F);
      } else {
        fac = ans[i].F;
        ini = true;
      }
    }
    if (ans[i].S != 0) {
      if (ini) {
        fac = gcd(fac, ans[i].S);
      } else {
        fac = ans[i].S;
        ini = true;
      }
    }
  }

  if (fac != -1) {
    for (int i = 0; i < m; ++i) {
      ans[i].F /= fac;
      ans[i].S /= fac;
    }
  }
  sort (ans.begin(), ans.end());
  return ans;
}

int main() {
  ios::sync_with_stdio(false);
  int n; cin >> n >> m;
  vector<pii> base = get_next();
  for (int i = 0; i < n; ++i) {
    vector<pii> shape = get_next();
    cout << ((shape == base) ? "YES":"NO") << "\n";
  }
  return 0;
}
