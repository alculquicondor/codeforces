#include <cstdio>
#include <algorithm>
#include <cstring>
#define N 100001

using namespace std;
char s[N];
char d[] = "desmond";
int main() {
  int m;
  scanf("%s %d", s, &m);
  int n = strlen(s);
  for (int i = 0; i+7 <= n; ++i) {
    bool eq = true;
    for (int a = 0; a < 7; ++a) {
      eq &= d[a] == s[i+a];
    }
    if (eq) {
      printf ("I love you, Desmond!\n%d\n", 0);
      return 0;
    }
  }
  for (int i = 0; i < m; ++i) {
    int p; char v;
    scanf("%d %c", &p, &v);
    --p;
    s[p] = v;
    for (int j = max(0, p-6); j <= p  && j+7 <= n; ++j) {
      bool eq = true;
      for (int a = 0; a < 7; ++a) {
        eq &= d[a] == s[j+a];
      }
      if (eq) {
        printf ("I love you, Desmond!\n%d\n", i+1);
        return 0;
      }
    }
  }
  printf("Goodbye, my love!\n");
  return 0;
}
