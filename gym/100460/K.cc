#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

vector<int> L[2], S[2];

int main() {
  int n, m;
  cin >> n;
  for (int i = 0; i < 2; ++i) {
    L[i].resize(n);
    for (int &x : L[i])
      cin >> x;
  }
  for (int i = 0; i < 2; ++i) {
    for (int j = 1; j < (int)L[i].size(); ++j)
      L[i][j] = max(L[i][j], L[i][j-1]);
  }
  cin >> m;
  for (int i = 0; i < 2; ++i) {
    S[i].resize(m);
    for (int &x : S[i])
      cin >> x;
  }
  for (int i = 0; i < m; ++i) {
    int p[2];
    for (int j = 0; j < 2; ++j)
      p[j] = upper_bound(L[j].begin(), L[j].end(), S[j][i]) - L[j].begin();
    //cerr << p[0] << ' ' << p[1] << endl;
    if (p[0] < p[1])
      cout << "Mike";
    else if (p[0] > p[1])
      cout << "Constantine";
    else
      cout << "Draw";
    cout << endl;
  }
}
