#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;
typedef long long ll;
vector<int> A, B, id;

int main() {
  int n;
  cin >> n;
  id.resize(n);
  A.resize(n);
  B.resize(n);
  for (int i = 0; i < n; ++i)
    id[i] = i;
  for (int &x : A)
    cin >> x;
  for (int &x : B)
    cin >> x;
  sort(id.begin(), id.end(), [&] (int i, int j) {
      return A[i] < A[j];
    });
  ll ac = 0;
  bool win = true;
  for (int i = 0; win and i < n; ++i) {
    ac += B[id[i]];
    win &= ac < A[id[i]];
  }
  cout << (win ? "Dire victory" : "Redemption") << endl;
}
