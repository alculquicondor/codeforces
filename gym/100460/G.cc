#include <vector>
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>

using namespace std;

typedef double type;

template<typename T>
struct p2d {
  T x, y;
  p2d(T x = 0, T y = 0) : x(x), y(y) {}
  p2d(const p2d &a) : x(a.x), y(a.y) {}
  p2d operator - (const p2d &a) const {
    return p2d(x-a.x, y-a.y);
  }
  p2d operator + (const p2d &a) const {
    return p2d(x+a.x, y+a.y);
  }
  p2d operator * (T k) const {
    return p2d(x*k, y*k);
  }
  p2d operator / (T k) const {
    return p2d(x/k, y/k);
  }
  void operator -= (const p2d &a) {
    x -= a.x, y -= a.y;
  }
  void operator += (const p2d &a) {
    x += a.x, y += a.y;
  }
  void operator *= (T k) {
    x *= k, y *= k;
  }
  void operator /= (T k) {
    x /= k, y /= k;
  }
  bool operator == (const p2d &a) const {
    return x == a.x and y == a.y;
  }
  bool operator != (const p2d &a) const {
    return x != a.x or y != a.y;
  }
  bool operator < (const p2d &a) const {
    if (x == a.x)
      return y < a.y;
    return x < a.x;
  }

  T norm2() const {
    return x * x + y * y;
  }
  T norm() const {
    return sqrt(norm2());
  }
  void reduce() {
    T g = __gcd(x, y);
    x /= g, y /= g;
  }
  p2d ort() const {
    return p2d(-y, x);
  }
  T operator % (const p2d &a) const {
    return x * a.y - y * a.x;
  }
  T operator * (const p2d &a) const {
    return x * a.x + y * a.y;
  }
};
typedef p2d<type> pt;

pt circuncenter(const pt &a, const pt &b, const pt &c) {
    pt c1 = (a+b) / 2, d1 = (b-a).ort();
    pt c2 = (a+c) / 2, d2 = (c-a).ort();
    type det = -d1.x*d2.y + d2.x*d1.y, v1 = c2.x - c1.x,
           v2 = c2.y - c1.y, t = (-d2.y*v1 + d2.x*v2) / det;
    return c1 + d1 * t;
}

struct rtype {
  double r;
  pt c;
};

rtype three(const pt &a, const pt &b, const pt &c) {
  pt d;
  double r;
  if (fabs((b - a) % (c - a)) < 1e-7) {
    d = circuncenter(a, b, c);
    r = (a - d).norm();
  } else {
    r = 1e4;
  }
  return {r, d};
}

rtype two(const pt &a, const pt &b, const pt &c) {
  pt d = (a + b) / 2;
  double r = (b - a).norm() / 2;
  r = max(r, (c - d).norm());
  return {r, d};
}

int main() {
  pt a, b, c, d;
  double r;
  cin >> r;
  cin >> a.x >> a.y;
  cin >> b.x >> b.y;
  cin >> c.x >> c.y;
  /*
  rtype R = three(a, b, c);
  rtype T = two(a, b, c);
  if (T.r < R.r)
    R = T;
  T = two(a, c, b);
  if (T.r < R.r)
    R = T;
  T = two(b, c, a);
  if (T.r < R.r)
    R = T;

  if (R.r < r + 1e-3) {
    cout << "YES" << endl;
    cout << fixed << setprecision(10) << R.c.x << ' ' << R.c.y << endl;
  } else {
    cout << "NO" << endl;
  }
  */
  cout << "NO" << endl;
}
