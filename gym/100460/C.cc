#include <cstdio>
#include <vector>
#include <utility>
#include <queue>
#include <algorithm>
#define N 100002
#define F first
#define S second
#define INF 0x3f3f3f3f


using namespace std;

typedef pair<int, int> pii;
typedef pair<int, pii> state;

int dsu[N];

void init(int n) {
  for (int i = 1; i <= n; ++i) {
    dsu[i] = i;
  }
}

int find(int i) {
  return (dsu[i] == i) ? i : (dsu[i] = find(dsu[i]));
}

void join(int i, int j) {
  dsu[find(i)] =  find(j);
}

vector<pii> gr[N];
int dis1[N], dis2[N];

vector<pii> tree[N];

void di(int v, int pr, int acc, int* dis) {
  for (pii nx: tree[v]) {
    int u = nx.F; int w = nx.S;
    di()
  }
}

int main() {
  int n, m;
  scanf("%d%d", &n, &m);
  if (n == 1) {
    printf("0\n");
    return 0;
  }
  vector<state> ed;
  for (int i = 0; i < m; ++i) {
    int a, b, c;
    scanf("%d%d%d", &a, &b, &c);
    ed.push_back(state(c, pii(a,b)));
    gr[a].push_back(pii(b, c));
    gr[b].push_back(pii(a, c));
  }
  sort(ed.begin(), ed.end());
  init(n);
  for (state s: ed) {
    int a = s.S.F, b= s.S.S;
    if (find(a) != find(b)) {
      tree[a].push_back(pii(b, c));
      tree[b].push_back(pii(a, c));
    }
    join(a, b);
  }

  int ans = INF;
  for (int v = 1; v <= n; ++v) {
    for (pii nx: gr[v]) {
      int u = nx.F, w = nx.S;
      vector<int> ord;
      ord.push_back(w);
      ord.push_back(dis1[v]);
      ord.push_back(dis2[u]);
      sort(ord.begin(), ord.end());
      if (ord.back() ! INF) {
        ans = min(ans, ord[1]);
      }
    }
  }
  printf("%d\n", ans);
  return 0;
}
