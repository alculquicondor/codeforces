#include <cstdio>
#include <algorithm>
#include <cstring>
#include <set>
#include <map>
#include <iostream>
#include <string>
#include <vector>
#include <cassert>
#define F first
#define S second
#define N 100
#define D 10
#define mp make_pair
using namespace std;
bool used[N];
vector<int> pos[D];
int dig[D];
vector<pair<int, string> > sol;
string s;
int n;

int toNum(string s) {
  int val = 0;
  for (int i = 0; i < (int)s.size(); ++i) {
    val *= 10;
    val += s[i] - '0';
  }
  return val;
}

bool brute(int d) {
  if (d == D) {
    set<int> elem;
    for (int i = 1; i < D; ++i) {
      elem.insert(i);
    }
    for (int i = 0; i < (int)sol.size(); ++i) {
      elem.insert(toNum(sol[i].S));
    }
    for (int i = 0; i < (int)s.size(); ) {
      if (!used[i]) {
        if (i + 1 >= (int)s.size() || used[i + 1]) {
          return false;
        }
        elem.insert(toNum(s.substr(i, 2)));
        i += 2;
      } else {
        ++i;
      }
    }
    for (int i = 1; i <= n; ++i) {
      if (elem.count(i) == 0) {
        return false;
      }
    }
    return true;
  }
  if (pos[d].empty()) {
    return brute(d + 1);
  }
  for (int i = 0; i < (int)pos[d].size(); ++i) {
    dig[d] = pos[d][i];
    used[dig[d]] = true;
    if (brute (d + 1)) {
      return true;
    }
    used[dig[d]] = false;
  }
  return false;
}

int main() {
  freopen("joke.in", "r", stdin);
  freopen("joke.out", "w", stdout);
  cin >> s;
  if (s.size() < 10) {
    n = s.size();
  } else {
    n = (s.size() + 9) / 2;
  }
  string ns;
  for (int i = 0; i < (int)s.size(); ++i) {
    if (s[i] == '0') {
      used[i-1] = used[i] = true;
      sol.push_back(mp(i - 1, s.substr(i-1, 2)));
    }
  }
  map<string, pair<int, int> > cnt;
  for (int i = 0 ; i < (int)s.size(); ++i) {
    if (used[i]) continue;
    if (s[i] >= '5') {
      if (i) {
        if (used[i-1]) {
          ++cnt[s.substr(i, 1)].F;
          cnt[s.substr(i, 1)].S = i;
        } else {
          int val = (s[i-1] - '0') * 10 + s[i] - '0';
          if (val <= n) {
            ++cnt[s.substr(i-1, 2)].F;
            cnt[s.substr(i-1, 2)].S = i-1;
          } else {
            ++cnt[s.substr(i, 1)].F;
            cnt[s.substr(i, 1)].S = i;
          }
        }
      } else {
        ++cnt[s.substr(i, 1)].F;
        cnt[s.substr(i, 1)].S = i;
      }
    }
  }
  for (auto data: cnt) {
    if (data.S.F == 1) {
      sol.push_back(mp(data.S.S, data.F));
      for (int i = 0 ; i < (int)data.F.size(); ++i) {
        used[data.S.S + i] = true;
      }
    }
  }
  for (int i = 0; i < (int)s.size(); ++i) {
    if (!used[i]) {
      pos[s[i]-'0'].push_back(i);
    }
  }
  memset(dig, -1, sizeof dig);

  assert(brute(1));
  for (int i = 1; i < D; ++i) {
    if (dig[i] != -1) {
      sol.push_back(mp(dig[i], s.substr(dig[i], 1)));
    }
  }
  sort(sol.begin(), sol.end());
  reverse(sol.begin(), sol.end());
  bool f = false;
  for (int i = 0; i < (int)s.size(); ) {
    if (f) {
      cout << ' ';
    }
    f = true;
    if (used[i]) {
      cout << sol.back().S;
      i += sol.back().S.size();
      sol.pop_back();
    } else {
      cout << s[i] << s[i + 1];
      i += 2;
    }
  }
  cout << endl;
  return 0;
}
