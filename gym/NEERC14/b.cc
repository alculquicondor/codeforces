#include <iostream>
#include <algorithm>
#include <cstdio>
#define MAXN 100000

using namespace std;

int n, A, B;
struct Descr {
  int g, a, b, id;
} D[MAXN];
double ans[MAXN];

int main() {
  freopen("burrito.in", "r", stdin);
  freopen("burrito.out", "w", stdout);
  cin >> n >> A >> B;
  for (int i = 0; i < n; ++i) {
    cin >> D[i].g >> D[i].a >> D[i].b;
    D[i].id = i;
  }
  sort(D, D+n, [](const Descr &x, const Descr &y) {
      return x.a * y.b > x.b * y.a;
    });
  double qa = 0, qb = 0;
  for (int i = 0; i < n; ++i) {
    Descr &d= D[i];
    if (qb + d.g * d.b <= B)
      ans[d.id] = d.g;
    else
      ans[d.id] = (B - qb) / d.b;
    qa += ans[d.id] * d.a;
    qb += ans[d.id] * d.b;
  }
  if (qa < A) {
    cout << "-1 -1" << endl;
  } else {
    cout.precision(9);
    cout << fixed;
    cout << qa << ' ' << qb << endl;
    cout << ans[0];
    for (int i = 1; i < n; ++i)
      cout << ' ' << ans[i];
    cout << endl;
  }
  return 0;
}
