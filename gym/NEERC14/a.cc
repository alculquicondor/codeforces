#include <iostream>
#include <cstdio>

using namespace std;

int main() {
  freopen("alter.in", "r", stdin);
  freopen("alter.out", "w", stdout);
  int n, m;
  cin >> n >> m;
  cout << n / 2 + m / 2 << endl;
  for (int i = 2; i <= n; i += 2)
    cout << i << ' ' << 1 << ' ' << i << ' ' << m << endl;
  for (int i = 2; i <= m; i += 2)
    cout << 1 << ' ' << i << ' ' << n << ' ' << i << endl;
  return 0;
}
