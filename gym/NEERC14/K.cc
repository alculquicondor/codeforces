#include <cstdio>
#include <cstdlib>
#define N 1000
int a[N], b[N];

int main() {
  freopen("knockout.in", "r", stdin);
  freopen("knockout.out", "w", stdout);
  int n, m; scanf("%d%d", &n, &m);
  for (int i = 0; i < n; ++i) {
    scanf("%d%d", a + i, b + i);
  }
  for (int i = 0; i < m; ++i) {
    int x, y, t;
    scanf("%d%d%d", &x, &y, &t);
    int ans = 0;
    for (int j = 0; j < n; ++j) {
      int p;
      int l = abs(b[j] - a[j]);
      int r = t % (l * 2);
      if (r <= l) {
        if (a[j] < b[j]) {
          p = a[j] + r;
        } else {
          p = a[j] - r;
        }
      } else {
        if (a[j] < b[j]) {
          p = b[j] - (r - l);
        } else {
          p = b[j] + (r - l);
        }
      }
      ans += x <= p && p <= y;
    }
    printf("%d\n", ans);
  }
  return 0;

}
