#include <iostream>
#include <bitset>
#include <cstdio>
#define MAXN 1000

using namespace std;

typedef unsigned long long T;
typedef bitset<MAXN> BS;

T a[MAXN];
BS p[MAXN];
bool valid[MAXN];

inline int val(char c) {
  if (c >= 'a')
    return 10 + c - 'a';
  return c - '0';
}

BS build(const string &s) {
  BS r;
  for (size_t i = 0; i < s.size(); ++i)
    for (size_t j = 0; j < 4; ++j)
      r.set((i<<2)+j, val(s[i]) & (1<<j));
  return r;
}

int main() {
  freopen("filter.in", "r", stdin);
  freopen("filter.out", "w", stdout);
  size_t m, f, n, q;
  cin >> m >> f;
  for (size_t i = 0; i < f; ++i)
    cin >> a[i];
  cin >> n;
  string s;
  for (size_t i = 0; i < n; ++i) {
    cin >> s;
    p[i] = build(s);
  }
  cin >> q;
  T u;
  for (size_t i = 0; i < q; ++i) {
    cin >> u;
    BS now;
    for (size_t j = 0; j < f; ++j)
      now.set((u*a[j])%m);
    for (size_t j = 0; j < n; ++j)
      valid[j] |= not valid[j] and (p[j] & now) == now;
  }
  size_t cnt = 0;
  for (size_t i = 0; i < n; ++i)
    if (valid[i])
      ++cnt;
  cout << cnt;
  for (size_t i = 0; i < n; ++i)
    if (valid[i])
      cout << ' ' << i;
  cout << endl;
  return 0;
}
