#include <iostream>
#include <cstdio>

using namespace std;
int n;

inline int count(string &s) {
    int ans = 0;
    int i = 0, j;
    while(i<n) {
        j = i+1;
        while(j<n and s[i]==s[j])
            j+=1;
        ans ++;
        i = j;
    }
    return ans;
}

int mini(string s) {
    int i=0, j;
    while(i<n) {
        j = i;
        while(j<n and s[j]=='?')
            j++;
        if(j==n) {
            if(i==0)
                return 1;
            for(int k=i; k<j; k++)
                s[k] = s[i-1];
        }
        else {
            for(int k=i; k<j; k++)
                s[k] = s[j];
        }
        i = j+1;
    }
    return count(s);
}

char cont(char c, int i) {
    if(c=='+')
        return i%2?'+':'-';
    return i%2?'-':'+';
}

int maxi(string s) {
    int i=0, j;
    while(i<n) {
        j = i;
        while(j<n and s[j]=='?')
            j++;
        if(i==j) {
            i = j+1;
            continue;
        }
        if(j==n) {
            if(i==0)
                return n;
            for(int k=i; k<j;k++)
                s[k] = cont(s[i-1], k-i);
        }
        else {
            for(int k=i; k<j; k++)
                s[k] = cont(s[j], j-k-1);
        }
        i = j+1;
    }
    return count(s);
}

int main() {
    freopen("rle-size.in", "r", stdin);
    freopen("rle-size.out", "w", stdout);
    string s;
    cin>>n>>s;
    cout<<mini(s)<<" "<<maxi(s)<<endl;
}
