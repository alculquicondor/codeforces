#include <iostream>
#include <algorithm>
#include <cstdio>
#define MAXN 100000

using namespace std;

template<typename T>
struct Frac {
  T num, den;
  inline void simplify() {
    T g = __gcd(num, den);
    num /= g; den /= g;
  }
  Frac(T n = 0, T d = 1) : num(n), den(d) {
    simplify();
  }
  Frac operator + (const Frac &b) {
    T g = __gcd(den, b.den);
    return Frac(b.den / g * num + den / g * b.num,
        den / g * b.den);
  }
  Frac operator - (const Frac &b) {
    T g = __gcd(den, b.den);
    return Frac(b.den / g * num - den / g * b.num,
        den / g * b.den);
  }
  bool operator < (const Frac &b) {
    T g = __gcd(den, b.den);
    return b.den / g * num < den / g * b.num;
  }
};
typedef long long ll;
typedef Frac<ll> Fraction;
typedef pair<int, int> pii;

pii A[MAXN];

int main() {
  int n;
  //freopen("caravan.in", "r", stdin);
  //freopen("caravan.out", "w", stdout);
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> A[i].first >> A[i].second;
  sort(A, A+n);
  Fraction ans(1000000), t;
  for (int start = 0, end = 0; end < n; ) {
    t = Fraction(A[end].second - A[end].first);
    if (t < ans)
      ans = t;
    t = Fraction(A[end].second - A[start].first, end-start+1);
    if (t < ans)
      ans = t;
    ++end;
    while (end - start > 1 and not
        (Fraction(A[start+1].first) < Fraction(A[start].first) + ans))
      ++start;
    //cout << end-start << endl;
  }
  cout << ans.num << '/' << ans.den << endl;
  return 0;
}

