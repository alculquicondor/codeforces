#include <iostream>
#include <algorithm>
#include <vector>
#include <cstring>
#define MAXN 201

using namespace std;

double D[MAXN][MAXN];
int tr[MAXN][MAXN];

struct pt {
  double x, y, z;
  double dist(const pt &b) const {
    return hypot(hypot(x - b.x, y - b.y), z - b.z);
  }
} P[MAXN];

void addEdge(int u, int v, double d) {
  D[u][v] = min(D[u][v], d);
  tr[u][v] = u;
}

void construct(int u, int v) {
  vector<int> V;
  while (v != u) {
    V.push_back(v);
    v = tr[u][v];
  }
  cout << u;
  reverse(V.begin(), V.end());
  for (int v : V)
    cout << ' ' << v;
  cout << endl;
}

int main() {
  ios::sync_with_stdio(0);
  int n, m;
  cin >> n >> m;
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      D[i][j] = 1e300;
  memset(tr, -1, sizeof tr);
  int fl;
  for (int i = 0; i < n; ++i) {
    cin >> fl >> P[i].x >> P[i].y;
    P[i].z = fl * 5;
  }
  string kind;
  while (m--) {
    int u, v;
    double d;
    cin >> u >> v >> kind;
    if (kind[0] == 'e') {
      d = P[u].dist(P[v]);
      addEdge(u, v, 1);
      addEdge(v, u, d * 3);
    } else if (kind[0] == 'l') {
      addEdge(u, v, 1);
      addEdge(v, u, 1);
    } else {
      d = P[u].dist(P[v]);
      addEdge(u, v, d);
      addEdge(v, u, d);
    }
  }
  for (int i = 0; i < n; ++i)
    for (int u = 0; u < n; ++u)
      for (int v = 0; v < n; ++v)
        if (D[u][v] > D[u][i] + D[i][v]) {
          D[u][v] = D[u][i] + D[i][v];
          tr[u][v] = tr[i][v];
        }
  int q;
  cin >> q;
  while (q --) {
    int u, v;
    cin >> u >> v;
    construct(u, v);
  }
  return 0;
}

