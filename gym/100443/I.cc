#include <iostream>
#include <map>
#include <queue>
#include <algorithm>

using namespace std;

typedef unsigned long long Hash;
typedef pair<int, Hash> pih;
typedef pair<pih, string> store;

bool operator < (const store &a, const store &b) {
  return a.first < b.first;
}

pih get_Hash(const string &s) {
  Hash h = 0;
  for (char c : s)
    h = h * 31 + (c - 'a');
  return {s.size(), h};
}

typedef map<pih, int> f_tab;
f_tab current;
queue<f_tab> Q;

void process(const f_tab &f) {
  if (Q.size() == 7) {
    for (auto p : Q.front())
      current[p.first] -= p.second;
    Q.pop();
  }
  for (auto p : f)
    current[p.first] += p.second;
  Q.push(f);
}

int main() {
  std::ios::sync_with_stdio(false);
  vector<store> words;
  string s;
  int t;
  while (cin >> s) {
    if (s[2] == 'e') {
      f_tab today;
      while (cin >> s and s[1] != '/') {
        if (s.size() > 3) {
          auto h = get_Hash(s);
          words.push_back({h, s});
          ++today[h];
        }
      }
      process(today);
    }
    else {
      cin >> t >> s;
      sort(words.begin(), words.end());
      words.resize(unique(words.begin(), words.end()) - words.begin());
      sort(words.begin(), words.end(), [&] (const store &a, const store &b) {
          return current[a.first] == current[b.first] ? a.second < b.second : current[a.first] > current[b.first];
        });
      cout << "<top " << t << ">" << endl;
      int upto = t <= (int)words.size() ? current[words[t-1].first] : 1000000;
      for (int i = 0; i < (int)words.size() and (current[words[i].first] >= upto); ++i)
        cout << words[i].second << ' ' << current[words[i].first] << endl;
      cout << "</top>" << endl;
    }
  }
  return 0;
}
