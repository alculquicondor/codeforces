#include <cstdio>
#include <utility>
#include <algorithm>
#include <vector>
#define N 105
#define F first
#define S second
using namespace std;
typedef pair<int, int> pii;
typedef pair<pii, int> tri;

tri courses[N];
tri event[2*N];
int cl[N][N];

int main() {
  int t; scanf("%d", &t);
  int caso = 0;
  while (t--) {
    int n, m; scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; ++i) {
      scanf("%d%d%d", &courses[i].F.F, &courses[i].F.S, &courses[i].S);
      event[2*i] = tri(pii(courses[i].F.F, i),  courses[i].S);
      event[2*i+1] = tri(pii(courses[i].F.S, -i), courses[i].S);
    }
    for  (int i = 1; i <= n; ++i) {
      for (int j = 1; j <= n; ++j) {
        scanf("%d", cl[i] + j);
      }
    }
    sort(event, event + 2*n + 2);
    vector<tri> avai;

    int ans = 0;
    for (int i = 2; i < 2*n+2; ++i) {
      if (event[i].F.S < 0) {
        avai.push_back(event[i]);
        avai.back().F.S = -avai.back().F.S;
        avai.back().S = (avai.back().S + m - 1) / m;
        continue;
      }
      int need = (event[i].S + m - 1)/m;
      for (tri &av: avai) {
        if (cl[av.F.S][event[i].F.S] + av.F.F+1  <= event[i].F.F) {

          int use = min(need, av.S);
          av.S -= use;
          need -= use;
        }
      }
      ans += need;
    }
    printf("Case %d: %d\n", ++caso, ans);
  }
  return 0;
}
