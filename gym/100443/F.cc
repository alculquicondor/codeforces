#include <iostream>
#define MAX 1000000000000000000LL
using namespace std;


typedef long long ll;

ll mod2_fives(ll x) {
  ll ans = 0;
  while (x) {
    x /= 5;
    ans += x;
  }
  return ans % 2;
}

ll f[100][2];
void init() {
  int e = 0;
  ll pw = 1;
  f[e][0] = 1;
  f[e][1] = 0;
  for (; pw*5 <= MAX; ) {
    ++e;
    pw *= 5;
    if (e % 2) {
      f[e][0] = 5 * f[e-1][0];
      f[e][1] = 5 * f[e-1][1];
    } else {
      f[e][0] = 3 * f[e-1][0] + 2 * f[e-1][1];
      f[e][1] = 2 * f[e-1][0] + 3 * f[e-1][1];
    }
  }
}

int main() {
  ios::sync_with_stdio(false);
  ll x;
  init();
  while (cin >> x and x != -1) {
    int e = 1;
    ll pw = 5;
    ll ans = 0;
    for (; x >= 0; ++e, pw *= 5) {
      while ((x+1) % pw != 0) {
        ans += f[e-1][mod2_fives(x - pw/5 + 1)];
        x -= pw/5;
      }
    }
    cout << ans << '\n';
  }
}
