#include <iostream>
#include <cmath>

using namespace std;

bool can(int a, int b) {
  int c2 = a * a + b * b;
  int c = sqrt((double)c2);
  if (c * c == c2)
    return true;
  c2 = a * a - b * b;
  c = sqrt((double)c2);
  return c * c == c2;
}

int main() {
  int a, b;
  cin >> a >> b;
  if (a < b)
    swap(a, b);
  cout << (can(a, b) ? "YES" : "NO") << endl;
}
