#include <cstdio>
#include <cstring>
#include <algorithm>
#define N 100
using namespace std;
int b[N][N];
int tmp[N][N];

int rot[128];

int n;
void rotate() {
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      tmp[n-1-j][i] = b[i][j];
    }
  }
  memcpy(b, tmp, sizeof tmp);
}

int main() {
  rot['L'] = 0;
  rot['U'] = 1;
  rot['R'] = 2;
  rot['D'] = 3;
  int k; scanf("%d %d", &n, &k);
  for (int i = 0; i < k; ++i) {
    int pw, r, c; scanf("%d%d%d", &pw, &r, &c);
    --r, --c;
    b[r][c] = pw;
  }
  int l; scanf("%d", &l);
  int score = 0;
  while (l--) {
    char mv;
    scanf(" %c", &mv);
    for (int i = 0; i < rot[(int)mv]; ++i, rotate());

    for (int i = 0; i  < n; ++i) {
      int l = 0;
      for (int j = 0; j < n; ++j) {
        if (b[i][j] != 0) {
          b[i][l++] = b[i][j];
        }
      }
      int ql = 0;
      for (int j = 0; j < l; ) {
        if (j + 1 < l && b[i][j] == b[i][j+1]) {
          score += 2 * b[i][j];
          b[i][ql++] = 2 * b[i][j];
          j += 2;
        } else {
          b[i][ql++] = b[i][j];
          ++j;
        }
      }
      for (int j = ql; j < n; ++j) {
        b[i][j] = 0;
      }
    }

    for (int i = (4-rot[(int)mv])%4; i > 0; --i, rotate());
    int val, r, c;
    scanf("%d%d%d", &val, &r, &c);
    --r; --c;
    b[r][c] = val;
  }
  printf("%d\n", score);
  return 0;
}
