#include <iostream>
#include <algorithm>
typedef long long LL;

using namespace std;

inline int solve(LL n, LL div, int d) {
  if(div == 1) {
    return 0;
  }
  int cnt = 0;
  for(; n; n /= div) {
    if (n % div != d) {
      break;
    }
    ++cnt;
  }
  return cnt;
}
int main() {
  LL n, d;
  cin >> n >> d;
  if (n == d) {
    cout << max(2LL, n+1) << ' ' << 1 << endl;
    return 0;
  }
  LL k = 2;  
  int r = 0; 
  LL last = 0;
  for (LL div = 1; div*div <= n-d; ++div) {
    if ((n-d) % div == 0) {
      int tmp = solve(n, div, d);
      if (r < tmp) {
        r = tmp;
        k = div;
      }
      last = div;
    }
  }
  for (LL div = last ; div > 0; --div) {
    if ((n-d) % div == 0) {
      int tmp = solve(n, (n-d)/div, d);
      if (r < tmp) {
        r = tmp;
        k = (n-d)/div;
      }
    }
  }
  cout << k << ' ' << r << endl;
  return 0;
}
