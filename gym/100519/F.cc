#include <iostream>
#include <cmath>
#include <cstdio>

using namespace std;

struct pt {
  int x, y;
  pt operator-(const pt &p) const {
    return {x - p.x, y - p.y};
  }
  int operator %(const pt &p) const {
    return x * p.y - y * p.x;
  }
  double norm() const {
    return hypot(x, y);
  }
};

inline bool passes(pt A, pt B, pt C) {
  return (B - A) % (C - A) == 0;
}

inline int to_secs(string s) {
  int h, m;
  sscanf(s.c_str(), "%d:%d", &h, &m);
  return (h * 60 + m) * 60;
}

pt A, B, C;

inline double alex(int d, int v) {
  return (C-A).norm() * 3600 / v + d * 60 + (B-C).norm() * 3600 / v;
}
inline int dmitry(string t, string f) {
  return to_secs(t) + to_secs(f) - 9 * 3600;
}
inline double petr(int w, int d) {
  return (B-A).norm() * 3600 / w + (passes(A, B, C) ? d * 60 : 0);
}

int main() {
  cin >> A.x >> A.y >> B.x >> B.y >> C.x >> C.y;
  double R[3];
  int D, V;
  cin >> D >> V;
  R[0] = alex(D, V);
  string T, F;
  cin >> T >> F;
  R[1] = dmitry(T, F);
  int W;
  cin >> W;
  R[2] = petr(W, D);
  int mini = 0;
  for (int i = 1; i < 3; ++i)
    if (R[i] < R[mini])
      mini = i;
  string names[] = {"Alex", "Dmitry", "Petr"};
  cout << names[mini] << endl;
}
