#include <cstdio>
#include <algorithm>
using namespace std;

typedef long long LL;

bool check(LL cap, LL nc, LL x, LL a, LL b) {
  LL y = b/a;
  LL used = min(x, y*nc);
  cap -= (LL)a * used;
  return cap <= (b - y*a) * min(nc, x-used);
}
int main() {
  int a, b, c;
  scanf("%d%d%d", &a, &b, &c);
  int f = min(a, b);
  int x = (c + f-1)/f;
  int lo = 1, hi = x;
  while (lo != hi) {
    int me = (lo+hi)/2;
    if (check(c, me, x, a, b)) {
      hi = me;
    } else {
      lo = me+1;
    }
  }
  printf("%d %d\n", x, lo);
  return 0;
}
