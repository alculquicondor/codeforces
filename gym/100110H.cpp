#include <cstdio>
#include <algorithm>
#include <vector>

using namespace std;
#define foreach(it,c) for(typeof((c).begin()) it=(c).begin();it!=(c).end();++it)

typedef pair<int, int> pii;

int main() {
    freopen("students.in", "r", stdin);
    freopen("students.out", "w", stdout);
    int k, n;
    scanf("%d %d", &k, &n);
    int N = 2*k*n;
    vector<pii> A(2*k*n);
    for(int i=0; i<N; i++)
        scanf("%d", &A[i].first);
    for(int i=0; i<N; i++)
        scanf("%d", &A[i].second);
    sort(A.rbegin(), A.rend());
    int fi = 0, fk = 0, si = -1, sk = 0, ans = 0;
    vector<vector<int> > M(n, vector<int> (2*k, -1));
    int num;
    bool op;
    foreach(it, A) {
        num = it->first;
        op = it->second;
        //printf("#%d %d\n", num, op);
        if(op) {
            if(fi<n) {
                M[fi][fk++] = num;
                if(fk==k) {
                    if(si==-1)
                        si = fi, sk = 0;
                    fi++, fk = 0;
                }
            }
            else {
                M[si][k+sk++] = num;
                ans ++;
                if(sk==k)
                    si++, sk = 0;
            }
        }
        else {
            if(si!=-1) {
                M[si][k+(sk++)] = num;
                if(sk==k) {
                    if(si==n-1)
                        break;
                    si = M[si+1][k-1]!=-1?si+1:-1;
                    sk = 0;
                }
            }
            else {
                M[fi][fk++] = num;
                ans ++;
                if(fk==k) {
                    if(si==-1)
                        si = fi, sk = 0;
                    fi++, fk = 0;
                }
            }
        }
    }
    printf("%d\n", N-ans);
    for(int i=0; i<n; i++) {
        printf("%d", M[i][0]);
        for(int j=1; j<2*k; j++)
            printf(" %d", M[i][j]);
        puts("");
    }
}
