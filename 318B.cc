#include <iostream>
#include <vector>
#define MAXN 1000010

using namespace std;

int pi[MAXN];

void kmpPreprocess(const string &P) {
  int i = 0, j = -1, m = P.size();
  pi[0] = -1;
  while(i < m) {
    while(j>=0 and P[i] != P[j])
      j = pi[j];
    i ++, j++;
    pi[i] = j;
  }
}

vector<int> kmpSearch(const string &T, const string &P) {
  vector<int> ans;
  int i = 0, j = 0, n = T.size(), m = P.size();
  while(i < n) {
    while(j>=0 and T[i]!=P[j])
      j = pi[j];
    i ++, j++;
    if(j == m) {
      ans.push_back(i-j);
      j = pi[j];
    }
  }
  return ans;
}

typedef long long ll;

int main() {
  ios::sync_with_stdio(0);
  string s, p = "heavy";
  cin >> s;
  kmpPreprocess(p);
  vector<int> A = kmpSearch(s, p);
  p = "metal";
  kmpPreprocess(p);
  vector<int> B = kmpSearch(s, p);
  ll ans = 0;
  for (int i = 0, j = 0; i < A.size() and j < B.size(); i++) {
    while (j < B.size() and B[j] < A[i])
      j++;
    ans += B.size() - j;
  }
  cout << ans << endl;
}
