#include <cstdio>

using namespace std;
typedef long double ld;

int main() {
  int n;
  scanf("%d", &n);
  float a, b;
  scanf("%f", &a);
  ld dp1 = 0, dp2 = a, ac = 0;
  while (--n) {
    scanf("%f", &b);
    dp1 = (dp1+a)*b;
    ac += dp1;
    dp2 += b;
    a = b;
  }
  printf("%.7f\n", (double)(2*ac+dp2));
}
